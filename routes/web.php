<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Session\SessionServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/*
Route::group(['middleware' => ['web']], function ($request) {
    if (session('idUser')) Route::get('/planos/login-page', 'PlanosController@pagamento');
    else $router->get('/planos/login-page', 'PlanosController@loginPage');

}); */


/* Route::group(['middleware' => ['protectedPages']], function(){
    Route::get('/planos/login-page', 'PlanosController@loginPage');
}); */

/** PAGARME **/
$router->get('pagamento/cupom/{cupom}/{plano}', ['as' => 'cupom', 'uses' => 'PlanosController@isCupomValid']);
$router->get('pagamento/boleto/{id}', ['as' => 'boleto', 'uses' => 'PlanosController@boleto']);
$router->post('postback', ['as' => 'postback', 'uses' => 'PlanosController@postback']);
$router->post('pagarme', ['as' => 'pagarme', 'uses' => 'PlanosController@pagarme']);

$router->post('popup-dismiss', ['as' => 'popup-dismiss', 'uses' => 'UserController@popupdismiss']);
$router->post('popup-trigger', ['as' => 'popup-trigger', 'uses' => 'UserController@popuptrigger']);
$router->post('rate-layout', ['as' => 'rate-layout', 'uses' => 'UserController@rateLayout']);
$router->post('redefinir', ['as' => 'redefinir-senha', 'uses' => 'UserController@redefinir']);
$router->get('verificar-token/{token}', ['as' => 'verificar-token', 'uses' => 'UserController@verificarToken']);
$router->post('recuperar-senha', ['as' => 'recuperar-senha', 'uses' => 'UserController@recuperarSenha']);
$router->post('conta/alterar-imagem', ['as' => 'alterar-imagem', 'uses' => 'UserController@alterarImagem']);
$router->get('logout', ['as' => 'logout', 'uses' => 'PlanosController@logout']);
$router->post('conta/alterar-senha', ['as' => 'alterar-senha', 'uses' => 'UserController@alterarSenha']);
$router->get('conta', ['as' => 'conta', 'uses' => 'UserController@index']);

$router->get('conta/curso/simulado/resultado/{id}/{plano}', ['as' => 'resultado', 'uses' => 'CursoController@resultado']);
$router->get('conta/curso/simulado/resultado/{id}', ['as' => 'resultado', 'uses' => 'CursoController@resultado']);
$router->post('simulado/finalizar', ['as' => 'finalizar-simulado', 'uses' => 'CursoController@finalizar']);
$router->get('conta/curso/simulado/{id}/{plano}', ['as' => 'simulado', 'uses' => 'CursoController@newSimulado']);
// $router->get('conta/curso/simulado/{id}/{plano}', ['as'=>'simulado', 'uses'=>'CursoController@simulado']);
$router->post('ebook/markAsSeen', ['as' => 'mark-ebook-as-seen', 'uses' => 'CursoController@markEbookAsSeen']);
$router->post('aula/play', ['as' => 'play', 'uses' => 'CursoController@videoPlayed']);
$router->get('conta/curso/playlist', ['as' => 'playlist', 'uses' => 'CursoController@playlist']);
$router->get('conta/curso/aula/{id}/{plano}', ['as' => 'aula', 'uses' => 'CursoController@newAula']);
// $router->get('conta/curso/aula/{id}/{plano}', ['as'=>'aula', 'uses'=>'CursoController@aula']);
$router->get('conta/plano/trocar/{id}', ['as' => 'curso', 'uses' => 'CursoController@trocarPlano']);
// $router->get('conta/curso', ['as'=>'curso', 'uses'=>'CursoController@index']);
$router->get('conta/curso', ['as' => 'curso', 'uses' => 'CursoController@newIndex']);

/* SESSION END*/

/** ADMIN **/
$router->get('/admin', 'AdminController@index');
$router->get('/admin/login/{link}', 'AdminController@login');
/** ADMIN **/

/** SIMULADO ABERTO **/
$router->get('desconto', 'UserController@assinarDesconto');
$router->post('has-desconto', 'CursoController@hasDesconto');
$router->get('planos/choose/{plano}/{desconto}', 'PlanosController@choose');
$router->post('responder-simulado-aberto', 'CursoController@responderSimuladoAberto');
$router->get('resultado_aberto/{userId}/{total}/{acertos}', 'CursoController@resultadoAberto');
$router->get('conta/curso/simulado/resolucao/{id}/{plano}', ['as' => 'resolucao', 'uses' => 'CursoController@resolucao']);
$router->get('conta/curso/simulado/resolucao-aberta/{id}/{userId}', ['as' => 'resolucao', 'uses' => 'CursoController@resolucaoAberta']);
$router->post('cadastrar-usuario-simulado-aberto', 'UserController@cadastrarUserSimAberto');
$router->get('simulado-aberto', function () {
    // Hash 'pao-de-queijo' para o link: eyJpdiI6IlFRekJCNDBESERVT1NCNk5TdmZnUXc9PSIsInZhbHVlIjoibHNlTmNQbFkrK0J0VHJrWVpOOFpRYlB2dDdjbFJkTDhRZUdTODNtTVc1Yz0iLCJtYWMiOiI1Y2VjNzE5YTMzMjJjNTZhNTc4N2VjMTZiNTNjZjFkZTlmNDQ2M2ZkODhlNTVkNDBhZWUwNmIzMjYyN2RjZjQyIiwidGFnIjoiIn0=
    // if (Crypt::decrypt($chave)) {
    //     if (Crypt::decrypt($chave) === "pao-de-queijo") {
    //         return view('home', ['title' => 'Biologia Aprova - termo de uso', 'page'=>'simulado-aberto', 'user'=>null, 'chave'=>$chave]);
    //     }
    // }
    // return view('content.erro', ['subject'=>'Acesso Negado', 'message'=>'Você não tem acesso a essa página.']);
    return view('home', ['title' => 'Biologia Aprova - Teste de Conhecimento', 'page' => 'simulado-aberto', 'user' => null]);
});
/** SIMULADO ABERTO **/

$router->get('termo-uso', 'UserController@termo');
$router->post('try', 'PlanosController@try');
$router->get('planos/teste', 'PlanosController@teste');
$router->get('planos/pagamento-page/', ['as' => 'pagamento-page', 'uses' => 'PlanosController@pagamentoPage']);
$router->get('planos/login-page/', ['as' => 'login-page', 'uses' => 'PlanosController@loginPage']);
$router->post('planos/cadastrar', 'PlanosController@cadastrar');
$router->post('planos/atualizar-dados-cartao', 'PlanosController@atualizarDadosCartao');
$router->post('planos/cidades', 'PlanosController@cidades');
$router->post('planos/cadastrar-page', 'PlanosController@cadastrarPage');
$router->post('planos/login', 'PlanosController@login');
$router->get('planos/choose/{plano}', 'PlanosController@choose');
$router->get('planos', 'PlanosController@index');

$router->post('/blog/post', ['middleware' => 'cors', 'uses' => 'BlogController@post']);
$router->get('/noticia/{id}', 'BlogController@show');
$router->get('autocomplete', ['as' => 'blog-autocomplete', 'uses' => 'BlogController@autocomplete']);
$router->get('blog-busca', ['as' => 'blog-busca', 'uses' => 'BlogController@buscar']);
$router->get('/blog/{categoria}', 'BlogController@byCategoria');
$router->get('/blog', 'BlogController@index');

$router->get('faq-autocomplete', ['as' => 'faq-autocomplete', 'uses' => 'FaqController@autocomplete']);
$router->get('faq-busca/{str}', ['as' => 'faq-busca', 'uses' => 'FaqController@buscar']);
$router->get('/faq/perguntas/{categoria}', 'FaqController@show');
$router->get('/faq', 'FaqController@index');

$router->get('/contato', 'UserController@contato');
$router->post('/contato/send', 'UserController@enviarMensagem');

$router->get('/videoaulas', 'VideoaulasController@index');
$router->post('/lp/save', 'HomeController@lpsave');
$router->get('/', 'HomeController@index');

// Adicionar registros manualmente.
// 1 - Adiciona registros da tabela usuarios_csv em usuarios.
$router->get('/insercao/csv', 'InsercaoController@addcsv');
// 2 - Adiciona registros de usuarios depois do id X para tabela ativos. (onde ficam as inscrições).
$router->get('/insercao/planos', 'InsercaoController@addplanos');

// $router->get('/insercao', 'InsercaoController@index');
// $router->get('/insercao/corrigir', 'InsercaoController@corrigirSenha');

$router->post('/postback', 'PagseguroController@postback');
$router->post('/transacao-salvar', 'PagseguroController@transactionSave');
$router->post('/comprar', 'PagseguroController@comprar');
$router->get('/pagseguro', 'PagseguroController@index');

$router->post('/venda-postback', 'VendaController@postback');
$router->post('/venda-transacao-salvar', 'VendaController@transactionSave');
$router->post('/venda-comprar', 'VendaController@comprar');
$router->get('/venda', 'VendaController@index');
