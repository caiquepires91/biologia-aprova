<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use Illuminate\Session\SessionServiceProvider;
use Illuminate\Http\Request;

$router->post('postback', ['as'=>'postback', 'uses'=>'PlanosController@postback']);