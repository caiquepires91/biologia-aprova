(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

$(document).ready(function () {
  $('.questao-alternativas input[type=radio]').on('change', function (event) {
    var $questao = $(event.target).closest('.questao-alternativas');
    $questao.find('.form-check-label').removeClass('checked');
    $(event.target).closest('.form-check-label').addClass('checked');
    timer.start();
  });
  
  $('#form_simulado').on('submit', Simulado.finalizarSimulado);
});

var Simulado =
/*#__PURE__*/
function () {
  function Simulado() {
    _classCallCheck(this, Simulado);
  }

  _createClass(Simulado, null, [{
    key: "finalizarSimulado",
    value: function finalizarSimulado(event) {
      event.preventDefault();
      timer.pause();
      event.data = {
        success_callback: Simulado.simuladoFinalizado
      };
      Main.submeteFormFile.call(this, event);
    }
  }, {
    key: "simuladoFinalizado",
    value: function simuladoFinalizado(resposta) {
      swal("Sucesso!", "Simulado finalizado com sucesso! Prossiga para conferir o seu resultado.", "success").then(function (click) {
      //console.log(resposta);

        var url_base = $(".base_url").data('url');
        window.location.href = url_base + "/conta/curso/simulado/resolucao/".concat(resposta.simulado_realizado.id);
      });
    }
  }]);

  return Simulado;
}();

window.Simulado = Simulado;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvc2ltdWxhZG8uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7OztBQ0FBLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWSxLQUFaLENBQWtCLFlBQVc7QUFDekIsRUFBQSxDQUFDLENBQUMseUNBQUQsQ0FBRCxDQUE2QyxFQUE3QyxDQUFnRCxRQUFoRCxFQUEwRCxVQUFBLEtBQUssRUFBSTtBQUMvRCxRQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQVAsQ0FBRCxDQUFnQixPQUFoQixDQUF3Qix1QkFBeEIsQ0FBakI7QUFDQSxJQUFBLFFBQVEsQ0FBQyxJQUFULENBQWMsbUJBQWQsRUFBbUMsV0FBbkMsQ0FBK0MsU0FBL0M7QUFDQSxJQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBUCxDQUFELENBQWdCLE9BQWhCLENBQXdCLG1CQUF4QixFQUE2QyxRQUE3QyxDQUFzRCxTQUF0RDtBQUNBLElBQUEsS0FBSyxDQUFDLEtBQU47QUFDSCxHQUxEO0FBT0EsRUFBQSxDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQixFQUFwQixDQUF1QixRQUF2QixFQUFpQyxRQUFRLENBQUMsaUJBQTFDO0FBQ0gsQ0FURDs7SUFXTSxROzs7Ozs7Ozs7c0NBQ3dCLEssRUFBTztBQUM3QixNQUFBLEtBQUssQ0FBQyxjQUFOO0FBRUEsTUFBQSxLQUFLLENBQUMsS0FBTjtBQUNBLE1BQUEsS0FBSyxDQUFDLElBQU4sR0FBYTtBQUFFLFFBQUEsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDO0FBQTdCLE9BQWI7QUFFQSxNQUFBLElBQUksQ0FBQyxlQUFMLENBQXFCLElBQXJCLENBQTBCLElBQTFCLEVBQWdDLEtBQWhDO0FBQ0g7Ozt1Q0FFMEIsUSxFQUFVO0FBQ2pDLE1BQUEsSUFBSSxDQUFDLFVBQUQsRUFBYSwwRUFBYixFQUF5RixTQUF6RixDQUFKLENBQXdHLElBQXhHLENBQTZHLFVBQUEsS0FBSyxFQUFJO0FBQ2xILFFBQUEsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsSUFBaEIsdUNBQW9ELFFBQVEsQ0FBQyxrQkFBVCxDQUE0QixFQUFoRjtBQUNILE9BRkQ7QUFHSDs7Ozs7O0FBR0wsTUFBTSxDQUFDLFFBQVAsR0FBa0IsUUFBbEIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICQoJy5xdWVzdGFvLWFsdGVybmF0aXZhcyBpbnB1dFt0eXBlPXJhZGlvXScpLm9uKCdjaGFuZ2UnLCBldmVudCA9PiB7XHJcbiAgICAgICAgY29uc3QgJHF1ZXN0YW8gPSAkKGV2ZW50LnRhcmdldCkuY2xvc2VzdCgnLnF1ZXN0YW8tYWx0ZXJuYXRpdmFzJyk7XHJcbiAgICAgICAgJHF1ZXN0YW8uZmluZCgnLmZvcm0tY2hlY2stbGFiZWwnKS5yZW1vdmVDbGFzcygnY2hlY2tlZCcpO1xyXG4gICAgICAgICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KCcuZm9ybS1jaGVjay1sYWJlbCcpLmFkZENsYXNzKCdjaGVja2VkJyk7XHJcbiAgICAgICAgdGltZXIuc3RhcnQoKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJyNmb3JtX3NpbXVsYWRvJykub24oJ3N1Ym1pdCcsIFNpbXVsYWRvLmZpbmFsaXphclNpbXVsYWRvKTtcclxufSk7XHJcblxyXG5jbGFzcyBTaW11bGFkbyB7XHJcbiAgICBzdGF0aWMgZmluYWxpemFyU2ltdWxhZG8gKGV2ZW50KSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIFxyXG4gICAgICAgIHRpbWVyLnBhdXNlKCk7XHJcbiAgICAgICAgZXZlbnQuZGF0YSA9IHsgc3VjY2Vzc19jYWxsYmFjazogU2ltdWxhZG8uc2ltdWxhZG9GaW5hbGl6YWRvIH07XHJcblxyXG4gICAgICAgIE1haW4uc3VibWV0ZUZvcm1GaWxlLmNhbGwodGhpcywgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBzaW11bGFkb0ZpbmFsaXphZG8gKHJlc3Bvc3RhKSB7XHJcbiAgICAgICAgc3dhbChcIlN1Y2Vzc28hXCIsIFwiU2ltdWxhZG8gZmluYWxpemFkbyBjb20gc3VjZXNzbyEgUHJvc3NpZ2EgcGFyYSBjb25mZXJpciBvIHNldSByZXN1bHRhZG8uXCIsIFwic3VjY2Vzc1wiKS50aGVuKGNsaWNrID0+IHtcclxuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBgc2ltdWxhZG9fcmVzdWx0YWRvLnBocD9pZD0ke3Jlc3Bvc3RhLnNpbXVsYWRvX3JlYWxpemFkby5pZH1gO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG53aW5kb3cuU2ltdWxhZG8gPSBTaW11bGFkbzsiXX0=

//# sourceMappingURL=simulado.js.map
