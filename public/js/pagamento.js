function assinar(usuario, plano, api_key, urls, cupom = null) {
    // console.log("assinar usuario", usuario);
    
    const PLANO_NOME = plano.nome;
    const PLANO_ID = plano.id;
    const PLANO_VALOR = plano.valor;

    const PAGAR_URL = urls.pagarme;
    const REDIRECT_SUCCESS = urls.redirect_success;
    const POSTBACK_URL = urls.postback;
    const REDIRECT_BOLETO = urls.redirect_boleto;

    const API_KEY = api_key;

    const HAS_CUPOM = cupom ? true : false;
    const CUPOM = HAS_CUPOM ? cupom : null;

    // fillOutCheckout(usuario);

    function error() {
        $('#autorizadoModal .modal-title').html("Pagamento falhou!");
        $('#autorizadoModal .modal-body').html("Por favor, tente mais tarde.");
        $('#autorizadoModal').modal('show');
    }

    var checkout = new PagarMeCheckout.Checkout({
        encryption_key: API_KEY,
        success: function(data) {
            // console.log("checkout data", data);

            if (HAS_CUPOM) data.cupom = CUPOM;
            data.plano_valor = PLANO_VALOR;

            $("#loader-wrapper").css("display", "block");
            $.post(PAGAR_URL, data, function(data) {
                $("#loader-wrapper").css("display", "none");
                //console.log("dados boleto:" + JSON.stringify(data));
                if (data.success) {
                    if (data.status == 'paid') {
                        window.location.href = REDIRECT_SUCCESS;
                    } else if (data.status == 'waiting_payment') {
                        window.location.href =  REDIRECT_BOLETO + data.boleto.id;
                    } else if (data.status == 'authorized') {
                        $('#autorizadoModal modal-title').html("Pagamento Pendente");
                        $('#autorizadoModal modal-body').html("O pagamento foi reservado e está pendente. Notificaremos por e-mail assim que recebermos a confirmação.");
                        $('#autorizadoModal').modal('show');
                    }
                } else {
                    error();
                    //alert("Transação falhou!");
                }
            })
            .fail(function() {
                $("#loader-wrapper").css("display", "none");
                error();
                //console.log("post pagamento falhou!!");
            });
        },
        error: function(err) {
            $("#loader-wrapper").css("display", "none");
            //console.log(err);
            error();
            alert(err);
        },
        close: function() {
            $("#loader-wrapper").css("display", "none"); 
            //alert("on pay modal colsed ");
            window.location.href = REDIRECT_SUCCESS;
            //console.log('The modal has been closed.');
        }
    });

    var phonenumber = cleanPhone(`+55${usuario.telefone}`);
    console.log('cep', cleanCEP(usuario.cep));

    checkout.open({
        amount: PLANO_VALOR,
        headerText: usuario.email_cartao,
        customerData: 'false',
        createToken: 'true',
        customer_editable : 'true',
        paymentMethods: 'boleto, credit_card',
        maxInstallments: 12,
        defaultInstallments: 1,
        freeInstallments: 12,
        boletoDiscountPercentage: 0,
        postbackUrl: POSTBACK_URL,
        costumer: [{
            name : usuario.nome,
            email: usuario.email,
            external_id:usuario.id,
            documents: [{
                type: 'CPF',
                number: usuario.cpf
            }],
            phone_numbers: [
                usuario.telefone
            ]
        }],
        customer: {
            external_id: usuario.id,
            name:  usuario.nome,
            type: 'individual',
            country: 'br',
            email: usuario.email_cartao,
            documents: [
              {
                type: 'cpf',
                number: usuario.cpf_cartao,
              },
            ],
            phone_numbers: [phonenumber],
          },
        billing: {
            name: usuario.nome,
            address: {
              country: 'br',
              state: usuario.estado,
              city: usuario.cidade,
              neighborhood: usuario.bairro,
              street: usuario.endereco,
              street_number: usuario.numero,
              zipcode: cleanCEP(usuario.cep)
            }
          },
        items: [{
            id: PLANO_ID,
            title: PLANO_NOME,
            unit_price: PLANO_VALOR,
            quantity: 1,
            tangible: 'false'
        }],
    });
  }

function cleanPhone(value) {
    return value.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
}

function cleanCEP(value) {
    return value.trim().replace("-", "");
}
