(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
module.exports = require("regenerator-runtime");

},{"regenerator-runtime":2}],2:[function(require,module,exports){
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
  typeof module === "object" ? module.exports : {}
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}

},{}],3:[function(require,module,exports){
"use strict";

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

$(document).ready(function () {
  // Home
  $('.plataforma-categorias .categoria-header').on('click', Plataforma.expandirCategoria);
  $('.categoria-video').on('click', Plataforma.acessarVideo);
  $('.plataforma-simulados-categorias .categoria-header').on('click', Plataforma.expandirSimuladoCategoria);
  
  var bar = new ProgressBar.Line('.progresso-bar', {
    strokeWidth: 4,
    easing: 'easeInOut',
    duration: 1400,
    color: '#ffed37',
    trailColor: 'rgba(255, 255, 255, .35)',
    trailWidth: 4,
    svgStyle: {
      width: '100%',
      height: '100%'
    }
  });
  var porcentagem = parseFloat($('.progresso-bar').data('porcentagem'));

  if (porcentagem) {
    bar.animate(porcentagem / 100);
  } // Vídeo


  $('.plataforma-video-categoria').on('click', Plataforma.expandirCategoriaVideo); // Avaliação

  if ($("#avaliacao_aula").length > 0) {
    $('#avaliacao_aula').each(function (index, element) {
      var options = {
        rating: 0,
        fullStar: true,
        starWidth: '30px',
        onSet: Plataforma.avaliarVideo
      };

      if (element.dataset.avaliacao.length) {
        options.rating = parseInt(element.dataset.avaliacao);
        options['readOnly'] = true;
      }

      $(element).rateYo(options);
    });
  } // Exibe aula ativa


  if (typeof id_aula !== "undefined") {
    Plataforma.habilitarAulaActive(id_aula);
    if (tipo_aula !== "questionario") Plataforma.habilitarVideoActive(); // Vídeo (iframe)

    // $('.video-embed iframe').iframeTracker(Plataforma.visualizarVideo);
    $('.plataforma-video-menu-roll').on('click', function (e) {
      $('.plataforma-video-content').toggleClass('hide');
      setTimeout(function () {
        return $('.plataforma-video-content i').toggleClass('fa-chevron-left fa-chevron-right');
      }, 500);
    }); // Documento
    // $('.aula-documento-item').on('click', Plataforma.acessarDocumento);

    Plataforma.habilitarMenuMobileLateral();
  } // Caso seja a home


  if (typeof is_home !== "undefined") {
    // Se houver último vídeo assistido, abre categoria deste vídeo
    if ((typeof ultimo_video_assistido === "undefined" ? "undefined" : _typeof(ultimo_video_assistido)) === "object" && ultimo_video_assistido && ultimo_video_assistido.hasOwnProperty("id_categoria")) {
      $(".plataforma-categoria[data-id=".concat(ultimo_video_assistido.id_categoria, "]")).find('.categoria-header').click();
    } // Caso não haja, abre primeira categoria normalmente
    else {
        $('.plataforma-categoria').first().find('.categoria-header').click();
      }
  } // Exibe primeiro módulo dos simulados


  if ($('.plataforma-card-simulados').length && !$('.plataforma-card-simulados.sem-categoria li').length) {
    $('.plataforma-simulado-categoria').first().find('.categoria-header').click();
  } // Alterar dados

/* CAIQUE 
  $('#form_alterar_dados').on('submit', {
    success_callback: function success_callback(resposta) {
      $('#modal_alterar_dados').modal('hide');
      swal("Sucesso!", resposta.msg, "success").then(function (action) {
        window.location.reload();
      });
    }
  }, Main.submeteFormFile); // Comunicados
 */
 /* CAIQUE
  $('#form_comunicado').on('submit', {
    success_callback: function success_callback(resposta) {
      $('#modal_comunicado').modal('hide');
      window.location.reload();
    }
  }, Main.submeteFormFile); */
});
$(window).on('scroll', function (e) {
  var scroll = document.documentElement.scrollTop;
  var element = $('.plataforma-navegacao').outerHeight();
  if (scroll >= element) $('.plataforma-video-menu').addClass('fixed');else $('.plataforma-video-menu').removeClass('fixed');
});
$(window).on('resize', function (e) {
  Plataforma.habilitarMenuMobileLateral();
});

var Plataforma =
/*#__PURE__*/
function () {
  function Plataforma() {
    _classCallCheck(this, Plataforma);
  }

  _createClass(Plataforma, null, [{
    key: "expandirCategoria",

    /* ========================================================================== */

    /* ==============================  Handlers  ================================ */

    /* ========================================================================== */
    value: function expandirCategoria(event) {
      var $categoria = $(event.target).closest('.plataforma-categoria');

      if ($categoria.hasClass('show')) {
        $categoria.find('.categoria-body').slideToggle(function (elemento) {
          return $categoria.toggleClass('show');
        });
      } else {
        $categoria.toggleClass('show');
        $categoria.find('.categoria-body').slideToggle();
      }
    }
  }, {
    key: "expandirSimuladoCategoria",
    value: function expandirSimuladoCategoria(event) {
      var $categoria = $(event.target).closest('.plataforma-simulado-categoria');

      if ($categoria.hasClass('show')) {
        $categoria.find('.categoria-body').slideToggle(function (elemento) {
          return $categoria.toggleClass('show');
        });
      } else {
        $categoria.toggleClass('show');
        $categoria.find('.categoria-body').slideToggle();
      }
    }
  }, {
    key: "acessarVideo",
    value: function acessarVideo(event) {
      event.stopPropagation();
      if (!('id' in this.dataset)) return false;
      var id = this.dataset.id;
      if ($(this).hasClass('video-nao-liberado')) return false; // Acessa documento

      if ($(this).hasClass('documento')) {
        Plataforma.acessarDocumento(id);
        return false;
      }

      var isQuestionario = this.dataset.questionario;
      var isPlaylist = this.dataset.playlist;
      var planoId = this.dataset.plano;
      //console.log("questionario: " + isQuestionario);
      var extra = "";
      if (isQuestionario) {
        extra = "?tipo=questionario"
      } else if (isPlaylist) {
        extra = "?tipo=playlist_med"
      } else {
        extra = "?tipo=video"
      }

      var APP_URL = $(".base_url").data('url');
      window.location.href = APP_URL + "/conta/curso/aula/".concat(id) + "/" + planoId + extra;
    }
  }, {
    key: "acessarDocumento",
    value: function acessarDocumento(value) {
      if (isNaN(value)) {
        value.stopPropagation();
        if (!('id' in this.dataset)) return false;
        var id = this.dataset.id;
        value = id;
      }

      window.open("documento.php?id=".concat(value));
    }
  }, {
    key: "expandirCategoriaVideo",
    value: function expandirCategoriaVideo(event) {
      var $categoria = $(event.target).closest('.plataforma-video-categoria');

      if ($categoria.hasClass('show')) {
        $categoria.find('.categoria-body').slideToggle(function (elemento) {
          return $categoria.toggleClass('show');
        });
      } else {
        $categoria.toggleClass('show');
        $categoria.find('.categoria-body').slideToggle();
      }
    }
  }, {
    key: "habilitarAulaActive",
    value: function habilitarAulaActive(id_aula) {
      $('.categoria-video').removeClass('active');
      if (tipo_aula == "questionario") $(".categoria-video[data-id=".concat(id_aula, "]:not(.video)")).addClass('active').closest('.plataforma-video-categoria').click();
      else $(".categoria-video[data-id=".concat(id_aula, "]:not(.questionario)")).addClass('active').closest('.plataforma-video-categoria').click();
    }
  }, {
    key: "handleScroll",
    value: function handleScroll(event) {
      console.log('>>' + document.documentElement.scrollHeight);
    }
  }, {
    key: "habilitarMenuMobileLateral",
    value: function habilitarMenuMobileLateral() {
      if (window.innerWidth < 991 && !$('.plataforma-video-content').hasClass('hide')) {
        $('.plataforma-video-menu-roll').click();
      }
    }
    /* ========================================================================== */

    /* ===============================  Embed  ================================== */

    /* ========================================================================== */

  }, {
    key: "parseVideo",
    value: function parseVideo(url) {
      /*
      * - Supported YouTube URL formats:
      *   - http://www.youtube.com/watch?v=My2FRPA3Gf8
      *   - http://youtu.be/My2FRPA3Gf8
      *   - https://youtube.googleapis.com/v/My2FRPA3Gf8
      * - Supported Vimeo URL formats:
      *   - http://vimeo.com/25451551
      *   - http://player.vimeo.com/video/25451551
      * - Also supports relative URLs:
      *   - //player.vimeo.com/video/25451551
      */
      url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

      if (RegExp.$3.indexOf('youtu') > -1) {
        var type = 'youtube';
      } else if (RegExp.$3.indexOf('vimeo') > -1) {
        var type = 'vimeo';
      }

      return {
        type: type,
        id: RegExp.$6
      };
    }
  }, {
    key: "getEmbedURL",
    value: function getEmbedURL(url) {
      var embeds = {
        youtube: 'https://www.youtube.com/embed/',
        vimeo: 'https://player.vimeo.com/video/'
      };
      var obj = Plataforma.parseVideo(url);
      return embeds[obj.type] + obj.id;
    }
  }, {
    key: "habilitarVideoActive",
    value: function habilitarVideoActive() {
      var $iframe = $('#iframe_video');
      var url = $iframe.data('url');
      $iframe.attr('src', Plataforma.getEmbedURL(url));
    }
    /* ========================================================================== */

    /* =============================  Visualização  ============================= */

    /* ========================================================================== */

  }, {
    key: "visualizarVideo",
    value: function () {
      var _visualizarVideo = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee(event) {
        var resposta;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!video_visualizado) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return", false);

              case 2:
                _context.prev = 2;
                _context.next = 5;
                return $.ajax({
                  type: 'POST',
                  url: $(".base_url").data("url") +  "/aula/play",
                  data: {
                    id_video: id_aula
                  },
                  dataType: 'JSON'
                });

              case 5:
                resposta = _context.sent;

                if (!(resposta.status !== 'success')) {
                  _context.next = 9;
                  break;
                }

                //swal('Ocorreu um erro!', resposta.mensagem_erro, 'error');
                return _context.abrupt("return", false);

              case 9:
                video_visualizado = true;
                _context.next = 16;
                break;

              case 12:
                _context.prev = 12;
                _context.t0 = _context["catch"](2);
                console.log('Error!', _context.t0);
                swal('Ops!', 'Ocorreu um erro! Por favor, atualize a página e tente novamente.', 'error');

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[2, 12]]);
      }));

      function visualizarVideo(_x) {
        return _visualizarVideo.apply(this, arguments);
      }

      return visualizarVideo;
    }()
  }, {
    key: "avaliarVideo",
    value: function () {
      var _avaliarVideo = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator["default"].mark(function _callee2(rating, rateYoInstance) {
        var resposta;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return $.ajax({
                  type: 'POST',
                  url: $(".base_url").data("url") +  "/aula/avaliar",
                  data: {
                    id_video: id_aula,
                    avaliacao: rating
                  },
                  dataType: 'JSON',
                  beforeSend: function beforeSend() {
                    return Main.abrir_overlay_loading();
                  }
                }).always(function () {
                  return Main.fechar_overlay_loading();
                });

              case 3:
                resposta = _context2.sent;

                if (!(resposta.status !== 'success')) {
                  _context2.next = 7;
                  break;
                }

                swal('Ocorreu um erro!', resposta.mensagem_erro, 'error');
                return _context2.abrupt("return", false);

              case 7:
                $('#avaliacao_aula').rateYo('option', 'readOnly', true);
                $('#avaliacao_aula').attr('title', "Avalia\xE7\xE3o de ".concat(rating, " ").concat(rating == 1 ? 'estrela' : 'estrelas', " feita em ").concat(moment().format("DD/MM/YYYY [às] HH:mm")));
                _context2.next = 15;
                break;

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2["catch"](0);
                console.log('Error!', _context2.t0);
                swal('Ops!', 'Ocorreu um erro! Por favor, atualize a página e tente novamente.', 'error');

              case 15:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 11]]);
      }));

      function avaliarVideo(_x2, _x3) {
        return _avaliarVideo.apply(this, arguments);
      }

      return avaliarVideo;
    }()
  }]);

  return Plataforma;
}();

window.Plataforma = Plataforma;

},{"@babel/runtime/regenerator":1}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvcmVnZW5lcmF0b3IvaW5kZXguanMiLCJub2RlX21vZHVsZXMvcmVnZW5lcmF0b3ItcnVudGltZS9ydW50aW1lLmpzIiwic3JjL2pzL3BsYXRhZm9ybWEuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBOztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3R0QkEsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZLEtBQVosQ0FBa0IsWUFBTTtBQUVwQjtBQUNBLEVBQUEsQ0FBQyxDQUFDLDBDQUFELENBQUQsQ0FBOEMsRUFBOUMsQ0FBaUQsT0FBakQsRUFBMEQsVUFBVSxDQUFDLGlCQUFyRTtBQUNBLEVBQUEsQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0IsRUFBdEIsQ0FBeUIsT0FBekIsRUFBa0MsVUFBVSxDQUFDLFlBQTdDO0FBQ0EsRUFBQSxDQUFDLENBQUMsb0RBQUQsQ0FBRCxDQUF3RCxFQUF4RCxDQUEyRCxPQUEzRCxFQUFvRSxVQUFVLENBQUMseUJBQS9FO0FBRUEsTUFBSSxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBaEIsQ0FBcUIsZ0JBQXJCLEVBQXVDO0FBQzdDLElBQUEsV0FBVyxFQUFFLENBRGdDO0FBRTdDLElBQUEsTUFBTSxFQUFFLFdBRnFDO0FBRzdDLElBQUEsUUFBUSxFQUFFLElBSG1DO0FBSTdDLElBQUEsS0FBSyxFQUFFLFNBSnNDO0FBSzdDLElBQUEsVUFBVSxFQUFFLDBCQUxpQztBQU03QyxJQUFBLFVBQVUsRUFBRSxDQU5pQztBQU83QyxJQUFBLFFBQVEsRUFBRTtBQUFDLE1BQUEsS0FBSyxFQUFFLE1BQVI7QUFBZ0IsTUFBQSxNQUFNLEVBQUU7QUFBeEI7QUFQbUMsR0FBdkMsQ0FBVjtBQVVBLE1BQU0sV0FBVyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQixJQUFwQixDQUF5QixhQUF6QixDQUFELENBQTlCOztBQUNBLE1BQUksV0FBSixFQUFpQjtBQUNiLElBQUEsR0FBRyxDQUFDLE9BQUosQ0FBWSxXQUFXLEdBQUcsR0FBMUI7QUFDSCxHQXBCbUIsQ0FzQnBCOzs7QUFDQSxFQUFBLENBQUMsQ0FBQyw2QkFBRCxDQUFELENBQWlDLEVBQWpDLENBQW9DLE9BQXBDLEVBQTZDLFVBQVUsQ0FBQyxzQkFBeEQsRUF2Qm9CLENBeUJwQjs7QUFDQSxNQUFJLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCLE1BQXJCLEdBQThCLENBQWxDLEVBQXFDO0FBQ2pDLElBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUIsSUFBckIsQ0FBMEIsVUFBQyxLQUFELEVBQVEsT0FBUixFQUFvQjtBQUMxQyxVQUFJLE9BQU8sR0FBRztBQUNWLFFBQUEsTUFBTSxFQUFFLENBREU7QUFFVixRQUFBLFFBQVEsRUFBRSxJQUZBO0FBR1YsUUFBQSxTQUFTLEVBQUUsTUFIRDtBQUlWLFFBQUEsS0FBSyxFQUFFLFVBQVUsQ0FBQztBQUpSLE9BQWQ7O0FBT0EsVUFBSSxPQUFPLENBQUMsT0FBUixDQUFnQixTQUFoQixDQUEwQixNQUE5QixFQUFzQztBQUNsQyxRQUFBLE9BQU8sQ0FBQyxNQUFSLEdBQWlCLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBUixDQUFnQixTQUFqQixDQUF6QjtBQUNBLFFBQUEsT0FBTyxDQUFDLFVBQUQsQ0FBUCxHQUFzQixJQUF0QjtBQUNIOztBQUVELE1BQUEsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxDQUFXLE1BQVgsQ0FBa0IsT0FBbEI7QUFDSCxLQWREO0FBZUgsR0ExQ21CLENBNENwQjs7O0FBQ0EsTUFBSSxPQUFPLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDaEMsSUFBQSxVQUFVLENBQUMsbUJBQVgsQ0FBK0IsT0FBL0I7QUFDQSxJQUFBLFVBQVUsQ0FBQyxvQkFBWCxHQUZnQyxDQUloQzs7QUFDQSxJQUFBLENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCLGFBQXpCLENBQXVDLFVBQVUsQ0FBQyxlQUFsRDtBQUVBLElBQUEsQ0FBQyxDQUFDLDZCQUFELENBQUQsQ0FBaUMsRUFBakMsQ0FBb0MsT0FBcEMsRUFBNkMsVUFBQSxDQUFDLEVBQUk7QUFDOUMsTUFBQSxDQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQixXQUEvQixDQUEyQyxNQUEzQztBQUNBLE1BQUEsVUFBVSxDQUFDO0FBQUEsZUFBTSxDQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQyxXQUFqQyxDQUE2QyxrQ0FBN0MsQ0FBTjtBQUFBLE9BQUQsRUFBeUYsR0FBekYsQ0FBVjtBQUNILEtBSEQsRUFQZ0MsQ0FZaEM7QUFDQTs7QUFFQSxJQUFBLFVBQVUsQ0FBQywwQkFBWDtBQUNILEdBN0RtQixDQStEcEI7OztBQUNBLE1BQUksT0FBTyxPQUFQLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ2hDO0FBQ0EsUUFBSSxRQUFPLHNCQUFQLHlDQUFPLHNCQUFQLE9BQWtDLFFBQWxDLElBQThDLHNCQUE5QyxJQUF3RSxzQkFBc0IsQ0FBQyxjQUF2QixDQUFzQyxjQUF0QyxDQUE1RSxFQUFtSTtBQUMvSCxNQUFBLENBQUMseUNBQWtDLHNCQUFzQixDQUFDLFlBQXpELE9BQUQsQ0FBMkUsSUFBM0UsQ0FBZ0YsbUJBQWhGLEVBQXFHLEtBQXJHO0FBQ0gsS0FGRCxDQUdBO0FBSEEsU0FJSztBQUNELFFBQUEsQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkIsS0FBM0IsR0FBbUMsSUFBbkMsQ0FBd0MsbUJBQXhDLEVBQTZELEtBQTdEO0FBQ0g7QUFDSixHQXpFbUIsQ0EyRXBCOzs7QUFDQSxNQUFJLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDLE1BQWhDLElBQTBDLENBQUMsQ0FBQyxDQUFDLDZDQUFELENBQUQsQ0FBaUQsTUFBaEcsRUFBd0c7QUFDcEcsSUFBQSxDQUFDLENBQUMsZ0NBQUQsQ0FBRCxDQUFvQyxLQUFwQyxHQUE0QyxJQUE1QyxDQUFpRCxtQkFBakQsRUFBc0UsS0FBdEU7QUFDSCxHQTlFbUIsQ0FnRnBCOzs7QUFDQSxFQUFBLENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCLEVBQXpCLENBQTRCLFFBQTVCLEVBQXNDO0FBQ2xDLElBQUEsZ0JBQWdCLEVBQUUsMEJBQUMsUUFBRCxFQUFjO0FBQzVCLE1BQUEsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEIsS0FBMUIsQ0FBZ0MsTUFBaEM7QUFDQSxNQUFBLElBQUksQ0FBQyxVQUFELEVBQWEsUUFBUSxDQUFDLEdBQXRCLEVBQTJCLFNBQTNCLENBQUosQ0FBMEMsSUFBMUMsQ0FBK0MsVUFBQSxNQUFNLEVBQUk7QUFDckQsUUFBQSxNQUFNLENBQUMsUUFBUCxDQUFnQixNQUFoQjtBQUNILE9BRkQ7QUFHSDtBQU5pQyxHQUF0QyxFQU9HLElBQUksQ0FBQyxlQVBSLEVBakZvQixDQTBGcEI7O0FBQ0EsRUFBQSxDQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQixFQUF0QixDQUF5QixRQUF6QixFQUFtQztBQUMvQixJQUFBLGdCQUFnQixFQUFFLDBCQUFDLFFBQUQsRUFBYztBQUM1QixNQUFBLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCLEtBQXZCLENBQTZCLE1BQTdCO0FBQ0EsTUFBQSxNQUFNLENBQUMsUUFBUCxDQUFnQixNQUFoQjtBQUNIO0FBSjhCLEdBQW5DLEVBS0csSUFBSSxDQUFDLGVBTFI7QUFNSCxDQWpHRDtBQW1HQSxDQUFDLENBQUMsTUFBRCxDQUFELENBQVUsRUFBVixDQUFhLFFBQWIsRUFBdUIsVUFBQSxDQUFDLEVBQUk7QUFDeEIsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLGVBQVQsQ0FBeUIsU0FBeEM7QUFDQSxNQUFNLE9BQU8sR0FBRyxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQixXQUEzQixFQUFoQjtBQUVBLE1BQUksTUFBTSxJQUFJLE9BQWQsRUFDSSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QixRQUE1QixDQUFxQyxPQUFyQyxFQURKLEtBR0ksQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEIsV0FBNUIsQ0FBd0MsT0FBeEM7QUFDUCxDQVJEO0FBVUEsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVLEVBQVYsQ0FBYSxRQUFiLEVBQXVCLFVBQUEsQ0FBQyxFQUFJO0FBQ3hCLEVBQUEsVUFBVSxDQUFDLDBCQUFYO0FBQ0gsQ0FGRDs7SUFJTSxVOzs7Ozs7Ozs7O0FBRUY7O0FBQ0E7O0FBQ0E7c0NBRTBCLEssRUFBTztBQUM3QixVQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQVAsQ0FBRCxDQUFnQixPQUFoQixDQUF3Qix1QkFBeEIsQ0FBakI7O0FBRUEsVUFBSSxVQUFVLENBQUMsUUFBWCxDQUFvQixNQUFwQixDQUFKLEVBQWlDO0FBQzdCLFFBQUEsVUFBVSxDQUFDLElBQVgsQ0FBZ0IsaUJBQWhCLEVBQW1DLFdBQW5DLENBQStDLFVBQUEsUUFBUTtBQUFBLGlCQUFJLFVBQVUsQ0FBQyxXQUFYLENBQXVCLE1BQXZCLENBQUo7QUFBQSxTQUF2RDtBQUNILE9BRkQsTUFHSztBQUNELFFBQUEsVUFBVSxDQUFDLFdBQVgsQ0FBdUIsTUFBdkI7QUFDQSxRQUFBLFVBQVUsQ0FBQyxJQUFYLENBQWdCLGlCQUFoQixFQUFtQyxXQUFuQztBQUNIO0FBQ0o7Ozs4Q0FFaUMsSyxFQUFPO0FBQ3JDLFVBQUksVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBUCxDQUFELENBQWdCLE9BQWhCLENBQXdCLGdDQUF4QixDQUFqQjs7QUFFQSxVQUFJLFVBQVUsQ0FBQyxRQUFYLENBQW9CLE1BQXBCLENBQUosRUFBaUM7QUFDN0IsUUFBQSxVQUFVLENBQUMsSUFBWCxDQUFnQixpQkFBaEIsRUFBbUMsV0FBbkMsQ0FBK0MsVUFBQSxRQUFRO0FBQUEsaUJBQUksVUFBVSxDQUFDLFdBQVgsQ0FBdUIsTUFBdkIsQ0FBSjtBQUFBLFNBQXZEO0FBQ0gsT0FGRCxNQUdLO0FBQ0QsUUFBQSxVQUFVLENBQUMsV0FBWCxDQUF1QixNQUF2QjtBQUNBLFFBQUEsVUFBVSxDQUFDLElBQVgsQ0FBZ0IsaUJBQWhCLEVBQW1DLFdBQW5DO0FBQ0g7QUFDSjs7O2lDQUVvQixLLEVBQU87QUFDeEIsTUFBQSxLQUFLLENBQUMsZUFBTjtBQUVBLFVBQUksRUFBRSxRQUFRLEtBQUssT0FBZixDQUFKLEVBQ0ksT0FBTyxLQUFQO0FBSm9CLFVBTWhCLEVBTmdCLEdBTVQsS0FBSyxPQU5JLENBTWhCLEVBTmdCO0FBUXhCLFVBQUksQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRLFFBQVIsQ0FBaUIsb0JBQWpCLENBQUosRUFDSSxPQUFPLEtBQVAsQ0FUb0IsQ0FXeEI7O0FBQ0EsVUFBSSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVEsUUFBUixDQUFpQixXQUFqQixDQUFKLEVBQW1DO0FBQy9CLFFBQUEsVUFBVSxDQUFDLGdCQUFYLENBQTRCLEVBQTVCO0FBQ0EsZUFBTyxLQUFQO0FBQ0g7O0FBRUQsTUFBQSxNQUFNLENBQUMsUUFBUCxDQUFnQixJQUFoQix5QkFBc0MsRUFBdEM7QUFDSDs7O3FDQUV3QixLLEVBQU87QUFDNUIsVUFBSSxLQUFLLENBQUMsS0FBRCxDQUFULEVBQWtCO0FBQ2QsUUFBQSxLQUFLLENBQUMsZUFBTjtBQUVBLFlBQUksRUFBRSxRQUFRLEtBQUssT0FBZixDQUFKLEVBQ0ksT0FBTyxLQUFQO0FBSlUsWUFNTixFQU5NLEdBTUMsS0FBSyxPQU5OLENBTU4sRUFOTTtBQU9kLFFBQUEsS0FBSyxHQUFHLEVBQVI7QUFDSDs7QUFFRCxNQUFBLE1BQU0sQ0FBQyxJQUFQLDRCQUFnQyxLQUFoQztBQUNIOzs7MkNBRThCLEssRUFBTztBQUNsQyxVQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQVAsQ0FBRCxDQUFnQixPQUFoQixDQUF3Qiw2QkFBeEIsQ0FBakI7O0FBRUEsVUFBSSxVQUFVLENBQUMsUUFBWCxDQUFvQixNQUFwQixDQUFKLEVBQWlDO0FBQzdCLFFBQUEsVUFBVSxDQUFDLElBQVgsQ0FBZ0IsaUJBQWhCLEVBQW1DLFdBQW5DLENBQStDLFVBQUEsUUFBUTtBQUFBLGlCQUFJLFVBQVUsQ0FBQyxXQUFYLENBQXVCLE1BQXZCLENBQUo7QUFBQSxTQUF2RDtBQUNILE9BRkQsTUFHSztBQUNELFFBQUEsVUFBVSxDQUFDLFdBQVgsQ0FBdUIsTUFBdkI7QUFDQSxRQUFBLFVBQVUsQ0FBQyxJQUFYLENBQWdCLGlCQUFoQixFQUFtQyxXQUFuQztBQUNIO0FBQ0o7Ozt3Q0FFMkIsTyxFQUFTO0FBQ2pDLE1BQUEsQ0FBQyxDQUFDLGtCQUFELENBQUQsQ0FBc0IsV0FBdEIsQ0FBa0MsUUFBbEM7QUFFQSxNQUFBLENBQUMsb0NBQTZCLE9BQTdCLHVCQUFELENBQ0ssUUFETCxDQUNjLFFBRGQsRUFFSyxPQUZMLENBRWEsNkJBRmIsRUFHSyxLQUhMO0FBSUg7OztpQ0FFb0IsSyxFQUFPO0FBQ3hCLE1BQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxPQUFPLFFBQVEsQ0FBQyxlQUFULENBQXlCLFlBQTVDO0FBQ0g7OztpREFFb0M7QUFDakMsVUFBSSxNQUFNLENBQUMsVUFBUCxHQUFvQixHQUFwQixJQUEyQixDQUFDLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCLFFBQS9CLENBQXdDLE1BQXhDLENBQWhDLEVBQWlGO0FBQzdFLFFBQUEsQ0FBQyxDQUFDLDZCQUFELENBQUQsQ0FBaUMsS0FBakM7QUFDSDtBQUNKO0FBRUQ7O0FBQ0E7O0FBQ0E7Ozs7K0JBRW1CLEcsRUFBSztBQUNwQjs7Ozs7Ozs7Ozs7QUFZQSxNQUFBLEdBQUcsQ0FBQyxLQUFKLENBQVUsb0pBQVY7O0FBRUEsVUFBSSxNQUFNLENBQUMsRUFBUCxDQUFVLE9BQVYsQ0FBa0IsT0FBbEIsSUFBNkIsQ0FBQyxDQUFsQyxFQUFxQztBQUNqQyxZQUFJLElBQUksR0FBRyxTQUFYO0FBQ0gsT0FGRCxNQUVPLElBQUksTUFBTSxDQUFDLEVBQVAsQ0FBVSxPQUFWLENBQWtCLE9BQWxCLElBQTZCLENBQUMsQ0FBbEMsRUFBcUM7QUFDeEMsWUFBSSxJQUFJLEdBQUcsT0FBWDtBQUNIOztBQUVELGFBQU87QUFDSCxRQUFBLElBQUksRUFBSixJQURHO0FBRUgsUUFBQSxFQUFFLEVBQUUsTUFBTSxDQUFDO0FBRlIsT0FBUDtBQUlIOzs7Z0NBRW1CLEcsRUFBSztBQUNyQixVQUFNLE1BQU0sR0FBRztBQUNYLFFBQUEsT0FBTyxFQUFFLGdDQURFO0FBRVgsUUFBQSxLQUFLLEVBQUU7QUFGSSxPQUFmO0FBS0EsVUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLFVBQVgsQ0FBc0IsR0FBdEIsQ0FBWjtBQUVBLGFBQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFMLENBQU4sR0FBbUIsR0FBRyxDQUFDLEVBQTlCO0FBQ0g7OzsyQ0FFOEI7QUFDM0IsVUFBTSxPQUFPLEdBQUcsQ0FBQyxDQUFDLGVBQUQsQ0FBakI7QUFDQSxVQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsSUFBUixDQUFhLEtBQWIsQ0FBWjtBQUNBLE1BQUEsT0FBTyxDQUFDLElBQVIsQ0FBYSxLQUFiLEVBQW9CLFVBQVUsQ0FBQyxXQUFYLENBQXVCLEdBQXZCLENBQXBCO0FBQ0g7QUFFRDs7QUFDQTs7QUFDQTs7Ozs7OztvREFFOEIsSzs7Ozs7O3FCQUN0QixpQjs7Ozs7aURBQ08sSzs7Ozs7dUJBR2dCLENBQUMsQ0FBQyxJQUFGLENBQU87QUFDMUIsa0JBQUEsSUFBSSxFQUFFLE1BRG9CO0FBRTFCLGtCQUFBLEdBQUcsRUFBRSx1Q0FGcUI7QUFHMUIsa0JBQUEsSUFBSSxFQUFFO0FBQUUsb0JBQUEsUUFBUSxFQUFFO0FBQVosbUJBSG9CO0FBSTFCLGtCQUFBLFFBQVEsRUFBRTtBQUpnQixpQkFBUCxDOzs7QUFBakIsZ0JBQUEsUTs7c0JBT0YsUUFBUSxDQUFDLE1BQVQsS0FBb0IsUzs7Ozs7QUFDcEIsZ0JBQUEsSUFBSSxDQUFDLGtCQUFELEVBQXFCLFFBQVEsQ0FBQyxhQUE5QixFQUE2QyxPQUE3QyxDQUFKO2lEQUNPLEs7OztBQUdYLGdCQUFBLGlCQUFpQixHQUFHLElBQXBCOzs7Ozs7O0FBRUEsZ0JBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxRQUFaO0FBQ0EsZ0JBQUEsSUFBSSxDQUFDLE1BQUQsRUFBUyxrRUFBVCxFQUE2RSxPQUE3RSxDQUFKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7cURBSW1CLE0sRUFBUSxjOzs7Ozs7Ozt1QkFFSixDQUFDLENBQUMsSUFBRixDQUFPO0FBQzFCLGtCQUFBLElBQUksRUFBRSxNQURvQjtBQUUxQixrQkFBQSxHQUFHLEVBQUUsb0NBRnFCO0FBRzFCLGtCQUFBLElBQUksRUFBRTtBQUFFLG9CQUFBLFFBQVEsRUFBRSxPQUFaO0FBQXFCLG9CQUFBLFNBQVMsRUFBRTtBQUFoQyxtQkFIb0I7QUFJMUIsa0JBQUEsUUFBUSxFQUFFLE1BSmdCO0FBSzFCLGtCQUFBLFVBQVUsRUFBRTtBQUFBLDJCQUFNLElBQUksQ0FBQyxxQkFBTCxFQUFOO0FBQUE7QUFMYyxpQkFBUCxFQU1wQixNQU5vQixDQU1iO0FBQUEseUJBQU0sSUFBSSxDQUFDLHNCQUFMLEVBQU47QUFBQSxpQkFOYSxDOzs7QUFBakIsZ0JBQUEsUTs7c0JBUUYsUUFBUSxDQUFDLE1BQVQsS0FBb0IsUzs7Ozs7QUFDcEIsZ0JBQUEsSUFBSSxDQUFDLGtCQUFELEVBQXFCLFFBQVEsQ0FBQyxhQUE5QixFQUE2QyxPQUE3QyxDQUFKO2tEQUNPLEs7OztBQUdYLGdCQUFBLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCLE1BQXJCLENBQTRCLFFBQTVCLEVBQXNDLFVBQXRDLEVBQWtELElBQWxEO0FBQ0EsZ0JBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUIsSUFBckIsQ0FBMEIsT0FBMUIsK0JBQW1ELE1BQW5ELGNBQTZELE1BQU0sSUFBSSxDQUFWLEdBQWMsU0FBZCxHQUEwQixVQUF2Rix1QkFBOEcsTUFBTSxHQUFHLE1BQVQsQ0FBZ0IsdUJBQWhCLENBQTlHOzs7Ozs7O0FBRUEsZ0JBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSxRQUFaO0FBQ0EsZ0JBQUEsSUFBSSxDQUFDLE1BQUQsRUFBUyxrRUFBVCxFQUE2RSxPQUE3RSxDQUFKOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLWixNQUFNLENBQUMsVUFBUCxHQUFvQixVQUFwQiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZ2VuZXJhdG9yLXJ1bnRpbWVcIik7XG4iLCIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxNC1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICovXG5cbnZhciBydW50aW1lID0gKGZ1bmN0aW9uIChleHBvcnRzKSB7XG4gIFwidXNlIHN0cmljdFwiO1xuXG4gIHZhciBPcCA9IE9iamVjdC5wcm90b3R5cGU7XG4gIHZhciBoYXNPd24gPSBPcC5oYXNPd25Qcm9wZXJ0eTtcbiAgdmFyIHVuZGVmaW5lZDsgLy8gTW9yZSBjb21wcmVzc2libGUgdGhhbiB2b2lkIDAuXG4gIHZhciAkU3ltYm9sID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiID8gU3ltYm9sIDoge307XG4gIHZhciBpdGVyYXRvclN5bWJvbCA9ICRTeW1ib2wuaXRlcmF0b3IgfHwgXCJAQGl0ZXJhdG9yXCI7XG4gIHZhciBhc3luY0l0ZXJhdG9yU3ltYm9sID0gJFN5bWJvbC5hc3luY0l0ZXJhdG9yIHx8IFwiQEBhc3luY0l0ZXJhdG9yXCI7XG4gIHZhciB0b1N0cmluZ1RhZ1N5bWJvbCA9ICRTeW1ib2wudG9TdHJpbmdUYWcgfHwgXCJAQHRvU3RyaW5nVGFnXCI7XG5cbiAgZnVuY3Rpb24gd3JhcChpbm5lckZuLCBvdXRlckZuLCBzZWxmLCB0cnlMb2NzTGlzdCkge1xuICAgIC8vIElmIG91dGVyRm4gcHJvdmlkZWQgYW5kIG91dGVyRm4ucHJvdG90eXBlIGlzIGEgR2VuZXJhdG9yLCB0aGVuIG91dGVyRm4ucHJvdG90eXBlIGluc3RhbmNlb2YgR2VuZXJhdG9yLlxuICAgIHZhciBwcm90b0dlbmVyYXRvciA9IG91dGVyRm4gJiYgb3V0ZXJGbi5wcm90b3R5cGUgaW5zdGFuY2VvZiBHZW5lcmF0b3IgPyBvdXRlckZuIDogR2VuZXJhdG9yO1xuICAgIHZhciBnZW5lcmF0b3IgPSBPYmplY3QuY3JlYXRlKHByb3RvR2VuZXJhdG9yLnByb3RvdHlwZSk7XG4gICAgdmFyIGNvbnRleHQgPSBuZXcgQ29udGV4dCh0cnlMb2NzTGlzdCB8fCBbXSk7XG5cbiAgICAvLyBUaGUgLl9pbnZva2UgbWV0aG9kIHVuaWZpZXMgdGhlIGltcGxlbWVudGF0aW9ucyBvZiB0aGUgLm5leHQsXG4gICAgLy8gLnRocm93LCBhbmQgLnJldHVybiBtZXRob2RzLlxuICAgIGdlbmVyYXRvci5faW52b2tlID0gbWFrZUludm9rZU1ldGhvZChpbm5lckZuLCBzZWxmLCBjb250ZXh0KTtcblxuICAgIHJldHVybiBnZW5lcmF0b3I7XG4gIH1cbiAgZXhwb3J0cy53cmFwID0gd3JhcDtcblxuICAvLyBUcnkvY2F0Y2ggaGVscGVyIHRvIG1pbmltaXplIGRlb3B0aW1pemF0aW9ucy4gUmV0dXJucyBhIGNvbXBsZXRpb25cbiAgLy8gcmVjb3JkIGxpa2UgY29udGV4dC50cnlFbnRyaWVzW2ldLmNvbXBsZXRpb24uIFRoaXMgaW50ZXJmYWNlIGNvdWxkXG4gIC8vIGhhdmUgYmVlbiAoYW5kIHdhcyBwcmV2aW91c2x5KSBkZXNpZ25lZCB0byB0YWtlIGEgY2xvc3VyZSB0byBiZVxuICAvLyBpbnZva2VkIHdpdGhvdXQgYXJndW1lbnRzLCBidXQgaW4gYWxsIHRoZSBjYXNlcyB3ZSBjYXJlIGFib3V0IHdlXG4gIC8vIGFscmVhZHkgaGF2ZSBhbiBleGlzdGluZyBtZXRob2Qgd2Ugd2FudCB0byBjYWxsLCBzbyB0aGVyZSdzIG5vIG5lZWRcbiAgLy8gdG8gY3JlYXRlIGEgbmV3IGZ1bmN0aW9uIG9iamVjdC4gV2UgY2FuIGV2ZW4gZ2V0IGF3YXkgd2l0aCBhc3N1bWluZ1xuICAvLyB0aGUgbWV0aG9kIHRha2VzIGV4YWN0bHkgb25lIGFyZ3VtZW50LCBzaW5jZSB0aGF0IGhhcHBlbnMgdG8gYmUgdHJ1ZVxuICAvLyBpbiBldmVyeSBjYXNlLCBzbyB3ZSBkb24ndCBoYXZlIHRvIHRvdWNoIHRoZSBhcmd1bWVudHMgb2JqZWN0LiBUaGVcbiAgLy8gb25seSBhZGRpdGlvbmFsIGFsbG9jYXRpb24gcmVxdWlyZWQgaXMgdGhlIGNvbXBsZXRpb24gcmVjb3JkLCB3aGljaFxuICAvLyBoYXMgYSBzdGFibGUgc2hhcGUgYW5kIHNvIGhvcGVmdWxseSBzaG91bGQgYmUgY2hlYXAgdG8gYWxsb2NhdGUuXG4gIGZ1bmN0aW9uIHRyeUNhdGNoKGZuLCBvYmosIGFyZykge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4geyB0eXBlOiBcIm5vcm1hbFwiLCBhcmc6IGZuLmNhbGwob2JqLCBhcmcpIH07XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICByZXR1cm4geyB0eXBlOiBcInRocm93XCIsIGFyZzogZXJyIH07XG4gICAgfVxuICB9XG5cbiAgdmFyIEdlblN0YXRlU3VzcGVuZGVkU3RhcnQgPSBcInN1c3BlbmRlZFN0YXJ0XCI7XG4gIHZhciBHZW5TdGF0ZVN1c3BlbmRlZFlpZWxkID0gXCJzdXNwZW5kZWRZaWVsZFwiO1xuICB2YXIgR2VuU3RhdGVFeGVjdXRpbmcgPSBcImV4ZWN1dGluZ1wiO1xuICB2YXIgR2VuU3RhdGVDb21wbGV0ZWQgPSBcImNvbXBsZXRlZFwiO1xuXG4gIC8vIFJldHVybmluZyB0aGlzIG9iamVjdCBmcm9tIHRoZSBpbm5lckZuIGhhcyB0aGUgc2FtZSBlZmZlY3QgYXNcbiAgLy8gYnJlYWtpbmcgb3V0IG9mIHRoZSBkaXNwYXRjaCBzd2l0Y2ggc3RhdGVtZW50LlxuICB2YXIgQ29udGludWVTZW50aW5lbCA9IHt9O1xuXG4gIC8vIER1bW15IGNvbnN0cnVjdG9yIGZ1bmN0aW9ucyB0aGF0IHdlIHVzZSBhcyB0aGUgLmNvbnN0cnVjdG9yIGFuZFxuICAvLyAuY29uc3RydWN0b3IucHJvdG90eXBlIHByb3BlcnRpZXMgZm9yIGZ1bmN0aW9ucyB0aGF0IHJldHVybiBHZW5lcmF0b3JcbiAgLy8gb2JqZWN0cy4gRm9yIGZ1bGwgc3BlYyBjb21wbGlhbmNlLCB5b3UgbWF5IHdpc2ggdG8gY29uZmlndXJlIHlvdXJcbiAgLy8gbWluaWZpZXIgbm90IHRvIG1hbmdsZSB0aGUgbmFtZXMgb2YgdGhlc2UgdHdvIGZ1bmN0aW9ucy5cbiAgZnVuY3Rpb24gR2VuZXJhdG9yKCkge31cbiAgZnVuY3Rpb24gR2VuZXJhdG9yRnVuY3Rpb24oKSB7fVxuICBmdW5jdGlvbiBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZSgpIHt9XG5cbiAgLy8gVGhpcyBpcyBhIHBvbHlmaWxsIGZvciAlSXRlcmF0b3JQcm90b3R5cGUlIGZvciBlbnZpcm9ubWVudHMgdGhhdFxuICAvLyBkb24ndCBuYXRpdmVseSBzdXBwb3J0IGl0LlxuICB2YXIgSXRlcmF0b3JQcm90b3R5cGUgPSB7fTtcbiAgSXRlcmF0b3JQcm90b3R5cGVbaXRlcmF0b3JTeW1ib2xdID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIHZhciBnZXRQcm90byA9IE9iamVjdC5nZXRQcm90b3R5cGVPZjtcbiAgdmFyIE5hdGl2ZUl0ZXJhdG9yUHJvdG90eXBlID0gZ2V0UHJvdG8gJiYgZ2V0UHJvdG8oZ2V0UHJvdG8odmFsdWVzKFtdKSkpO1xuICBpZiAoTmF0aXZlSXRlcmF0b3JQcm90b3R5cGUgJiZcbiAgICAgIE5hdGl2ZUl0ZXJhdG9yUHJvdG90eXBlICE9PSBPcCAmJlxuICAgICAgaGFzT3duLmNhbGwoTmF0aXZlSXRlcmF0b3JQcm90b3R5cGUsIGl0ZXJhdG9yU3ltYm9sKSkge1xuICAgIC8vIFRoaXMgZW52aXJvbm1lbnQgaGFzIGEgbmF0aXZlICVJdGVyYXRvclByb3RvdHlwZSU7IHVzZSBpdCBpbnN0ZWFkXG4gICAgLy8gb2YgdGhlIHBvbHlmaWxsLlxuICAgIEl0ZXJhdG9yUHJvdG90eXBlID0gTmF0aXZlSXRlcmF0b3JQcm90b3R5cGU7XG4gIH1cblxuICB2YXIgR3AgPSBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZS5wcm90b3R5cGUgPVxuICAgIEdlbmVyYXRvci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEl0ZXJhdG9yUHJvdG90eXBlKTtcbiAgR2VuZXJhdG9yRnVuY3Rpb24ucHJvdG90eXBlID0gR3AuY29uc3RydWN0b3IgPSBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZTtcbiAgR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUuY29uc3RydWN0b3IgPSBHZW5lcmF0b3JGdW5jdGlvbjtcbiAgR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGVbdG9TdHJpbmdUYWdTeW1ib2xdID1cbiAgICBHZW5lcmF0b3JGdW5jdGlvbi5kaXNwbGF5TmFtZSA9IFwiR2VuZXJhdG9yRnVuY3Rpb25cIjtcblxuICAvLyBIZWxwZXIgZm9yIGRlZmluaW5nIHRoZSAubmV4dCwgLnRocm93LCBhbmQgLnJldHVybiBtZXRob2RzIG9mIHRoZVxuICAvLyBJdGVyYXRvciBpbnRlcmZhY2UgaW4gdGVybXMgb2YgYSBzaW5nbGUgLl9pbnZva2UgbWV0aG9kLlxuICBmdW5jdGlvbiBkZWZpbmVJdGVyYXRvck1ldGhvZHMocHJvdG90eXBlKSB7XG4gICAgW1wibmV4dFwiLCBcInRocm93XCIsIFwicmV0dXJuXCJdLmZvckVhY2goZnVuY3Rpb24obWV0aG9kKSB7XG4gICAgICBwcm90b3R5cGVbbWV0aG9kXSA9IGZ1bmN0aW9uKGFyZykge1xuICAgICAgICByZXR1cm4gdGhpcy5faW52b2tlKG1ldGhvZCwgYXJnKTtcbiAgICAgIH07XG4gICAgfSk7XG4gIH1cblxuICBleHBvcnRzLmlzR2VuZXJhdG9yRnVuY3Rpb24gPSBmdW5jdGlvbihnZW5GdW4pIHtcbiAgICB2YXIgY3RvciA9IHR5cGVvZiBnZW5GdW4gPT09IFwiZnVuY3Rpb25cIiAmJiBnZW5GdW4uY29uc3RydWN0b3I7XG4gICAgcmV0dXJuIGN0b3JcbiAgICAgID8gY3RvciA9PT0gR2VuZXJhdG9yRnVuY3Rpb24gfHxcbiAgICAgICAgLy8gRm9yIHRoZSBuYXRpdmUgR2VuZXJhdG9yRnVuY3Rpb24gY29uc3RydWN0b3IsIHRoZSBiZXN0IHdlIGNhblxuICAgICAgICAvLyBkbyBpcyB0byBjaGVjayBpdHMgLm5hbWUgcHJvcGVydHkuXG4gICAgICAgIChjdG9yLmRpc3BsYXlOYW1lIHx8IGN0b3IubmFtZSkgPT09IFwiR2VuZXJhdG9yRnVuY3Rpb25cIlxuICAgICAgOiBmYWxzZTtcbiAgfTtcblxuICBleHBvcnRzLm1hcmsgPSBmdW5jdGlvbihnZW5GdW4pIHtcbiAgICBpZiAoT2JqZWN0LnNldFByb3RvdHlwZU9mKSB7XG4gICAgICBPYmplY3Quc2V0UHJvdG90eXBlT2YoZ2VuRnVuLCBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGdlbkZ1bi5fX3Byb3RvX18gPSBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZTtcbiAgICAgIGlmICghKHRvU3RyaW5nVGFnU3ltYm9sIGluIGdlbkZ1bikpIHtcbiAgICAgICAgZ2VuRnVuW3RvU3RyaW5nVGFnU3ltYm9sXSA9IFwiR2VuZXJhdG9yRnVuY3Rpb25cIjtcbiAgICAgIH1cbiAgICB9XG4gICAgZ2VuRnVuLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoR3ApO1xuICAgIHJldHVybiBnZW5GdW47XG4gIH07XG5cbiAgLy8gV2l0aGluIHRoZSBib2R5IG9mIGFueSBhc3luYyBmdW5jdGlvbiwgYGF3YWl0IHhgIGlzIHRyYW5zZm9ybWVkIHRvXG4gIC8vIGB5aWVsZCByZWdlbmVyYXRvclJ1bnRpbWUuYXdyYXAoeClgLCBzbyB0aGF0IHRoZSBydW50aW1lIGNhbiB0ZXN0XG4gIC8vIGBoYXNPd24uY2FsbCh2YWx1ZSwgXCJfX2F3YWl0XCIpYCB0byBkZXRlcm1pbmUgaWYgdGhlIHlpZWxkZWQgdmFsdWUgaXNcbiAgLy8gbWVhbnQgdG8gYmUgYXdhaXRlZC5cbiAgZXhwb3J0cy5hd3JhcCA9IGZ1bmN0aW9uKGFyZykge1xuICAgIHJldHVybiB7IF9fYXdhaXQ6IGFyZyB9O1xuICB9O1xuXG4gIGZ1bmN0aW9uIEFzeW5jSXRlcmF0b3IoZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gaW52b2tlKG1ldGhvZCwgYXJnLCByZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIHZhciByZWNvcmQgPSB0cnlDYXRjaChnZW5lcmF0b3JbbWV0aG9kXSwgZ2VuZXJhdG9yLCBhcmcpO1xuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgICAgcmVqZWN0KHJlY29yZC5hcmcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IHJlY29yZC5hcmc7XG4gICAgICAgIHZhciB2YWx1ZSA9IHJlc3VsdC52YWx1ZTtcbiAgICAgICAgaWYgKHZhbHVlICYmXG4gICAgICAgICAgICB0eXBlb2YgdmFsdWUgPT09IFwib2JqZWN0XCIgJiZcbiAgICAgICAgICAgIGhhc093bi5jYWxsKHZhbHVlLCBcIl9fYXdhaXRcIikpIHtcbiAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHZhbHVlLl9fYXdhaXQpLnRoZW4oZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIGludm9rZShcIm5leHRcIiwgdmFsdWUsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XG4gICAgICAgICAgICBpbnZva2UoXCJ0aHJvd1wiLCBlcnIsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHZhbHVlKS50aGVuKGZ1bmN0aW9uKHVud3JhcHBlZCkge1xuICAgICAgICAgIC8vIFdoZW4gYSB5aWVsZGVkIFByb21pc2UgaXMgcmVzb2x2ZWQsIGl0cyBmaW5hbCB2YWx1ZSBiZWNvbWVzXG4gICAgICAgICAgLy8gdGhlIC52YWx1ZSBvZiB0aGUgUHJvbWlzZTx7dmFsdWUsZG9uZX0+IHJlc3VsdCBmb3IgdGhlXG4gICAgICAgICAgLy8gY3VycmVudCBpdGVyYXRpb24uXG4gICAgICAgICAgcmVzdWx0LnZhbHVlID0gdW53cmFwcGVkO1xuICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgfSwgZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgICAvLyBJZiBhIHJlamVjdGVkIFByb21pc2Ugd2FzIHlpZWxkZWQsIHRocm93IHRoZSByZWplY3Rpb24gYmFja1xuICAgICAgICAgIC8vIGludG8gdGhlIGFzeW5jIGdlbmVyYXRvciBmdW5jdGlvbiBzbyBpdCBjYW4gYmUgaGFuZGxlZCB0aGVyZS5cbiAgICAgICAgICByZXR1cm4gaW52b2tlKFwidGhyb3dcIiwgZXJyb3IsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBwcmV2aW91c1Byb21pc2U7XG5cbiAgICBmdW5jdGlvbiBlbnF1ZXVlKG1ldGhvZCwgYXJnKSB7XG4gICAgICBmdW5jdGlvbiBjYWxsSW52b2tlV2l0aE1ldGhvZEFuZEFyZygpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICAgIGludm9rZShtZXRob2QsIGFyZywgcmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBwcmV2aW91c1Byb21pc2UgPVxuICAgICAgICAvLyBJZiBlbnF1ZXVlIGhhcyBiZWVuIGNhbGxlZCBiZWZvcmUsIHRoZW4gd2Ugd2FudCB0byB3YWl0IHVudGlsXG4gICAgICAgIC8vIGFsbCBwcmV2aW91cyBQcm9taXNlcyBoYXZlIGJlZW4gcmVzb2x2ZWQgYmVmb3JlIGNhbGxpbmcgaW52b2tlLFxuICAgICAgICAvLyBzbyB0aGF0IHJlc3VsdHMgYXJlIGFsd2F5cyBkZWxpdmVyZWQgaW4gdGhlIGNvcnJlY3Qgb3JkZXIuIElmXG4gICAgICAgIC8vIGVucXVldWUgaGFzIG5vdCBiZWVuIGNhbGxlZCBiZWZvcmUsIHRoZW4gaXQgaXMgaW1wb3J0YW50IHRvXG4gICAgICAgIC8vIGNhbGwgaW52b2tlIGltbWVkaWF0ZWx5LCB3aXRob3V0IHdhaXRpbmcgb24gYSBjYWxsYmFjayB0byBmaXJlLFxuICAgICAgICAvLyBzbyB0aGF0IHRoZSBhc3luYyBnZW5lcmF0b3IgZnVuY3Rpb24gaGFzIHRoZSBvcHBvcnR1bml0eSB0byBkb1xuICAgICAgICAvLyBhbnkgbmVjZXNzYXJ5IHNldHVwIGluIGEgcHJlZGljdGFibGUgd2F5LiBUaGlzIHByZWRpY3RhYmlsaXR5XG4gICAgICAgIC8vIGlzIHdoeSB0aGUgUHJvbWlzZSBjb25zdHJ1Y3RvciBzeW5jaHJvbm91c2x5IGludm9rZXMgaXRzXG4gICAgICAgIC8vIGV4ZWN1dG9yIGNhbGxiYWNrLCBhbmQgd2h5IGFzeW5jIGZ1bmN0aW9ucyBzeW5jaHJvbm91c2x5XG4gICAgICAgIC8vIGV4ZWN1dGUgY29kZSBiZWZvcmUgdGhlIGZpcnN0IGF3YWl0LiBTaW5jZSB3ZSBpbXBsZW1lbnQgc2ltcGxlXG4gICAgICAgIC8vIGFzeW5jIGZ1bmN0aW9ucyBpbiB0ZXJtcyBvZiBhc3luYyBnZW5lcmF0b3JzLCBpdCBpcyBlc3BlY2lhbGx5XG4gICAgICAgIC8vIGltcG9ydGFudCB0byBnZXQgdGhpcyByaWdodCwgZXZlbiB0aG91Z2ggaXQgcmVxdWlyZXMgY2FyZS5cbiAgICAgICAgcHJldmlvdXNQcm9taXNlID8gcHJldmlvdXNQcm9taXNlLnRoZW4oXG4gICAgICAgICAgY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmcsXG4gICAgICAgICAgLy8gQXZvaWQgcHJvcGFnYXRpbmcgZmFpbHVyZXMgdG8gUHJvbWlzZXMgcmV0dXJuZWQgYnkgbGF0ZXJcbiAgICAgICAgICAvLyBpbnZvY2F0aW9ucyBvZiB0aGUgaXRlcmF0b3IuXG4gICAgICAgICAgY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmdcbiAgICAgICAgKSA6IGNhbGxJbnZva2VXaXRoTWV0aG9kQW5kQXJnKCk7XG4gICAgfVxuXG4gICAgLy8gRGVmaW5lIHRoZSB1bmlmaWVkIGhlbHBlciBtZXRob2QgdGhhdCBpcyB1c2VkIHRvIGltcGxlbWVudCAubmV4dCxcbiAgICAvLyAudGhyb3csIGFuZCAucmV0dXJuIChzZWUgZGVmaW5lSXRlcmF0b3JNZXRob2RzKS5cbiAgICB0aGlzLl9pbnZva2UgPSBlbnF1ZXVlO1xuICB9XG5cbiAgZGVmaW5lSXRlcmF0b3JNZXRob2RzKEFzeW5jSXRlcmF0b3IucHJvdG90eXBlKTtcbiAgQXN5bmNJdGVyYXRvci5wcm90b3R5cGVbYXN5bmNJdGVyYXRvclN5bWJvbF0gPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH07XG4gIGV4cG9ydHMuQXN5bmNJdGVyYXRvciA9IEFzeW5jSXRlcmF0b3I7XG5cbiAgLy8gTm90ZSB0aGF0IHNpbXBsZSBhc3luYyBmdW5jdGlvbnMgYXJlIGltcGxlbWVudGVkIG9uIHRvcCBvZlxuICAvLyBBc3luY0l0ZXJhdG9yIG9iamVjdHM7IHRoZXkganVzdCByZXR1cm4gYSBQcm9taXNlIGZvciB0aGUgdmFsdWUgb2ZcbiAgLy8gdGhlIGZpbmFsIHJlc3VsdCBwcm9kdWNlZCBieSB0aGUgaXRlcmF0b3IuXG4gIGV4cG9ydHMuYXN5bmMgPSBmdW5jdGlvbihpbm5lckZuLCBvdXRlckZuLCBzZWxmLCB0cnlMb2NzTGlzdCkge1xuICAgIHZhciBpdGVyID0gbmV3IEFzeW5jSXRlcmF0b3IoXG4gICAgICB3cmFwKGlubmVyRm4sIG91dGVyRm4sIHNlbGYsIHRyeUxvY3NMaXN0KVxuICAgICk7XG5cbiAgICByZXR1cm4gZXhwb3J0cy5pc0dlbmVyYXRvckZ1bmN0aW9uKG91dGVyRm4pXG4gICAgICA/IGl0ZXIgLy8gSWYgb3V0ZXJGbiBpcyBhIGdlbmVyYXRvciwgcmV0dXJuIHRoZSBmdWxsIGl0ZXJhdG9yLlxuICAgICAgOiBpdGVyLm5leHQoKS50aGVuKGZ1bmN0aW9uKHJlc3VsdCkge1xuICAgICAgICAgIHJldHVybiByZXN1bHQuZG9uZSA/IHJlc3VsdC52YWx1ZSA6IGl0ZXIubmV4dCgpO1xuICAgICAgICB9KTtcbiAgfTtcblxuICBmdW5jdGlvbiBtYWtlSW52b2tlTWV0aG9kKGlubmVyRm4sIHNlbGYsIGNvbnRleHQpIHtcbiAgICB2YXIgc3RhdGUgPSBHZW5TdGF0ZVN1c3BlbmRlZFN0YXJ0O1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIGludm9rZShtZXRob2QsIGFyZykge1xuICAgICAgaWYgKHN0YXRlID09PSBHZW5TdGF0ZUV4ZWN1dGluZykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBydW5uaW5nXCIpO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RhdGUgPT09IEdlblN0YXRlQ29tcGxldGVkKSB7XG4gICAgICAgIGlmIChtZXRob2QgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgIHRocm93IGFyZztcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEJlIGZvcmdpdmluZywgcGVyIDI1LjMuMy4zLjMgb2YgdGhlIHNwZWM6XG4gICAgICAgIC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1nZW5lcmF0b3JyZXN1bWVcbiAgICAgICAgcmV0dXJuIGRvbmVSZXN1bHQoKTtcbiAgICAgIH1cblxuICAgICAgY29udGV4dC5tZXRob2QgPSBtZXRob2Q7XG4gICAgICBjb250ZXh0LmFyZyA9IGFyZztcblxuICAgICAgd2hpbGUgKHRydWUpIHtcbiAgICAgICAgdmFyIGRlbGVnYXRlID0gY29udGV4dC5kZWxlZ2F0ZTtcbiAgICAgICAgaWYgKGRlbGVnYXRlKSB7XG4gICAgICAgICAgdmFyIGRlbGVnYXRlUmVzdWx0ID0gbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCk7XG4gICAgICAgICAgaWYgKGRlbGVnYXRlUmVzdWx0KSB7XG4gICAgICAgICAgICBpZiAoZGVsZWdhdGVSZXN1bHQgPT09IENvbnRpbnVlU2VudGluZWwpIGNvbnRpbnVlO1xuICAgICAgICAgICAgcmV0dXJuIGRlbGVnYXRlUmVzdWx0O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb250ZXh0Lm1ldGhvZCA9PT0gXCJuZXh0XCIpIHtcbiAgICAgICAgICAvLyBTZXR0aW5nIGNvbnRleHQuX3NlbnQgZm9yIGxlZ2FjeSBzdXBwb3J0IG9mIEJhYmVsJ3NcbiAgICAgICAgICAvLyBmdW5jdGlvbi5zZW50IGltcGxlbWVudGF0aW9uLlxuICAgICAgICAgIGNvbnRleHQuc2VudCA9IGNvbnRleHQuX3NlbnQgPSBjb250ZXh0LmFyZztcblxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgICBpZiAoc3RhdGUgPT09IEdlblN0YXRlU3VzcGVuZGVkU3RhcnQpIHtcbiAgICAgICAgICAgIHN0YXRlID0gR2VuU3RhdGVDb21wbGV0ZWQ7XG4gICAgICAgICAgICB0aHJvdyBjb250ZXh0LmFyZztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb250ZXh0LmRpc3BhdGNoRXhjZXB0aW9uKGNvbnRleHQuYXJnKTtcblxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInJldHVyblwiKSB7XG4gICAgICAgICAgY29udGV4dC5hYnJ1cHQoXCJyZXR1cm5cIiwgY29udGV4dC5hcmcpO1xuICAgICAgICB9XG5cbiAgICAgICAgc3RhdGUgPSBHZW5TdGF0ZUV4ZWN1dGluZztcblxuICAgICAgICB2YXIgcmVjb3JkID0gdHJ5Q2F0Y2goaW5uZXJGbiwgc2VsZiwgY29udGV4dCk7XG4gICAgICAgIGlmIChyZWNvcmQudHlwZSA9PT0gXCJub3JtYWxcIikge1xuICAgICAgICAgIC8vIElmIGFuIGV4Y2VwdGlvbiBpcyB0aHJvd24gZnJvbSBpbm5lckZuLCB3ZSBsZWF2ZSBzdGF0ZSA9PT1cbiAgICAgICAgICAvLyBHZW5TdGF0ZUV4ZWN1dGluZyBhbmQgbG9vcCBiYWNrIGZvciBhbm90aGVyIGludm9jYXRpb24uXG4gICAgICAgICAgc3RhdGUgPSBjb250ZXh0LmRvbmVcbiAgICAgICAgICAgID8gR2VuU3RhdGVDb21wbGV0ZWRcbiAgICAgICAgICAgIDogR2VuU3RhdGVTdXNwZW5kZWRZaWVsZDtcblxuICAgICAgICAgIGlmIChyZWNvcmQuYXJnID09PSBDb250aW51ZVNlbnRpbmVsKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdmFsdWU6IHJlY29yZC5hcmcsXG4gICAgICAgICAgICBkb25lOiBjb250ZXh0LmRvbmVcbiAgICAgICAgICB9O1xuXG4gICAgICAgIH0gZWxzZSBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgIHN0YXRlID0gR2VuU3RhdGVDb21wbGV0ZWQ7XG4gICAgICAgICAgLy8gRGlzcGF0Y2ggdGhlIGV4Y2VwdGlvbiBieSBsb29waW5nIGJhY2sgYXJvdW5kIHRvIHRoZVxuICAgICAgICAgIC8vIGNvbnRleHQuZGlzcGF0Y2hFeGNlcHRpb24oY29udGV4dC5hcmcpIGNhbGwgYWJvdmUuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICAgICAgY29udGV4dC5hcmcgPSByZWNvcmQuYXJnO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIC8vIENhbGwgZGVsZWdhdGUuaXRlcmF0b3JbY29udGV4dC5tZXRob2RdKGNvbnRleHQuYXJnKSBhbmQgaGFuZGxlIHRoZVxuICAvLyByZXN1bHQsIGVpdGhlciBieSByZXR1cm5pbmcgYSB7IHZhbHVlLCBkb25lIH0gcmVzdWx0IGZyb20gdGhlXG4gIC8vIGRlbGVnYXRlIGl0ZXJhdG9yLCBvciBieSBtb2RpZnlpbmcgY29udGV4dC5tZXRob2QgYW5kIGNvbnRleHQuYXJnLFxuICAvLyBzZXR0aW5nIGNvbnRleHQuZGVsZWdhdGUgdG8gbnVsbCwgYW5kIHJldHVybmluZyB0aGUgQ29udGludWVTZW50aW5lbC5cbiAgZnVuY3Rpb24gbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCkge1xuICAgIHZhciBtZXRob2QgPSBkZWxlZ2F0ZS5pdGVyYXRvcltjb250ZXh0Lm1ldGhvZF07XG4gICAgaWYgKG1ldGhvZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAvLyBBIC50aHJvdyBvciAucmV0dXJuIHdoZW4gdGhlIGRlbGVnYXRlIGl0ZXJhdG9yIGhhcyBubyAudGhyb3dcbiAgICAgIC8vIG1ldGhvZCBhbHdheXMgdGVybWluYXRlcyB0aGUgeWllbGQqIGxvb3AuXG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcblxuICAgICAgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgLy8gTm90ZTogW1wicmV0dXJuXCJdIG11c3QgYmUgdXNlZCBmb3IgRVMzIHBhcnNpbmcgY29tcGF0aWJpbGl0eS5cbiAgICAgICAgaWYgKGRlbGVnYXRlLml0ZXJhdG9yW1wicmV0dXJuXCJdKSB7XG4gICAgICAgICAgLy8gSWYgdGhlIGRlbGVnYXRlIGl0ZXJhdG9yIGhhcyBhIHJldHVybiBtZXRob2QsIGdpdmUgaXQgYVxuICAgICAgICAgIC8vIGNoYW5jZSB0byBjbGVhbiB1cC5cbiAgICAgICAgICBjb250ZXh0Lm1ldGhvZCA9IFwicmV0dXJuXCI7XG4gICAgICAgICAgY29udGV4dC5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCk7XG5cbiAgICAgICAgICBpZiAoY29udGV4dC5tZXRob2QgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgICAgLy8gSWYgbWF5YmVJbnZva2VEZWxlZ2F0ZShjb250ZXh0KSBjaGFuZ2VkIGNvbnRleHQubWV0aG9kIGZyb21cbiAgICAgICAgICAgIC8vIFwicmV0dXJuXCIgdG8gXCJ0aHJvd1wiLCBsZXQgdGhhdCBvdmVycmlkZSB0aGUgVHlwZUVycm9yIGJlbG93LlxuICAgICAgICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICAgIGNvbnRleHQuYXJnID0gbmV3IFR5cGVFcnJvcihcbiAgICAgICAgICBcIlRoZSBpdGVyYXRvciBkb2VzIG5vdCBwcm92aWRlIGEgJ3Rocm93JyBtZXRob2RcIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIHZhciByZWNvcmQgPSB0cnlDYXRjaChtZXRob2QsIGRlbGVnYXRlLml0ZXJhdG9yLCBjb250ZXh0LmFyZyk7XG5cbiAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICBjb250ZXh0LmFyZyA9IHJlY29yZC5hcmc7XG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIHZhciBpbmZvID0gcmVjb3JkLmFyZztcblxuICAgIGlmICghIGluZm8pIHtcbiAgICAgIGNvbnRleHQubWV0aG9kID0gXCJ0aHJvd1wiO1xuICAgICAgY29udGV4dC5hcmcgPSBuZXcgVHlwZUVycm9yKFwiaXRlcmF0b3IgcmVzdWx0IGlzIG5vdCBhbiBvYmplY3RcIik7XG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIGlmIChpbmZvLmRvbmUpIHtcbiAgICAgIC8vIEFzc2lnbiB0aGUgcmVzdWx0IG9mIHRoZSBmaW5pc2hlZCBkZWxlZ2F0ZSB0byB0aGUgdGVtcG9yYXJ5XG4gICAgICAvLyB2YXJpYWJsZSBzcGVjaWZpZWQgYnkgZGVsZWdhdGUucmVzdWx0TmFtZSAoc2VlIGRlbGVnYXRlWWllbGQpLlxuICAgICAgY29udGV4dFtkZWxlZ2F0ZS5yZXN1bHROYW1lXSA9IGluZm8udmFsdWU7XG5cbiAgICAgIC8vIFJlc3VtZSBleGVjdXRpb24gYXQgdGhlIGRlc2lyZWQgbG9jYXRpb24gKHNlZSBkZWxlZ2F0ZVlpZWxkKS5cbiAgICAgIGNvbnRleHQubmV4dCA9IGRlbGVnYXRlLm5leHRMb2M7XG5cbiAgICAgIC8vIElmIGNvbnRleHQubWV0aG9kIHdhcyBcInRocm93XCIgYnV0IHRoZSBkZWxlZ2F0ZSBoYW5kbGVkIHRoZVxuICAgICAgLy8gZXhjZXB0aW9uLCBsZXQgdGhlIG91dGVyIGdlbmVyYXRvciBwcm9jZWVkIG5vcm1hbGx5LiBJZlxuICAgICAgLy8gY29udGV4dC5tZXRob2Qgd2FzIFwibmV4dFwiLCBmb3JnZXQgY29udGV4dC5hcmcgc2luY2UgaXQgaGFzIGJlZW5cbiAgICAgIC8vIFwiY29uc3VtZWRcIiBieSB0aGUgZGVsZWdhdGUgaXRlcmF0b3IuIElmIGNvbnRleHQubWV0aG9kIHdhc1xuICAgICAgLy8gXCJyZXR1cm5cIiwgYWxsb3cgdGhlIG9yaWdpbmFsIC5yZXR1cm4gY2FsbCB0byBjb250aW51ZSBpbiB0aGVcbiAgICAgIC8vIG91dGVyIGdlbmVyYXRvci5cbiAgICAgIGlmIChjb250ZXh0Lm1ldGhvZCAhPT0gXCJyZXR1cm5cIikge1xuICAgICAgICBjb250ZXh0Lm1ldGhvZCA9IFwibmV4dFwiO1xuICAgICAgICBjb250ZXh0LmFyZyA9IHVuZGVmaW5lZDtcbiAgICAgIH1cblxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBSZS15aWVsZCB0aGUgcmVzdWx0IHJldHVybmVkIGJ5IHRoZSBkZWxlZ2F0ZSBtZXRob2QuXG4gICAgICByZXR1cm4gaW5mbztcbiAgICB9XG5cbiAgICAvLyBUaGUgZGVsZWdhdGUgaXRlcmF0b3IgaXMgZmluaXNoZWQsIHNvIGZvcmdldCBpdCBhbmQgY29udGludWUgd2l0aFxuICAgIC8vIHRoZSBvdXRlciBnZW5lcmF0b3IuXG4gICAgY29udGV4dC5kZWxlZ2F0ZSA9IG51bGw7XG4gICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gIH1cblxuICAvLyBEZWZpbmUgR2VuZXJhdG9yLnByb3RvdHlwZS57bmV4dCx0aHJvdyxyZXR1cm59IGluIHRlcm1zIG9mIHRoZVxuICAvLyB1bmlmaWVkIC5faW52b2tlIGhlbHBlciBtZXRob2QuXG4gIGRlZmluZUl0ZXJhdG9yTWV0aG9kcyhHcCk7XG5cbiAgR3BbdG9TdHJpbmdUYWdTeW1ib2xdID0gXCJHZW5lcmF0b3JcIjtcblxuICAvLyBBIEdlbmVyYXRvciBzaG91bGQgYWx3YXlzIHJldHVybiBpdHNlbGYgYXMgdGhlIGl0ZXJhdG9yIG9iamVjdCB3aGVuIHRoZVxuICAvLyBAQGl0ZXJhdG9yIGZ1bmN0aW9uIGlzIGNhbGxlZCBvbiBpdC4gU29tZSBicm93c2VycycgaW1wbGVtZW50YXRpb25zIG9mIHRoZVxuICAvLyBpdGVyYXRvciBwcm90b3R5cGUgY2hhaW4gaW5jb3JyZWN0bHkgaW1wbGVtZW50IHRoaXMsIGNhdXNpbmcgdGhlIEdlbmVyYXRvclxuICAvLyBvYmplY3QgdG8gbm90IGJlIHJldHVybmVkIGZyb20gdGhpcyBjYWxsLiBUaGlzIGVuc3VyZXMgdGhhdCBkb2Vzbid0IGhhcHBlbi5cbiAgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWdlbmVyYXRvci9pc3N1ZXMvMjc0IGZvciBtb3JlIGRldGFpbHMuXG4gIEdwW2l0ZXJhdG9yU3ltYm9sXSA9IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIEdwLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIFwiW29iamVjdCBHZW5lcmF0b3JdXCI7XG4gIH07XG5cbiAgZnVuY3Rpb24gcHVzaFRyeUVudHJ5KGxvY3MpIHtcbiAgICB2YXIgZW50cnkgPSB7IHRyeUxvYzogbG9jc1swXSB9O1xuXG4gICAgaWYgKDEgaW4gbG9jcykge1xuICAgICAgZW50cnkuY2F0Y2hMb2MgPSBsb2NzWzFdO1xuICAgIH1cblxuICAgIGlmICgyIGluIGxvY3MpIHtcbiAgICAgIGVudHJ5LmZpbmFsbHlMb2MgPSBsb2NzWzJdO1xuICAgICAgZW50cnkuYWZ0ZXJMb2MgPSBsb2NzWzNdO1xuICAgIH1cblxuICAgIHRoaXMudHJ5RW50cmllcy5wdXNoKGVudHJ5KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlc2V0VHJ5RW50cnkoZW50cnkpIHtcbiAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbiB8fCB7fTtcbiAgICByZWNvcmQudHlwZSA9IFwibm9ybWFsXCI7XG4gICAgZGVsZXRlIHJlY29yZC5hcmc7XG4gICAgZW50cnkuY29tcGxldGlvbiA9IHJlY29yZDtcbiAgfVxuXG4gIGZ1bmN0aW9uIENvbnRleHQodHJ5TG9jc0xpc3QpIHtcbiAgICAvLyBUaGUgcm9vdCBlbnRyeSBvYmplY3QgKGVmZmVjdGl2ZWx5IGEgdHJ5IHN0YXRlbWVudCB3aXRob3V0IGEgY2F0Y2hcbiAgICAvLyBvciBhIGZpbmFsbHkgYmxvY2spIGdpdmVzIHVzIGEgcGxhY2UgdG8gc3RvcmUgdmFsdWVzIHRocm93biBmcm9tXG4gICAgLy8gbG9jYXRpb25zIHdoZXJlIHRoZXJlIGlzIG5vIGVuY2xvc2luZyB0cnkgc3RhdGVtZW50LlxuICAgIHRoaXMudHJ5RW50cmllcyA9IFt7IHRyeUxvYzogXCJyb290XCIgfV07XG4gICAgdHJ5TG9jc0xpc3QuZm9yRWFjaChwdXNoVHJ5RW50cnksIHRoaXMpO1xuICAgIHRoaXMucmVzZXQodHJ1ZSk7XG4gIH1cblxuICBleHBvcnRzLmtleXMgPSBmdW5jdGlvbihvYmplY3QpIHtcbiAgICB2YXIga2V5cyA9IFtdO1xuICAgIGZvciAodmFyIGtleSBpbiBvYmplY3QpIHtcbiAgICAgIGtleXMucHVzaChrZXkpO1xuICAgIH1cbiAgICBrZXlzLnJldmVyc2UoKTtcblxuICAgIC8vIFJhdGhlciB0aGFuIHJldHVybmluZyBhbiBvYmplY3Qgd2l0aCBhIG5leHQgbWV0aG9kLCB3ZSBrZWVwXG4gICAgLy8gdGhpbmdzIHNpbXBsZSBhbmQgcmV0dXJuIHRoZSBuZXh0IGZ1bmN0aW9uIGl0c2VsZi5cbiAgICByZXR1cm4gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgIHdoaWxlIChrZXlzLmxlbmd0aCkge1xuICAgICAgICB2YXIga2V5ID0ga2V5cy5wb3AoKTtcbiAgICAgICAgaWYgKGtleSBpbiBvYmplY3QpIHtcbiAgICAgICAgICBuZXh0LnZhbHVlID0ga2V5O1xuICAgICAgICAgIG5leHQuZG9uZSA9IGZhbHNlO1xuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIFRvIGF2b2lkIGNyZWF0aW5nIGFuIGFkZGl0aW9uYWwgb2JqZWN0LCB3ZSBqdXN0IGhhbmcgdGhlIC52YWx1ZVxuICAgICAgLy8gYW5kIC5kb25lIHByb3BlcnRpZXMgb2ZmIHRoZSBuZXh0IGZ1bmN0aW9uIG9iamVjdCBpdHNlbGYuIFRoaXNcbiAgICAgIC8vIGFsc28gZW5zdXJlcyB0aGF0IHRoZSBtaW5pZmllciB3aWxsIG5vdCBhbm9ueW1pemUgdGhlIGZ1bmN0aW9uLlxuICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcbiAgICAgIHJldHVybiBuZXh0O1xuICAgIH07XG4gIH07XG5cbiAgZnVuY3Rpb24gdmFsdWVzKGl0ZXJhYmxlKSB7XG4gICAgaWYgKGl0ZXJhYmxlKSB7XG4gICAgICB2YXIgaXRlcmF0b3JNZXRob2QgPSBpdGVyYWJsZVtpdGVyYXRvclN5bWJvbF07XG4gICAgICBpZiAoaXRlcmF0b3JNZXRob2QpIHtcbiAgICAgICAgcmV0dXJuIGl0ZXJhdG9yTWV0aG9kLmNhbGwoaXRlcmFibGUpO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIGl0ZXJhYmxlLm5leHQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICByZXR1cm4gaXRlcmFibGU7XG4gICAgICB9XG5cbiAgICAgIGlmICghaXNOYU4oaXRlcmFibGUubGVuZ3RoKSkge1xuICAgICAgICB2YXIgaSA9IC0xLCBuZXh0ID0gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgICAgICB3aGlsZSAoKytpIDwgaXRlcmFibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duLmNhbGwoaXRlcmFibGUsIGkpKSB7XG4gICAgICAgICAgICAgIG5leHQudmFsdWUgPSBpdGVyYWJsZVtpXTtcbiAgICAgICAgICAgICAgbmV4dC5kb25lID0gZmFsc2U7XG4gICAgICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICAgIG5leHQudmFsdWUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcblxuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBuZXh0Lm5leHQgPSBuZXh0O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIFJldHVybiBhbiBpdGVyYXRvciB3aXRoIG5vIHZhbHVlcy5cbiAgICByZXR1cm4geyBuZXh0OiBkb25lUmVzdWx0IH07XG4gIH1cbiAgZXhwb3J0cy52YWx1ZXMgPSB2YWx1ZXM7XG5cbiAgZnVuY3Rpb24gZG9uZVJlc3VsdCgpIHtcbiAgICByZXR1cm4geyB2YWx1ZTogdW5kZWZpbmVkLCBkb25lOiB0cnVlIH07XG4gIH1cblxuICBDb250ZXh0LnByb3RvdHlwZSA9IHtcbiAgICBjb25zdHJ1Y3RvcjogQ29udGV4dCxcblxuICAgIHJlc2V0OiBmdW5jdGlvbihza2lwVGVtcFJlc2V0KSB7XG4gICAgICB0aGlzLnByZXYgPSAwO1xuICAgICAgdGhpcy5uZXh0ID0gMDtcbiAgICAgIC8vIFJlc2V0dGluZyBjb250ZXh0Ll9zZW50IGZvciBsZWdhY3kgc3VwcG9ydCBvZiBCYWJlbCdzXG4gICAgICAvLyBmdW5jdGlvbi5zZW50IGltcGxlbWVudGF0aW9uLlxuICAgICAgdGhpcy5zZW50ID0gdGhpcy5fc2VudCA9IHVuZGVmaW5lZDtcbiAgICAgIHRoaXMuZG9uZSA9IGZhbHNlO1xuICAgICAgdGhpcy5kZWxlZ2F0ZSA9IG51bGw7XG5cbiAgICAgIHRoaXMubWV0aG9kID0gXCJuZXh0XCI7XG4gICAgICB0aGlzLmFyZyA9IHVuZGVmaW5lZDtcblxuICAgICAgdGhpcy50cnlFbnRyaWVzLmZvckVhY2gocmVzZXRUcnlFbnRyeSk7XG5cbiAgICAgIGlmICghc2tpcFRlbXBSZXNldCkge1xuICAgICAgICBmb3IgKHZhciBuYW1lIGluIHRoaXMpIHtcbiAgICAgICAgICAvLyBOb3Qgc3VyZSBhYm91dCB0aGUgb3B0aW1hbCBvcmRlciBvZiB0aGVzZSBjb25kaXRpb25zOlxuICAgICAgICAgIGlmIChuYW1lLmNoYXJBdCgwKSA9PT0gXCJ0XCIgJiZcbiAgICAgICAgICAgICAgaGFzT3duLmNhbGwodGhpcywgbmFtZSkgJiZcbiAgICAgICAgICAgICAgIWlzTmFOKCtuYW1lLnNsaWNlKDEpKSkge1xuICAgICAgICAgICAgdGhpc1tuYW1lXSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgc3RvcDogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmRvbmUgPSB0cnVlO1xuXG4gICAgICB2YXIgcm9vdEVudHJ5ID0gdGhpcy50cnlFbnRyaWVzWzBdO1xuICAgICAgdmFyIHJvb3RSZWNvcmQgPSByb290RW50cnkuY29tcGxldGlvbjtcbiAgICAgIGlmIChyb290UmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICB0aHJvdyByb290UmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMucnZhbDtcbiAgICB9LFxuXG4gICAgZGlzcGF0Y2hFeGNlcHRpb246IGZ1bmN0aW9uKGV4Y2VwdGlvbikge1xuICAgICAgaWYgKHRoaXMuZG9uZSkge1xuICAgICAgICB0aHJvdyBleGNlcHRpb247XG4gICAgICB9XG5cbiAgICAgIHZhciBjb250ZXh0ID0gdGhpcztcbiAgICAgIGZ1bmN0aW9uIGhhbmRsZShsb2MsIGNhdWdodCkge1xuICAgICAgICByZWNvcmQudHlwZSA9IFwidGhyb3dcIjtcbiAgICAgICAgcmVjb3JkLmFyZyA9IGV4Y2VwdGlvbjtcbiAgICAgICAgY29udGV4dC5uZXh0ID0gbG9jO1xuXG4gICAgICAgIGlmIChjYXVnaHQpIHtcbiAgICAgICAgICAvLyBJZiB0aGUgZGlzcGF0Y2hlZCBleGNlcHRpb24gd2FzIGNhdWdodCBieSBhIGNhdGNoIGJsb2NrLFxuICAgICAgICAgIC8vIHRoZW4gbGV0IHRoYXQgY2F0Y2ggYmxvY2sgaGFuZGxlIHRoZSBleGNlcHRpb24gbm9ybWFsbHkuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgICBjb250ZXh0LmFyZyA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAhISBjYXVnaHQ7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcblxuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSBcInJvb3RcIikge1xuICAgICAgICAgIC8vIEV4Y2VwdGlvbiB0aHJvd24gb3V0c2lkZSBvZiBhbnkgdHJ5IGJsb2NrIHRoYXQgY291bGQgaGFuZGxlXG4gICAgICAgICAgLy8gaXQsIHNvIHNldCB0aGUgY29tcGxldGlvbiB2YWx1ZSBvZiB0aGUgZW50aXJlIGZ1bmN0aW9uIHRvXG4gICAgICAgICAgLy8gdGhyb3cgdGhlIGV4Y2VwdGlvbi5cbiAgICAgICAgICByZXR1cm4gaGFuZGxlKFwiZW5kXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYpIHtcbiAgICAgICAgICB2YXIgaGFzQ2F0Y2ggPSBoYXNPd24uY2FsbChlbnRyeSwgXCJjYXRjaExvY1wiKTtcbiAgICAgICAgICB2YXIgaGFzRmluYWxseSA9IGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIik7XG5cbiAgICAgICAgICBpZiAoaGFzQ2F0Y2ggJiYgaGFzRmluYWxseSkge1xuICAgICAgICAgICAgaWYgKHRoaXMucHJldiA8IGVudHJ5LmNhdGNoTG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuY2F0Y2hMb2MsIHRydWUpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0NhdGNoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wcmV2IDwgZW50cnkuY2F0Y2hMb2MpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGhhbmRsZShlbnRyeS5jYXRjaExvYywgdHJ1ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0ZpbmFsbHkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwidHJ5IHN0YXRlbWVudCB3aXRob3V0IGNhdGNoIG9yIGZpbmFsbHlcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcblxuICAgIGFicnVwdDogZnVuY3Rpb24odHlwZSwgYXJnKSB7XG4gICAgICBmb3IgKHZhciBpID0gdGhpcy50cnlFbnRyaWVzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgIHZhciBlbnRyeSA9IHRoaXMudHJ5RW50cmllc1tpXTtcbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYgJiZcbiAgICAgICAgICAgIGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIikgJiZcbiAgICAgICAgICAgIHRoaXMucHJldiA8IGVudHJ5LmZpbmFsbHlMb2MpIHtcbiAgICAgICAgICB2YXIgZmluYWxseUVudHJ5ID0gZW50cnk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGZpbmFsbHlFbnRyeSAmJlxuICAgICAgICAgICh0eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICAgdHlwZSA9PT0gXCJjb250aW51ZVwiKSAmJlxuICAgICAgICAgIGZpbmFsbHlFbnRyeS50cnlMb2MgPD0gYXJnICYmXG4gICAgICAgICAgYXJnIDw9IGZpbmFsbHlFbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgIC8vIElnbm9yZSB0aGUgZmluYWxseSBlbnRyeSBpZiBjb250cm9sIGlzIG5vdCBqdW1waW5nIHRvIGFcbiAgICAgICAgLy8gbG9jYXRpb24gb3V0c2lkZSB0aGUgdHJ5L2NhdGNoIGJsb2NrLlxuICAgICAgICBmaW5hbGx5RW50cnkgPSBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmVjb3JkID0gZmluYWxseUVudHJ5ID8gZmluYWxseUVudHJ5LmNvbXBsZXRpb24gOiB7fTtcbiAgICAgIHJlY29yZC50eXBlID0gdHlwZTtcbiAgICAgIHJlY29yZC5hcmcgPSBhcmc7XG5cbiAgICAgIGlmIChmaW5hbGx5RW50cnkpIHtcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgdGhpcy5uZXh0ID0gZmluYWxseUVudHJ5LmZpbmFsbHlMb2M7XG4gICAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jb21wbGV0ZShyZWNvcmQpO1xuICAgIH0sXG5cbiAgICBjb21wbGV0ZTogZnVuY3Rpb24ocmVjb3JkLCBhZnRlckxvYykge1xuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgICAgdGhyb3cgcmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICByZWNvcmQudHlwZSA9PT0gXCJjb250aW51ZVwiKSB7XG4gICAgICAgIHRoaXMubmV4dCA9IHJlY29yZC5hcmc7XG4gICAgICB9IGVsc2UgaWYgKHJlY29yZC50eXBlID09PSBcInJldHVyblwiKSB7XG4gICAgICAgIHRoaXMucnZhbCA9IHRoaXMuYXJnID0gcmVjb3JkLmFyZztcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcInJldHVyblwiO1xuICAgICAgICB0aGlzLm5leHQgPSBcImVuZFwiO1xuICAgICAgfSBlbHNlIGlmIChyZWNvcmQudHlwZSA9PT0gXCJub3JtYWxcIiAmJiBhZnRlckxvYykge1xuICAgICAgICB0aGlzLm5leHQgPSBhZnRlckxvYztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgfSxcblxuICAgIGZpbmlzaDogZnVuY3Rpb24oZmluYWxseUxvYykge1xuICAgICAgZm9yICh2YXIgaSA9IHRoaXMudHJ5RW50cmllcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICB2YXIgZW50cnkgPSB0aGlzLnRyeUVudHJpZXNbaV07XG4gICAgICAgIGlmIChlbnRyeS5maW5hbGx5TG9jID09PSBmaW5hbGx5TG9jKSB7XG4gICAgICAgICAgdGhpcy5jb21wbGV0ZShlbnRyeS5jb21wbGV0aW9uLCBlbnRyeS5hZnRlckxvYyk7XG4gICAgICAgICAgcmVzZXRUcnlFbnRyeShlbnRyeSk7XG4gICAgICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJjYXRjaFwiOiBmdW5jdGlvbih0cnlMb2MpIHtcbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSB0cnlMb2MpIHtcbiAgICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcbiAgICAgICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgICAgdmFyIHRocm93biA9IHJlY29yZC5hcmc7XG4gICAgICAgICAgICByZXNldFRyeUVudHJ5KGVudHJ5KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRocm93bjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBUaGUgY29udGV4dC5jYXRjaCBtZXRob2QgbXVzdCBvbmx5IGJlIGNhbGxlZCB3aXRoIGEgbG9jYXRpb25cbiAgICAgIC8vIGFyZ3VtZW50IHRoYXQgY29ycmVzcG9uZHMgdG8gYSBrbm93biBjYXRjaCBibG9jay5cbiAgICAgIHRocm93IG5ldyBFcnJvcihcImlsbGVnYWwgY2F0Y2ggYXR0ZW1wdFwiKTtcbiAgICB9LFxuXG4gICAgZGVsZWdhdGVZaWVsZDogZnVuY3Rpb24oaXRlcmFibGUsIHJlc3VsdE5hbWUsIG5leHRMb2MpIHtcbiAgICAgIHRoaXMuZGVsZWdhdGUgPSB7XG4gICAgICAgIGl0ZXJhdG9yOiB2YWx1ZXMoaXRlcmFibGUpLFxuICAgICAgICByZXN1bHROYW1lOiByZXN1bHROYW1lLFxuICAgICAgICBuZXh0TG9jOiBuZXh0TG9jXG4gICAgICB9O1xuXG4gICAgICBpZiAodGhpcy5tZXRob2QgPT09IFwibmV4dFwiKSB7XG4gICAgICAgIC8vIERlbGliZXJhdGVseSBmb3JnZXQgdGhlIGxhc3Qgc2VudCB2YWx1ZSBzbyB0aGF0IHdlIGRvbid0XG4gICAgICAgIC8vIGFjY2lkZW50YWxseSBwYXNzIGl0IG9uIHRvIHRoZSBkZWxlZ2F0ZS5cbiAgICAgICAgdGhpcy5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cbiAgfTtcblxuICAvLyBSZWdhcmRsZXNzIG9mIHdoZXRoZXIgdGhpcyBzY3JpcHQgaXMgZXhlY3V0aW5nIGFzIGEgQ29tbW9uSlMgbW9kdWxlXG4gIC8vIG9yIG5vdCwgcmV0dXJuIHRoZSBydW50aW1lIG9iamVjdCBzbyB0aGF0IHdlIGNhbiBkZWNsYXJlIHRoZSB2YXJpYWJsZVxuICAvLyByZWdlbmVyYXRvclJ1bnRpbWUgaW4gdGhlIG91dGVyIHNjb3BlLCB3aGljaCBhbGxvd3MgdGhpcyBtb2R1bGUgdG8gYmVcbiAgLy8gaW5qZWN0ZWQgZWFzaWx5IGJ5IGBiaW4vcmVnZW5lcmF0b3IgLS1pbmNsdWRlLXJ1bnRpbWUgc2NyaXB0LmpzYC5cbiAgcmV0dXJuIGV4cG9ydHM7XG5cbn0oXG4gIC8vIElmIHRoaXMgc2NyaXB0IGlzIGV4ZWN1dGluZyBhcyBhIENvbW1vbkpTIG1vZHVsZSwgdXNlIG1vZHVsZS5leHBvcnRzXG4gIC8vIGFzIHRoZSByZWdlbmVyYXRvclJ1bnRpbWUgbmFtZXNwYWNlLiBPdGhlcndpc2UgY3JlYXRlIGEgbmV3IGVtcHR5XG4gIC8vIG9iamVjdC4gRWl0aGVyIHdheSwgdGhlIHJlc3VsdGluZyBvYmplY3Qgd2lsbCBiZSB1c2VkIHRvIGluaXRpYWxpemVcbiAgLy8gdGhlIHJlZ2VuZXJhdG9yUnVudGltZSB2YXJpYWJsZSBhdCB0aGUgdG9wIG9mIHRoaXMgZmlsZS5cbiAgdHlwZW9mIG1vZHVsZSA9PT0gXCJvYmplY3RcIiA/IG1vZHVsZS5leHBvcnRzIDoge31cbikpO1xuXG50cnkge1xuICByZWdlbmVyYXRvclJ1bnRpbWUgPSBydW50aW1lO1xufSBjYXRjaCAoYWNjaWRlbnRhbFN0cmljdE1vZGUpIHtcbiAgLy8gVGhpcyBtb2R1bGUgc2hvdWxkIG5vdCBiZSBydW5uaW5nIGluIHN0cmljdCBtb2RlLCBzbyB0aGUgYWJvdmVcbiAgLy8gYXNzaWdubWVudCBzaG91bGQgYWx3YXlzIHdvcmsgdW5sZXNzIHNvbWV0aGluZyBpcyBtaXNjb25maWd1cmVkLiBKdXN0XG4gIC8vIGluIGNhc2UgcnVudGltZS5qcyBhY2NpZGVudGFsbHkgcnVucyBpbiBzdHJpY3QgbW9kZSwgd2UgY2FuIGVzY2FwZVxuICAvLyBzdHJpY3QgbW9kZSB1c2luZyBhIGdsb2JhbCBGdW5jdGlvbiBjYWxsLiBUaGlzIGNvdWxkIGNvbmNlaXZhYmx5IGZhaWxcbiAgLy8gaWYgYSBDb250ZW50IFNlY3VyaXR5IFBvbGljeSBmb3JiaWRzIHVzaW5nIEZ1bmN0aW9uLCBidXQgaW4gdGhhdCBjYXNlXG4gIC8vIHRoZSBwcm9wZXIgc29sdXRpb24gaXMgdG8gZml4IHRoZSBhY2NpZGVudGFsIHN0cmljdCBtb2RlIHByb2JsZW0uIElmXG4gIC8vIHlvdSd2ZSBtaXNjb25maWd1cmVkIHlvdXIgYnVuZGxlciB0byBmb3JjZSBzdHJpY3QgbW9kZSBhbmQgYXBwbGllZCBhXG4gIC8vIENTUCB0byBmb3JiaWQgRnVuY3Rpb24sIGFuZCB5b3UncmUgbm90IHdpbGxpbmcgdG8gZml4IGVpdGhlciBvZiB0aG9zZVxuICAvLyBwcm9ibGVtcywgcGxlYXNlIGRldGFpbCB5b3VyIHVuaXF1ZSBwcmVkaWNhbWVudCBpbiBhIEdpdEh1YiBpc3N1ZS5cbiAgRnVuY3Rpb24oXCJyXCIsIFwicmVnZW5lcmF0b3JSdW50aW1lID0gclwiKShydW50aW1lKTtcbn1cbiIsIiQoZG9jdW1lbnQpLnJlYWR5KCgpID0+IHtcclxuXHJcbiAgICAvLyBIb21lXHJcbiAgICAkKCcucGxhdGFmb3JtYS1jYXRlZ29yaWFzIC5jYXRlZ29yaWEtaGVhZGVyJykub24oJ2NsaWNrJywgUGxhdGFmb3JtYS5leHBhbmRpckNhdGVnb3JpYSk7XHJcbiAgICAkKCcuY2F0ZWdvcmlhLXZpZGVvJykub24oJ2NsaWNrJywgUGxhdGFmb3JtYS5hY2Vzc2FyVmlkZW8pO1xyXG4gICAgJCgnLnBsYXRhZm9ybWEtc2ltdWxhZG9zLWNhdGVnb3JpYXMgLmNhdGVnb3JpYS1oZWFkZXInKS5vbignY2xpY2snLCBQbGF0YWZvcm1hLmV4cGFuZGlyU2ltdWxhZG9DYXRlZ29yaWEpO1xyXG5cclxuICAgIHZhciBiYXIgPSBuZXcgUHJvZ3Jlc3NCYXIuTGluZSgnLnByb2dyZXNzby1iYXInLCB7XHJcbiAgICAgICAgc3Ryb2tlV2lkdGg6IDQsXHJcbiAgICAgICAgZWFzaW5nOiAnZWFzZUluT3V0JyxcclxuICAgICAgICBkdXJhdGlvbjogMTQwMCxcclxuICAgICAgICBjb2xvcjogJyNmZmVkMzcnLFxyXG4gICAgICAgIHRyYWlsQ29sb3I6ICdyZ2JhKDI1NSwgMjU1LCAyNTUsIC4zNSknLFxyXG4gICAgICAgIHRyYWlsV2lkdGg6IDQsXHJcbiAgICAgICAgc3ZnU3R5bGU6IHt3aWR0aDogJzEwMCUnLCBoZWlnaHQ6ICcxMDAlJ31cclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IHBvcmNlbnRhZ2VtID0gcGFyc2VGbG9hdCgkKCcucHJvZ3Jlc3NvLWJhcicpLmRhdGEoJ3BvcmNlbnRhZ2VtJykpO1xyXG4gICAgaWYgKHBvcmNlbnRhZ2VtKSB7XHJcbiAgICAgICAgYmFyLmFuaW1hdGUocG9yY2VudGFnZW0gLyAxMDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFbDrWRlb1xyXG4gICAgJCgnLnBsYXRhZm9ybWEtdmlkZW8tY2F0ZWdvcmlhJykub24oJ2NsaWNrJywgUGxhdGFmb3JtYS5leHBhbmRpckNhdGVnb3JpYVZpZGVvKTtcclxuXHJcbiAgICAvLyBBdmFsaWHDp8Ojb1xyXG4gICAgaWYgKCQoXCIjYXZhbGlhY2FvX2F1bGFcIikubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICQoJyNhdmFsaWFjYW9fYXVsYScpLmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICAgICAgcmF0aW5nOiAwLFxyXG4gICAgICAgICAgICAgICAgZnVsbFN0YXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzdGFyV2lkdGg6ICczMHB4JyxcclxuICAgICAgICAgICAgICAgIG9uU2V0OiBQbGF0YWZvcm1hLmF2YWxpYXJWaWRlbyxcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChlbGVtZW50LmRhdGFzZXQuYXZhbGlhY2FvLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgb3B0aW9ucy5yYXRpbmcgPSBwYXJzZUludChlbGVtZW50LmRhdGFzZXQuYXZhbGlhY2FvKTtcclxuICAgICAgICAgICAgICAgIG9wdGlvbnNbJ3JlYWRPbmx5J10gPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKGVsZW1lbnQpLnJhdGVZbyhvcHRpb25zKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBFeGliZSBhdWxhIGF0aXZhXHJcbiAgICBpZiAodHlwZW9mIGlkX2F1bGEgIT09IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICBQbGF0YWZvcm1hLmhhYmlsaXRhckF1bGFBY3RpdmUoaWRfYXVsYSk7XHJcbiAgICAgICAgUGxhdGFmb3JtYS5oYWJpbGl0YXJWaWRlb0FjdGl2ZSgpO1xyXG5cclxuICAgICAgICAvLyBWw61kZW8gKGlmcmFtZSlcclxuICAgICAgICAkKCcudmlkZW8tZW1iZWQgaWZyYW1lJykuaWZyYW1lVHJhY2tlcihQbGF0YWZvcm1hLnZpc3VhbGl6YXJWaWRlbyk7XHJcblxyXG4gICAgICAgICQoJy5wbGF0YWZvcm1hLXZpZGVvLW1lbnUtcm9sbCcpLm9uKCdjbGljaycsIGUgPT4ge1xyXG4gICAgICAgICAgICAkKCcucGxhdGFmb3JtYS12aWRlby1jb250ZW50JykudG9nZ2xlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiAkKCcucGxhdGFmb3JtYS12aWRlby1jb250ZW50IGknKS50b2dnbGVDbGFzcygnZmEtY2hldnJvbi1sZWZ0IGZhLWNoZXZyb24tcmlnaHQnKSwgNTAwKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gRG9jdW1lbnRvXHJcbiAgICAgICAgLy8gJCgnLmF1bGEtZG9jdW1lbnRvLWl0ZW0nKS5vbignY2xpY2snLCBQbGF0YWZvcm1hLmFjZXNzYXJEb2N1bWVudG8pO1xyXG5cclxuICAgICAgICBQbGF0YWZvcm1hLmhhYmlsaXRhck1lbnVNb2JpbGVMYXRlcmFsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FzbyBzZWphIGEgaG9tZVxyXG4gICAgaWYgKHR5cGVvZiBpc19ob21lICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgLy8gU2UgaG91dmVyIMO6bHRpbW8gdsOtZGVvIGFzc2lzdGlkbywgYWJyZSBjYXRlZ29yaWEgZGVzdGUgdsOtZGVvXHJcbiAgICAgICAgaWYgKHR5cGVvZiB1bHRpbW9fdmlkZW9fYXNzaXN0aWRvID09PSBcIm9iamVjdFwiICYmIHVsdGltb192aWRlb19hc3Npc3RpZG8gJiYgdWx0aW1vX3ZpZGVvX2Fzc2lzdGlkby5oYXNPd25Qcm9wZXJ0eShcImlkX2NhdGVnb3JpYVwiKSkge1xyXG4gICAgICAgICAgICAkKGAucGxhdGFmb3JtYS1jYXRlZ29yaWFbZGF0YS1pZD0ke3VsdGltb192aWRlb19hc3Npc3RpZG8uaWRfY2F0ZWdvcmlhfV1gKS5maW5kKCcuY2F0ZWdvcmlhLWhlYWRlcicpLmNsaWNrKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIENhc28gbsOjbyBoYWphLCBhYnJlIHByaW1laXJhIGNhdGVnb3JpYSBub3JtYWxtZW50ZVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAkKCcucGxhdGFmb3JtYS1jYXRlZ29yaWEnKS5maXJzdCgpLmZpbmQoJy5jYXRlZ29yaWEtaGVhZGVyJykuY2xpY2soKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRXhpYmUgcHJpbWVpcm8gbcOzZHVsbyBkb3Mgc2ltdWxhZG9zXHJcbiAgICBpZiAoJCgnLnBsYXRhZm9ybWEtY2FyZC1zaW11bGFkb3MnKS5sZW5ndGggJiYgISQoJy5wbGF0YWZvcm1hLWNhcmQtc2ltdWxhZG9zLnNlbS1jYXRlZ29yaWEgbGknKS5sZW5ndGgpIHtcclxuICAgICAgICAkKCcucGxhdGFmb3JtYS1zaW11bGFkby1jYXRlZ29yaWEnKS5maXJzdCgpLmZpbmQoJy5jYXRlZ29yaWEtaGVhZGVyJykuY2xpY2soKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBbHRlcmFyIGRhZG9zXHJcbiAgICAkKCcjZm9ybV9hbHRlcmFyX2RhZG9zJykub24oJ3N1Ym1pdCcsIHtcclxuICAgICAgICBzdWNjZXNzX2NhbGxiYWNrOiAocmVzcG9zdGEpID0+IHtcclxuICAgICAgICAgICAgJCgnI21vZGFsX2FsdGVyYXJfZGFkb3MnKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICBzd2FsKFwiU3VjZXNzbyFcIiwgcmVzcG9zdGEubXNnLCBcInN1Y2Nlc3NcIikudGhlbihhY3Rpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgfSwgTWFpbi5zdWJtZXRlRm9ybUZpbGUpO1xyXG5cclxuICAgIC8vIENvbXVuaWNhZG9zXHJcbiAgICAkKCcjZm9ybV9jb211bmljYWRvJykub24oJ3N1Ym1pdCcsIHtcclxuICAgICAgICBzdWNjZXNzX2NhbGxiYWNrOiAocmVzcG9zdGEpID0+IHtcclxuICAgICAgICAgICAgJCgnI21vZGFsX2NvbXVuaWNhZG8nKS5tb2RhbCgnaGlkZScpO1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgTWFpbi5zdWJtZXRlRm9ybUZpbGUpO1xyXG59KTtcclxuXHJcbiQod2luZG93KS5vbignc2Nyb2xsJywgZSA9PiB7XHJcbiAgICBjb25zdCBzY3JvbGwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgY29uc3QgZWxlbWVudCA9ICQoJy5wbGF0YWZvcm1hLW5hdmVnYWNhbycpLm91dGVySGVpZ2h0KCk7XHJcblxyXG4gICAgaWYgKHNjcm9sbCA+PSBlbGVtZW50KVxyXG4gICAgICAgICQoJy5wbGF0YWZvcm1hLXZpZGVvLW1lbnUnKS5hZGRDbGFzcygnZml4ZWQnKTtcclxuICAgIGVsc2VcclxuICAgICAgICAkKCcucGxhdGFmb3JtYS12aWRlby1tZW51JykucmVtb3ZlQ2xhc3MoJ2ZpeGVkJyk7XHJcbn0pO1xyXG5cclxuJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBlID0+IHtcclxuICAgIFBsYXRhZm9ybWEuaGFiaWxpdGFyTWVudU1vYmlsZUxhdGVyYWwoKTtcclxufSk7XHJcblxyXG5jbGFzcyBQbGF0YWZvcm1hIHtcclxuXHJcbiAgICAvKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xyXG4gICAgLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBIYW5kbGVycyAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cclxuICAgIC8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcblxyXG4gICAgc3RhdGljIGV4cGFuZGlyQ2F0ZWdvcmlhIChldmVudCkge1xyXG4gICAgICAgIHZhciAkY2F0ZWdvcmlhID0gJChldmVudC50YXJnZXQpLmNsb3Nlc3QoJy5wbGF0YWZvcm1hLWNhdGVnb3JpYScpO1xyXG5cclxuICAgICAgICBpZiAoJGNhdGVnb3JpYS5oYXNDbGFzcygnc2hvdycpKSB7XHJcbiAgICAgICAgICAgICRjYXRlZ29yaWEuZmluZCgnLmNhdGVnb3JpYS1ib2R5Jykuc2xpZGVUb2dnbGUoZWxlbWVudG8gPT4gJGNhdGVnb3JpYS50b2dnbGVDbGFzcygnc2hvdycpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICRjYXRlZ29yaWEudG9nZ2xlQ2xhc3MoJ3Nob3cnKTtcclxuICAgICAgICAgICAgJGNhdGVnb3JpYS5maW5kKCcuY2F0ZWdvcmlhLWJvZHknKS5zbGlkZVRvZ2dsZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZXhwYW5kaXJTaW11bGFkb0NhdGVnb3JpYSAoZXZlbnQpIHtcclxuICAgICAgICB2YXIgJGNhdGVnb3JpYSA9ICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KCcucGxhdGFmb3JtYS1zaW11bGFkby1jYXRlZ29yaWEnKTtcclxuXHJcbiAgICAgICAgaWYgKCRjYXRlZ29yaWEuaGFzQ2xhc3MoJ3Nob3cnKSkge1xyXG4gICAgICAgICAgICAkY2F0ZWdvcmlhLmZpbmQoJy5jYXRlZ29yaWEtYm9keScpLnNsaWRlVG9nZ2xlKGVsZW1lbnRvID0+ICRjYXRlZ29yaWEudG9nZ2xlQ2xhc3MoJ3Nob3cnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAkY2F0ZWdvcmlhLnRvZ2dsZUNsYXNzKCdzaG93Jyk7XHJcbiAgICAgICAgICAgICRjYXRlZ29yaWEuZmluZCgnLmNhdGVnb3JpYS1ib2R5Jykuc2xpZGVUb2dnbGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGFjZXNzYXJWaWRlbyAoZXZlbnQpIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgaWYgKCEoJ2lkJyBpbiB0aGlzLmRhdGFzZXQpKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgIGNvbnN0IHsgaWQgfSA9IHRoaXMuZGF0YXNldDtcclxuXHJcbiAgICAgICAgaWYgKCQodGhpcykuaGFzQ2xhc3MoJ3ZpZGVvLW5hby1saWJlcmFkbycpKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgIC8vIEFjZXNzYSBkb2N1bWVudG9cclxuICAgICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcygnZG9jdW1lbnRvJykpIHtcclxuICAgICAgICAgICAgUGxhdGFmb3JtYS5hY2Vzc2FyRG9jdW1lbnRvKGlkKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBgYXVsYS5waHA/aWQ9JHtpZH1gO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBhY2Vzc2FyRG9jdW1lbnRvICh2YWx1ZSkge1xyXG4gICAgICAgIGlmIChpc05hTih2YWx1ZSkpIHtcclxuICAgICAgICAgICAgdmFsdWUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoISgnaWQnIGluIHRoaXMuZGF0YXNldCkpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB7IGlkIH0gPSB0aGlzLmRhdGFzZXQ7XHJcbiAgICAgICAgICAgIHZhbHVlID0gaWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB3aW5kb3cub3BlbihgZG9jdW1lbnRvLnBocD9pZD0ke3ZhbHVlfWApO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBleHBhbmRpckNhdGVnb3JpYVZpZGVvIChldmVudCkge1xyXG4gICAgICAgIHZhciAkY2F0ZWdvcmlhID0gJChldmVudC50YXJnZXQpLmNsb3Nlc3QoJy5wbGF0YWZvcm1hLXZpZGVvLWNhdGVnb3JpYScpO1xyXG5cclxuICAgICAgICBpZiAoJGNhdGVnb3JpYS5oYXNDbGFzcygnc2hvdycpKSB7XHJcbiAgICAgICAgICAgICRjYXRlZ29yaWEuZmluZCgnLmNhdGVnb3JpYS1ib2R5Jykuc2xpZGVUb2dnbGUoZWxlbWVudG8gPT4gJGNhdGVnb3JpYS50b2dnbGVDbGFzcygnc2hvdycpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICRjYXRlZ29yaWEudG9nZ2xlQ2xhc3MoJ3Nob3cnKTtcclxuICAgICAgICAgICAgJGNhdGVnb3JpYS5maW5kKCcuY2F0ZWdvcmlhLWJvZHknKS5zbGlkZVRvZ2dsZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFiaWxpdGFyQXVsYUFjdGl2ZSAoaWRfYXVsYSkge1xyXG4gICAgICAgICQoJy5jYXRlZ29yaWEtdmlkZW8nKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblxyXG4gICAgICAgICQoYC5jYXRlZ29yaWEtdmlkZW9bZGF0YS1pZD0ke2lkX2F1bGF9XTpub3QoLmRvY3VtZW50bylgKVxyXG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgIC5jbG9zZXN0KCcucGxhdGFmb3JtYS12aWRlby1jYXRlZ29yaWEnKVxyXG4gICAgICAgICAgICAuY2xpY2soKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlU2Nyb2xsIChldmVudCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCc+PicgKyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFiaWxpdGFyTWVudU1vYmlsZUxhdGVyYWwgKCkge1xyXG4gICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDk5MSAmJiAhJCgnLnBsYXRhZm9ybWEtdmlkZW8tY29udGVudCcpLmhhc0NsYXNzKCdoaWRlJykpIHtcclxuICAgICAgICAgICAgJCgnLnBsYXRhZm9ybWEtdmlkZW8tbWVudS1yb2xsJykuY2xpY2soKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cclxuICAgIC8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIEVtYmVkICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcbiAgICAvKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xyXG5cclxuICAgIHN0YXRpYyBwYXJzZVZpZGVvICh1cmwpIHtcclxuICAgICAgICAvKlxyXG4gICAgICAgICogLSBTdXBwb3J0ZWQgWW91VHViZSBVUkwgZm9ybWF0czpcclxuICAgICAgICAqICAgLSBodHRwOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9TXkyRlJQQTNHZjhcclxuICAgICAgICAqICAgLSBodHRwOi8veW91dHUuYmUvTXkyRlJQQTNHZjhcclxuICAgICAgICAqICAgLSBodHRwczovL3lvdXR1YmUuZ29vZ2xlYXBpcy5jb20vdi9NeTJGUlBBM0dmOFxyXG4gICAgICAgICogLSBTdXBwb3J0ZWQgVmltZW8gVVJMIGZvcm1hdHM6XHJcbiAgICAgICAgKiAgIC0gaHR0cDovL3ZpbWVvLmNvbS8yNTQ1MTU1MVxyXG4gICAgICAgICogICAtIGh0dHA6Ly9wbGF5ZXIudmltZW8uY29tL3ZpZGVvLzI1NDUxNTUxXHJcbiAgICAgICAgKiAtIEFsc28gc3VwcG9ydHMgcmVsYXRpdmUgVVJMczpcclxuICAgICAgICAqICAgLSAvL3BsYXllci52aW1lby5jb20vdmlkZW8vMjU0NTE1NTFcclxuICAgICAgICAqL1xyXG4gICAgXHJcbiAgICAgICAgdXJsLm1hdGNoKC8oaHR0cDp8aHR0cHM6fClcXC9cXC8ocGxheWVyLnx3d3cuKT8odmltZW9cXC5jb218eW91dHUoYmVcXC5jb218XFwuYmV8YmVcXC5nb29nbGVhcGlzXFwuY29tKSlcXC8odmlkZW9cXC98ZW1iZWRcXC98d2F0Y2hcXD92PXx2XFwvKT8oW0EtWmEtejAtOS5fJS1dKikoXFwmXFxTKyk/Lyk7XHJcbiAgICBcclxuICAgICAgICBpZiAoUmVnRXhwLiQzLmluZGV4T2YoJ3lvdXR1JykgPiAtMSkge1xyXG4gICAgICAgICAgICB2YXIgdHlwZSA9ICd5b3V0dWJlJztcclxuICAgICAgICB9IGVsc2UgaWYgKFJlZ0V4cC4kMy5pbmRleE9mKCd2aW1lbycpID4gLTEpIHtcclxuICAgICAgICAgICAgdmFyIHR5cGUgPSAndmltZW8nO1xyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHR5cGUsXHJcbiAgICAgICAgICAgIGlkOiBSZWdFeHAuJDZcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXRFbWJlZFVSTCAodXJsKSB7XHJcbiAgICAgICAgY29uc3QgZW1iZWRzID0ge1xyXG4gICAgICAgICAgICB5b3V0dWJlOiAnaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvJyxcclxuICAgICAgICAgICAgdmltZW86ICdodHRwczovL3BsYXllci52aW1lby5jb20vdmlkZW8vJyxcclxuICAgICAgICB9O1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnN0IG9iaiA9IFBsYXRhZm9ybWEucGFyc2VWaWRlbyh1cmwpO1xyXG5cclxuICAgICAgICByZXR1cm4gZW1iZWRzW29iai50eXBlXSArIG9iai5pZDtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFiaWxpdGFyVmlkZW9BY3RpdmUgKCkge1xyXG4gICAgICAgIGNvbnN0ICRpZnJhbWUgPSAkKCcjaWZyYW1lX3ZpZGVvJyk7XHJcbiAgICAgICAgY29uc3QgdXJsID0gJGlmcmFtZS5kYXRhKCd1cmwnKTtcclxuICAgICAgICAkaWZyYW1lLmF0dHIoJ3NyYycsIFBsYXRhZm9ybWEuZ2V0RW1iZWRVUkwodXJsKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cclxuICAgIC8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICBWaXN1YWxpemHDp8OjbyAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cclxuICAgIC8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcblxyXG4gICAgc3RhdGljIGFzeW5jIHZpc3VhbGl6YXJWaWRlbyAoZXZlbnQpIHtcclxuICAgICAgICBpZiAodmlkZW9fdmlzdWFsaXphZG8pXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgcmVzcG9zdGEgPSBhd2FpdCAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgdXJsOiAnYWpheC92aWRlb3MucGhwP2FjYW89dmlzdWFsaXphcl92aWRlbycsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7IGlkX3ZpZGVvOiBpZF9hdWxhIH0sXHJcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ0pTT04nXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKHJlc3Bvc3RhLnN0YXR1cyAhPT0gJ3N1Y2Nlc3MnKSB7XHJcbiAgICAgICAgICAgICAgICBzd2FsKCdPY29ycmV1IHVtIGVycm8hJywgcmVzcG9zdGEubWVuc2FnZW1fZXJybywgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZpZGVvX3Zpc3VhbGl6YWRvID0gdHJ1ZTtcclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnRXJyb3IhJywgZXJyb3IpO1xyXG4gICAgICAgICAgICBzd2FsKCdPcHMhJywgJ09jb3JyZXUgdW0gZXJybyEgUG9yIGZhdm9yLCBhdHVhbGl6ZSBhIHDDoWdpbmEgZSB0ZW50ZSBub3ZhbWVudGUuJywgJ2Vycm9yJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBhc3luYyBhdmFsaWFyVmlkZW8gKHJhdGluZywgcmF0ZVlvSW5zdGFuY2UpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCByZXNwb3N0YSA9IGF3YWl0ICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6ICdhamF4L3ZpZGVvcy5waHA/YWNhbz1hdmFsaWFyX3ZpZGVvJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHsgaWRfdmlkZW86IGlkX2F1bGEsIGF2YWxpYWNhbzogcmF0aW5nIH0sXHJcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ0pTT04nLFxyXG4gICAgICAgICAgICAgICAgYmVmb3JlU2VuZDogKCkgPT4gTWFpbi5hYnJpcl9vdmVybGF5X2xvYWRpbmcoKVxyXG4gICAgICAgICAgICB9KS5hbHdheXMoKCkgPT4gTWFpbi5mZWNoYXJfb3ZlcmxheV9sb2FkaW5nKCkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHJlc3Bvc3RhLnN0YXR1cyAhPT0gJ3N1Y2Nlc3MnKSB7XHJcbiAgICAgICAgICAgICAgICBzd2FsKCdPY29ycmV1IHVtIGVycm8hJywgcmVzcG9zdGEubWVuc2FnZW1fZXJybywgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICQoJyNhdmFsaWFjYW9fYXVsYScpLnJhdGVZbygnb3B0aW9uJywgJ3JlYWRPbmx5JywgdHJ1ZSk7XHJcbiAgICAgICAgICAgICQoJyNhdmFsaWFjYW9fYXVsYScpLmF0dHIoJ3RpdGxlJywgYEF2YWxpYcOnw6NvIGRlICR7cmF0aW5nfSAke3JhdGluZyA9PSAxID8gJ2VzdHJlbGEnIDogJ2VzdHJlbGFzJ30gZmVpdGEgZW0gJHttb21lbnQoKS5mb3JtYXQoXCJERC9NTS9ZWVlZIFvDoHNdIEhIOm1tXCIpfWApO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFcnJvciEnLCBlcnJvcik7XHJcbiAgICAgICAgICAgIHN3YWwoJ09wcyEnLCAnT2NvcnJldSB1bSBlcnJvISBQb3IgZmF2b3IsIGF0dWFsaXplIGEgcMOhZ2luYSBlIHRlbnRlIG5vdmFtZW50ZS4nLCAnZXJyb3InKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbndpbmRvdy5QbGF0YWZvcm1hID0gUGxhdGFmb3JtYTsiXX0=

//# sourceMappingURL=plataforma.js.map
