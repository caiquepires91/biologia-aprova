(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;
},{}],2:[function(require,module,exports){
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;
},{}],3:[function(require,module,exports){
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;
},{}],4:[function(require,module,exports){
"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

$(document).ready(function () {
  // Autenticação
  $('#form_login').on('submit', {
    success_callback: function success_callback(resposta) {
      window.location.href = "plataforma/";
    }
  }, Main.submeteFormFile); // Recuperar senha

  $('#form_recuperar_senha').on('submit', {
    success_callback: function success_callback(resposta) {
      swal("Sucesso!", resposta.msg, "success").then(function (action) {
        $('#modal_recuperar_senha').modal('hide');
      });
    }
  }, Main.submeteFormFile); // Redefinir senha

  $('#form_redefinir_senha').on('submit', {
    success_callback: function success_callback(resposta) {
      swal("Sucesso!", resposta.msg, "success").then(function (action) {
        window.location.href = "/";
      });
    }
  }, Main.submeteFormFile);
});

var Login =
/*#__PURE__*/
function () {
  function Login() {
    (0, _classCallCheck2["default"])(this, Login);
  }

  (0, _createClass2["default"])(Login, null, [{
    key: "recuperarSenha",

    /* ========================================================================== */

    /* ==============================  Handlers  ================================ */

    /* ========================================================================== */
    value: function recuperarSenha() {
      $('#modal_login').modal('hide');
      $('#modal_recuperar_senha').modal('show');
    }
  }]);
  return Login;
}();

window.Login = Login;

},{"@babel/runtime/helpers/classCallCheck":1,"@babel/runtime/helpers/createClass":2,"@babel/runtime/helpers/interopRequireDefault":3}]},{},[4])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIm5vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwibm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvaW50ZXJvcFJlcXVpcmVEZWZhdWx0LmpzIiwic3JjL2pzL2xvZ2luLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUNOQSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVksS0FBWixDQUFrQixZQUFNO0FBQ3BCO0FBQ0EsRUFBQSxDQUFDLENBQUMsYUFBRCxDQUFELENBQWlCLEVBQWpCLENBQW9CLFFBQXBCLEVBQThCO0FBQzFCLElBQUEsZ0JBQWdCLEVBQUUsMEJBQUMsUUFBRCxFQUFjO0FBQzVCLE1BQUEsTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsSUFBaEI7QUFDSDtBQUh5QixHQUE5QixFQUlHLElBQUksQ0FBQyxlQUpSLEVBRm9CLENBUXBCOztBQUNBLEVBQUEsQ0FBQyxDQUFDLHVCQUFELENBQUQsQ0FBMkIsRUFBM0IsQ0FBOEIsUUFBOUIsRUFBd0M7QUFDcEMsSUFBQSxnQkFBZ0IsRUFBRSwwQkFBQyxRQUFELEVBQWM7QUFDNUIsTUFBQSxJQUFJLENBQUMsVUFBRCxFQUFhLFFBQVEsQ0FBQyxHQUF0QixFQUEyQixTQUEzQixDQUFKLENBQTBDLElBQTFDLENBQStDLFVBQUEsTUFBTSxFQUFJO0FBQ3JELFFBQUEsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEIsS0FBNUIsQ0FBa0MsTUFBbEM7QUFDSCxPQUZEO0FBR0g7QUFMbUMsR0FBeEMsRUFNRyxJQUFJLENBQUMsZUFOUixFQVRvQixDQWlCcEI7O0FBQ0EsRUFBQSxDQUFDLENBQUMsdUJBQUQsQ0FBRCxDQUEyQixFQUEzQixDQUE4QixRQUE5QixFQUF3QztBQUNwQyxJQUFBLGdCQUFnQixFQUFFLDBCQUFDLFFBQUQsRUFBYztBQUM1QixNQUFBLElBQUksQ0FBQyxVQUFELEVBQWEsUUFBUSxDQUFDLEdBQXRCLEVBQTJCLFNBQTNCLENBQUosQ0FBMEMsSUFBMUMsQ0FBK0MsVUFBQSxNQUFNLEVBQUk7QUFDckQsUUFBQSxNQUFNLENBQUMsUUFBUCxDQUFnQixJQUFoQixHQUF1QixHQUF2QjtBQUNILE9BRkQ7QUFHSDtBQUxtQyxHQUF4QyxFQU1HLElBQUksQ0FBQyxlQU5SO0FBT0gsQ0F6QkQ7O0lBMkJNLEs7Ozs7Ozs7Ozs7QUFDRjs7QUFDQTs7QUFDQTtxQ0FFeUI7QUFDckIsTUFBQSxDQUFDLENBQUMsY0FBRCxDQUFELENBQWtCLEtBQWxCLENBQXdCLE1BQXhCO0FBQ0EsTUFBQSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QixLQUE1QixDQUFrQyxNQUFsQztBQUNIOzs7OztBQUdMLE1BQU0sQ0FBQyxLQUFQLEdBQWUsS0FBZiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsImZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2NsYXNzQ2FsbENoZWNrOyIsImZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlO1xuICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gIGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xuICBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gIHJldHVybiBDb25zdHJ1Y3Rvcjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfY3JlYXRlQ2xhc3M7IiwiZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHtcbiAgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHtcbiAgICBcImRlZmF1bHRcIjogb2JqXG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdDsiLCIkKGRvY3VtZW50KS5yZWFkeSgoKSA9PiB7XHJcbiAgICAvLyBBdXRlbnRpY2HDp8Ojb1xyXG4gICAgJCgnI2Zvcm1fbG9naW4nKS5vbignc3VibWl0Jywge1xyXG4gICAgICAgIHN1Y2Nlc3NfY2FsbGJhY2s6IChyZXNwb3N0YSkgPT4ge1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGBwbGF0YWZvcm1hL2A7XHJcbiAgICAgICAgfSxcclxuICAgIH0sIE1haW4uc3VibWV0ZUZvcm1GaWxlKTtcclxuXHJcbiAgICAvLyBSZWN1cGVyYXIgc2VuaGFcclxuICAgICQoJyNmb3JtX3JlY3VwZXJhcl9zZW5oYScpLm9uKCdzdWJtaXQnLCB7XHJcbiAgICAgICAgc3VjY2Vzc19jYWxsYmFjazogKHJlc3Bvc3RhKSA9PiB7XHJcbiAgICAgICAgICAgIHN3YWwoXCJTdWNlc3NvIVwiLCByZXNwb3N0YS5tc2csIFwic3VjY2Vzc1wiKS50aGVuKGFjdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAkKCcjbW9kYWxfcmVjdXBlcmFyX3NlbmhhJykubW9kYWwoJ2hpZGUnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgIH0sIE1haW4uc3VibWV0ZUZvcm1GaWxlKTtcclxuXHJcbiAgICAvLyBSZWRlZmluaXIgc2VuaGFcclxuICAgICQoJyNmb3JtX3JlZGVmaW5pcl9zZW5oYScpLm9uKCdzdWJtaXQnLCB7XHJcbiAgICAgICAgc3VjY2Vzc19jYWxsYmFjazogKHJlc3Bvc3RhKSA9PiB7XHJcbiAgICAgICAgICAgIHN3YWwoXCJTdWNlc3NvIVwiLCByZXNwb3N0YS5tc2csIFwic3VjY2Vzc1wiKS50aGVuKGFjdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IFwiL1wiO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgfSwgTWFpbi5zdWJtZXRlRm9ybUZpbGUpO1xyXG59KTtcclxuXHJcbmNsYXNzIExvZ2luIHtcclxuICAgIC8qID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09ICovXHJcbiAgICAvKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gIEhhbmRsZXJzICA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xyXG4gICAgLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cclxuXHJcbiAgICBzdGF0aWMgcmVjdXBlcmFyU2VuaGEgKCkge1xyXG4gICAgICAgICQoJyNtb2RhbF9sb2dpbicpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgJCgnI21vZGFsX3JlY3VwZXJhcl9zZW5oYScpLm1vZGFsKCdzaG93Jyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbndpbmRvdy5Mb2dpbiA9IExvZ2luOyJdfQ==

//# sourceMappingURL=login.js.map
