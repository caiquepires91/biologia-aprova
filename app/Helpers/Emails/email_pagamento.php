<?php

namespace App\Helpers\Emails\Pagamento;

$email_conteudo ="<style>img {display:block}</style>";
$email_conteudo .= "<body style='margin: 0px'>";
$email_conteudo .= "<table valign='top' cellpadding='0' cellmarging='0' cellspacing='0' border='0' width='100%' bgcolor='' align='center' style='border-collapse: collapse; padding: 0px; margin: 0px;'><tr style='margin: 0px;'>";
$email_conteudo .= "<td valign='top' style='border-collapse: collapse;'><table cellpadding='0' cellspacing='0' border='0' width='600' align='center' style='border-collapse: collapse; font-family: Verdana; font-size: 12px;line-height:130%'>";
$email_conteudo .= "<tr><td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse; width: 72%' >";
$email_conteudo .= "<img src='http://www.biologiaaprova.com.br/img/logo_100x100.png' height='80' style='margin-left: 20px;'/>";
$email_conteudo .= "</td><td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>";
$email_conteudo .= "<a href='https://twitter.com/biologiaaprova'><img src='http://www.biologiaaprova.com.br/img/ico_twitter_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Twitter' /></a>";
$email_conteudo .= "</td><td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>";
$email_conteudo .= "<a href='https://www.youtube.com/channel/UCuolE1BPgaa1DWBRh7ttLGQ' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_you_tube_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso canal no YouTube' /></a>";
$email_conteudo .= "</td><td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>";
$email_conteudo .= "<a href='https://www.instagram.com/biologiaaprova/' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_instagram_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Instagram' /></a>";
$email_conteudo .= "</td><td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>";
$email_conteudo .= "<a href='https://www.facebook.com/biologiaaprova/' style='margin-right: 10px;'><img src='http://www.biologiaaprova.com.br/img/ico_facebook_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Facebook' /></a>";
$email_conteudo .= "</td></tr><tr><td valign='top' bgcolor='#FFFFFF' style='color: #333333; border-collapse: collapse;' colspan='10'>";
$email_conteudo .= "<hr style='width: 90%; height: 6px; border: none; background: #29b372; float: left; margin: 10px 5%; '>
							<h3 style='text-align: center; color:#064976; font-size: 17px; font-weight: bold; margin-top:60px;'>PAGAMENTO APROVADO</h3>
							<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'>
							Ol&aacute;, <b> $nome</b>!<br><br>

							Informamos que seu pagamento foi confirmado. A partir de agora voc&ecirc; j&aacute; tem acesso a mais completa plataforma de biologia.
							
							<h4 style='text-align: left; color:#29b372; font-weight: bold;  padding: 0px 80px;'>Informa&ccedil;&otilde;es sobre sua conta:</br></h4>
							<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'><b>Nome completo:</b> $nome </br>
							<b>Plano:</b> $plano</br>
							<b>Dura&ccedil;&atilde;o:</b> $duracao</br>
							<b>Per&iacute;odo de vig&ecirc;ncia:</b> $inicio &agrave; $fim</br>
							Caso deseje alterar seu plano, fa&ccedil;a seu login na plataforma: <br>
							Acesse: <font color='#999999'>Alterar plano.</font>
							</p>
							<h4 style='text-align: left; color:#29b372; font-weight: bold;  padding: 0px 80px;'>O que encontrarei na plataforma?</h4>							
							<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'> <ul style='text-align: justify;  padding: 0px 100px;'><li> videoaulas explicativas in&eacute;ditas e claras;</li>
								<li>resolu&ccedil;&otilde;es de quest&otilde;es gravadas;</li>
								<li>atividades com gabarito e coment&aacute;rio;</li>
								<li>material de apoio com conte&uacute;do e exerc&iacute;cios em formato de livro virtual, dispon&iacute;vel para <i>download</i>;</li>
								<li>simulados customiz&aacute;veis;</li>
								<li>tira-d&uacute;vidas com a professora Mary Ann e muito mais.</li>
							</ul>
							</p>
							<h4 style='text-align: center; color:#064976; font-weight: bold;  padding: 0px 80px;'>Esse curso &eacute; tudo que voc&ecirc; deseja para ter sucesso no ENEM, vestibular ou concurso.<br/><br/>
								Esperamos que aproveite ao m&aacute;ximo o que a nossa plataforma tem a oferecer e que tenha um &oacute;timo aprendizado.<br/><br/>BONS ESTUDOS!</h4>													
						</td>
					</tr>			
					<tr>
						<td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse;' height='22' colspan='10'>
							<hr style='width: 90%; height: 6px; border: none; background: #29b372; float: left; margin: 10px 5%; margin-bottom: 0px; '>
						</td>
					</tr>			
					<tr>
						<td valign='top' height='50' style='padding-top: 0px; border-collapse: collapse;' colspan='10'>
							<p style=' line-height: 110%; color: #999999; padding: 5px 80px; margin: 0px; font-size: 10px;'>N&atilde;o responda a esta mensagem. Este e-mail foi enviado por um sistema autom&aacute;tico que n&atilde;o processa respostas.</br>
							Para esclarecer d&uacute;vidas ou enviar sugest&otilde;es, acesse nossa Central de relacionamento, escreva para contato@biologiaaprova.com.br<br><br>
							<a href='http://biologiaaprova.com.br/politica-privacidade.php' target='blank' style='color: #999999; text-decoration: none;'>Pol&iacute;tica de Privacidade</a></br>
	 						<a href='http://biologiaaprova.com.br/termos-uso.php' target='blank' style='color: #999999; text-decoration: none;'>Contrato</a></br></br>
							Copyright &copy; ".date('Y')." Biologia Aprova. Todos os direitos reservados.
							</p>
							
						</td>
					</tr>						
				</table>
			</td>
		</tr>
		</table>
</body>";

$email_conteudo_gmail = "<style>img {display:block}</style>;
					<body style='margin: 0px'>
					<table valign='top' cellpadding='0' cellmarging='0' cellspacing='0' border='0' width='100%' bgcolor='' align='center' style='border-collapse: collapse; padding: 0px; margin: 0px;'>
					<tr style='margin: 0px;'>
						<td valign='top' style='border-collapse: collapse;' height='944'>
						<table cellpadding='0' cellspacing='0' border='0' width='600' align='center' style='border-collapse: collapse; font-family: Verdana; font-size: 12px;line-height:130%'>
							<tr>
								<td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse; width: 72%' >
								<img src='http://www.biologiaaprova.com.br/img/logo_100x100.png' height='80' style='margin-left: 20px;'/></td>
								<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
									<a href='https://twitter.com/biologiaaprova'><img src='http://www.biologiaaprova.com.br/img/ico_twitter_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Twitter' /></a>
								</td>
								<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'><a href='https://www.youtube.com/channel/UCuolE1BPgaa1DWBRh7ttLGQ' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_you_tube_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso canal no YouTube' /></a>
								</td>
								<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
									<a href='https://www.instagram.com/biologiaaprova/' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_instagram_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Instagram' /></a>
								</td>
								<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
									<a href='https://www.facebook.com/biologiaaprova/' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_facebook_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Facebook' /></a>						
								</td>
							</tr>
							<tr>
								<td valign='top' bgcolor='#FFFFFF' style='color: #333333; border-collapse: collapse;' colspan='10'>
									<hr style='width: 90%; height: 6px; border: none; background: #29b372; float: left; margin: 10px 5%; '>
									<h3 style='text-align: center; color:#064976; font-size: 17px; font-weight: bold; margin-top:60px;' >PAGAMENTO APROVADO</h3>
									<p style='text-align: justify;  padding: 0px 80px;'>
									Ol&aacute;, <b> $nome</b>!<br><br>

									Informamos que seu pagamento foi confirmado. A partir de agora voc&ecirc; j&aacute; tem acesso a mais completa plataforma de biologia.
							
									<h4 style='text-align: left; color:#29b372; font-weight: bold;  padding: 0px 80px;'>Informa&ccedil;&otilde;es sobre sua conta:</br></h4>
									<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'><b>Nome completo:</b> $nome </br>
									<b>Plano:</b> $plano</br>
									<b>Dura&ccedil;&atilde;o:</b> $duracao</br>
									<b>Per&iacute;odo de vig&ecirc;ncia:</b> $inicio &agrave; $fim</br>
									</br>
									Caso deseje alterar seu plano, fa&ccedil;a seu login na plataforma: <br>
									Acesse: <font color='#999999'>Alterar plano.</font>
									</p>
									<h4 style='text-align: left; color:#29b372; font-weight: bold;  padding: 0px 80px;'>O que encontrarei na plataforma?</h4>							
									<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'> <ul style='text-align: justify;  padding: 0px 100px;'><li> videoaulas explicativas in&eacute;ditas e claras;</li>
										<li>resolu&ccedil;&otilde;es de quest&otilde;es gravadas;</li>
										<li>atividades com gabarito e coment&aacute;rio;</li>
										<li>material de apoio com conte&uacute;do e exerc&iacute;cios em formato de livro virtual, dispon&iacute;vel para <i>download</i>;</li>
										<li>simulados customiz&aacute;veis;</li>
										<li>tira-d&uacute;vidas com a professora Mary Ann e muito mais.</li>
									</ul>
									</p>
									<h4 style='text-align: center; color:#064976; font-weight: bold;  padding: 0px 80px;'>Esse curso &eacute; tudo que voc&ecirc; deseja para ter sucesso no ENEM, vestibular ou concurso.<br/><br/>
										Esperamos que aproveite ao m&aacute;ximo o que a nossa plataforma tem a oferecer e que tenha um &oacute;timo aprendizado.<br/><br/>BONS ESTUDOS!</h4>
								</td>
							</tr>			
						<tr>
						<td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse;' height='22' colspan='10'>
							<hr style='width: 90%; height: 6px; border: none; background: #29b372; float: left; margin: 10px 5%; margin-bottom: 0px; '>
						</td>
					</tr>			
					<tr>
						<td valign='top' height='50' style='padding-top: 0px; border-collapse: collapse;' colspan='10'>
							<p style=' line-height: 110%; color: #999999; padding: 5px 60px; margin: 0px; font-size: 10px;'>N&atilde;o responda a esta mensagem. Este e-mail foi enviado por um sistema autom&aacute;tico que n&atilde;o processa respostas.</br>
							Para esclarecer d&uacute;vidas ou enviar sugest&otilde;es, acesse nossa Central de relacionamento, escreva para contato@biologiaaprova.com.br<br><br>
							<a href='http://biologiaaprova.com.br/politicas_privacidade.php' target='blank' style='color: #999999; text-decoration: none;'>Pol&iacute;tica de Privacidade</a></br>
	 						<a href='http://biologiaaprova.com.br/termos_uso.php' target='blank' style='color: #999999; text-decoration: none;'>Contrato</a></br></br>
							Copyright &copy; ".date('Y')." Biologia Aprova. Todos os direitos reservados.
							</p>
							
						</td>
					</tr>						
				</table>
			</td>
		</tr>
		</table>
</body>";

?>