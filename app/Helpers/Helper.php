<?php

namespace App\Helpers;

class Helper {
    public static function getSession(Request $request) {
        return $request->session()->getSession($request->input("userId"), "default");
    }

    public static function storeSession($key, $value)
    {
        session([$key => $value]);
        return session($key, "default");
    }
}