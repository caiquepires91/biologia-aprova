<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\PlanoAtivo;

class AguardandoPagamento extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $tempo;
    public $plano;
    public $inicio;
    public $fim;
    public $sender;
    public $receiver;
    public $subject;
    public $header;
    public $view;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $to, $subject, $nome, $tempo, $plano, $inicio, $fim, $view)
    {
        $this->nome = $nome;
        $this->tempo = $tempo;
        $this->plano = $plano;
        $this->inicio = $inicio;
        $this->fim = $fim;
        $this->sender = $from;
        $this->receiver = $to;
        $this->subject = $subject;
        $this->view = $view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from_user = "=?UTF-8?B?".base64_encode("Biologia Aprova")."?=";
        $subject = "=?UTF-8?B?".base64_encode($this->subject)."?=";

        $this->header = "From: $from_user <no-reply@biologiaaprova.com.br>\r\n".  
        "MIME-Version: 1.0" . "\r\n" . 
        "Content-type: text/html; charset=UTF-8" . "\r\n";

        $this->withSwiftMessage(function ($message) {
            $message->getHeaders()->addTextHeader($this->header);
        });

         return $this->from($this->sender, "Biologia Aprova")->view($this->view)->with([
            'nome' => $this->nome,
            'plano'=> $this->plano,
            'duracao' =>  $this->tempo,
            'inicio' => $this->inicio,
            'fim' => $this->fim
        ]);
    }
}
