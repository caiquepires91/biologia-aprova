<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioSimuladoAbertoResposta extends Model
{
    protected $table = "usuarios_simulado_aberto_resultados";
}
