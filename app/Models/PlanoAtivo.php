<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanoAtivo extends Model
{
    protected $table = 'ativo';
}
