<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioSimuladoAberto extends Model
{
    protected $table = "usuarios_simulado_aberto";
}
