<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaylistEbook extends Model
{
    protected $table = 'playlist_ebooks';
}
