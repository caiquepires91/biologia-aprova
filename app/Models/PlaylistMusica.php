<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaylistMusica extends Model
{
    protected $table = 'playlist_musicas';
}
