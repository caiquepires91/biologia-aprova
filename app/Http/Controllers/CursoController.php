<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Documento;
use App\Models\Video;
use App\Models\Area;
use App\Models\VideoUsuario;
use App\Models\Simulado;
use App\Models\User;
use App\Models\PlanoAtivo;
use App\Models\SimuladoUsuario;
use App\Models\Questao;
use App\Models\Ebook;
use App\Models\Alternativa;
use App\Models\UsuarioEbook;
use App\Models\UsuarioConteudo;
use App\Models\Playlist;
use App\Models\Musica;
use App\Models\Boleto;
use App\Models\Estado;
use App\Models\PlaylistVideo;
use App\Models\PlaylistQuestionario;
use App\Models\PlaylistSimulado;
use App\Models\PlaylistConteudo;
use App\Models\PlaylistEbook;
use App\Models\PlaylistMusica;
use App\Models\UsuarioSimuladoAberto;
use App\Models\UsuarioSimuladoAbertoResposta;
use DateTime;
use Illuminate\Support\Facades\Crypt;
use Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

/* $page == 'curso' || $page == 'aula' || $page == 'simulado' */

class CursoController extends Controller
{
    private $USER_SESSION = null;
    private $SIMULADOS_GERAIS_ID = 20;
    // local
    // private $SIMULADOS_ABERTOS = [123];
    // on
    private $SIMULADOS_ABERTOS = [];

    public function __construct(Request $request) {
        $this->SIMULADOS_ABERTOS[] = env("SIMULADO_ABERTO_1", 145);
        /* return user if there is a session open */
        if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
            // verifica se usuário possui plano ativo para eliminar botão 'teste grátis'.
            if ($this->USER_SESSION != null) {
                // Salva assinatura mais recente (ativa ou não);
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  $this->USER_SESSION->plano = $planoAtivo;
                // Salva todas as assinaturas (somente ativas);
                $this->USER_SESSION->planos = $this->getPlanosValidos($this->USER_SESSION->id);
                if (isset($this->USER_SESSION->planos) && sizeof($this->USER_SESSION->planos) > 0)
                    $this->USER_SESSION->plano = $this->USER_SESSION->planos[0];
            }
        }
    }

    public function newIndex(Request $request) {
        $user = $this->USER_SESSION;
        $estados = Estado::all();
        if ($user) {
            // verifica se usuário possui um plano.
            if ($user->plano) {
                // verifica se o plano é válido (não expirou e está pago).
                // if ($this->isPlanoValido($planoAtivo)) {
                if ($user->planos->count()) {
                    $user->temPlanoValido = true;

                    // pegando plano id através do nome (para plano n registrado no db).
                    // TODO: Receber diretamente já o id do plano.
                    $planoId = $this->getPlanoIdByName($user->plano->plano);
                    $playlists = $this->getPlanoPlaylists($planoId);

                    // carrega informações sobre os videos do usuário
                    $qtdVideos = Video::count();
                    $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
                    $ultimoVideoAssistido = VideoUsuario::where('idUsuario', $user->id)->orderBy('updated_at', 'desc')->first();
                    if ($ultimoVideoAssistido != null) {
                        $video = Video::find($ultimoVideoAssistido->idVideo);
                        if ($video != null) $user->ultimoVideoAssistido = $video;
                    }

                    return view('home',
                    [
                        'title' => 'Biologia Aprova - Área do Aluno',
                        'page'=>'curso',
                        'qtdVideos'=>$qtdVideos,
                        'qtdVideosAssistidos'=>$qtdVideosAssistidos,
                        'user'=>$user,
                        'playlists'=>$playlists
                    ]);
                } else if ($this->hasBoleto($user->plano)) {
                    $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
                    if ($boleto) {
                        if (!$this->hasExpired($boleto->expirar)) {
                            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user, 'boleto'=>$boleto, 'estados'=>$estados]);
                        }
                    }
                }
            }

            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user, 'estados'=>$estados]);
        }
        return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
    }

    public function index(Request $request) {
        $user = $this->USER_SESSION;
        if ($user != null) {
            // verifica se usuário possui um plano (!= redirect pra pagamento)
            if ($user->plano != null) {
                $planoAtivo = $user->plano;
                $planoDate = new DateTime($planoAtivo->fim);
                $today = new DateTime("now");

                if ($this->isPlanoValido($planoAtivo)) {

                    $playlists = $this->getPlanoPlaylists($planoAtivo->id);

                    $songs = $this->getSongs();
                    $ebooksSemCategoria = $this->setEbooksUserSemCategoria($user->id);

                    // carrega informações sobre os videos do usuário
                    $qtdVideos = Video::count();
                    $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
                    $ultimoVideoAssistido = VideoUsuario::where('idUsuario', $user->id)->orderBy('updated_at', 'desc')->first();
                    if ($ultimoVideoAssistido != null) {
                        $video = Video::find($ultimoVideoAssistido->idVideo);
                        if ($video != null) $user->ultimoVideoAssistido = $video;
                    }

                    // separa os vídeos e simulados por categoria (area)
                    $areas = Area::all();
                    foreach ($areas as $area) {
                        $questionarios = $this->setQuestionarioUser($area->id, $user->id);
                        if ($questionarios != null) $area->questionarios = $questionarios;

                        $simulados = $this->setSimuladosUser($area->id, $user->id);
                        if ($simulados != null) $area->simulados = $simulados;

                        $videos = $this->setVideosUser($area->id, $user->id);
                        if ($videos != null) $area->videos = $videos;

                        $ebooks = $this->setEbooksUser($area->id, $user->id);
                        if ($ebooks != null) $area->ebooks = $ebooks;
                    }

                    // adiciona uma nova area para simulados sem area (Simulado.idArea = 21)
                    $areaSimuladosGerais = $this->createAreaSimuladosGerais($this->SIMULADOS_GERAIS_ID, $user->id);
                    if ($areaSimuladosGerais != null) $areas[] = $areaSimuladosGerais;
                    //return $areas;

                    return view('home', ['title' => 'Biologia Aprova - Área do Aluno', 'page'=>'curso', 'qtdVideos'=>$qtdVideos,
                    'qtdVideosAssistidos'=>$qtdVideosAssistidos, 'areas'=>$areas, 'user'=>$user, 'ebooks'=>$ebooksSemCategoria, 'songs'=> $songs,
                    'playlists'=>$playlists]);
                } else if ($this->hasBoleto($planoAtivo)) {
                    $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
                    if ($boleto) {
                        if (!$this->hasExpired($boleto->expirar)) {
                            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user, 'boleto'=>$boleto]);
                        }
                    }
                }
            }

            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    private function isPlanoValido($planoAtivo) {
        // verifica se o plano foi pago ou se é teste grátis ou se é um plano antigo (status = null) e se não expirou (!= redirect pra pagamento)
        $planoDate = new DateTime($planoAtivo->fim);
        return $planoDate >= $this->TODAY &&
        (
            $planoAtivo->status == config('constants.plano.pago') ||
            $planoAtivo->status == config('constants.plano.teste') ||
            $planoAtivo->status == null
        );
    }

    private function getPlanoPlaylists($planoId) {
        $playlistsVideo = [];
        $playlistsSimulado = [];
        $playlistsConteudo = [];
        $playlistsEbook = [];
        $playlistsMusica = [];
        $playlists = Playlist::where('idPlano', $planoId)->whereAtivo(1)->whereDeletado(0)->orderBy('tipo')->get();
        foreach ($playlists as $playlist) {
            switch ($playlist->tipo) {
                case config('constants.playlist.video'):
                    $playlist->videos = $this->getPlaylistVideos($playlist->id);
                    $playlist->questionarios = $this->getPlaylistQuestionarios($playlist->id);
                    break;
                case config('constants.playlist.simulado'):
                    $playlist->simulados = $this->getPlaylistSimulados($playlist->id);
                    break;
                case config('constants.playlist.conteudo'):
                    $playlist->conteudos = $this->getPlaylistConteudos($playlist->id);
                    break;
                case config('constants.playlist.ebook'):
                    $playlist->ebooks = $this->getPlaylistEbooks($playlist->id);
                    break;
                case config('constants.playlist.musica'):
                    $playlist->musicas = $this->getPlaylistMusicas($playlist->id);
                    break;
            }
        }
        return ['playlists' => $playlists];
    }

    private function getPlanoPlaylistsVideo($planoId) {
        $playlists = Playlist::where('idPlano', $planoId)->whereTipo(config('constants.playlist.video'))->whereAtivo(1)->whereDeletado(0)->get();
        foreach ($playlists as $playlist) {
            $playlist->videos = $this->getPlaylistVideos($playlist->id);
            $playlist->questionarios = $this->getPlaylistQuestionarios($playlist->id);
        }
        return $playlists;
    }

    private function getPlanoPlaylistsSimulado($planoId) {
        $playlists = Playlist::where('idPlano', $planoId)->whereTipo(config('constants.playlist.simulado'))->whereAtivo(1)->whereDeletado(0)->get();
        foreach ($playlists as $playlist) {
            $playlist->simulados = $this->getPlaylistSimulados($playlist->id);
        }
        return $playlists;
    }

    private function getPlaylistVideos($playlistId) {
        $playlistsVideo = PlaylistVideo::where('idPlaylist', $playlistId)
            ->where('playlist_videos.deletado', 0)
            ->join('videos', 'idVideo', '=', 'videos.id')
            ->where('videos.deletado', 0)
            ->get();

        foreach ($playlistsVideo as $video) {
            $videoUsuario = VideoUsuario::where('idUsuario', $this->USER_SESSION->id)->where('idVideo', $video->idVideo)->first();
            if ($videoUsuario != null) $video->assistido = $videoUsuario->assistido;
            else $video->assistido = false;
        }

        return $playlistsVideo;
    }

    private function getPlaylistQuestionarios($playlistId) {
        $playlistsQuestionario = PlaylistQuestionario::where('idPlaylist', $playlistId)
        ->where('playlist_questionarios.deletado', 0)
        ->join('simulado', 'idQuestionario', '=', 'simulado.id')
        ->where('simulado.deletado', 0)
        ->where('simulado.ativo', 1)
        ->where('simulado.tipo', config('constants.curso.tipo_questionario'))
        ->get();

        foreach ($playlistsQuestionario as $simulado) {
            $simuladoUsuario = SimuladoUsuario::where('idUser', $this->USER_SESSION->id)->where('idSimulado', $simulado->idQuestionario )->first();
            if ($simuladoUsuario != null) {
                $simulado->realizado = $simuladoUsuario->realizado;
                $simulado->visualizado = $simuladoUsuario->visualizado;
                $simulado->dataRealizado = $simuladoUsuario->data;
            }
            else {
                $simulado->realizado = false;
                $simulado->visualizado = false;
            }
        }

        return $playlistsQuestionario;
    }

    private function getPlaylistSimulados($playlistId) {
        $playlistsSimulado = PlaylistSimulado::where('idPlaylist', $playlistId)
        ->where('playlist_simulados.deletado', 0)
        ->join('simulado', 'idSimulado', '=', 'simulado.id')
        ->where('simulado.deletado', 0)
        ->where('simulado.ativo', 1)
        ->get();

        foreach ($playlistsSimulado as $simulado) {
            $simuladoUsuario = SimuladoUsuario::where('idUser', $this->USER_SESSION->id)->where('idSimulado', $simulado->idSimulado )->first();
            if ($simuladoUsuario != null) {
                $simulado->realizado = $simuladoUsuario->realizado;
                $simulado->visualizado = $simuladoUsuario->visualizado;
                $simulado->dataRealizado = $simuladoUsuario->data;
            }
            else {
                $simulado->realizado = false;
                $simulado->visualizado = false;
            }
        }

        return $playlistsSimulado;
    }

    private function getPlaylistConteudos($playlistId) {
        $playlistsConteudo = PlaylistConteudo::where('idPlaylist', $playlistId)
            ->where('playlist_conteudos.deletado', 0)
            ->join('conteudos', 'idConteudo', '=', 'conteudos.id')
            ->where('conteudos.deletado', 0)
            ->get();

        foreach ($playlistsConteudo as $conteudo) {
            $usuarioConteudo = UsuarioConteudo::where('idUser', $this->USER_SESSION->id)->where('idConteudo', $conteudo->idConteudo)->first();
            if ($usuarioConteudo != null) $conteudo->visualizado = $usuarioConteudo->visualizado;
            else $conteudo->visualizado = false;
        }

        return $playlistsConteudo;
    }

    private function getPlaylistEbooks($playlistId) {
        $playlistsEbook = PlaylistEbook::where('idPlaylist', $playlistId)
            ->where('playlist_ebooks.deletado', 0)
            ->join('ebooks', 'idEbook', '=', 'ebooks.id')
            ->where('ebooks.deletado', 0)
            ->get();

        foreach ($playlistsEbook as $ebook) {
            $usuarioEbook = UsuarioEbook::where('idUser', $this->USER_SESSION->id)->where('idEbook', $ebook->idEbook)->first();
            if ($usuarioEbook != null) $ebook->visualizado = $usuarioEbook->visualizado;
            else $ebook->visualizado = false;
        }

        return $playlistsEbook;
    }

    private function getPlaylistMusicas($playlistId) {
        return PlaylistMusica::where('idPlaylist', $playlistId)
            ->where('playlist_musicas.deletado', 0)
            ->join('musicas', 'idMusica', '=', 'musicas.id')
            ->where('musicas.deletado', 0)
            ->get();
    }

    private function hasBoleto($planoAtivo) {
       return ($planoAtivo->status = config('constants.plano.aguardando') && $planoAtivo->pagamento == config('constants.pagamento.boleto'));
    }

    private function getPlanosValidos($userId) {
        $today = (new DateTime('now'))->format('Y-m-d');
        $planosValidos = PlanoAtivo::whereRaw("idUser = ? AND (status = ? OR status IS NULL)", [$userId, config('constants.plano.pago')])->whereDate('fim','>=', $today)->get();
        return $planosValidos ? $planosValidos : [];
    }

    public function trocarPlano(Request $request) {
       $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->whereId($request->route('id'))->first();
        if ($planoAtivo) {
            $this->USER_SESSION->plano = $planoAtivo;
            return $this->newIndex($request);
        } else {
            return back();
        }
    }

    private function getPlaylists($user) {
        $planoId = $this->getPlanoIdByName($user->plano->plano);
        $playlists = Playlist::where("idPlano", $planoId)->whereAtivo(1)->get();
        return $playlists;
    }

    private function setEbooksUser($areaId, $userId) {
        $ebooks = Ebook::where('idArea', $areaId)->whereAtivo(1)->orderBy('id','desc')->get();
        foreach ($ebooks as $ebook) {
            $usuarioEbook = UsuarioEbook::where('idUser', $userId)->where('idEbook', $ebook->id)->first();
            if ($usuarioEbook != null) $ebook->visualizado = $usuarioEbook->visualizado;
            else $ebook->visualizado = false;
        }
        return $ebooks;
    }

    private function setEbooksUserSemCategoria($userId) {
        $ebooks = Ebook::where('idArea', null)->whereAtivo(1)->orderBy('id','desc')->get();
        foreach ($ebooks as $ebook) {
            $usuarioEbook = UsuarioEbook::where('idUser', $userId)->where('idEbook', $ebook->id)->first();
            if ($usuarioEbook != null) $ebook->visualizado = $usuarioEbook->visualizado;
            else $ebook->visualizado = false;
        }
        return $ebooks;
    }

    // Simulado BD - tipo: questionario
    private function setQuestionarioUser($areaId, $userId) {
        $questionarios = Simulado::where('idArea', $areaId)->whereTipo(config('constants.curso.tipo_questionario'))->whereAtivo(1)->orderBy('id','desc')->get();
        foreach ($questionarios as $questionario) {
            $questionarioUsuario = SimuladoUsuario::where('idUser', $userId)->where('idSimulado', $questionario->id)->first();
            if ($questionarioUsuario != null) {
                $questionario->realizado = true;
                $questionario->dataRealizado = $questionarioUsuario->updated_at;
            }
            else $questionario->realizado = false;
        }
        return $questionarios;
    }

    private function createAreaSimuladosGerais($areaId, $userId) {
        $simuladosGerais = $this->setSimuladosUser($areaId, $userId);
        $area = null;
        if ($simuladosGerais != null) {
            $area = new Area();
            $area->id = $areaId;
            $area->nome = "Simulados Gerais";
            $area->url = "";
            $area->questionarios = [];
            $area->videos = [];
            $area->simulados = $simuladosGerais;
        }
        return $area;
    }

    // Simulado BD - tipo: simulado
    private function setSimuladosUser($areaId, $userId) {
        $simulados = Simulado::where('idArea', $areaId)->whereTipo(config('constants.curso.tipo_simulado'))->whereAtivo(1)->whereDeletado(0)->orderBy('id','desc')->get();
        foreach ($simulados as $simulado) {
            $simuladoUsuario = SimuladoUsuario::where('idUser', $userId)->where('idSimulado', $simulado->id)->first();
            if ($simuladoUsuario != null) {
                $simulado->realizado = $simuladoUsuario->realizado;
                $simulado->visualizado = $simuladoUsuario->visualizado;
                $simulado->dataRealizado = $simuladoUsuario->data;
            }
            else {
                $simulado->realizado = false;
                $simulado->visualizado = false;
            }
        }
        return $simulados;
    }

    private function setVideosUser($areaId, $userId) {
        $videos = Video::where("idArea", $areaId)->orderBy('aula')->get();
        foreach ($videos as $video) {
            $videoUsuario = VideoUsuario::where('idUsuario', $userId)->where('idVideo', $video->id)->first();
            if ($videoUsuario != null) $video->assistido = $videoUsuario->assistido;
            else $video->assistido = false;
        }
        return $videos;
    }

    // handle aula vídeo, aula questionário, aula playlist.
    public function newAula(Request $request) {
        $tipo = $request->input("tipo");
        $id = $request->route('id');
        $planoId = $request->route('plano');

        // get plano.
        $planoAtivo = PlanoAtivo::find($planoId);
        if ($planoAtivo) {
            $this->USER_SESSION->plano = $planoAtivo;
            $planoId = $this->getPlanoIdByName($planoAtivo->plano);
            $this->USER_SESSION->plano->idPlano = $planoId;
        } else {
            return back();
        }

        $user = $this->USER_SESSION;
        if ($user != null) {
            $qtdSimulados = null;

            // carrega informações sobre os videos do usuário
            $qtdVideos = Video::count();
            $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
            $videoUsuarioAssistido = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->first();
            if ($videoUsuarioAssistido != null) $ultimoVideoAssistido = Video::find($videoUsuarioAssistido->idVideo);
            else $ultimoVideoAssistido = null;

            if ($tipo == config('constants.curso.tipo_questionario')) {
                $aula = Simulado::find($id);
                // verificando simulados realizados pelo usuário
                $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $aula->id)->latest('id')->first();
                if ($simuladoUsuario != null) $user->simulado = $simuladoUsuario;
                $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
                if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;
                $qtdSimulados = Simulado::count();
                $questoes = $this->setQuestoes($aula->id);
                if ($questoes != null) $aula->perguntas = $questoes;
            } else if ($tipo == config('constants.curso.tipo_playlist_med')){
                if ($id < 0) {
                    // get ultimo assistido de acordo com as condições do plano
                    if ($ultimoVideoAssistido != null) $aula = $ultimoVideoAssistido;
                    else $aula = Video::orderBy('id', 'asc')->first();
                } else {
                    $aula = Video::find($id);
                }
            } else {
                $aula = Video::find($id);
            }

            $documentos = Documento::where("idArea", $aula->idArea)->get();
            $playlistsVideo = $this->getPlanoPlaylistsVideo($planoId);
            //dd($playlistsVideos);

            return view('home',
            [
                'title' => 'Biologia Aprova - Área do Aluno',
                'page'=>'aula',
                'user'=>$user,
                'aula'=>$aula,
                'playlists' => $playlistsVideo,
                'simulado'=>$aula,
                'qtdSimulados'=>$qtdSimulados,
                'documentos'=>$documentos,
                'tipo'=>$tipo,
                'qtdVideos'=>$qtdVideos,
                'qtdVideosAssistidos'=>$qtdVideosAssistidos
            ]);

        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    // handle aula vídeo, aula questionário, aula playlist.
    public function aula(Request $request) {
        $tipo = $request->input("tipo");
        $id = $request->route('id');
        $planoId = $request->route('plano');

        // get plano.
        $planoAtivo = PlanoAtivo::find($planoId);
        if ($planoAtivo) {
            $this->USER_SESSION->plano = $planoAtivo;
        } else {
            return back();
        }
        // get plano.

        $user = $this->USER_SESSION;
        if ($user != null) {
            $areas = Area::all();
            $qtdSimulados = null;

            $qtdVideos = Video::count();
            $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
            $videoUsuarioAssistido = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->first();
            if ($videoUsuarioAssistido != null) $ultimoVideoAssistido = Video::find($videoUsuarioAssistido->idVideo);
            else $ultimoVideoAssistido = null;

            if ($tipo == config('constants.curso.tipo_questionario')) {
                $aula = Simulado::find($id);

                // verificando simulados pelo usuário
                $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $aula->id)->latest('id')->first();
                if ($simuladoUsuario != null) $user->simulado = $simuladoUsuario;
                $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
                if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;
                $qtdSimulados = Simulado::count();
                $questoes = $this->setQuestoes($aula->id);
                if ($questoes != null) $aula->perguntas = $questoes;

            } else if ($tipo == config('constants.curso.tipo_playlist_med')){
                if ($id < 0) {
                    // get ultimo assistido de acordo com as condições do plano
                    if ($ultimoVideoAssistido != null) $aula = $ultimoVideoAssistido;
                    else $aula = Video::orderBy('id', 'asc')->first();
                } else {
                    $aula = Video::find($id);
                }
            } else {
                $aula = Video::find($id);
            }

            $documentos = Documento::where("idArea", $aula->idArea)->get();

            foreach ($areas as $area) {

                // TODO: change tipo_playlist_med para tipo_playlist
                if ($tipo == config('constants.curso.tipo_playlist_med')) {
                    $videos = $this->getVideosByPlaylistIdByAreaId($request->input("playlist"), $area, $user);
                } else {
                    $questionarios = $this->setQuestionarioUser($area->id, $user->id);
                    if ($questionarios != null) $area->questionarios = $questionarios;
                    $videos = $this->setVideosUser($area->id, $user->id);
                }

                if ($videos != null) $area->videos = $videos;
                if ($documentos != null) $area->documentos = $documentos;
            }

            return view('home', ['title' => 'Biologia Aprova - Área do Aluno',
            'page'=>'aula', 'user'=>$user, 'areas'=>$areas, 'aula'=>$aula, 'simulado'=>$aula,
            'qtdSimulados'=>$qtdSimulados, 'documentos'=>$documentos, 'tipo'=>$tipo,
            'qtdVideos'=>$qtdVideos, 'qtdVideosAssistidos'=>$qtdVideosAssistidos,]);

        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    private function getVideosByPlaylistIdByAreaId($playlistId, $area, $user) {
        $playlist = Playlist::find($playlistId);
        $planoId = $this->getPlanoIdByName($user->plano->plano);
        if ($planoId == $playlist->idPlano) {
            // TODO: get by playlist id;
            $videos = $this->setVideosUser($area->id, $user->id);
        } else {
            $videos = [];
        }
        return $videos;
    }

    // atualmente a função aluno gerencia a playlist;
    private function getPlaylist($user) {
         // check if aluno has extensivo med.
         if ($user->plano->plano == 'EXTENSIVO MED') {

            $qtdVideos = Video::count();
            $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
            $ultimoVideoAssistido =  VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->latest('id')->first();

            $aula = $ultimoVideoAssistido;
            $documentos = Documento::where("idArea", $aula->idArea)->get();

            $areas = Area::all();
            foreach ($areas as $area) {
                $videos = $this->setVideosUser($area->id, $user->id);
                if ($videos != null) $area->videos = $videos;

                if ($documentos != null) $area->documentos = $documentos;
            }

            return ['videos'=>$videos, 'ultimoVideoAssistido'=>$ultimoVideoAssistido];
        } else {
            $result = ['success'=>false, 'message'=>'exclusivo pra assinantes do plano Extensivo Med'];
        }
    }

    public function playlist(Request $request) {
        $user = $this->USER_SESSION;
        if ($user != null) {

            // check if aluno has extensivo med.
            if ($user->plano->plano == 'EXTENSIVO MED') {

                $qtdVideos = Video::count();
                $qtdVideosAssistidos = VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->count();
                $ultimoVideoAssistido =  VideoUsuario::where('idUsuario', $user->id)->whereAssistido(true)->latest('id')->first();

                $aula = $ultimoVideoAssistido;
                $documentos = Documento::where("idArea", $aula->idArea)->get();

                $areas = Area::all();
                foreach ($areas as $area) {
                    $videos = $this->setVideosUser($area->id, $user->id);
                    if ($videos != null) $area->videos = $videos;

                    if ($documentos != null) $area->documentos = $documentos;
                }

                return view('home', ['title' => 'Biologia Aprova - Área do Aluno',
                'page'=>'aula', 'user'=>$user, 'areas'=>$areas, 'aula'=>$aula, 'simulado'=>$aula,
                'qtdSimulados'=>null, 'documentos'=>$documentos, 'tipo'=>null,
                'qtdVideos'=>$qtdVideos, 'qtdVideosAssistidos'=>$qtdVideosAssistidos, 'ultimoVideoAssistido'=> $ultimoVideoAssistido]);
            } else {
                $result = ['success'=>false, 'message'=>'exclusivo pra assinantes do plano Extensivo Med'];
            }

        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    public function newSimulado(Request $request) {

        $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->whereId($request->route('plano'))->first();
        if ($planoAtivo) {
            $this->USER_SESSION->plano = $planoAtivo;
            $planoId = $this->getPlanoIdByName($planoAtivo->plano);
            $this->USER_SESSION->plano->idPlano = $planoId;
        }

        $user = $this->USER_SESSION;
        if ($user != null) {
            $thisSimulado = Simulado::find($request->route('id'));
            $thisSimulado->questoes = $this->getQtdQuestoes($thisSimulado->id);
            $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $thisSimulado->id)->first();
            if ($simuladoUsuario != null) $user->simulado = $simuladoUsuario;
            $qtdSimulados = Simulado::count();
            $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
            if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;

            $acertos = SimuladoUsuario::where('idSimulado', $request->route('id'))->avg('acertos');
            $tempos = SimuladoUsuario::where('idSimulado', $request->route('id'))->sum('tempo');

            $playlists = $this->getPlanoPlaylistsSimulado($planoId);
            //dd($playlists);

            return view('home',
                ['title' => 'Biologia Aprova - Área do Aluno',
                'page'=>'simulado',
                'user'=>$user,
                'playlists'=>$playlists,
                'simulado'=>$thisSimulado,
                'acertos'=>$acertos,
                'qtdSimulados'=>$qtdSimulados,
                'tempos'=>$tempos]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    public function simulado(Request $request) {

        $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->whereId($request->route('plano'))->first();
        if ($planoAtivo) {
            $this->USER_SESSION->plano = $planoAtivo;
        }

        $user = $this->USER_SESSION;
        if ($user != null) {
            $thisSimulado = Simulado::find($request->route('id'));
            $thisSimulado->questoes = $this->getQtdQuestoes($thisSimulado->id);
            $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $thisSimulado->id)->first();
            if ($simuladoUsuario != null) $user->simulado = $simuladoUsuario;
            $qtdSimulados = Simulado::count();
            $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
            if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;

            $acertos = SimuladoUsuario::where('idSimulado', $request->route('id'))->avg('acertos');
            $tempos = SimuladoUsuario::where('idSimulado', $request->route('id'))->sum('tempo');

            $areas = Area::all();
            foreach ($areas as $area) {
                $simulados = $this->setSimuladosUser($area->id, $user->id);
                if ($simulados != null) $area->simulados = $simulados;
            }
            // adiciona uma nova area para simulados sem area
            $areaSimuladosGerais = $this->createAreaSimuladosGerais($this->SIMULADOS_GERAIS_ID, $user->id);
            if ($areaSimuladosGerais != null) $areas[] = $areaSimuladosGerais;
            //return $areas;

            return view('home', ['title' => 'Biologia Aprova - Área do Aluno', 'page'=>'simulado', 'user'=>$user, 'areas'=>$areas, 'simulado'=>$thisSimulado, 'acertos'=>$acertos,
            'qtdSimulados'=>$qtdSimulados, 'tempos'=>$tempos]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    private function getQtdQuestoes($simuladoId) {
        return Questao::where('idSimulado', $simuladoId)->count();
    }

    private function setQuestoes($simuladoId) {
        $questoes = Questao::where('idSimulado', $simuladoId)->orderBy('id')->get();
        if ($questoes != null) {
            foreach ($questoes as $questao) {
                $alternativas = Alternativa::where('idQuestao', $questao->id)->orderBy('id')->get();
                if ($alternativas != null) $questao->alternativas = $alternativas;
            }
            return $questoes;
        } else {
            return null;
        }
    }

    public function resultadoAberto(Request $request) {
        $userId = $request->route('userId');
        $acertos = $request->route('acertos');
        $total = $request->route('total');
        $user_aberto = UsuarioSimuladoAberto::find($userId);
        if (isset($userId) && isset($acertos) && isset($total))
            return view('home', ['title' => 'Biologia Aprova - Teste de conhecimento', 'page'=>'resultado_aberto', 'acertos'=>$acertos, 'total'=>$total, 'user_aberto'=>$user_aberto]);

        return view('content.erro', ['subject'=>'Acesso Negado', 'message'=>'Você não tem acesso a essa página.']);
    }

    public function resolucaoAberta(Request $request) {
        if (in_array($request->route('id'), $this->SIMULADOS_ABERTOS)) {
            // $user_aberto = UsuarioSimuladoAberto::find(Crypt::decrypt($request->route('userId')));
            $user_aberto = UsuarioSimuladoAberto::find($request->route('userId'));
            $simulado = Simulado::find($request->route('id'));
            $questoes = $this->setQuestoes($simulado->id);
            if ($questoes != null) $simulado->perguntas = $questoes;
            if ($user_aberto) {
                return view('home', ['title' => 'Biologia Aprova - Simulado Aberto 2022', 'page'=>'resolucao_aberta', 'user'=>NULL, 'user_aberto'=>$user_aberto, 'simulado'=>$simulado,'qtdSimulados'=>1]);
            }
        }
        return view('content.erro', ['subject'=>'Acesso Negado', 'message'=>'Você não tem acesso a essa página.']);
    }

    public function responderSimuladoAberto(Request $request) {
        $inputs = $request->all();
        $resposta = UsuarioSimuladoAbertoResposta::where('idSimulado', $inputs["id_simulado"])->where('idUsuario', $inputs["id_usuario"])->first();
        if (!$resposta) {
           $resposta = new UsuarioSimuladoAbertoResposta();
           $resposta->idSimulado = $inputs["id_simulado"];
           $resposta->idUsuario = $inputs["id_usuario"];
        }
        $resposta->total = $inputs["total"];
        $resposta->acertos = $inputs["acertos"];
        $resposta->resposta = $inputs["resposta"];
        $resposta->tempo = $this->stringTimeToSec($inputs["tempo"]);
        $resposta->save();
        if ($resposta)
            return ["success"=>true, "resposta"=>$resposta];

        return ["success"=>false];
    }

    public function resolucao(Request $request) {
        $user = $this->USER_SESSION;
        if ($user != null) {
            $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->whereId($request->route('plano'))->first();
            if ($planoAtivo) {
                $this->USER_SESSION->plano = $planoAtivo;
                $planoId = $this->getPlanoIdByName($planoAtivo->plano);
                $this->USER_SESSION->plano->idPlano = $planoId;
            }

            $simulado = Simulado::find($request->route('id'));

            // verificando simulados pelo usuário
            $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $simulado->id)->first();
            if ($simuladoUsuario != null) {
                if (!$simuladoUsuario->visualizado) {
                    $simuladoUsuario->visualizado = true;
                    $simuladoUsuario->save();
                }
            } else {
                $simuladoUsuario = new SimuladoUsuario();
                $simuladoUsuario->idUser = $user->id;
                $simuladoUsuario->idSimulado = $simulado->id;
                $simuladoUsuario->visualizado = true;
                $simuladoUsuario->save();
            }
            $user->simulado =  $simuladoUsuario;

            $qtdSimulados = Simulado::count();
            $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
            if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;

            $questoes = $this->setQuestoes($simulado->id);
            if ($questoes != null) $simulado->perguntas = $questoes;

            return view('home', ['title' => 'Biologia Aprova - Área do Aluno', 'page'=>'resolucao', 'user'=>$user, 'simulado'=>$simulado,
            'qtdSimulados'=>$qtdSimulados]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    // verifica se o usuário tem cadastro no simulado aberto para concedê-lo desconto.
    public function hasDesconto(Request $request) {
        $inputs = $request->all();
        $usuarioSimuladoAberto = UsuarioSimuladoAberto::whereEmail($inputs["email"])->first();
        if ($usuarioSimuladoAberto) {
            $resposta = UsuarioSimuladoAbertoResposta::where("idUsuario", $usuarioSimuladoAberto->id)->first();
            if ($resposta)
                return ["success"=>true, "resposta"=>$resposta, "usuario_simulado"=>$usuarioSimuladoAberto];
        }
        return ["success"=>false, "msg"=>"Resposta não encontrada."];
    }

    public function finalizar(Request $request) {
        $idUser = $this->USER_SESSION->id;
        $idSimulado = $request->input("idSimulado");
        $respostas = $request->input("respostas");
        $acertos = $request->input("acertos");
        $tempo = $request->input("tempo");

        $simuladoUsuario = SimuladoUsuario::where('idUser', $idUser)->where("idSimulado", $idSimulado)->first();
        if ($simuladoUsuario == null) {
            $simuladoUsuario = new SimuladoUsuario();
            $simuladoUsuario->idUser = $idUser;
            $simuladoUsuario->idSimulado = $idSimulado;
        }
        $simuladoUsuario->realizado = true;
        $simuladoUsuario->visualizado = true;
        $simuladoUsuario->resposta = $respostas;
        $simuladoUsuario->acertos = $acertos;
        $simuladoUsuario->tempo = $tempo;
        $simuladoUsuario->min = 0;
        $simuladoUsuario->data = new DateTime("now");
        $simuladoUsuario->save();

        $result = [];
        if($simuladoUsuario) {
            $result = ['success'=>true, 'simuladoRealizado'=>$simuladoUsuario];
        } else {
            $result = ['success'=>false];
        }
        return $result;
    }

    public function resultado(Request $request) {
        $tipo = $request->input("tipo");
        $user = $this->USER_SESSION;
        if ($user != null) {
            $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->whereId($request->route('plano'))->first();
            if ($planoAtivo) {
                $this->USER_SESSION->plano = $planoAtivo;
                $planoId = $this->getPlanoIdByName($planoAtivo->plano);
                $this->USER_SESSION->plano->idPlano = $planoId;
            }

            $simulado = Simulado::find($request->route('id'));
            $simulado->gabarito = Str::of($simulado->gabarito)->split("~,~");
            $simuladoUsuario = SimuladoUsuario::where('idUser', $user->id)->where('idSimulado', $simulado->id)->first();

            if ($simuladoUsuario != null) {
                $user->simulado = $simuladoUsuario;
                $user->simulado->resposta = Str::of($user->simulado->resposta)->split("~,~");
            }
            $qtdSimulados = Simulado::count();
            $qtdRealizados = SimuladoUsuario::where('idUser', $user->id)->count();
            if ($qtdRealizados != null) $user->simuladosRealizados = $qtdRealizados;

            $questoes = Questao::where('idSimulado', $simulado->id)->orderBy('id')->get();
            if ($questoes != null) {
                foreach ($questoes as $questao) {
                    $alternativas = Alternativa::where('idQuestao', $questao->id)->orderBy('id')->get();
                    if ($alternativas != null) $questao->alternativas = $alternativas;
                }
                $simulado->perguntas = $questoes;
                $simulado->questoes = count($questoes); // se trata da qtd de perguntas
            }

            return view('home', ['title' => 'Biologia Aprova - Área do Aluno', 'page'=>'resultado', 'user'=>$user, 'simulado'=>$simulado,
            'qtdSimulados'=>$qtdSimulados, 'tipo'=>$tipo]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    public function videoPlayed(Request $request) {
        $idUser = $request->session()->has("userId") ? $request->session()->get("userId") : null;
        $idVideo = $request->input("id_video");

        $videoUsuario = VideoUsuario::where('idUsuario', $idUser)->where("idVideo", $idVideo)->first();
        if ($videoUsuario == null) {
            $videoUsuario = new VideoUsuario();
            $videoUsuario->idUsuario = $idUser;
            $videoUsuario->idVideo = $idVideo;
        }
        $videoUsuario->assistido = true;
        $videoUsuario->save();

        $result = [];
        if($videoUsuario != null) {
            $result = ['success'=>true, 'videoUsuario'=>$videoUsuario];
        } else {
            $result = ['success'=>false];
        }
        return $result;
    }

    public function markEbookAsSeen(Request $request) {
        $idEbook = $request->input('idEbook');
        $user = $this->USER_SESSION;
        if ($user != null) {
            $ebook = Ebook::find($idEbook);
            if ($ebook != null) {
                $usuarioEbook = new UsuarioEbook();
                $usuarioEbook->idUser = $user->id;
                $usuarioEbook->idEbook = $ebook->id;
                $usuarioEbook->visualizado = true;
                $usuarioEbook->save();
                if ($usuarioEbook != null) $result = ['success'=>true, 'user'=>$user, 'ebook'=>$ebook];
                else $result = ['success'=>false, 'message'=>'não foi possível marcar como visualizado.'];
            } else $result = ['success'=>false, 'message'=>'ebook não encontrado.'];
        } else $result = ['success'=>false, 'message'=>'usuário precisa fazer login.'];

        return $result;
    }

    private function getSongs() {
       $musicas = Musica::whereAtivo(1)->orderBy('id')->get();
       return $musicas;
    }

   /*  private function getSongs() {
        $fileNames = [];
       // ini_set('memory_limit', '2024M');
        $files = File::allFiles(app()->basePath('public/musicas'));
        //$files = Storage::disk('local')->allFiles('teste');
        foreach($files as $file) {
            $extension = pathinfo($file)['extension'];
            if ($extension == "mp3" || $extension == "wma")
                array_push($fileNames, pathinfo($file)['basename']);
        }
        return $fileNames;
    } */
}
