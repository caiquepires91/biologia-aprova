<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\User;
use App\Models\PlanoAtivo;
use App\Models\Depoimento;
use App\Models\Faq;
use App\Models\Area;
use App\Models\Preinscricao;
use Spatie\Sitemap\SitemapGenerator;
use Session;

class HomeController extends Controller
{
    private $USER_SESSION = null;

    public function __construct(Request $request) {

         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
            // verifica se usuári possui plano ativo para eliminar botão 'teste grátis'.
            if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  $this->USER_SESSION->plano = $planoAtivo;
            }
        }
         /* /return user if there is a session open */

       // SitemapGenerator::create(url('/'))->writeToFile("/public");
    }

    public function index(Request $request) {
        $videos = Video::wherePrivado(0)->limit(5)->get();
        if ($videos != null) {
            foreach ($videos as $video) {
                $area = Area::find($video->idArea);
                if ($area != null) $video->area = $area;
            }
        }

        $depoimentos = Depoimento::whereAtivo(1)->orderBy('ordem')->get();
        $faq = Faq::all();
    	return view('home', ['title' => 'Biologia Aprova', 'page'=>'main', 'videos'=>$videos, 'depoimentos'=>$depoimentos, 'perguntas'=>$faq, 'user'=>$this->USER_SESSION]);
    }

    public function lpsave(Request $request) {
        $email = $request->input('email');
        $concordo = $request->input('concordo');
        if ($email) {
            $preinscricao = Preinscricao::whereEmail($email)->first();
            if (!$preinscricao) {
                $preinscricao = new Preinscricao();
            }
            $preinscricao->email = $email;
            $preinscricao->concordo = $concordo;
            $preinscricao->save();
            $request->session()->put('user_email', $preinscricao->email);
            return ['success'=>true, 'preinscricao'=>$preinscricao];
        }
        return ['success'=>false];
    }
}
