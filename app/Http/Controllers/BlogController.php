<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\User;
use App\Models\PlanoAtivo;
use Laravel\Lumen\Http\Redirector;

class BlogController extends Controller
{
    private $USER_SESSION = null;

    public function __construct(Request $request) {
         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
             // verifica se usuári possui plano ativo para eliminar botão 'teste grátis'.
             if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  $this->USER_SESSION->plano = $planoAtivo;
            }
        }
         /* /return user if there is a session open */
    }

    public function index(Request $request) {
        $categorias = Noticia::select('categoria')->groupBy('categoria')->orderBy('visualizacoes', 'desc')->limit(5)->get();
        $noticias = Noticia::orderBy('data_publicacao', 'desc')->paginate(6);
        $ultimasNoticias = Noticia::limit(6)->orderBy('data_publicacao', 'desc')->get();
        return view('home', ['title'=>'Biologia Aprova - Blog', 'page'=>'blog', 'categorias'=>$categorias, 
        'noticias'=>$noticias, 'ultimasnoticias'=>$ultimasNoticias, 'user'=> $this->USER_SESSION]);
    }

    public function show($id) {
        $noticia = Noticia::find($id);
        $noticias = Noticia::limit(6)->orderBy('data_publicacao', 'desc')->get();
        //$decodedId = Crypt::decode($id);
        return view('home', ['title'=>'Biologia Aprova - Blog','page'=>'news', 'ultimasnoticias'=>$noticias, 'user'=> $this->USER_SESSION])->withNoticia($noticia);
    }

    public function byCategoria($categoria) {
        $categoria = urldecode($categoria);
        $categorias = Noticia::select('categoria')->groupBy('categoria')->orderBy('visualizacoes', 'desc')->limit(5)->get();
        $noticias = Noticia::whereCategoria($categoria)->orderBy('data_publicacao', 'desc')->paginate(6);
        $ultimasNoticias = Noticia::limit(6)->orderBy('data_publicacao', 'desc')->get();
        //return $noticias;
        return view('home', ['title'=>'Biologia Aprova - Blog','page'=>'blog', 'categ'=>$categoria, 'categorias'=>$categorias,
        'noticias'=>$noticias, 'ultimasnoticias'=>$ultimasNoticias, 'user'=> $this->USER_SESSION]);
    }

    public function autocomplete(Request $request) {
        $query = '%'.$request->str.'%';
        //$noticias = Noticia::where("titulo", 'like', $query)->limit(6)->orderBy('data_publicacao', 'desc')->get();
        $noticias = Noticia::where("titulo", 'like', $query)->orderBy('data_publicacao', 'desc')->paginate(6);
        return response()->json($noticias);
    }

    public function buscar(Request $request) {
        $noticias = Noticia::where("titulo", 'like', '%' . $request->titulo . '%')->get();
        return response()->json($noticias);
    }

    public function post(Request $request) {
        $titulo = $request->input("titulo");
        $texto = $request->input("texto");
        $autor = $request->input("autor");
        $img = $request->input("img");
        $lingua = $request->input("lingua");
        $nota = $request->input("nota");
        $visualizacoes = $request->input("visualizacoes");
        $data_publicacao = $request->input("data_publicacao");
        $imagem = $request->hasFile('img');

        $img = $this->upload($imagem);

        $noticia = new Noticia();
        $noticia->titulo = $titulo;
        $noticia->texto = $texto;
        $noticia->autor = $autor;
        $noticia->img = $img;
        $noticia->lingua = $lingua;
        $noticia->nota = $nota;
        $noticia->visualizacoes = $visualizacoes;
        $noticia->data_publicacao = $data_publicacao;
        $notica->save();

        if ($noticia) {
            $result =  ['success'=>true, 'noticia'=>$noticia];
        } else {
            $result = ['success'=>false, 'msg'=> 'não foi possível criar postagem.'];
        }

        return $result;
    }

    private function upload($image) {

        if ($imagem) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        
            $imageName = 'post_'.time().'.'.$request->image->extension(); 
       
            if ($imageName != "") {
                $user->foto = $imageName;
                $user->save();
            } 
         
            $request->image->move(app()->basePath('public/img/blog'), $imageName);
       
            $result = $imageName;
        } else {
            $result = "";
        }

        return $result;
    }
}
