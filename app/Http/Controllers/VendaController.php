<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Compra;


class VendaController extends Controller
{
    protected $pagseguro_token;
    protected $pagseguro_vendedor;
    protected $pagseguro_postback;

    public function __construct()
    {
        $this->pagseguro_token = env("PAGSEGURO_TOKEN");
        $this->pagseguro_vendedor = env("PAGSEGURO_VENDEDOR");
        $this->pagseguro_postback = env("PAGSEGURO_POSTBACK");
    }

    // Lista de produtos
    public function index(Request $request)
    {
        $produtos = Produto::all();
        $compras = Compra::orderBy('id', 'DESC')->get();
        return view('venda')->with(['produtos' => $produtos, 'compras' => $compras]);
    }

    // Gera código de checkout
    public function comprar(Request $request)
    {
        return $checkout ?? [];
    }

    // link para o checkout
    // [POST] https://ws.sandbox.pagseguro.uol.com.br/v2/checkout
    private function checkout($item)
    {
    }

    // salvar registro com o código da transação (vindo do lightbox)
    // consultar transação
    // [GET] https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/
    public function transactionSave(Request $request)
    {


        return ["success" => true, "compra" => $compra];
    }

    // o postback do pagarme envia o código da notificação.
    // consultar transação
    // [GET] https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/notifications/
    public function postback(Request $request)
    {
    }

    private function transactionStatus($base_url, $code)
    {
    }
}
