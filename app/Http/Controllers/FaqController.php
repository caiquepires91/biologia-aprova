<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Models\PlanoAtivo;
use App\Models\User;

class FaqController extends Controller
{
    private $USER_SESSION = null;

    public function __construct(Request $request) {
         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
             // verifica se usuári possui plano ativo para eliminar botão 'teste grátis'.
            if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  $this->USER_SESSION->plano = $planoAtivo;
            }
        }
         /* /return user if there is a session open */
    }

    public function index(Request $request) {
        return view('home',['title' => 'Biologia Aprova - faq', 'page'=>'faq', 'isSearch'=>false, 'user'=>$this->USER_SESSION]);
    }

    public function show($categoria) {
        $titulo = '';
        $string = '';
        $faqs = Faq::whereCategoria($categoria)->get();

        switch ($categoria) {
            case 0:
                $titulo = 'planos e pagamento';
                break;
            case 1:
                $titulo = 'cadastro, login e senhas';
                break;
            case 2:
                $titulo = 'funcionalidades do ba';
                break;
        }

        return view('home',['titulo'=>$titulo, 'perguntas' => $faqs, 'title' => 'Biologia Aprova - faq', 'page'=>'perguntas', 'isSearch'=>false, 'string'=>$string, 'user'=>$this->USER_SESSION]);
    }

    public function autocomplete(Request $request) {
        // field renamed to str because query is lumnen's preloaded function
        $query = '%'.$request->str.'%';
        $faqs = Faq::select("pergunta")->where("pergunta", 'like', $query)->limit(10)->get();
        $perguntas = array();
        foreach ($faqs as $faq) {
            $perguntas[] = $faq->pergunta;
        }
        return response()->json($perguntas);
    }

    public function buscar($str) {
        $string = urldecode($str);
        $faqs = Faq::where("pergunta", 'like', '%' .$string . '%')->get();
        $titulo = 'faq';
        return view('home',['titulo'=>$titulo, 'perguntas' => $faqs, 'title' => 'Biologia Aprova - faq', 'page'=>'perguntas', 'isSearch'=>true, 'string'=>$string, 'user'=>$this->USER_SESSION]);
    }
}
