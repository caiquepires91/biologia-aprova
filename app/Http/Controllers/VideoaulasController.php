<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\User;
use App\Models\Area;
use App\Models\PlanoAtivo;
use App\Models\Cupom;
use DateTime;

class VideoaulasController extends Controller
{

    private $USER_SESSION = null;

    public function __construct(Request $request) {
         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
            // verifica se usuári possui plano ativo para eliminar botão 'teste grátis'.
            if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  {
                    $this->USER_SESSION->plano = $planoAtivo;
                }
                // Salva todas as assinaturas (somente ativas);
                $this->USER_SESSION->planos = $this->getPlanosValidos($this->USER_SESSION->id);
                if (isset($this->USER_SESSION->planos) && sizeof($this->USER_SESSION->planos) > 0)
                    $this->USER_SESSION->plano = $this->USER_SESSION->planos[0];
            }
        }
         /* /return user if there is a session open */
    }

    public function index(Request $request) {
        $areas = Area::all();
        $videoPorAreas = [];
        $areasName = [];
        foreach ($areas as $key=>$area) {
            $areasName[] = $area->nome;
            $videos = Video::where("idArea", $area->id)->orderBy('aula', 'asc')->get();
            if ($videos != null) {
                $videoPorAreas[] = $videos;
            }
        }

        if ($this->USER_SESSION != null) {
            if ($this->USER_SESSION->plano != null) {
                if ($this->isPlanoValid($this->USER_SESSION->plano)) {
                    $this->USER_SESSION->plano->ativo = true;
                } else {
                    $this->USER_SESSION->plano->ativo = false;
                }
            }
        }

        return view('home', ['title' => 'Biologia Aprova - videoaulas', 'page'=>'videoaulas', "videosPorArea"=>$videoPorAreas, 'areas'=>$areasName, 'user'=> $this->USER_SESSION]);
    }

    private function isPlanoValid($plano) {
        $today = new DateTime("now");
        $planoDate = new DateTime($plano->fim);
        if ($planoDate >= $today) {
            return true;
        } else {
            return false;
        }
    }

    private function getPlanosValidos($userId) {
        $today = (new DateTime('now'))->format('Y-m-d');
        $planosValidos = PlanoAtivo::whereRaw("idUser = ? AND (status = ? OR status IS NULL)", [$userId, config('constants.plano.pago')])->whereDate('fim','>=', $today)->get();
        if ($planosValidos) {
            foreach ($planosValidos as $plano) {
                if ($plano->idCupom){
                    $cupom = Cupom::find($plano->idCupom);
                    if ($cupom) {
                        $plano->cupom = $cupom;
                        $plano->planoDesconto = (object) $this->setPlanoByCupom($cupom);
                    }
                }
            }
        }
        return $planosValidos ? $planosValidos : [];
    }

    private function setPlanoByCupom($cupom) {
        if ($cupom->idPlano == 5) {
            // Plano 5 - 26280.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 26280 - (26280 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 26280 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO COMPLETO";
            $tempo = "12";
        } else {
            // Plano 6 - 9480.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 9480 - (9480 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 9480 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO TOTAL";
            $tempo = "12";
        }

        return ['plano'=>$plano, 'tempo'=>$tempo, 'valor'=>$valorDesc, 'desconto'=>$desconto];
    }
}
