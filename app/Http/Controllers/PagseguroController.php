<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Compra;

class PagseguroController extends Controller
{

    protected $pagseguro_token;
    protected $pagseguro_vendedor;
    protected $pagseguro_postback;

    public function __construct()
    {
        $this->pagseguro_token = env("PAGSEGURO_TOKEN");
        $this->pagseguro_vendedor = env("PAGSEGURO_VENDEDOR");
        $this->pagseguro_postback = env("PAGSEGURO_POSTBACK");
    }

    // Gera código de checkout
    public function index(Request $request)
    {
        $produtos = Produto::all();
        $compras = Compra::orderBy('id', 'DESC')->get();
        return view('pagseguro')->with(['produtos' => $produtos, 'compras' => $compras]);
    }

    // Gera código de checkout
    public function comprar(Request $request)
    {
        // 1. Buscar informações do item com id.
        $id = $request->input('id');
        $produto = Produto::find($id);

        // 2. Realizar o checkout
        if ($produto)
            $checkout = $this->checkout($produto);

        return $checkout ?? [];
    }

    // link para o checkout
    // [POST] https://ws.sandbox.pagseguro.uol.com.br/v2/checkout
    private function checkout($item)
    {
        $curl = curl_init();

        $data = [
            "itemId1" => $item->id,
            "itemDescription1" => $item->descricao,
            "itemAmount1" => $item->valor,
            "itemQuantity1" => 1,
            "itemWeight1" => 0,
            "currency" => "BRL",
            "shippingType" => 1,
            "notificationURL" => $this->pagseguro_postback,
            "installmentQuantity" => 12,
        ];

        $checkout = [
            CURLOPT_URL => "https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/?email={$this->pagseguro_vendedor}&token={$this->pagseguro_token}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/x-www-form-urlencoded',
            ],
        ];

        curl_setopt_array($curl, $checkout);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ["erro" => $err];
        } else {
            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            return $array;
        }
    }

    // salvar registro com o código da transação (vindo do lightbox)
    // consultar transação
    // [GET] https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/
    public function transactionSave(Request $request)
    {

        // código da TRANSAÇÃO.
        $transactionCode = str_replace("-", "", $request->input("transactionCode"));
        $transactionUrl = "https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/";

        // 1. Verificar status do checkout.
        $checkoutStatus = $this->transactionStatus($transactionUrl, $transactionCode);

        // 2. atualiza o status da compra
        if ($checkoutStatus["status"]) {
            // 2.1 busca compra pelo código de transação.
            $compra = Compra::whereTransacao($transactionCode)->first();
            if ($compra) {
                // 2.2 atualizar status.
                $compra["status"] = $checkoutStatus["status"];
                $compra->save();
            } else {
                // 2.3 criar nova compra com código da transação, id do usuário, status da transação.
                $compra = new Compra();
                $compra->transacao = str_replace("-", "", $checkoutStatus["code"]);
                $compra->email = $checkoutStatus["sender"]["email"];
                $compra->produto_id = $checkoutStatus["items"]["item"]["id"];
                $compra->metodo = $checkoutStatus["paymentMethod"]["type"];
                $compra->status = $checkoutStatus["status"];
                $compra->boleto = $checkoutStatus["paymentLink"];
                $compra->valor = $checkoutStatus["grossAmount"];
                $compra->save();
            }
        }

        return ["success" => true, "compra" => $compra];
    }

    // o postback do pagarme envia o código da notificação.
    // consultar transação
    // [GET] https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/notifications/
    public function postback(Request $request)
    {
        // Código da Notificação.
        $notificationCode = $request->input("notificationCode");
        $notificationUrl = "https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/notifications/";

        // 1. verificar status do checkout.
        $checkoutStatus = $this->transactionStatus($notificationUrl, $notificationCode);

        // 2. atualiza o status da compra
        if ($checkoutStatus) {
            // 2.1 busca compra pelo código de transação.
            $compra = Compra::whereTransacao(str_replace("-", "", $checkoutStatus["code"]))->first();
            if ($compra) {
                // 2.2 atualizar status.
                $compra["status"] = $checkoutStatus["status"];
                $compra->save();
            } else {
                // 2.3 criar nova compra com código da transação, id do usuário, status da transação.
                $compra = new Compra();
                $compra->postback = json_encode($checkoutStatus);
                $compra->save();
                $compra->transacao = str_replace("-", "", $checkoutStatus["code"]);
                $compra->email = $checkoutStatus["sender"]["email"];
                $compra->produto_id = $checkoutStatus["items"]["item"]["id"];
                $compra->metodo = $checkoutStatus["paymentMethod"]["type"];
                $compra->status = $checkoutStatus["status"];
                $compra->boleto = $checkoutStatus["paymentLink"];
                $compra->valor = $checkoutStatus["grossAmount"];
                $compra->save();
            }
        }
    }

    private function transactionStatus($base_url, $code)
    {
        $code = str_replace("-", "", $code);
        $curl = curl_init();

        $checkout = [
            CURLOPT_URL => "$base_url$code?email={$this->pagseguro_vendedor}&token={$this->pagseguro_token}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ];

        curl_setopt_array($curl, $checkout);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ["erro" => $err];
        } else {
            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            return $array;
        }
    }
}
