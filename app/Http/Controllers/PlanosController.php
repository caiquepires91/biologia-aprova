<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Admin;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Cupom;
use App\Models\CupomUsuario;
use App\Models\Boleto;
use App\Models\Venda;
use App\Models\PlanoAtivo;
use App\Models\Postback;
use App\Models\Login;
use App\Models\Transacao;
use App\Models\UsuarioTeste;
use Illuminate\Support\Str;
use DateTime;
use Session;
use PagarMe\Client;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use App\Mail\AguardandoPagamento;
use App\Jobs\SendEmail;
use RDStation\RDStation;
// use bubbstore\RDStation\RD;
// use bubbstore\RDStation\Exceptions\RDException;

class PlanosController extends Controller
{
    private $USER_SESSION = null;
    private $PLANO_APROVA_MAIS = "CURSO TOTAL";

    public function __construct(Request $request) {
         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
             // verifica se usuário possui plano ativo para eliminar botão 'teste grátis'.
             if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  $this->USER_SESSION->plano = $planoAtivo;
                // Salva todas as assinaturas (somente ativas);
                $this->USER_SESSION->planos = $this->getPlanosValidos($this->USER_SESSION->id);
                if (isset($this->USER_SESSION->planos) && sizeof($this->USER_SESSION->planos) > 0)
                    $this->USER_SESSION->plano = $this->USER_SESSION->planos[0];
            }
        }
         /* /return user if there is a session open */
    }

    public function index(Request $request) {
        return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos', 'user'=> $this->USER_SESSION ]);
    }

    private function getPlanosValidos($userId) {
        $today = (new DateTime('now'))->format('Y-m-d');
        $planosValidos = PlanoAtivo::whereRaw("idUser = ? AND (status = ? OR status IS NULL)", [$userId, config('constants.plano.pago')])->whereDate('fim','>=', $today)->get();
        return $planosValidos ? $planosValidos : [];
    }

    public function teste(Request $request) {
        if ($this->USER_SESSION != null) {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'teste', 'user'=> $this->USER_SESSION ]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login', 'next_route'=>'conta/curso']);
        }
    }

    public function try() {
        if ($this->USER_SESSION != null) {
            $user = $this->USER_SESSION;
            $planoAtivo = PlanoAtivo::where('idUser', $user->id)->latest('id')->first();
            if ($planoAtivo != null) {
                // verificar se status do plano é 'try'
                if ($planoAtivo->status == 'try') {
                    $planoDate = new DateTime($planoAtivo->fim);
                    $today = new DateTime("now");
                    // verifica se o plano venceu (!= redirect pra pagamento)
                    if ($planoDate >= $today) {
                        $result = ['success'=>false, 'message'=>'você já está fazendo o teste grátis.'];
                    } else {
                        $result = ['success'=>false, 'message'=>'você já realizou o teste grátis.'];
                    }
                } else {
                    $result = ['success'=>false, 'message'=>'você já possui um plano em andamento.'];
                }
            } else {
                $today = date('Y-m-d');
                $planoAtivo = new PlanoAtivo();
                $planoAtivo->idUser = $user->id;
                $planoAtivo->plano = "TESTE GRATIS";
                $planoAtivo->tempo = 7;
                $planoAtivo->inicio = $today;
                $planoAtivo->fim = date('Y-m-d', strtotime("+7 days", strtotime($today)));
                $planoAtivo->pagamento = 'try';
                $planoAtivo->status = 'try';
                $planoAtivo->save();

                if ($planoAtivo != null) {
                    // registra teste grátis.
                    $usuarioTeste = new UsuarioTeste();
                    $usuarioTeste->idUser = $user->id;
                    $usuarioTeste->plano = "EXTENSIVO MED";
                    $usuarioTeste->dias = 7;
                    $usuarioTeste->data_inicio = $today;
                    $usuarioTeste->data_fim = date('Y-m-d', strtotime("+7 days", strtotime($today)));
                    $usuarioTeste->horario = new DateTime("now");
                    $usuarioTeste->deletado = false;
                    $usuarioTeste->save();

                   /*  if ($usuarioTeste != null) {
                        $this->updateLead($user);
                        $this->sendLead($user);
                    } */
                }
            }

            if ($planoAtivo != null) {
                $user->plano = $planoAtivo;
                $result = ['success'=>true, 'user'=>$user];
            } else {
                $result = ['success'=>false, 'message'=>'Algo não saiu como esperado. Por favor, tente mais tarde.'];
            }
        } else {
            $result = ['success'=>false, 'message'=>'Você não está logado. Por favor, atualize a página, faça login e tente de novo.'];
        }

        return $result;
    }

    private function createFreeTry($user) {
        $rdResponse = null;
        $planoAtivo = PlanoAtivo::where('idUser', $user->id)->latest('id')->first();
        if ($planoAtivo != null) {
            // verificar se status do plano é 'try'
            if ($planoAtivo->status == 'try') {
                $planoDate = new DateTime($planoAtivo->fim);
                $today = new DateTime("now");
                // verifica se o plano venceu (!= redirect pra pagamento)
                if ($planoDate >= $today) {
                    $result = ['success'=>false, 'message'=>'você já está fazendo o teste grátis.'];
                } else {
                    $result = ['success'=>false, 'message'=>'você já realizou o teste grátis.'];
                }
            } else {
                $result = ['success'=>false, 'message'=>'você já possui um plano em andamento.'];
            }
        } else {
            $today = date('Y-m-d');
            $planoAtivo = new PlanoAtivo();
            $planoAtivo->idUser = $user->id;
            $planoAtivo->plano = "TESTE GRATIS";
            $planoAtivo->tempo = 7;
            $planoAtivo->inicio = $today;
            $planoAtivo->fim = date('Y-m-d', strtotime("+7 days", strtotime($today)));
            $planoAtivo->pagamento = 'try';
            $planoAtivo->status = 'try';
            $planoAtivo->save();

            if ($planoAtivo != null) {
                // registra teste grátis.
                $usuarioTeste = new UsuarioTeste();
                $usuarioTeste->idUser = $user->id;
                $usuarioTeste->plano = "EXTENSIVO MED";
                $usuarioTeste->dias = 7;
                $usuarioTeste->data_inicio = $today;
                $usuarioTeste->data_fim = date('Y-m-d', strtotime("+7 days", strtotime($today)));
                $usuarioTeste->horario = new DateTime("now");
                $usuarioTeste->deletado = false;
                $usuarioTeste->save();

              /*   if ($usuarioTeste != null) {
                     $rdResponse->update =  $this->updateLead($user);
                     $rdResponse->send = $this->sendLead($user);
                } */
            }
        }

        if ($planoAtivo != null) {
            $user->plano = $planoAtivo;
            $result = ['success'=>true, 'user'=>$user, 'rd'=>$rdResponse];
        } else {
            $result = ['success'=>false, 'message'=>'Algo não saiu como esperado. Por favor, tente mais tarde.'];
        }

        return $result;
    }

    // rota alternativa pra salvar variável plano na sessão
    public function choose(Request $request) {
        $plano = $request->route('plano');
        $desconto = $request->route('desconto');
        if ($plano) {
            $request->session()->put('plano', $plano);
        }
        if ($desconto) {
            $request->session()->put('desconto', $desconto);
        }
        return redirect()->route('pagamento-page');
    }

    public function pagamentoPage(Request $request) {
        $estados = Estado::all();
        // verifica se há plano escolhido flashed na sessão.
        $idPlano = $request->session()->get('plano');
        // verifica se há desconto flashed na sessão.
        $desconto = $request->session()->get('desconto');
        $user = $this->USER_SESSION;
        if ($user) {
            $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
            if ($boleto != null) {
                $today = new DateTime("now");
                $vencimento = new DateTime($boleto->expirar);
                if ($vencimento < $today) $boleto = null;
            }
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'estados'=>$estados, 'boleto'=>$boleto, 'user'=>$user, 'idPlano'=> ($idPlano ? $idPlano : ""), 'desconto'=> ($desconto ? $desconto : 0)]);
        }
        else
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login', 'desconto'=> ($desconto ? $desconto : 0)]);
    }

    private function hasPlanoExpired($plano) {
        $today = new DateTime("now");
        $planoDate = new DateTime($plano->fim);
        if ($planoDate >= $today) {
            return true;
        } else {
            return false;
        }
    }

    // renovar plano
    // permitir usuário possuir mais planos ativos
    // inicio :fim do ultimo plano, fim: inicio + x tempo
    public function loginPage(Request $request) {
        $estados = Estado::all();

        // verifica se há plano escolhido flashed na sessão.
        $idPlano = $request->session()->get('plano');
        // verifica se usuário correto está logado
        $user = $this->USER_SESSION;
        if ($user != null) {
            // verifica se usuário possui um plano (!= redirect pra pagamento)
            $planoAtivo = $user->plano;
            if ($planoAtivo != null) {
                $user->plano = $planoAtivo;

                // verificar se o pagamento foi confirmado ou verifica se é um plano antigo (status = null).
                if ($planoAtivo->status == 'paid' || $planoAtivo->status == null || $planoAtivo->status == 'try') {

                    $planoDate = new DateTime($planoAtivo->fim);
                    $today = new DateTime("now");
                    if ($planoDate >= $today) {
                        return redirect()->route('curso');
                    }
                } else if ($planoAtivo->status = 'waiting_payment' && $planoAtivo->pagamento == 'boleto') {
                    $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
                    if ($boleto != null) {
                        $today = new DateTime("now");
                        $vencimento = new DateTime($boleto->expirar);
                        if ($vencimento >= $today) {
                            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user, 'estados'=>$estados, 'boleto'=>$boleto, 'idPlano'=>$idPlano]);
                        }
                    }
                }
            }

            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'estados'=>$estados, 'user'=>$user, 'idPlano'=> ($idPlano ? $idPlano : "")]);
        }

        return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);

    }

    public function login(Request $request) {
        $nextRoute = $request->input("next_route");
        $responseTry = null;

        $user = User::whereEmail($request->input('email'))->latest('id')->first();
        if ($user != null) {
            if (md5($request->input('senha')) == $user->senha) {
                // registra sessão.
                $request->session()->put('userId', $user->id);

                // registra login.
                $login = new Login();
                $login->idUser = $user->id;
                $login->data = new DateTime("now");
                $login->save();

                // verifica se houve solicitação de plano grátis;
                if ($nextRoute == 'conta/curso') {
                    $responseTry = $this->createFreeTry($user);
                }

                // vincula plano caso haja algum.
                $planoAtivo = PlanoAtivo::where('idUser', $user->id)->latest('id')->first();
                if ($planoAtivo != null) $user->plano = $planoAtivo;

                $result = ['success'=>true, 'user'=>$user, 'next_route'=>$nextRoute, "try"=>$responseTry];
            } else {
                $result = ['success'=>false];
            }
        } else {
            $result = ['success'=>false];
        }
        return response()->json($result);
    }

    public function logout(Request $request) {
        if ($request->session()->has("userId")) {
            $user = User::find($request->session()->get("userId"));
            $request->session()->forget("userId");
            $request->session()->forget("popup_layout_dismissed");
            if (!$request->session()->has("userId")) {
                return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
            } else {
                return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user]);
            }
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    public function cadastrarPage(Request $request) {
        $email = $request->input("email");
        $user = User::whereEmail($email)->first();
        if ($user != null) {
            $result = ['success'=>false, 'message'=>'usuário já possui cadastro!'];
        } else {
            $estados = Estado::all();
            $result = ["success"=>true, 'email'=>$email, 'estados'=>$estados];
        }

        return $result;
    }

    public function cadastrar(Request $request) {
        $responseTry = null;
        $nextRoute = $request->input("next_route");
        $nome = $request->input('nome');
        $email = $request->input('email');
        $cpf = $this->limpaCPF($request->input('cpf'));
        $nascimento = $request->input('nascimento') ? $this->format($request->input('nascimento')) : null;
        $estado = $request->input('estado');
        $cidade = $request->input('cidade');
        $cep = $request->input('cep');
        $cpf_cartao = $request->input('cpf-cartao');
        $email_cartao = $request->input('email-cartao');
        $telefone = $request->input('telefone');
        $endereco = $request->input('endereco');
        $numero = $request->input('numero');
        $bairro = $request->input('bairro');
        $senha = $request->input('senha');

        $user = User::whereEmail($email)->first();
        if ($user) {
            $result = ["success"=>true, "wasCadastrado"=>true, "user"=>$user];
        } else {
            $user = new User();
            $user->nome = $nome;
            $user->email = $email;
            $user->cpf = $this->limpaCPF($cpf);
            //$date = new DateTime("06/18/1991");
            $user->datanascimento = $nascimento;
            $user->estado = $estado;
            $user->cidade = $cidade;
            $user->cep = $cep;
            $user->cpf_cartao = $this->limpaCPF($cpf_cartao);
            $user->email_cartao = $email_cartao;
            $user->endereco = $endereco;
            $user->numero = $numero;
            $user->bairro = $bairro;
            $user->telefone = $telefone;
            $user->senha = md5($senha);
            $user->tipo = 0;
            $user->foto = "perfil.jpg";
            $user->deletado = 0;
            $user->save();
            if ($user) {
                // registra sessão.
                $request->session()->put('userId', $user->id);

                // registra login.
                $login = new Login();
                $login->idUser = $user->id;
                $login->data = new DateTime("now");
                $login->save();

                 // verifica se houve solicitação de plano grátis;
                if ($nextRoute == 'conta/curso') {
                    $responseTry = $this->createFreeTry($user);
                }
                $result = ["success"=>true, "wasCadastrado"=>false, "user"=>$user, "next_route"=>$nextRoute, "try"=>$responseTry];
            } else {
                $result = ["success"=>false];
            }
        }

        return response()->json($result);
    }

    public function atualizarDadosCartao(Request $request) {
        $result = ["success"=>false];

        $id = $request->input('id');
        $estado = $request->input('estado');
        $endereco = $request->input('endereco');
        $bairro = $request->input('bairro');
        $cidade = $request->input('cidade');
        $numero = $request->input('numero');
        $cep = $request->input('cep');
        $cpf_cartao = $request->input('cpf-cartao');
        $email_cartao = $request->input('email-cartao');

        $user = User::find($id);
        if ($user) {
            $user->estado = $estado;
            $user->cidade = $cidade;
            $user->cep = $cep;
            $user->bairro = $bairro;
            $user->endereco = $endereco;
            $user->numero = $numero;
            $user->cpf_cartao = $this->limpaCPF($cpf_cartao);
            $user->email_cartao = $email_cartao;
            $user->save();
            $result = ["success"=>true, "user"=>$user];
        }

        return response()->json($result);
    }

    public function cidades(Request $request) {
        $codEstado = $request->input('estado');
        $cidades = Cidade::where('estados_cod_estados', $codEstado)->get();
        if ($cidades) {
            $result = ['success'=>true, 'cidades'=>$cidades];
        } else {
            $result = ['success'=>false];
        }
        return $result;
    }

    private function format($date) {
        $collection = Str::of($date)->split('/[\s\/]+/');
        $day = $collection->values()->get(0);
        $month = $collection->values()->get(1);
        $year = $collection->values()->get(2);
        return $month . '/' . $day . '/' . $year;
    }

    private function limpaCPF($valor){
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }

    public function pagamento() {
        if ($request->session()->has("userId")) {
            $user = User::find($request->session()->get("userId"));
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    public function boleto(Request $request) {
        $planoDesconto = null;

        if ($request->session()->has('userId')) {
            $userId = $request->session()->get('userId');
            $user = User::find($userId);
            $boleto = Boleto::find($request->route('id'));
            if ($boleto->idAssinatura)
                $planoAtivo = PlanoAtivo::find($boleto->idAssinatura);
            else
                $planoAtivo = PlanoAtivo::where('idUser', $boleto->idUser)->latest('id')->first();

            if ($boleto != null) {
                if ($boleto->cupom != null) {
                    $cupom = Cupom::find($boleto->cupom);
                    if ($cupom != null) {
                        $boleto->cupom = $cupom;
                        if ($planoAtivo != null) $planoAtivo->cupom = $cupom;
                        // calcular valor com desconto
                        $planoDesconto = (object) $this->setPlanoByCupom($cupom);
                        //$boleto->planoDesconto = $planoDesconto;
                    }
                }

                if ($planoAtivo != null) {
                    $user->plano = $planoAtivo;
                    if ($planoDesconto != null) $user->plano->planoDesconto = $planoDesconto;
                }

                $user->boleto = $boleto;
                return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-boleto', 'user'=>$user, 'boleto'=>$boleto]);
            } else {
                return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
            }
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    private function setPlanoByCupom($cupom) {
        if ($cupom->idPlano == 5) {
            // Plano 5 - 26280.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 26280 - (26280 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 26280 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO COMPLETO";
            $tempo = "12";
        } else {
            // Plano 6 - 4320.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 4320 - (4320 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 4320 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO TOTAL";
            $tempo = "12";
        }

        return ['plano'=>$plano, 'tempo'=>$tempo, 'valor'=>$valorDesc, 'desconto'=>$desconto];
    }

    public function isCupomValid($cupom, $plano) {
        $cupom = Cupom::whereCupom($cupom)->where('idPlano', $plano)->first();

        if ($cupom) {
            if ($this->USER_SESSION != null)
                $cupomUsuario = CupomUsuario::where('idUser',$this->USER_SESSION->id)->where('idCupom', $cupom->id)->whereUtilizado(true)->first();
            else
                $cupomUsuario = null;

            if ($cupomUsuario == null) {
                $inicio = new DateTime($cupom->data_inicio);
                $fim = new DateTime($cupom->data_fim);
                $today = new DateTime("now");
                if ( $today >= $inicio && $today < $fim) {
                    $result = ['success'=>true, 'cupom'=>$cupom];
                } else {
                    $result = ['success'=>false, 'message'=>'cupom expirou ou ainda não está ativo.'];
                }
            } else {
                $result = ['success'=>false, 'message'=>'cupom já foi utilizado.'];
            }
        } else {
            $result = ['success'=>false, 'message'=>'cupom inválido.'];
        }

        return $result;
    }

    public function isCupomValidOld($cupom, $plano) {
        if ($this->USER_SESSION != null) {
             // plano ainda não registrado no bd.
            $cupom = Cupom::whereCupom($cupom)->where('idPlano', $plano)->first();

            if ($cupom) {
                $cupomUsuario = CupomUsuario::where('idUser',$this->USER_SESSION->id)->where('idCupom', $cupom->id)->whereUtilizado(true)->first();
                if ($cupomUsuario == null) {
                    $inicio = new DateTime($cupom->data_inicio);
                    $fim = new DateTime($cupom->data_fim);
                    $today = new DateTime("now");
                    if ( $today >= $inicio && $today < $fim) {
                        $result = ['success'=>true, 'cupom'=>$cupom];
                    } else {
                        $result = ['success'=>false, 'message'=>'cupom expirou ou ainda não está ativo.'];
                    }
                } else {
                    $result = ['success'=>false, 'message'=>'cupom já foi utilizado.'];
                }
            } else {
                $result = ['success'=>false, 'message'=>'cupom inválido.'];
            }
        } else {
            $result = ['success'=>false, 'message'=>'usuário não está logado.'];
        }

        return $result;
    }

    public function geraDescontoString($cupom) {
        return $cupom->tipo_desconto ? $cupom->desconto . "%" : "R$" . $cupom->desconto;
    }

    // Vendas e renovações dos planos são setadas aqui.
    public function handleTransacao($plano, $transacao_id, $status, $method, $user, $amount, $cupom) {
        // $resultado = 1 - pago novo plano, 2 - plano renovado.
        $resultado = 0;
        $transacao = Transacao::where('idTransacao', '=', $transacao_id)->first();
        if ($transacao) {
            // gerencia transacões existentes não finalizadas.
            $transacao->atualizacoes = $transacao->atualizacoes + 1;
            if (!$transacao->finalizada) {
                $transacao->status = $status;
                $transacao->pagamento = $method;
                if ($status == config('constants.plano.pago')) {
                    $transacao->finalizada = 1;
                    $resultado = 1;
                    $plano = PlanoAtivo::find($transacao->idAssinatura);
                    if ($plano) {
                        if ($plano->status == config('constants.plano.pago')) {
                            $plano->fim = date('Y-m-d', strtotime("+$plano->tempo months", strtotime($plano->fim)));
                            $plano->renovacoes = $plano->renovacoes + 1;
                            $resultado = 2;
                        }
                        $plano->status = config('constants.plano.pago');
                        $plano->save();
                    }
                    // $this->registrarVenda($user, $plano, $amount, $cupom);
                }
            }
        } else {
            // gerencia novas transações.
            $transacao = new Transacao();
            $transacao->idAssinatura = $plano->id;
            $transacao->idTransacao = $transacao_id;
            $transacao->status = $status;
            $transacao->pagamento = $method;
            if ($cupom) $transacao->idCupom = $cupom->id;
            if ($status == config('constants.plano.pago')) {
                $transacao->finalizada = 1;
                $resultado = 1;
                $plano = PlanoAtivo::find($transacao->idAssinatura);
                if ($plano) {
                    if ($plano->status == config('constants.plano.pago')) {
                        $plano->fim = date('Y-m-d', strtotime("+$plano->tempo months", strtotime($plano->fim)));
                        $plano->renovacoes = $plano->renovacoes + 1;
                        $resultado = 2;
                    }
                    $plano->status = config('constants.plano.pago');
                    $plano->save();
                }
                // $this->registrarVenda($user, $plano, $amount, $cupom);
            }
        }
        $transacao->save();
        $transacao->resultado = $resultado;
        return $transacao;
    }

    public function registrarVenda($user, $planoAtivo, $amount, $cupom, $method) {
        $venda = new Venda();
        $venda->idUser = $user->id;
        $venda->plano =  $planoAtivo->plano;
        $venda->valor = $amount/100;
        $venda->tempo =  $planoAtivo->tempo;
        $venda->data =  date("Y-m-d H:i:s");
        $venda->metodo_pagamento = $method == config("constants.pagamento.boleto") ? 1 : 0;
        if ($cupom != null) {
            $venda->desconto = $this->geraDescontoString($cupom);
        }
        $venda->save();
        return $venda;
    }

    public function pagarme(Request $request) {
        $CADASTRO_REALIZADO = false;
        $RENOVACAO = false;
        $user = User::find($request->session()->get("userId"));
        $token =  $request->input("token");
        $method = $request->input("payment_method");
        $planoValor = $request->input("plano_valor");
        $cupom =  $request->input("cupom");
        if ($cupom != null) $cupom = (object) $cupom;
        $pagarme = new Client(env('PAGARME_API_KEY'));
        $capTrans = $pagarme->transactions()->capture([
            'id' => $token,
            'amount' => $planoValor
        ]);
        $transId = $capTrans->id;
        $exteralId = $capTrans->customer->external_id;
        $dataHoje = date('Y-m-d');
        $dataFim = "";
        $result = ['success'=>false, 'status'=> $capTrans->status, 'transId'=> $capTrans->id, 'exteralId'=> $exteralId , 'constatn'=>config('constants.planos.aguardando'), 'message'=>'status da transação diferente de paid, authorized ou waiting_payment'];

        if ($user != null) {
            // gera pre plano pelo valor do postback e pelo cupom enviado por post (diferente do postback).
            $prePlano = $this->setPlanoByValor($capTrans->amount, $cupom);

            // 1 - verificar se já há um plano ativo nessa modalidade.
            $planoAtivo = $this->getPlanoByUser($exteralId, $prePlano["plano"]);
            if (!$planoAtivo) {
                $planoAtivo = new PlanoAtivo();
                $planoAtivo->idUser = $user->id;
                $planoAtivo->idTransacao = $transId;
                $planoAtivo->plano = $prePlano["plano"];
                $planoAtivo->tempo = $prePlano["tempo"];
                $planoAtivo->inicio = $prePlano["dataHoje"];
                $planoAtivo->fim = $prePlano["dataFim"];
                $planoAtivo->pagamento = $method;
                $planoAtivo->status = $capTrans->status;
                $planoAtivo->save();

                $CADASTRO_REALIZADO = true;

                // // plano CURSO TOTAL para quem comprar até o dia 15/03
                // $dataHoje = date('Y-m-d');
                // $dataAprovaMais = date('Y-m-d', strtotime("2022-03-20"));
                // // $dataAprovaMais = date('Y-m-d', strtotime(config('constants.outras.deadline_aprova_mais')));
                // if ($dataHoje <= $dataAprovaMais && $prePlano["plano"] == "CURSO COMPLETO") {
                //     $planoAprovaMais = new PlanoAtivo();
                //     $planoAprovaMais->plano = $this->PLANO_APROVA_MAIS;
                //     $planoAprovaMais->tempo = $prePlano["tempo"];
                //     $planoAprovaMais->inicio = $prePlano["dataHoje"];
                //     $planoAprovaMais->fim = $prePlano["dataFim"];
                //     $planoAprovaMais->pagamento = $method;
                //     $planoAprovaMais->status = $capTrans->status;
                //     $planoAprovaMais->save();
                //     $transacaoAprovaMais = $this->handleTransacao($planoAtivo, $transId, $capTrans->status, $method);
                //     $planoAprovaMais->transacao = $transacaoAprovaMais;
                //     $planoAtivo->aprovaMais = $planoAprovaMais;
                // }
            }

            // 2 - verificar se há cupom para ser vinculado.
            if ($cupom != null) {
                $this->handleCupomUsuario($cupom, $user, $prePlano["plano"], false);
                $planoAtivo->idCupom = $cupom->id;
                $planoAtivo->save();
            }

            // 3 - gerencia a transação (se é necessário criar ou atualizar - renovações do plano).
            $transacao = $this->handleTransacao($planoAtivo, $transId, $capTrans->status, $method, $user, $capTrans->amount, $cupom);
            $planoAtivo->transacao = $transacao;

            // transação status PAGO.
            if ($capTrans->status == config('constants.plano.pago') && $transacao->resultado) {
                // SEND E-MAIL WITH RESULTS
                $subject = "Pagamento aprovado!";
                $email_remetente = "contato@biologiaaprova.com.br";
                $dataHoje = $prePlano["dataHoje"];
                $dataFim = $prePlano["dataFim"];
                $view = 'emails.pago';
                if ($dataFim == "") $dataFim = "0000-00-00";
                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $dataFim, $view));

                $COMPRA_REALIZADA = false;
                $result = ['success'=>true, 'plano'=>$planoAtivo, 'renovacao'=>$RENOVACAO, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'compra'=>$COMPRA_REALIZADA, 'status'=>$capTrans->status, 'cupom'=>$cupom];
            }

            // transação status AUTORIZADO.
            if ($capTrans->status == config('constants.plano.autorizado')) {

                // SEND E-MAIL WITH RESULTS
                $subject = "Aguardando Confirmação";
                $email_remetente = "contato@biologiaaprova.com.br";
                $dataFim = $prePlano["dataFim"];
                $dataHoje = $prePlano["dataHoje"];
                $view = 'emails.aguardando';
                if ($dataFim == "") $dataFim = "0000-00-00";
                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $dataFim, $view));

                $COMPRA_REALIZADA = false;
                $result = ['success'=>true, 'plano'=>$planoAtivo, 'renovacao'=>$RENOVACAO, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'compra'=>$COMPRA_REALIZADA, 'status'=>$capTrans->status, 'cupom'=>$cupom];
            }

            // transação status AGUARDANDO.
            if ($capTrans->status == config('constants.plano.aguardando')) {
                $url_boleto = $capTrans->boleto_url;

                $dataFim = date('Y-m-d', strtotime("+2 days", strtotime($prePlano["dataHoje"])));
                $boleto = new Boleto();
                $boleto->idUser = $user->id;
                $boleto->idAssinatura = $planoAtivo->id;
                $boleto->boleto = $url_boleto;
                $boleto->expirar = $dataFim;
                if ($cupom != null) $boleto->cupom = $cupom->id;
                $boleto->save();

                // SEND E-MAIL WITH RESULTS
                $subject = "Pagamento aprovado!";
                $email_remetente = "contato@biologiaaprova.com.br";
                $dataHoje = $prePlano["dataHoje"];
                $dataFim = $prePlano["dataFim"];
                $view = 'emails.pago';
                //($from, $to, $subject, $nome, $tempo, $plano, $inicio, $fim, $view)
                if ($dataFim == "") $dataFim = "0000-00-00";
                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $dataFim, $view));

                $COMPRA_REALIZADA = false;
                $result = ['success'=>true, 'plano'=>$planoAtivo, 'boleto'=>$boleto,'renovacao'=>$RENOVACAO, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'status'=>$capTrans->status, 'cupom'=>$cupom];
            }

            if ($transacao->resultado) {
                $this->registrarVenda($user, $planoAtivo, $capTrans->amount, $cupom, $method);
            }
        } else {
            $result = ['success'=>false, 'message'=>'usuário não encontrado na sessão'];
        }

        return $result;
    }

    public function pagarmeBD(Request $request) {
        $CADASTRO_REALIZADO = false;
        $COMPRA_REALIZADA = false;
        $RENOVACAO = false;
        $user = User::find($request->session()->get("userId"));

        $token =  $request->input("token");
        $method = $request->input("payment_method");
        // verificar se é seguro utilizar essa variável
        $planoValor = $request->input("plano_valor");
        $cupom =  $request->input("cupom");
        if ($cupom != null) $cupom = (object) $cupom;

        $pagarme = new Client(env('PAGARME_API_KEY'));
        //$pagarme = new Client('ak_test_zorlWIVpWuTUymZyzE14hE4B4khy0A');
        $capTrans = $pagarme->transactions()->capture([
            'id' => $token,
            'amount' => $planoValor
        ]);
        $transId = $capTrans->id;

        $dataHoje = date('Y-m-d');
        $dataFim = "";
        $plano = "";
        $tempo = "";
        $desconto = "";
        $p = "";

        $result = ['success'=>false, 'status'=> $capTrans->status, 'transId'=> $capTrans->id, 'message'=>'status da transação diferente de paid, authorized ou waiting_payment'];

        if ($user != null) {
            if ($capTrans->status == "paid") {
                // gera pre plano pelo valor do postback e pelo cupom enviado por post (diferente do postback).
                $prePlano = $this->setPlanoByValorBD($capTrans->amount, $cupom);
                $planoAtivo = $this->createPlanoAtivo($user, $prePlano, $method, $capTrans, $cupom);
                if ($planoAtivo) {
                    $CADASTRO_REALIZADO = true;
                }

                $venda = new Venda();
                $venda->idUser = $user->id;
                $venda->idPlano = $prePlano->id;
                $venda->plano =  $planoAtivo->plano;
                $venda->valor = $capTrans->amount/100;
                $venda->tempo =  $planoAtivo->tempo;
                $venda->data =  $dataHoje;
                $venda->metodo_pagamento = 1;
                if ($cupom != null) {
                    $venda->desconto = $cupom->desconto;
                }
                $venda->save();

                // SEND E-MAIL WITH RESULTS
                $subject = "Pagamento aprovado!";
                $email_remetente = "contato@biologiaaprova.com.br";
                $dataFim = $prePlano->dataFim;
                $view = 'emails.pago';

                //($from, $to, $subject, $nome, $tempo, $plano, $inicio, $fim, $view)
                if ($dataFim == "") $dataFim = "0000-00-00";
                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $dataFim, $view));

                $result = ['success'=>true, 'plano'=>$planoAtivo, 'venda'=>$venda, 'renovacao'=>$RENOVACAO, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'compra'=>$COMPRA_REALIZADA, 'status'=>$capTrans->status, 'cupom'=>$cupom];
            }

            if ($capTrans->status == "authorized") {
                // gera pre plano pelo valor do postback e pelo cupom enviado por post (diferente do postback).
                $prePlano = $this->setPlanoByValorBD($capTrans->amount, $cupom);
                $planoAtivo = $this->createPlanoAtivo($user, $prePlano, $method, $capTrans, $cupom);
                if ($planoAtivo) {
                    $CADASTRO_REALIZADO = true;
                }

                $result = ['success'=>true, 'plano'=>$planoAtivo, 'renovacao'=>false, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'compra'=>$COMPRA_REALIZADA, 'status'=>$capTrans->status, 'cupom'=>$cupom];

                // SEND E-MAIL WITH RESULTS
                $subject = "Aguardando Confirmação";
                $email_remetente = "contato@biologiaaprova.com.br";
                $dataFim = $prePlano->dataFim;
                $view = 'emails.aguardando';

                //($from, $to, $subject, $nome, $tempo, $plano, $inicio, $fim, $view)
                if ($dataFim == "") $dataFim = "0000-00-00";
                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $dataFim, $view));
            }

            if ($capTrans->status == "waiting_payment") {

                $url_boleto = $capTrans->boleto_url;

                // gera pre plano pelo valor do postback e pelo cupom enviado por post (diferente do postback).
                $prePlano = $this->setPlanoByValorBD($capTrans->amount, $cupom);
                $planoAtivo = $this->createPlanoAtivo($user, $prePlano, $method, $capTrans, $cupom);
                if ($planoAtivo) {
                    $CADASTRO_REALIZADO = true;
                }

                $dataFim = date('Y-m-d', strtotime("+2 days", strtotime($dataHoje)));

                $boleto = new Boleto();
                $boleto->idUser = $user->id;
                $boleto->idAssinatura = $planoAtivo->id;
                $boleto->boleto = $url_boleto;
                $boleto->expirar = $dataFim;
                if ($cupom != null) $boleto->cupom = $cupom->id;
                $boleto->save();

                $result = ['success'=>true, 'plano'=>$planoAtivo, 'boleto'=>$boleto,'renovacao'=>false, 'cadastroRealizado'=>$CADASTRO_REALIZADO, 'status'=>$capTrans->status, 'cupom'=>$cupom];

                // SEND E-MAIL WITH RESULTS

                $subject = "Aguardando Pagamento";
                $email_remetente = "contato@biologiaaprova.com.br";
                // the following will garantee fim is related to the plan and not to the boleto.
                $fim =  date('Y-m-d', strtotime("+10 month", strtotime($dataHoje)));
                $view = 'emails.aguardando';
                /* Queue::push(new SendEmail($email_remetente, $user->email,
                    $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                    $dataHoje, $fim, $view)); */

                Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                $dataHoje, $fim, $view));

            }
        } else {
            $result = ['success'=>false, 'message'=>'usuário não encontrado na sessão'];
        }

        return $result;
    }

    public function createPlanoAtivo($user, $prePlano, $method, $capTrans, $cupom) {
        $dataHoje = date("Y-m-d");
        $planoAtivo = new PlanoAtivo();
        $planoAtivo->idUser = $user->id;
        $planoAtivo->idTransacao = $capTrans->id;
        $planoAtivo->idPlano = $prePlano->id;
        $planoAtivo->plano = $prePlano->plano;
        $planoAtivo->tempo = $prePlano->tempo;
        $planoAtivo->inicio = $dataHoje;
        $planoAtivo->fim = $prePlano->dataFim;
        $planoAtivo->pagamento = $method;
        $planoAtivo->status = $capTrans->status;
        if ($cupom != null) {
            $cupomUsuario = $this->handleCupomUsuarioBD($cupom, $user, $prePlano, true);
            $planoAtivo->idCupom = $cupom->id;
        }
        $planoAtivo->save();

        // plano CURSO TOTAL para quem comprar até o dia 15/03
        $dataAprovaMais = date('Y-m-d', strtotime("2022-03-20"));
        if ($dataHoje <= $dataAprovaMais && $prePlano->id == 5) {
            $planoAprovaMais = new PlanoAtivo();
            $planoAprovaMais->idUser = $user->id;
            $planoAprovaMais->idTransacao = $capTrans->id;
            $planoAprovaMais->idPlano = $prePlano->id;
            $planoAprovaMais->plano = $prePlano->plano;
            $planoAprovaMais->tempo = $prePlano->tempo;
            $planoAprovaMais->inicio = $dataHoje;
            $planoAprovaMais->fim = $prePlano->dataFim;
            $planoAprovaMais->pagamento = $method;
            $planoAprovaMais->status = $capTrans->status;
            $planoAprovaMais->save();
            $planoAtivo->aprovaMais = $planoAprovaMais;
        }

        return $planoAtivo;
    }

    private function handleCupomUsuario($cupom, $user, $plano, $utilizado){
        $cupomUsuario = CupomUsuario::where('idUser', $user->id)->where('idCupom', $cupom->id)->first();
        if ($cupomUsuario == null) {
            $cupomUsuario = new CupomUsuario();
        }
        $cupomUsuario->idUser = $user->id;
        $cupomUsuario->idCupom = $cupom->id;
        $cupomUsuario->cupom = $cupom->cupom;
        $cupomUsuario->data = new DateTime("now");
        $cupomUsuario->plano = $plano;
        $cupomUsuario->utilizado = $utilizado;
        $cupomUsuario->dataGerado = new DateTime($cupom->created_at);
        $cupomUsuario->save();
        return $cupomUsuario;
    }

    private function handleCupomUsuarioBD($cupom, $user, $plano, $utilizado){
        $cupomUsuario = CupomUsuario::where('idUser', $user->id)->where('idCupom', $cupom->id)->first();
        if ($cupomUsuario == null) {
            $cupomUsuario = new CupomUsuario();
        }
        $cupomUsuario->idUser = $user->id;
        $cupomUsuario->idCupom = $cupom->id;
        $cupomUsuario->cupom = $cupom->cupom;
        $cupomUsuario->data = new DateTime("now");
        $cupomUsuario->plano = $plano->plano;
        $cupomUsuario->idPlano = $plano->id;
        $cupomUsuario->utilizado = $utilizado;
        $cupomUsuario->dataGerado = new DateTime($cupom->created_at);
        $cupomUsuario->save();
        return $cupomUsuario;
    }

    // método para o aguardo do postback do pagarme.
    public function postback(Request $request) {
        $pagarme = new Client(env('PAGARME_API_KEY'));
        $requestBody = $request->getContent();
        $signature = $request->server('HTTP_X_HUB_SIGNATURE');
        $isValidPostback = $pagarme->postbacks()->validate($requestBody, $signature);

        if ($isValidPostback) {
            $transaction = $request->input('transaction');
            $exteralId = $transaction['customer']['external_id'];
            $cupom = null;

            /* teste */
            $postback = new Postback();
            $postback->json = json_encode($request->all());
            $postback->external_id = $exteralId;
            $postback->save();
            /* /teste*/

            if ($transaction != null) {

                $transId = $transaction['id'];
                $status = $transaction['status'];
                $exteralId = $transaction['customer']['external_id'];
                $amount = $transaction['amount'];
                $pagamento = $transaction['payment_method'];
                $planoNome = $transaction['items'][0]['title']; // sempre haverá apenas um item.

                // verificar se há cupom vinculado à transacao.
                $transacao = Transacao::where('idTransacao', '=', $transId)->first();
                if ($transacao && $transacao->idCupom) {
                    $cupom = Cupom::find($transacao->idCupom);
                }

                /* teste*/
                $result = [
                    'teste'=>'transacao',
                    'id'=>$transId,
                    'status'=>$status,
                    'external_id'=>$exteralId,
                    'plano nome' => $planoNome,
                    'amount'=>$amount,
                    'pagamento'=> $pagamento
                ];
                $postback = new Postback();
                $postback->json = json_encode($result);
                $postback->external_id = $exteralId;
                $postback->save();
                /* /teste*/

                if ($status == config('constants.plano.pago')) {
                    $user = User::find(intval($exteralId));
                    if ($user != null) {
                        // gera pre plano pelo valor do postback e pelo cupom enviado por post (diferente do postback).
                        $prePlano = $this->setPlanoByValor($amount, $cupom);

                        /* Testando Postback */
                        $result = [
                            'teste'=>'preplano',
                            'transId'=>$transId,
                            'userId'=>$user->id,
                            'plano pre'=>$prePlano["plano"],
                            'plano nome'=>$planoNome,
                            'valor'=>$amount/100,
                            'tempo'=>$prePlano["tempo"],
                            'inicio'=> $prePlano["dataHoje"],
                            'fim'=> $prePlano["dataFim"],
                            'metodo_pagamento'=> $pagamento
                        ];
                        $postback = new Postback();
                        $postback->json = json_encode($result);
                        $postback->external_id = $exteralId;
                        $postback->save();
                        /* /Testando Postback */

                        // 1 - verificar se já há um plano ativo nessa modalidade.
                        $planoAtivo = $this->getPlanoByUser($exteralId, $planoNome);
                        if (!$planoAtivo) {

                            /* Testando Postback */
                            $result = [
                                'teste'=>'plano novo',
                                'transId' => $transId
                            ];
                            $postback = new Postback();
                            $postback->json = json_encode($result);
                            $postback->external_id = $exteralId;
                            $postback->save();
                            /* /Testando Postback */

                            $planoAtivo = new PlanoAtivo();
                            $planoAtivo->idUser = $user->id;
                            $planoAtivo->idTransacao = $transId;
                            $planoAtivo->plano = $prePlano["plano"];
                            $planoAtivo->tempo = $prePlano["tempo"];
                            $planoAtivo->inicio = $prePlano["dataHoje"];
                            $planoAtivo->fim = $prePlano["dataFim"];
                            $planoAtivo->pagamento = $pagamento;
                            $planoAtivo->status = $status;
                            $planoAtivo->save();
                        }

                        // 2 - verificar se há cupom para ser vinculado.
                        // if ($cupom != null) {
                        //     $this->handleCupomUsuario($cupom, $user, $prePlano["plano"], false);
                        //     $planoAtivo->idCupom = $cupom->id;
                        //     $planoAtivo->save();
                        // }

                        // 3 - gerencia a transação (se é necessário criar ou atualizar - renovações do plano).
                        $transacao = $this->handleTransacao($planoAtivo, $transId, $status, $pagamento, $user, $amount, $cupom);
                        $planoAtivo->transacao = $transacao;

                        // se o plano for PAGO ou RENOVADO.
                        if ($transacao->resultado) {

                            // 4 - adiciona plano CURSO TOTAL para quem comprar até o dia yyyy-mm-dd.
                            // $today = new DateTime("now");
                            // $target_day = new DateTime(config('constants.outras.deadline_aprova_mais'));
                            // if ($today <= $target_day && $planoNome == "CURSO COMPLETO") {
                            //     $planoAprovaMais = new PlanoAtivo();
                            //     $planoAprovaMais->idUser = $user->id;
                            //     $planoAprovaMais->idTransacao = $transId;
                            //     $planoAprovaMais->plano = $this->PLANO_APROVA_MAIS;
                            //     $planoAprovaMais->tempo = $prePlano["tempo"];
                            //     $planoAprovaMais->inicio = $prePlano["dataHoje"];
                            //     $planoAprovaMais->fim = $prePlano["dataFim"];
                            //     $planoAprovaMais->pagamento = $pagamento;
                            //     $planoAprovaMais->status = config('constants.plano.pago');
                            //     $planoAprovaMais->save();
                            //     $planoAtivo->aprovaMais = $planoAprovaMais;
                            // }

                            // registra uma venda.
                            $this->registrarVenda($user, $planoAtivo, $amount, $cupom, $pagamento);

                            $subject = "Pagamento realizado!";
                            $email_remetente = "contato@biologiaaprova.com.br";
                            $view = 'emails.pago';

                            /* Testando Postback */
                            $result = [
                                'teste'=>'email',
                                'transid'=>$transId,
                                'transResultado'=>$transacao->resultado,
                                'remetente'=>$email_remetente,
                                'user id'=>$user->id,
                                'email'=>$user->email,
                                'amount'=>$amount,
                                'method'=>$pagamento,
                                'hoje'=>date("Y-m-d H:i:s"),
                                'subject'=>$subject,
                                'nome'=>$user->nome,
                                'tempo'=>$planoAtivo->tempo,
                                'plano'=>$planoAtivo->plano,
                                'inicio'=> $prePlano["dataHoje"],
                                'fim'=> $prePlano["dataFim"],
                                'view'=>$view,
                                'metodo_pagamento'=> ($planoAtivo->pagamento == "credit_card" ? 0 : 1),
                            ];
                            $postback = new Postback();
                            $postback->json = json_encode($result);
                            $postback->external_id = $exteralId;
                            $postback->save();
                            /* /Testando Postback */

                            // 6 - envia o email de confirmação.
                            $hoje =  new DateTime($prePlano["dataHoje"]);
                            $fim = new DateTime($prePlano["dataFim"]);
                            Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                            $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                            strtotime($hoje->format('d/m/Y')), strtotime($fim->format('d/m/Y')), $view));
                        }

                    } else {
                        $result = ['success'=>false, 'tranid'=>$transId, 'message'=>'usuário não encontrado na sessão'];
                        $postback = new Postback();
                        $postback->json = json_encode($result);
                        $postback->external_id = $exteralId;
                        $postback->save();
                    }
                }
            }
        } else {
            $result = [
                'status'=>'postback não válido!',
                'signature'=>$signature,
                'request_body'=>$requestBody,
                'is_valid'=>$isValidPostback,
            ];
            $postback = new Postback();
            $postback->json = json_encode($result);
            $postback->external_id = 0;
            $postback->save();
        }
    }

    // método para o aguardo do postback do pagarme.
    public function postbackBackup(Request $request) {
        $pagarme = new Client(env('PAGARME_API_KEY'));
        $requestBody = $request->getContent();
        $signature = $request->server('HTTP_X_HUB_SIGNATURE');
        $isValidPostback = $pagarme->postbacks()->validate($requestBody, $signature);

        if ($isValidPostback) {
            $transaction = $request->input('transaction');
            $exteralId = $transaction['customer']['external_id'];
            $cupom = null;

            /* teste */
            $postback = new Postback();
            $postback->json = json_encode($request->all());
            $postback->external_id = $exteralId;
            $postback->save();
            /* /teste*/

            if ($transaction != null) {

                $transId = $transaction['id'];
                $status = $transaction['status'];
                $exteralId = $transaction['customer']['external_id'];
                $amount = $transaction['amount'];
                $pagamento = $transaction['payment_method'];

                /* teste*/
                $result = [
                    'teste'=>'transacao',
                    'id'=>$transId,
                    'status'=>$status,
                    'external_id'=>$exteralId,
                    'amount'=>$amount,
                    'pagamento'=> $pagamento
                ];
                $postback = new Postback();
                $postback->json = json_encode($result);
                $postback->external_id = $exteralId;
                $postback->save();
                /* /teste*/

                if ($status == 'paid') {
                    $user = User::find(intval($exteralId));
                    if ($user != null) {

                        // gera pre plano pelo valor do postback
                        $prePlano = $this->setPlanoByValor($amount, null);

                        // 1 - verificar se já há um plano ativo nessa modalidade.
                        $planoAtivo = $this->getPlanoAtivoByUser(intval($exteralId), $prePlano["plano"]);
                        if (!$planoAtivo)
                            $planoAtivo = $this->getPlanoAtivoByName($user->id, $prePlano["plano"]);

                        // 2 - caso sim: renovar.
                        if ($planoAtivo) {
                            if ($planoAtivo->idTransacao != $transId) {
                                $planoAtivo->fim = date('Y-m-d', strtotime("+$planoAtivo->tempo months", strtotime($planoAtivo->fim)));
                                $planoAtivo->renovacoes = $planoAtivo->renovacoes + 1;
                                $planoAtivo->idTransacao = $transId;
                                if ($cupom != null) {
                                    $cupomUsuario = $this->handleCupomUsuario($cupom, $user, $prePlano["plano"], false);
                                    $planoAtivo->idCupom = $cupom->id;
                                }
                                $planoAtivo->save();
                            }
                        } else {

                            // busca último plano
                            $planoAtivo = PlanoAtivo::where('idUser', $user->id)->latest('id')->first();

                            if ($planoAtivo != null) {

                                /* verificar se há desconto com cupom */
                                if ($planoAtivo->idCupom != null) {
                                    $cupom = Cupom::find($planoAtivo->idCupom);
                                } else {
                                    $cupom = null;
                                }

                                // gera pre plano pelo valor do postback
                                $prePlano = $this->setPlanoByValor($amount, $cupom);

                                /* Testando Postback */
                                $result = [
                                'teste'=>'preplano',
                                'userId'=>$user->id,
                                'plano'=>$prePlano["plano"],
                                'valor'=>$amount/100,
                                'tempo'=>$prePlano["tempo"],
                                'inicio'=> $prePlano["dataHoje"],
                                'fim'=> $prePlano["dataFim"],
                                'metodo_pagamento'=> $pagamento,
                                'cupom' => ($cupom == null ? "null" : $cupom->id),
                                ];
                                $postback = new Postback();
                                $postback->json = json_encode($result);
                                $postback->external_id = $exteralId;
                                $postback->save();
                                /* /Testando Postback */

                                // Se já houver um plano (diferente dessa transacao) ou null (plano antigo) => cria um plano novo.
                                if ($planoAtivo->status == null || $planoAtivo->idTransacao == null) {
                                    $planoAtivo = new PlanoAtivo();
                                    $planoAtivo->idUser = $user->id;
                                    $planoAtivo->idTransacao = $transId;
                                } else if ($planoAtivo->idTransacao != $transId ) {
                                    $planoAtivo = new PlanoAtivo();
                                    $planoAtivo->idUser = $user->id;
                                    $planoAtivo->idTransacao = $transId;
                                }
                                $planoAtivo->plano = $prePlano["plano"];
                                $planoAtivo->tempo = $prePlano["tempo"];
                                $planoAtivo->inicio = $prePlano["dataHoje"];
                                $planoAtivo->fim = $prePlano["dataFim"];
                                $planoAtivo->pagamento = $pagamento;
                                $planoAtivo->status = "paid";
                                if ($cupom != null) {
                                    $cupomUsuario = $this->handleCupomUsuario($cupom, $user, $prePlano["plano"], true);
                                    $planoAtivo->idCupom = $cupom->id;
                                }
                                $planoAtivo->save();

                                // adiciona plano CURSO TOTAL até dia 15/03/22.
                                // $dataHoje = date('Y-m-d');
                                // $dataAprovaMais = date('Y-m-d', strtotime("2022-03-15"));
                                // if ($dataHoje <= $dataAprovaMais && $prePlano["plano"] == "CURSO COMPLETO") {
                                //     $planoAprovaMais = new PlanoAtivo();
                                //     $planoAprovaMais->idUser = $user->id;
                                //     $planoAprovaMais->idTransacao = $transId;
                                //     $planoAprovaMais->plano = $this->PLANO_APROVA_MAIS;
                                //     $planoAprovaMais->tempo = $prePlano["tempo"];
                                //     $planoAprovaMais->inicio = $prePlano["dataHoje"];
                                //     $planoAprovaMais->fim = $prePlano["dataFim"];
                                //     $planoAprovaMais->pagamento = $pagamento;
                                //     $planoAprovaMais->status = "paid";
                                //     $planoAprovaMais->save();
                                //     $planoAtivo->aprovaMais = $planoAprovaMais;
                                // }
                            }

                        }

                        /*   $dataHoje = date('Y-m-d');
                        $dataFim = date('Y-m-d', strtotime("+". $planoAtivo->tempo . " month", strtotime($dataHoje)));
                        $planoAtivo->inicio = $dataHoje;
                        $planoAtivo->fim = $dataFim;
                        $planoAtivo->status = "paid";
                        $planoAtivo->save(); */

                        /* Testando Postback */
                        $result = [
                            'teste'=>'venda',
                            'userId'=>$user->id,
                            'plano'=>$planoAtivo->plano,
                            'valor'=>$amount/100,
                            'tempo'=>$planoAtivo->tempo,
                            'inicio'=> $prePlano["dataHoje"],
                            'fim'=> $prePlano["dataFim"],
                            'metodo_pagamento'=> ($planoAtivo->pagamento == "credit_card" ? 0 : 1),
                        ];
                        $postback = new Postback();
                        $postback->json = json_encode($result);
                        $postback->external_id = $exteralId;
                        $postback->save();
                        /* /Testando Postback */

                        $venda = new Venda();
                        $venda->idUser = $user->id;
                        $venda->plano = $planoAtivo->plano;
                        $venda->valor = $amount/100;
                        $venda->tempo = $planoAtivo->tempo;
                        $venda->data = $prePlano["dataHoje"];
                        $venda->metodo_pagamento = ($planoAtivo->pagamento == "credit_card" ? 0 : 1);
                        if ($cupom != null) {
                            $venda->desconto = $cupom->desconto;
                        }
                        $venda->save();

                        $subject = "Pagamento realizado!";
                        $email_remetente = "contato@biologiaaprova.com.br";
                        $view = 'emails.pago';

                        /* Testando Postback */
                        $result = [
                            'teste'=>'email',
                            'remetente'=>$email_remetente,
                            'email'=>$user->email,
                            'subject'=>$subject,
                            'nome'=>$user->nome,
                            'tempo'=>$planoAtivo->tempo,
                            'plano'=>$planoAtivo->plano,
                            'inicio'=> $prePlano["dataHoje"],
                            'fim'=> $prePlano["dataFim"],
                            'view'=>$view,
                            'metodo_pagamento'=> ($planoAtivo->pagamento == "credit_card" ? 0 : 1),
                        ];
                        $postback = new Postback();
                        $postback->json = json_encode($result);
                        $postback->external_id = $exteralId;
                        $postback->save();
                        /* /Testando Postback */
                        $hoje =  new DateTime($prePlano["dataHoje"]);
                        $fim = new DateTime($prePlano["dataFim"]);
                        Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                        $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                        strtotime($hoje->format('d/m/Y')), strtotime($fim->format('d/m/Y')), $view));
                    } else {
                        $result = [
                            'status'=>'postback não válido!!! usuario n encontrado.',
                            'signature'=>$signature,
                            'request_body'=>$requestBody,
                            'is_valid'=>$isValidPostback,
                        ];
                        $postback = new Postback();
                        $postback->json = json_encode($result);
                        $postback->external_id = 0;
                        $postback->save();
                    }
                }
            }
        } else {
            $result = [
                'status'=>'postback não válido!!!',
                'signature'=>$signature,
                'request_body'=>$requestBody,
                'is_valid'=>$isValidPostback,
            ];
            $postback = new Postback();
            $postback->json = json_encode($result);
            $postback->external_id = 0;
            $postback->save();
        }
    }

    // método para o aguardo do postback do pagarme.
    public function postbackBD(Request $request) {
        $pagarme = new Client(env('PAGARME_API_KEY'));
        $requestBody = $request->getContent();
        $signature = $request->server('HTTP_X_HUB_SIGNATURE');
        $isValidPostback = $pagarme->postbacks()->validate($requestBody, $signature);
        $dataHoje = date("Y-m-d");

        if ($isValidPostback) {
            $transaction = $request->input('transaction');
            $exteralId = $transaction['customer']['external_id'];
            $cupom = null;

            /* teste */
            $this->createPostbackLog($exteralId, $request->all());
            /* /teste*/

            if ($transaction != null) {

                $transId = $transaction['id'];
                $status = $transaction['status'];
                $exteralId = $transaction['customer']['external_id'];
                $amount = $transaction['amount'];
                $pagamento = $transaction['payment_method'];

                /* teste*/
                $data = [
                    'teste'=>'transacao',
                    'id'=>$transId,
                    'status'=>$status,
                    'external_id'=>$exteralId,
                    'amount'=>$amount,
                    'pagamento'=> $pagamento
                ];
                $this->createPostbackLog($exteralId, $data);
                /* /teste*/

                if ($status == 'paid') {
                    $user = User::find(intval($exteralId));
                    if ($user != null) {

                        // busca último plano
                        $planoAtivo = PlanoAtivo::where('idUser', $user->id)->latest('id')->first();

                        if ($planoAtivo) {

                            /* verificar se há desconto com cupom */
                            if ($planoAtivo->idCupom != null) {
                                $cupom = Cupom::find($planoAtivo->idCupom);
                            } else {
                                $cupom = null;
                            }

                            // gera pre plano pelo valor do postback
                            $prePlano = $this->setPlanoByValorBD($amount, $cupom);

                            /* Testando Postback */
                            $data = [
                            'userId'=>$user->id,
                            'plano'=>$prePlano,
                            'metodo_pagamento'=> $pagamento,
                            'cupom' => ($cupom == null ? "null" : $cupom->id),
                            ];
                            $this->createPostbackLog($exteralId, $data);
                            /* /Testando Postback */

                            // Se já houver um plano (diferente dessa transacao) ou null (plano antigo) => cria um plano novo.
                            if ($planoAtivo->status == null || $planoAtivo->idTransacao == null || $planoAtivo->idTransacao != $transId) {
                                $planoAtivo = new PlanoAtivo();
                                $planoAtivo->idUser = $user->id;
                                $planoAtivo->idTransacao = $transId;
                                $planoAtivo->idPlano = $prePlano->id;
                            }
                            $planoAtivo->plano = $prePlano->plano;
                            $planoAtivo->tempo = $prePlano->tempo;
                            $planoAtivo->inicio = $dataHoje;
                            $planoAtivo->fim = $prePlano->dataFim;
                            $planoAtivo->pagamento = $pagamento;
                            $planoAtivo->status = "paid";
                            if ($cupom != null) {
                                $cupomUsuario = $this->handleCupomUsuarioBD($cupom, $user, $prePlano, true);
                                $planoAtivo->idCupom = $cupom->id;
                            }
                            $planoAtivo->save();

                            /* Testando Postback */
                            $data = [
                                'teste'=>'venda',
                                'userId'=>$user->id,
                                'plano'=>$planoAtivo,
                                'metodo_pagamento'=> ($planoAtivo->pagamento == "credit_card" ? 0 : 1),
                            ];
                            $this->createPostbackLog($exteralId, $data);
                            /* /Testando Postback */

                            $venda = new Venda();
                            $venda->idUser = $user->id;
                            $venda->idPlano = $prePlano->id;
                            $venda->plano = $planoAtivo->plano;
                            $venda->valor = $amount/100;
                            $venda->tempo = $planoAtivo->tempo;
                            $venda->data = $prePlano["dataHoje"];
                            $venda->metodo_pagamento = ($planoAtivo->pagamento == "credit_card" ? 0 : 1);
                            if ($cupom != null) {
                                $venda->desconto = $cupom->desconto;
                            }
                            $venda->save();

                            // SEND E-MAIL WITH RESULTS
                            $subject = "Pagamento realizado!";
                            $email_remetente = "contato@biologiaaprova.com.br";
                            $view = 'emails.pago';

                            /* Testando Postback */
                            $data = [
                                'teste'=>'email',
                                'remetente'=>$email_remetente,
                                'email'=>$user->email,
                                'subject'=>$subject,
                                'nome'=>$user->nome,
                                'view'=>$view,
                                'metodo_pagamento'=> ($planoAtivo->pagamento == "credit_card" ? 0 : 1),
                            ];
                            $this->createPostbackLog($exteralId, $data);
                            /* /Testando Postback */

                            $hoje =  new DateTime($dataHoje);
                            $fim = new DateTime($prePlano->dataFim);
                            /*  Queue::push(new SendEmail($email_remetente, $user->email,
                            $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                            strtotime($hoje->format('d/m/Y')), strtotime($fim->format('d/m/Y')), $view)); */

                            Mail::to($user->email)->send(new AguardandoPagamento($email_remetente, $user->email,
                            $subject, $user->nome, $planoAtivo->tempo, $planoAtivo->plano,
                            strtotime($hoje->format('d/m/Y')), strtotime($fim->format('d/m/Y')), $view));
                        }
                    }
                }
            }
        } else {
            $data = [
                'status'=>'postback não válido!!!',
                'signature'=>$signature,
                'request_body'=>$requestBody,
                'is_valid'=>$isValidPostback,
            ];
            $this->createPostbackLog(-1, $data);
        }
    }

    private function setPlanoByValor($valor, $cupom) {
        $dataHoje = "";
        $dataFim = "";
        $plano = "";
        $tempo = "";
        $desconto = "";

        // Plano 0 - id bd 1.
        if ($valor == 19000) {
            $dataHoje = date('Y-m-d');
            $dataFim = date('Y-m-d', strtotime("+10 month", strtotime($dataHoje)));
            $plano = "EXTENSIVO MED";
            $tempo = "10";
            $desconto = "";
        }

        // Plano 1 - id bd 2.
        if ($valor == 16000) {
            $dataHoje = date('Y-m-d');
            $dataFim = date('Y-m-d', strtotime("+10 month", strtotime($dataHoje)));
            $plano = "EXTENSIVO ENEM";
            $tempo = "10";
            $desconto = "";
        }

        // Plano 3 - id bd 4.
        if ($valor == 25000) {
            $dataHoje = date('Y-m-d');
            //$dataFim = date('Y-m-d', strtotime("+4 month", strtotime($dataHoje)));
            $dataFim = date('2022-01-31');
            $plano = "INTENSIVÃO ENEM";
            $tempo = "4";
            $desconto = "";
        }

        // Plano 5 - id bd 5.
        if ($valor == 26280) {
            $dataHoje = date('Y-m-d');
            $dataFim = date('Y-m-d', strtotime("+12 month", strtotime($dataHoje)));
            $plano = "CURSO COMPLETO";
            $tempo = "12";
            $desconto = "";
        }

        // Plano 5 - id bd 5.
        if ($valor == 4320) {
            $dataHoje = date('Y-m-d');
            $dataFim = date('Y-m-d', strtotime("+12 month", strtotime($dataHoje)));
            $plano = "CURSO TOTAL";
            $tempo = "12";
            $desconto = "";
        }

        /* teste*/
        $result = [
            'funcao' => 'setPlanoByValor',
            'teste'=>'antes de cupom',
            'plano'=>$plano,
            'valor'=>$valor/100,
            'tempo'=>$tempo,
            'inicio'=> $dataHoje,
            'fim'=> $dataFim,
        ];
        $postback = new Postback();
        $postback->json = json_encode($result);
        $postback->external_id = "user";
        $postback->save();
        /* /teste*/

         /* inserir condições para cupom aqui */
         if ($cupom != null) {

            if ($cupom->idPlano == 1) {
                // Plano 1 - 16000
                if ($cupom->tipo_desconto) {
                    // %
                    $valorDesc = 16000 - (16000 * $cupom->desconto/100);
                    $desconto = $cupom->desconto . "%";
                } else {
                    // R$
                    $valorDesc = 16000 - $cupom->desconto;
                    $desconto = $cupom->desconto;
                }

                if ($valor == $valorDesc) {
                    $dataHoje = date('Y-m-d');
                    $dataFim = date('Y-m-d', strtotime("+10 month", strtotime($dataHoje)));
                    $plano = "EXTENSIVO ENEM";
                    $tempo = "10";
                }
            } else if ($cupom->idPlano == 0) {
                // Plano 2 - 19000
                if ($cupom->tipo_desconto) {
                    // %
                    $valorDesc = 19000 - (19000 * $cupom->desconto/100);
                    $desconto = $cupom->desconto . "%";
                } else {
                    // R$
                    $valorDesc = 19000 - $cupom->desconto;
                    $desconto = $cupom->desconto;
                }

                if ($valor == $valorDesc) {
                    $dataHoje = date('Y-m-d');
                    $dataFim = date('Y-m-d', strtotime("+10 month", strtotime($dataHoje)));
                    $plano = "EXTENSIVO MED";
                    $tempo = "10";
                }
            } else if ($cupom->idPlano == 4){
                // Plano 3 - 25000
                if ($cupom->tipo_desconto) {
                    // %
                    $valorDesc = 25000 - (25000 * $cupom->desconto/100);
                    $desconto = $cupom->desconto . "%";
                } else {
                    // R$
                    $valorDesc = 25000 - $cupom->desconto;
                    $desconto = $cupom->desconto;
                }

                if ($valor == $valorDesc) {
                    $dataHoje = date('Y-m-d');
                    //$dataFim = date('Y-m-d', strtotime("+4 month", strtotime($dataHoje)));
                    $dataFim = date('2022-01-31');
                    $plano = "INTENSIVÃO ENEM";
                    $tempo = "4";
                }
            } else if ($cupom->idPlano == 5){
                // Plano 5 - 26280
                if ($cupom->tipo_desconto) {
                    // %
                    $valorDesc = 26280 - (26280 * $cupom->desconto/100);
                    $desconto = $cupom->desconto . "%";
                } else {
                    // R$
                    $valorDesc = 26280 - $cupom->desconto;
                    $desconto = $cupom->desconto;
                }

                if ($valor == $valorDesc) {
                    $dataHoje = date('Y-m-d');
                    $dataFim = date('Y-m-d', strtotime("+12 month", strtotime($dataHoje)));
                    $plano = "CURSO COMPLETO";
                    $tempo = "12";
                }
            }  else {
                // Plano 6 - 4320
                if ($cupom->tipo_desconto) {
                    // %
                    $valorDesc = 4320 - (4320 * $cupom->desconto/100);
                    $desconto = $cupom->desconto . "%";
                } else {
                    // R$
                    $valorDesc = 4320 - $cupom->desconto;
                    $desconto = $cupom->desconto;
                }

                if ($valor == $valorDesc) {
                    $dataHoje = date('Y-m-d');
                    $dataFim = date('Y-m-d', strtotime("+12 month", strtotime($dataHoje)));
                    $plano = "CURSO TOTAL";
                    $tempo = "12";
                }
            }

             /* teste*/
             $result = [
                'teste'=>'durante cupom',
                'plano'=>$cupom->idPlano,
                'tipo desconto'=>$cupom->tipo_desconto,
                'desconto'=>$cupom->desconto,
                'valor'=>$valor,
                'valor desconto' => $valorDesc
            ];
            $postback = new Postback();
            $postback->json = json_encode($result);
            $postback->external_id = "user";
            $postback->save();
            /* /teste*/
        }

         /* teste*/
         $result = [
            'teste'=>'depois do cupom',
            'plano'=>$plano,
            'valor'=>$valor,
            'tempo'=>$tempo,
            'inicio'=> $dataHoje,
            'fim'=> $dataFim,
            'desconto'=>$desconto,
        ];
        $postback = new Postback();
        $postback->json = json_encode($result);
        $postback->external_id = "user";
        $postback->save();
        /* /teste*/

        return ['dataHoje'=>$dataHoje, 'dataFim'=>$dataFim, 'plano'=>$plano, 'tempo'=>$tempo, 'desconto'=>$desconto];
    }

    private function setPlanoByValorBD($valor, $cupom) {
        $dataHoje = date('Y-m-d');
        $planoSelecionado = new Plano();

        $planos = Plano::all();

        if ($planos) {
            foreach ($planos as $key=>$plano) {
                // inserir condições de cupom
                if ($cupom != null) {

                    if ($cupom->idPlano == $plano->id) {
                        if ($cupom->tipo_desconto) {
                            // %
                            $valorDesc = $plano->valor - ($plano->valor * $cupom->desconto/100);
                            $desconto = $cupom->desconto . "%";
                        } else {
                            // R$
                            $valorDesc = $plano->valor - $cupom->desconto;
                            $desconto = $cupom->desconto;
                        }

                        if ($valor == $valorDesc) {
                            $planoSelecionado = $plano;
                            $dataFim = date('Y-m-d', strtotime("+" . $plano->tempo . " month", strtotime($dataHoje)));
                            $planoSelecionado->desconto = $desconto;
                        }
                    }

                    return $planoSelecionado;

                } elseif ($valor == $plano->valor) {
                    // selecionar plano por valor.
                    $planoSelecionado = $plano;
                    $dataFim = date('Y-m-d', strtotime("+" . $plano->tempo . " month", strtotime($dataHoje)));
                    $planoSelecionado->dataFim = $dataFim;

                    return $planoSelecionado;
                }
            }
        }

        $data = [
            'teste'=>'depois do cupom',
            'plano'=>$planoSelecionado,
        ];
        $this->createPostbackLog("setPlanoByValorBD", $data);
    }

    private function createPostbackLog($external_id, $data) {
        /* teste*/
        $postback = new Postback();
        $postback->json = json_encode($data);
        $postback->external_id = $external_id;
        $postback->save();
        /* /teste*/
    }

    // ** RD STATION 2 DIFFERENT APIs ** //

    // private function sendLead($user) {
    //     $rdStation = new RDStation($user->email);
    //     //$rdStation->setApiBaseUrl("https://api.rd.services/platform/conversions?api_key=uWIJyyvPenoeMIWsCBpUxWFHSJAufHFTtbMV");
    //     $rdStation->setApiToken('f2bfa627f9be9ed09886af3c9391a4cb');
    //     $rdStation->setLeadData('event_type' , "CONVERSION");
    //     $rdStation->setLeadData('event_family' , "CDP");
    //     $rdStation->setLeadData('conversion_identifier', 'teste_gratis');
    //     $rdStation->setLeadData('identifier', 'teste_gratis');
    //     $rdStation->setLeadData('email' ,  $user->email);

    //     $response = $rdStation->sendLead();
    //     return $response;
    // }

    // private function sendLeadAlt($user) {
    //     try {
    //         $rd = new RD('f2bfa627f9be9ed09886af3c9391a4cb');
    //         $lead = $rd->lead()->create([
    //             'event_family' => 'CDP',
    //             'event_type' => "CONVERSION",
    //             'conversion_identifier'=>'teste_gratis',
    //             'identifier', 'teste_gratis',
    //             'email' => $user->email
    //         ]);

    //         return $lead;

    //     } catch (RDException $e) {
    //         return $e->getMessage();
    //     }
    // }
}
