<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Plano;
use App\Models\PlanoAtivo;
use DateTime;

class Controller extends BaseController
{

    protected $TODAY = null;

    // private $SIMULADOS_INTESIVAO_ENEM = [10, 15];
    // private $AREAS_INTESIVAO_ENEM = [3, 4];

    public function __construct() {
        $this->TODAY = new DateTime("now");
    }

    protected function getPlanoIdByName($planoName) {
        $plano = Plano::whereNome(strtoupper($planoName))->first();
        return $plano->id;
    }

    protected function getPlanoAtivoByName($idUser, $planoName) {
        $plano = PlanoAtivo::wherePlano($planoName)
            ->whereStatus(config('constants.plano.pago'))
            ->whereDeletado(0)
            ->whereDate("fim",">=",new DateTime("now"))
            ->where("idUser","=",$idUser)
            ->first();
        return $plano;
    }

    protected function getPlanoAtivoByTrans($idTransaction, $planoName) {
        $plano = PlanoAtivo::wherePlano($planoName)
            ->whereStatus(config('constants.plano.pago'))
            ->whereDeletado(0)
            ->whereDate("fim",">=",new DateTime("now"))
            ->where("idTransacao","=",$idTransaction)
            ->first();
        return $plano;
    }

    protected function getPlanoAtivoByUser($idUser, $planoName) {
        $plano = PlanoAtivo::wherePlano($planoName)
            ->whereStatus(config('constants.plano.pago'))
            ->whereDeletado(0)
            ->whereDate("fim",">=",new DateTime("now"))
            ->where("idUser","=",$idUser)
            ->first();
        return $plano;
    }

    protected function getPlanoByUser($idUser, $planoName) {
        $plano = PlanoAtivo::wherePlano($planoName)
            ->whereDeletado(0)
            ->whereDate("fim",">=",new DateTime("now"))
            ->where("idUser","=",$idUser)
            ->first();
        return $plano;
    }

    protected function hasExpired($date) {
        $vencimento = new DateTime($date);
        return ($vencimento < $this->TODAY);
    }

    // 00:00:00
    protected function stringTimeToSec($time_string) {
        $time_array = explode(":",$time_string);
        return (int) $time_array[2] + ((int) $time_array[1]) * 60 + ((int) $time_array[0]) * 60 * 60;
    }

    protected function cleanCpf($cpf) {
        $cpf_formatado = trim($cpf);
        $cpf_formatado = str_replace(".", "", $cpf_formatado);
        $cpf_formatado = str_replace("-", "", $cpf_formatado);
        return $cpf_formatado;
    }
}
