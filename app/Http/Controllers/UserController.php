<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use App\Mail\SimpleEmail;
use App\Models\User;
use App\Models\Admin;
use App\Models\PlanoAtivo;
use App\Models\Cidade;
use App\Models\Boleto;
use App\Models\Estado;
use App\Models\Cupom;
use App\Models\RecuperarSenha;
use App\Models\UsuarioFeedback;
use App\Models\Login;
use App\Models\UsuarioSimuladoAberto;
use App\Models\UsuarioSimuladoAbertoResposta;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Storage;
use Session;


class UserController extends Controller
{
    private $USER_SESSION = null;

    public function __construct(Request $request) {
         /* return user if there is a session open */
         if ($request->session()->has('userId')) {
            $this->USER_SESSION = User::find($request->session()->get('userId'));
             // verifica se usuári possui plano ativo para eliminar botão 'teste grátis'.
             if ($this->USER_SESSION != null) {
                $planoAtivo = PlanoAtivo::where('idUser', $this->USER_SESSION->id)->latest('id')->first();
                if ($planoAtivo != null)  {
                    // verifica se tem cupom
                    if ($planoAtivo->idCupom != null) {
                        $cupom = Cupom::find($planoAtivo->idCupom);
                        if ($cupom != null) {
                            $planoAtivo->cupom = $cupom;
                            // calcular valor com desconto
                            $planoDesconto = (object) $this->setPlanoByCupom($cupom);
                            $planoAtivo->planoDesconto = $planoDesconto;
                        }
                    }
                    $this->USER_SESSION->plano = $planoAtivo;
                }
                 // Salva todas as assinaturas (somente ativas);
                 $this->USER_SESSION->planos = $this->getPlanosValidos($this->USER_SESSION->id);
                 if (isset($this->USER_SESSION->planos) && sizeof($this->USER_SESSION->planos) > 0)
                     $this->USER_SESSION->plano = $this->USER_SESSION->planos[0];
            }
        }
         /* /return user if there is a session open */
    }

    public function assinarDesconto(Request $request) {
        if ($user = $this->USER_SESSION) {
            $cidade = Cidade::where('cod_cidades', $user->cidade)->first();
            $estado = Estado::where('cod_estados', $user->estado)->first();
             /* verifica se usuário possui plano ativo para eliminar botão 'teste grátis'*/
            $planoAtivo = $user->plano;
            if ($planoAtivo != null)  {

                $planoAtivo->ativo = false;
                // verificar se o pagamento foi confirmado.
                if ($planoAtivo->status == 'paid') {
                    $planoDate = new DateTime($planoAtivo->fim);
                    $today = new DateTime("now");
                    // verifica se o plano venceu (!= redirect pra pagamento)
                    if ($planoDate >= $today) {
                        $planoAtivo->ativo = true;
                    } else {
                        $planoAtivo->ativo = false;
                    }
                    // verifica se há um boleto aguardando (!= redirect pra pagamento).
                } else if ($planoAtivo->status == 'waiting_payment' && $planoAtivo->pagamento == 'boleto') {
                    $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
                    if ($boleto != null) {
                        $today = new DateTime("now");
                        $vencimento = new DateTime($boleto->expirar);
                        if ($vencimento >= $today) {
                           $boleto->ativo = true;
                        } else {
                           $boleto->ativo = false;
                        }
                        $user->boleto = $boleto;
                    } else {
                        return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user]);
                    }

                    // tratamento de planos antigos status = null.
                } else if ($planoAtivo->status == null) {
                    if ($this->isPlanoValid($planoAtivo)) {
                        $planoAtivo->ativo = true;
                    } else {
                        $planoAtivo->ativo = false;
                    }
                }
                $user->plano = $planoAtivo;
            }

            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user, 'link_desconto'=>1, 'cidade'=>$cidade, 'estado'=>$estado]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login', 'link_desconto'=>1]);
        }
    }

    // return user's account page
    public function index(Request $request) {
        $user = $this->USER_SESSION;
        if ($user) {
            $cidade = Cidade::where('cod_cidades', $user->cidade)->first();
            $estado = Estado::where('cod_estados', $user->estado)->first();
             /* verifica se usuário possui plano ativo para eliminar botão 'teste grátis'*/
            $planoAtivo = $user->plano;
            if ($planoAtivo != null)  {

                $planoAtivo->ativo = false;
                // verificar se o pagamento foi confirmado.
                if ($planoAtivo->status == 'paid') {
                    $planoDate = new DateTime($planoAtivo->fim);
                    $today = new DateTime("now");
                    // verifica se o plano venceu (!= redirect pra pagamento)
                    if ($planoDate >= $today) {
                        $planoAtivo->ativo = true;
                    } else {
                        $planoAtivo->ativo = false;
                    }
                    // verifica se há um boleto aguardando (!= redirect pra pagamento).
                } else if ($planoAtivo->status == 'waiting_payment' && $planoAtivo->pagamento == 'boleto') {
                    $boleto = Boleto::where('idUser', $user->id)->orderBy('expirar', 'desc')->first();
                    if ($boleto != null) {
                        $today = new DateTime("now");
                        $vencimento = new DateTime($boleto->expirar);
                        if ($vencimento >= $today) {
                           $boleto->ativo = true;
                        } else {
                           $boleto->ativo = false;
                        }
                        $user->boleto = $boleto;
                    } else {
                        return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento', 'user'=>$user]);
                    }

                    // tratamento de planos antigos status = null.
                } else if ($planoAtivo->status == null) {
                    if ($this->isPlanoValid($planoAtivo)) {
                        $planoAtivo->ativo = true;
                    } else {
                        $planoAtivo->ativo = false;
                    }
                }
                $user->plano = $planoAtivo;
            }
            /* /verifica se usuário possui plano ativo para eliminar botão 'teste grátis'*/

            return view('home', ['title' => 'Biologia Aprova - minha área', 'page'=>'area-aluno', 'user'=>$user, 'cidade'=>$cidade, 'estado'=>$estado]);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }
    }

    private function isPlanoValid($plano) {
        $today = new DateTime("now");
        $planoDate = new DateTime($plano->fim);
        if ($planoDate >= $today) {
            return true;
        } else {
            return false;
        }
    }

    private function getPlanosValidos($userId) {
        $today = (new DateTime('now'))->format('Y-m-d');
        $planosValidos = PlanoAtivo::whereRaw("idUser = ? AND (status = ? OR status IS NULL)", [$userId, config('constants.plano.pago')])->whereDate('fim','>=', $today)->get();
        if ($planosValidos) {
            foreach ($planosValidos as $plano) {
                if ($plano->idCupom){
                    $cupom = Cupom::find($plano->idCupom);
                    if ($cupom) {
                        $plano->cupom = $cupom;
                        $plano->planoDesconto = (object) $this->setPlanoByCupom($cupom);
                    }
                }
            }
        }
        return $planosValidos ? $planosValidos : [];
    }

    private function setPlanoByCupom($cupom) {
        if ($cupom->idPlano == 5) {
            // Plano 5 - 26280.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 26280 - (26280 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 26280 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO COMPLETO";
            $tempo = "12";
        } else {
            // Plano 6 - 9480.00
            if ($cupom->tipo_desconto) {
                // %
                $valorDesc = 9480 - (9480 * $cupom->desconto/100);
                $desconto = $cupom->desconto . "%";
            } else {
                // R$
                $valorDesc = 9480 - $cupom->desconto;
                $desconto = $cupom->desconto;
            }

            $plano = "CURSO TOTAL";
            $tempo = "12";
        }

        return ['plano'=>$plano, 'tempo'=>$tempo, 'valor'=>$valorDesc, 'desconto'=>$desconto];
    }

    public function alterarSenha(Request $request) {
        $senhaAtual = $request->input("senha-atual");
        $senhaNova = $request->input("senha-nova");

        if ($request->session()->has("userId")) {
            $userId = $request->session()->get('userId');
            $user = User::find($userId);
            if ($user) {
                if (md5($senhaAtual) == $user->senha) {
                    $user->senha = md5($senhaNova);
                    $user->save();
                    $result = ['success'=>true, 'user'=>$user];
                } else {
                    $result = ['success'=>false, 'hasSession'=>true, 'hasUser'=>true, 'hasPass'=>false];
                }
            } else {
                $result = ['success'=>false, 'hasSession'=>true, 'hasUser'=>false, 'hasPass'=>true];
            }
        } else {
            $result = ['success'=>false, 'hasSession'=>false, 'hasUser'=>false, 'hasPass'=>true];
        }

        return response()->json($result);
    }

    public function termo() {
        return view('home', ['title' => 'Biologia Aprova - termo de uso', 'page'=>'termo-uso', 'user'=>$this->USER_SESSION]);
    }

    public function alterarImagem(Request $request) {
        if ($this->USER_SESSION != null) {
            $user = User::find($this->USER_SESSION->id);
            $imagem = $request->hasFile('image');
            if ($imagem) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);

                $imageName = 'profile_'.$user->id.'.'.$request->image->extension();

                // resize
               /*  $filePath = app()->basePath('public/img/profile/thumbnails');

                $img = Image::make($request->image->path());
                $img->resize(200, 200, function ($const) {
                    $const->aspectRatio();
                })->save($filePath.'/'.$imageName); */

                if ($imageName != "") {
                    $user->foto = $imageName;
                    $user->save();
                }

                $request->image->move(app()->basePath('public/img/profile'), $imageName);

                $result = ['success'=>true, 'name'=> $imageName];
            } else {
                $result = ['success'=>false, 'message'=> 'Por favor, escolha um arquivo válido.'];
            }
        } else {
            $result = ['success'=>false, 'message'=> 'Por favor, recarregue a página e faça login novamente.'];
        }

        return $result;

    }

    public function contato() {
        return view('home', ['title' => 'Biologia Aprova - contato', 'page'=>'contato', 'user'=>$this->USER_SESSION]);
    }

    public function enviarMensagem(Request $request) {
        $nome = $request->input("nome");
        $email = $request->input("email");
        $assunto = $request->input("assunto");
        $telefone = $request->input("telefone");
        $mensagem = $request->input("mensagem");

        $order   = array('\r\n', '\n', '\r');
        $replace = '<br/>';
        $mensagem = str_replace($order, $replace, $mensagem);

        $data = [
            'nome'=>$nome,
            'email'=>$email,
            'assunto'=>$assunto,
            'telefone'=>$telefone,
            'mensagem'=>$mensagem
        ];

        $emails = ["contato@biologiaaprova.com.br", "contato.biologiaaprova@gmail.com"];
        //$emails = ["caiquepires91@gmail.com", "kayklp@hotmail.com"];

        //$to = "caiquepires91@gmail.com";
        $to = "contato@biologiaaprova.com.br, contato.biologiaaprova@gmail.com";
        $subject = $assunto;
        //$from = "contato@biologiaaprova.com.br";
        $from = $email;
        $view = 'emails.mensagem';

        Mail::to($emails)->send(new SimpleEmail($from, $to, $subject, $data, $view));

    }

    public function recuperarSenha(Request $request) {
        $email = $request->input("email");
        $user = User::whereEmail($email)->latest('id')->first();
        if ($user != null) {
            $token = $this->generateToken();
            $recuperarSenha = new RecuperarSenha();
            $recuperarSenha->idUser = $user->id;
            $recuperarSenha->codigo = $token;
            $recuperarSenha->email = $user->email;
            $recuperarSenha->horaEnvio = new DateTime('now');
            $recuperarSenha->usado = false;
            $recuperarSenha->save();

            $data = [
                'nome'=>$user->nome,
                'url'=> url('/verificar-token') . "/" . $token
            ];

           // return $data;

            $to = $user->email;
            $subject = "Recuperar senha";
            $from = "contato@biologiaaprova.com.br";
            $view = 'emails.recuperar_senha';

            Mail::to($to)->send(new SimpleEmail($from, $to, $subject, $data, $view));

            $result = ['success'=>true, 'data'=>$data, 'message'=>'Enviamos um link para o e-mail: ' . $user->email];
        } else {
            $result = ['success'=>false, 'message'=>'Nenhum usuário cadastrado para ' . $email];
        }
        return $result;
    }

    function generateToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function verificarToken($token) {
        $codigo = $token;
        $recuperarSenha = RecuperarSenha::whereCodigo($codigo)->first();
        if ($recuperarSenha != null) {
            if ($recuperarSenha->usado) {
                return view('home', ['title' => 'Biologia Aprova - redefinir senha', 'page'=>'erro', 'subject'=>'redefinir senha', 'message'=>"esse código já foi utilizado."]);
            } else {
                return view('home', ['title' => 'Biologia Aprova - redefinir senha', 'page'=>'redefinir-senha', 'token'=>$codigo]);
            }
        } else {
            return view('home', ['title' => 'Biologia Aprova - redefinir senha', 'page'=>'erro', 'subject'=>'redefinir senha', 'message'=>"código inválido."]);
        }
    }

    public function redefinir(Request $request) {
        $senha = $request->input("nova_senha");
        $codigo = $request->input("token");

        $recuperarSenha = RecuperarSenha::whereCodigo($codigo)->whereUsado(false)->first();
        if ($recuperarSenha != null) {
            $user = User::find($recuperarSenha->idUser);
            if ($user != null) {
                $user->senha = md5($senha);
                $user->save();
                if ($user != null) {
                    $recuperarSenha->usado = true;
                    $recuperarSenha->save();
                    $result = ['success'=>true, 'user'=>$user, 'message'=>'Senha redefinida com sucesso.'];
                } else {
                    $result = ['success'=>false, 'message'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
                }
            } else {
                $result = ['success'=>false, 'message'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
            }
        } else {
            $result = ['success'=>false, 'message'=>"Ops! Algo deu errado. Por favor, tente mais tarde."];
        }

        return $result;
    }

    public function rateLayout(Request $request) {
        $user = $this->USER_SESSION;
        if ($user) {
            $valor = $request->input('valor');
            $comment = $request->input('comentario');
            $feedback = UsuarioFeedback::whereTipo('layout')->where('idUser', $user->id)->first();
            if ($feedback == null) {
               $feedback = new UsuarioFeedback();
               $feedback->idUser = $user->id;
               $feedback->tipo ='layout';
            }
            $feedback->valor = $valor;
            $feedback->comentario = $comment;
            $feedback->save();
            if ($feedback) $result = ['success'=>true, 'feedback'=>$feedback];
            else $result = ['success'=>false, 'message'=>'Ops! Algo deu errado. Por favor, tente mais tarde.'];
        } else {
            $result = ['success'=>false, 'message'=>'Por favor, atualize a página e faça login.'];
        }
        return $result;
    }

    public function popuptrigger(Request $request) {
        $user = $this->USER_SESSION;
        if ($user) {
            $feedback = UsuarioFeedback::whereTipo('layout')->where('idUser', $user->id)->first();
            if ($feedback != null) $result = ['success'=>true, 'trigger'=>false];
            else {
                $date = "2021-04-08";
                $numLastLogs = Login::where('idUser', $user->id)->whereDate('data', '>=', date($date))->count();
                if ($numLastLogs >= 2) {
                    $result = ['success'=>true, 'trigger'=>true];
                } else {
                    $result = ['success'=>true, 'trigger'=>false];
                }
            }
        } else {
            $result = ['success'=>false, 'message'=>'esse popup exige usuario logado.'];
        }
        return $result;
    }

    public function popupdismiss(Request $request) {
        $user = $this->USER_SESSION;
        if ($user) {
            $request->session()->put("popup_layout_dismissed", true);
            $hasDismissed = $request->session()->has("popup_layout_dismissed");
            return ($hasDismissed);
            if ($hasDismissed) {
                $result = ['success'=>true, 'is_dismissed'=>$request->session()->get("popup_layout_dismissed")];
            } else {
                $result = ['success'=>true, 'is_dismissed'=>false];
            }
        } else {
            $result = ['success'=>false, 'message'=>'esse popup exige usuario logado.'];
        }
        return $result;
    }

    public function cadastrarUserSimAberto(Request $request) {
        $erro = ['success'=>false, 'msg'=>'não foi possível cadastrar o usuário'];
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'nome' => 'required',
            'email' => 'required',
            'telefone' => 'required',
        ]);

        if ($validator->fails())
            return response()->json($erro);

        $nome = $inputs["nome"];
        $email = $inputs["email"];
        $telefone = $inputs["telefone"];
        $user = UsuarioSimuladoAberto::whereEmail($email)->first();

        if ($user) {
            // verifica se usuário já respondeu o simulado.
            $resposta = UsuarioSimuladoAbertoResposta::where('idUsuario','=', $user->id)->first();
            $this->enviarEmailSimuladoAberto($user);
            $user->token_id = Crypt::encrypt($user->id);
            return response()->json(['success'=>true, 'user'=>$user, 'resposta'=>$resposta]);
        }

        $user = new UsuarioSimuladoAberto();
        $user->nome = $nome;
        $user->email = $email;
        $user->telefone = $this->clearPhone($telefone);
        $user->save();

        if ($user) {
            $this->enviarEmailSimuladoAberto($user);
            $user->token_id = Crypt::encrypt($user->id);
            return response()->json(['success'=>true, 'user'=>$user]);
        }

        return response()->json($erro);
    }

    private function enviarEmailSimuladoAberto($user) {
        $nome = $user->nome;
        $email = $user->email;
        $assunto = "Biologia Aprova - DESCONTO 50%";
        $telefone = $user->telefone;
        $mensagem ="";
        $order   = array('\r\n', '\n', '\r');
        $replace = '<br/>';
        $mensagem = str_replace($order, $replace, $mensagem);

        $data = [
            'nome'=>$nome,
            'email'=>$email,
            'assunto'=>$assunto,
            'telefone'=>$telefone,
            'mensagem'=>$mensagem
        ];

        $emails = [$email];
        $to = $email;
        $subject = $assunto;
        $from = "contato.biologiaaprova@gmail.com";
        $view = 'emails.desconto';

        Mail::to($emails)->send(new SimpleEmail($from, $to, $subject, $data, $view));

    }

    private function clearPhone($telefone) {
        $chars = ['(',')','-',' '];
        return str_replace($chars, "", trim($telefone));
    }
}
