<?php

namespace App\Http\Middleware;

use Closure;

class loginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Pre-Middleware Action
        if ($request->session()->has('userId')) {
            return redirect('pagamento');
        }

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
