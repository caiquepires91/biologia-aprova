<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Pre-Middleware Action

        // Allow Options Method
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin' 
        ];

       /*  Sometimes, your user-agent (browser) sends an OPTIONS request first as a form of verification request.
         However, lumen, somehow, does not recognize the OPTIONS request method and rejects it with a 508 */
        if ($request->getMethod() == 'OPTIONS') {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            return response('OK', 200)->withHeaders($headers);
        }

        $response = $next($request);

        // Post-Middleware Action
        foreach($headers as $key => $value)
            $response->header($key, $value);

        return $response;
    }
}
