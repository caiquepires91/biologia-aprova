<?php

namespace App\Http\Middleware;

use Closure;

class SessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Pre-Middleware Action
        if ($request->session()->has('userId')) {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-pagamento']);
        } else {
            return view('home', ['title' => 'Biologia Aprova - planos', 'page'=>'planos-login']);
        }

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
