<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;
use App\Mail\AguardandoPagamento;
use App\Models\User;
use App\Models\PlanoAtivo;
   

class SendEmail extends Job
{

    public $nome;
    public $tempo;
    public $plano;
    public $inicio;
    public $fim;
    public $sender;
    public $receiver;
    public $subject;
    public $view;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($from, $to, $subject, $nome, $tempo, $plano, $inicio, $fim, $view)
    {
        $this->nome = $nome;
        $this->tempo = $tempo;
        $this->plano = $plano;
        $this->inicio = $inicio;
        $this->fim = $fim;
        $this->sender = $from;
        $this->receiver = $to;
        $this->subject = $subject;
        $this->view = $view;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->receiver)->send(new AguardandoPagamento(
            $this->sender,  $this->receiver,  $this->subject,  $this->nome,
            $this->tempo, $this->plano,  $this->inicio, $this->fim, $this->view
        ));
    }
}
