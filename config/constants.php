<?php

return [

    'curso' => [
        'tipo_simulado' => 'simulado',
        'tipo_questionario' => 'questionario',
        'tipo_video' => 'video',
        'tipo_playlist_med' => 'playlist_med',
    ],
    'plano' => [
        'pago' => 'paid',
        'autorizado' => 'authorized',
        'aguardando' => 'waiting_payment',
        'teste' => 'try'
    ],
    'planos_valores' =>[
        'EXTENSIVO MED' =>  '19000',
        'EXTENSIVO ENEM' =>  '16000',
        'TESTE GRÁTIS' =>  '00',
        'INTENSIVÃO ENEM' =>  '25000',
        'CURSO COMPLETO' => '26280',
        'CURSO TOTAL' =>  '4320',
    ],
    'pagamento' => [
        'boleto' => 'boleto',
        'cartao' => 'credit_card',
        'teste' => 'try'
    ],
    'playlist' => [
        'video' => 1,
        'simulado' => 2,
        'conteudo' => 3,
        'ebook' => 4,
        'musica' => 5,
        'questionario' => 6,
    ],
    'video' => [
        'video' => 1,
        'live' => 2,
        'link' => 3,
    ],
    'outras' => [
        'deadline_aprova_mais' => '2022-04-30'
    ]


];

?>
