<!DOCTYPE html>
<html lang="pt-br">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113092297-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-113092297-1');
	</script>

    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="content-language" content="pt-br">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<meta name="description" content="Cursinho online de Biologia com videoaulas e materiais exclusivos com tudo o que você precisa saber pra gabaritar o ENEM ou o vestibular pra Medicina. Quem faz, aprova!">
	
	<link rel="icon" href="{{ URL::to('img\ico1.png') }}" type="image/x-icon"/>

	<!-- JQuery -->
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
	<script type="text/javascript" src="{{URL::to('/assets/js/jquery-3.3.1.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('/assets/js/jquery.mask.min.js')}}"></script>
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script> -->

	<!-- React dependencies -->
	<script crossorigin src="https://unpkg.com/react@17/umd/react.production.min.js"></script>
	<script crossorigin src="https://unpkg.com/react-dom@17/umd/react-dom.production.min.js"></script>
	<!-- <script crossorigin src="https://unpkg.com/babel-standalone@6/babel.min.js"></script> -->
	
	<!-- bootstrap css -->
	<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
	<link href="{{url::to('assets\lib\bootstrap-5.0.0\css\bootstrap.min.css')}}" rel="stylesheet">
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet"> -->

	<!-- Audio Player Calamansi -->
	<link rel="stylesheet" href="{{url::to('assets/calamansi/calamansi.min.css')}}">
	
	<link href="{{URL::to('/css/fonts/stylesheet.css')}}" rel="stylesheet">
	<link href="{{URL::to('/css/animation.css')}}" rel="stylesheet">
	<link href="{{URL::to('/css/index.css?v=4.0')}}" rel="stylesheet">
	<link href="{{URL::to('/css/blog.css')}}" rel="stylesheet">
	<link href="{{URL::to('/css/faq.css')}}" rel="stylesheet">
	<link href="{{URL::to('/css/planos.css?v=1')}}" rel="stylesheet">
	<link href="{{URL::to('/css/videos.css')}}" rel="stylesheet">

	<!-- iframe -->
	<link href="{{URL::to('assets/css/vi-lazyload.min.css')}}" rel="stylesheet">

	<!-- pagarme -->
	<!-- <script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script> -->

	<!-- vimeo -->
	<script src="https://player.vimeo.com/api/player.js"></script>

	<!-- fontawesome -->
	<link href="{{url::to('assets\lib\fontawesome-5.15.3\css\all.css')}}" rel="stylesheet"> <!--load all styles -->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" /> -->
	
	<!-- bootstrap js -->
	<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> -->
	<script src="{{url::to('assets\lib\bootstrap-5.0.0\js\bootstrap.bundle.min.js')}}"></script>
	<!-- always after jquery.js and bootstrap.js -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> -->
	<script src="{{url::to('assets/js/bootstrap3-typeahead.min.js')}}" integrity=""></script>

	<!-- Audio Player Calamansi -->
	<script src="{{url::to('assets/calamansi/calamansi.min.js')}}"></script>

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '284944602959456');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=284944602959456&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->

    <title>{{ $title }}</title>
</head>
<body style="overflow-x:hidden; width:100vw">

	<style>
		#modalPopup .checked {
			color: orange;
		}

		#modalPopup .modal-header {
			border:none;
		}

		#modalPopup .modal-header button {
			color:white;
		}

		#modalPopup .modal-footer {
			border:none;
			padding-bottom: 2rem;
		}

		#modalPopup .modal-footer button {
			color: #0b3573;
			font-weight: 700;
			border-radius: 2rem;
			width: 15rem;
			margin:auto;
		}

		#modalPopup .modal-content {
			background: #0d6efd;
			color:white;
			border:none;
		}

		#modalPopup .star-rating {
			margin:auto;
		}

		#modalPopup .star-rating span{
			font-size:2rem;
			cursor:pointer;
		}

		#modalPopup .modal-body-wrapper {
			display:flex;
			flex-direction:column;
		}

		#modalPopup .modal-body-wrapper h2 {
			text-align: center !important;
			margin: 2rem !important;
			font-size: xx-large !important;
			font-family: 'Lufga' !important;
			font-weight: 200 !important;
		}

		#modalPopup .modal-body {
			padding-top: 0rem;
			padding-right: 1rem;
			padding-bottom: 2rem;
			padding-left: 1rem;
		}

		#modalPopup .modal-body .feedback {
			text-align: center;
			margin-top: 1rem;
			margin-bottom: 0;
			visibility:visible;
		}

		#modalPopup .modal-body .input-group textarea {
			margin-top: 2rem;
			margin-right: 2rem;
			margin-bottom: 0rem;
			border-radius: 6px !important;
			margin-left: 2rem;
			height: 6rem;
		}

		#player-wrapper {
			position: fixed;
			visibility: hidden;
			width: 300px;
			height: 135px;
			bottom: 1rem;
			right: 1rem;
			z-index: 1;
		}

		#player-close {
			width: 20px;
			height: 20px;
			background: white;
			box-shadow: rgb(0 0 0 / 35%) 0px 5px 15px;
			display: flex;
			border-radius: 10px;
			margin-top: 5px;
			margin-bottom: 5px;
			margin-left:auto;
			padding: 5px;
			cursor: pointer;
		}

		#player-close:hover {
			background:#8ba989;
		}

		#player-close:hover i {
			color:white;
		}

		#player-close i {
			color: #8ba989;
			margin: auto;
			font-size: 12px;
		}



	</style>

	<!-- Global Player -->
	<div id="player-wrapper">
		<div id="player-close"><i class="fas fa-times"></i></div>
		<div id="calamansi-player-1">
		</div>
	</div>

	<!-- Modal Pop-up -->
	<div class="modal fade" id="modalPopup" tabindex="-1" data-bs-backdrop="static" aria-labelledby="modalPopupLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalPopupLabel"></h5>
				<a role="button" data-bs-dismiss="modal" onclick="popupDismiss(this)" aria-label="Close"><i class="fas fa-times"></i></a>
			</div>
			<div class="modal-body">
				<div class="modal-body-wrapper">
					<h2>o que está achando do novo layout do Biologia Aprova?</h2>
					<div class="star-rating">
						<span class="fa fa-star" value="1" onclick="checkStar(this)"></span>
						<span class="fa fa-star" value="2" onclick="checkStar(this)"></span>
						<span class="fa fa-star" value="3" onclick="checkStar(this)"></span>
						<span class="fa fa-star" value="4" onclick="checkStar(this)"></span>
						<span class="fa fa-star" value="5" onclick="checkStar(this)"></span>
					</div>
					<div class="input-group">
						<textarea class="form-control" placeholder="deixe seu comentário aqui..."></textarea>
					</div>
					<p class="feedback">envie seu feedback!</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="rate(this)" class="btn btn-light">enviar</button>
			</div>
		</div>
		</div>
	</div>
	<!-- /Modal pop-up -->

	<!-- o atributo overflow-x é ignorado nas tags html, body pelo navegador mobile -->
	<!-- <div class="container-fluid">
    </div> -->
    @yield('content')

	<!-- Iframe -->
	<script src="{{url::to('assets/js/vi-lazyload.min.js')}}"></script>

	<!-- <script src="{{url::to('')}}" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> -->
	<script src="{{URL::to('/js/bundle.js')}}"></script>
	<!-- RD -->
	<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/fd128469-4b07-44fe-9714-51974d3dd1e4-loader.js"></script>
	<!-- /RD -->

	<script>

	/* Global Player */

	/* popup settings */

	function popupTrigger() {
		var url = "{{url::to('popup-trigger')}}";
		var data = {};

		$.post(url, data, function(data) {
			//console.log(data);
			if (data.success) {
				//console.log("popup trigger: " + data);
				if (data.trigger) {
					$('#modalPopup').modal('show');
				} else {
					//console.log("trigger false");
				}
			} else {
				//console.log(data.message);
			}
		}).fail(function(e){
			console.log("check popup trigger error!");
		});
	}

	function popupDismiss(e) {
		var url = "{{url::to('popup-dismiss')}}";
		var data = {};

		$.post(url, data, function(data) {
			//console.log(data);
			if (data.success) {
				console.log("popup dismiss: " + data);
				if (data.is_dismissed) {
					consol.log("post success: " + data.is_dismissed);
					//$('#modalPopup').modal('show');
				} else {
					console.log("dismiss false");
				}
			} else {
				//console.log(data.message);
			}
		}).fail(function(e){
			console.log("check popup trigger error!");
		});
	}

	/* /popup settings */

	/* Layout Rating */

	function checkStar(e) {
		var val = $(e).attr("value");
		stars = $(".star-rating").find(".fa-star");
		for (i=0; i < val; i++) {
			var star = $(stars[i]);
			star.addClass("checked");
		}
		for (i=val; i < stars.length; i++) {
			var star = $(stars[i]);
			star.removeClass("checked", false);
		}
	}

	function getStarRatingVal() {
		var last_rated_star = $(".star-rating span.checked:last");
		return last_rated_star != null ? $(last_rated_star).attr("value") : 0;
	}

	function feedback(message) {
		$("#modalPopup .modal-body .feedback").html(message);
		$("#modalPopup .modal-body .feedback").css({visibility:'visible', opacity: 0.0}).animate({opacity:1.0});
		setTimeout(() => {
			$("#modalPopup .modal-body .feedback").css({visibility:'invisible', opacity: 1.0}).animate({opacity:0.0});
		}, 5000);
	}

	function rate(e) {
		var val = getStarRatingVal();
		var comment = $("#modalPopup textarea").val();
		var url = "{{url::to('rate-layout')}}";
		//console.log(comment);

		if (val == null) {
			//feedback("Ops! Parece que você não selecinou nenhuma estrela. <i class='fas fa-grin-beam-sweat'></i>");
			val = 0;
		}

		data = {'valor':val, 'comentario':comment};

		$.post(url, data, function(data) {
			if (data.success) {
				//console.log(data.feedback);
				$("#modalPopup textarea").prop('disabled', true);
				$("#modalPopup .modal-footer button").prop('disabled', true);
				feedback("muito obrigado pelo feedback. <i class='fas fa-smile-wink'></i>");
				setTimeout(() => {
					$('#modalPopup').modal('hide');
				}, 2000);
			} else {
				feedback(data.message + " <i class='fas fa-sad-tear'></i>");
			}
		}).fail(function(e){
			feedback("Ops! Algo deu errado. <i class='fas fa-sad-tear'></i>");
		});
	}

	/* /Layout Rating */

	$( document ).ready(function() {
		// check star rating pop
		var isDismissed = "{{ app('request')->session()->has('popup_layout_dismissed') ? app('request')->session()->get('popup_layout_dismissed') : false }}";
		// console.log("is dismissed: " + isDismissed);
		if (!isDismissed) popupTrigger();
	});
	</script>
</body>
</html>