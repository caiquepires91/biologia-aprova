<style>

	#logout-btn i {
		line-height: 21px;
	}

	.brand-wrapper {
		display: flex;
	}
	
	.navbar-brand img {
		width: 60px !important;
	}

	.navbar-brand h1 {
		font-size: 1.3rem;
		margin: auto;
		float: left;
	}

	.navbar-brand p {
		padding: 0;
		text-align: left;
		font-weight: 700;
		margin: 0;
		font-family: 'LUFGA';
		color: #003373;
	}

	.navbar-brand .logo-subtitulo {
		font-size: 9px;
		line-height: 9px;
		font-weight: 500;
	}

	.navbar-brand .brand-text-wrapper {
		display: flex;
		margin-left: -22px;
    	margin-top: 10px;
	}

	.rodape-brand h2 {
		font-size: 2rem;
		margin: auto;
		float: left;
	}

	.rodape-brand img {
		width:95px;
	}

	.rodape-brand p {
		padding: 0 !important;
		text-align: left !important;
		font-weight: 600 !important;
		margin: 0 !important;
		font-family: 'LUFGA' !important;
		color: white !important;
	}

	.rodape-brand .logo-subtitulo {
		font-size: 14px;
		line-height: 9px;
		font-weight: 300 !important;
	}

	.rodape-brand .brand-text-wrapper {
		display: flex;
		margin-left: -24px;
		margin-top: 10px;
	}



</style>


@extends('master')
@section('content')

<div class="row">
	<div class="col">
		<!-- Nav tabs -->
		<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light">
			<div class="container">
					<div class="brand-wrapper">
						<a class="navbar-brand" href="{{url::to('/')}}">
							<div class="row">
								<div class="col-6">
									<img src="{{url::to('img/logo_100x100.png')}}" alt="plataforma online - biologia aprova"/>
								</div>
								<div class="col-6 brand-text-wrapper">
									<h1 class="head">
										<p style="line-height: 10px;">biologia</p><p>aprova</p><p class="logo-subtitulo">Prof.<sup>a</sup> Mary Ann</p>
									</h1>
								</div>
							</div>
						</a>
					</div>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav nav nav-pills ms-auto mb-2 mb-lg-0 p-3 me-3" id="pills-tab" role="tablist">
						<li class="nav-item position-relative" role="presentation">
							<a class="stretched-link" href="{{url::to('/#section-3')}}"></a>
							<a class="nav-link" id="pills-faq-tab" data-bs-toggle="pill" href="#pills-faq" aria-controls="pills-faq" aria-selected="false">o que é?</a>
						</li>
						<li class="nav-item position-relative" role="presentation">
							<a class="stretched-link" href="{{url::to('/#section-2')}}"></a>
							<a class="nav-link" id="pills-faq-tab" data-bs-toggle="pill" href="#pills-faq" aria-controls="pills-faq" aria-selected="false">quem é Mary Ann Saraiva?</a>
						</li>
						<li class="nav-item position-relative" role="presentation">
							{{-- <a class="stretched-link" href="{{url::to('/conta/curso')}}"></a> --}}
							<a class="stretched-link" href="{{url::to('/planos')}}"></a>
							<a class="nav-link" id="pills-faq-tab" data-bs-toggle="pill" href="#pills-faq" aria-controls="pills-faq" aria-selected="false">assine já!</a>
						</li>
						<li class="nav-item position-relative" role="presentation">
							<a class="stretched-link" href="{{url::to('/#section-6')}}"></a>
							<a class="nav-link" id="pills-faq-tab" data-bs-toggle="pill" href="#pills-faq" aria-controls="pills-faq" aria-selected="false">cases de sucesso</a>
						</li>
						<!-- <li class="nav-item position-relative" role="presentation">
							<a class="stretched-link" href="{{url::to('/#section-questions')}}"></a>
							<a class="nav-link" id="pills-faq-tab" data-bs-toggle="pill" href="#pills-faq" aria-controls="pills-faq" aria-selected="false">faq</a>
						</li> -->
						<li class="nav-item position-relative me-md-1" role="presentation">
							<a class="stretched-link" href="
							@if(isset($user->plano))
								{{url::to('conta/curso')}}
							@else
								{{url::to('conta')}}
							@endif"
							></a>
							<a class="nav-link {{$page == 'area-aluno' || $page == 'curso' || $page == 'aula' || $page == 'simulado' || $page == 'resolucao' || $page == 'resultado' || $page == 'termo-uso' || $page == 'redefinir-senha'  ? 'active' : ''}} nav-last-button"
							id="pills-aluno-tab" data-bs-toggle="pill" href="#pills-aluno" role="tab" aria-controls="pills-aluno" aria-selected="false">
							@if (isset($user))
								@php $strings = explode(" ", $user->nome); @endphp
								@isset($strings[0])
									{{$strings[0]}}
								@endisset
								@isset($strings[1])
									{{" " . $strings[1]}}
								@endisset
							@else
							área do aluno
							@endif
							</a>
						</li>
						@if (isset($user))
						<li style="display:flex" >
							<a id="logout-btn" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Sair" href="{{url::to('/logout')}}" style="white-space: nowrap;"><i class="fas fa-sign-out-alt"></i></a>
						</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>

		<div class="tab-content p-0" id="pills-tabContent">
			<div class="tab-pane fade {{$page == 'main' || $page == 'erro' ? 'active show' : ''}}" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
				@if ($page == 'erro')
					@include('content.erro')
				@elseif ($page == 'main')
					@include('content.home')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'videoaulas' ? 'active show' : ''}}" id="pills-videoaulas" role="tabpanel" aria-labelledby="pills-videoaulas-tab">
				@if ($page == 'videoaulas')
					@include('content.videoaulas')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'news' || $page == 'blog' ? 'active show' : ''}}" id="pills-blog" role="tabpanel" aria-labelledby="pills-blog-tab">
				@if ($page == 'news' || $page == 'blog')
					@include('content.blog')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'planos-pagamento' || $page == 'planos-login' || $page == 'planos' || $page == 'planos-boleto' ? 'active show' : ''}}" id="pills-planos" role="tabpanel" aria-labelledby="pills-planos-tab">
				@if ($page == 'planos-login')
					@include('content.planos.login')	
				@elseif ($page == 'planos')
					@include('content.planos')
				@elseif ($page == 'planos-pagamento')
					@php
						$now = new DateTime("now");
						$dueTime = new DateTime("2021-10-06 23:59");
					@endphp 
			
					{{-- $now <= $dueTime --}}
					{{-- @if ($now <= $dueTime)  --}}
					@if (true) 
						@include('content.planos.pagamento')
					@else
						@include('content.planos.nopagamento')
					@endif
				@elseif ($page == 'planos-boleto')
					@include('content.planos.boleto')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'faq' || $page == 'perguntas' ? 'active show' : ''}}" id="pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">
				@if ($page == 'faq')
					@include('content.faq')
				@elseif ($page == 'perguntas')
					@include('content.faq')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'contato' ? 'active show' : ''}}" id="pills-contato" role="tabpanel" aria-labelledby="pills-contato-tab">
				@if ($page == 'contato')
					@include('content.contato')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'teste' ? 'active show' : ''}}" id="pills-teste" role="tabpanel" aria-labelledby="pills-teste-tab">
				@if ($page == 'teste')
					@include('content.planos.teste')
				@endif
			</div>
			<div class="tab-pane fade {{$page == 'area-aluno' || $page == 'curso' || $page == 'aula' || $page == 'simulado' || $page == 'resolucao' || $page == 'resolucao_aberta' || $page == 'resultado_aberto' || $page == 'resultado' || $page == 'termo-uso' || $page == 'simulado-aberto' || $page == 'redefinir-senha' ? 'active show' : ''}}" id="pills-aluno" role="tabpanel" aria-labelledby="pills-aluno-tab">
				@if ($page == 'area-aluno')
					@include('content.conta')
				@elseif ($page == 'curso')
					@include('content.curso')
				@elseif ($page == 'aula')
					@include('content.curso.aula')
				@elseif ($page == 'simulado')
					@include('content.curso.simulado')
				@elseif ($page == 'resolucao')
					@include('content.curso.resolucao')
				@elseif ($page == 'resolucao_aberta')
					@include('content.curso.resolucao_aberta')
				@elseif ($page == 'resultado_aberto')
					@include('content.curso.resultado_aberto')
				@elseif ($page == 'resultado')
					@include('content.curso.resultado')
				@elseif ($page == 'termo-uso')
					@include('content.termo')
				@elseif ($page == 'simulado-aberto')
					@include('content.simulado_aberto')
				@elseif ($page == 'redefinir-senha')
					@include('content.recuperarSenha')
				@endif
			</div>
		</div>
	</div>
</div>
<!-- <div class="row rodape mt-md-5">
	<div class="col-md-6">
		<p style="margin-top: 35px; padding:1rem;">Copyright © 2018-{{date('Y')}} Biologia Aprova. Todos os direitos reservados.</p>
	</div>
	<div class="col-md-6 p-3 postion-relative">
		<p>Desenvolvido por <a href="{{url::to('https://re9agencia.com.br/index.php')}}"><img src="{{url::to('img/logo-menor-re9.png')}}"/></a></p>
	</div>
</div> -->

<div class="rodape">
	<div class="container">
		<div class="row justify-content-center align-items-center row-2">
			<div class="col-md-3">
				<p style="text-align:left" >
					<!-- <a class="navbar-brand" href="{{url::to('/')}}">
						<img src="{{url::to('img/logo.png')}}" alt="biologia aprova" style="width:185px">
					</a> -->
					<div class="logo-wrapper" style="max-width: 225px;">
						<a class="rodape-brand" href="{{url::to('/')}}">
							<div class="row">
								<div class="col-6">
									<img src="{{url::to('img/logo_300x300.png')}}" alt="plataforma online - biologia aprova"/>
								</div>
								<div class="col-6 brand-text-wrapper">
									<h2>
										<p style="line-height: 10px;">biologia</p><p>aprova</p><p class="logo-subtitulo">Prof.<sup>a</sup> Mary Ann</p>
									</h2>
								</div>
							</div>
						</a>
					</div>
				</p>
			</div>
			<div class="col-md-6 col-links" style="display: flex;">
				<div style="margin:auto" >
					<!-- <span><a href="{{url::to('contato')}}" target="_blank">Contato</a></span> -->
					<span><a href="{{url::to('termo-uso')}}" target="_blank">Termos de Uso</a></span>
					<!-- <span><a href="{{url::to('/#section-questions')}}">FAQ</a></span> -->
				</div>
			</div>
			<div class="col-md-3" style="display: flex;" >
				<div class="social-media-wrapper">
					</a>
					<a target="_blank" href="{{url::to('https://www.instagram.com/biologiaaprova')}}" >
					<div class="social-media">
						<i class="fab fa-instagram"></i>
					</div>
					</a>
					<a target="_blank" href="{{url::to('https://www.facebook.com/biologiaaprova')}}" >
					<div class="social-media">
						<i class="fab fa-facebook-f"></i>
					</div>
					</a>
					<a target="_blank" href="{{url::to('https://www.youtube.com/channel/UCuolE1BPgaa1DWBRh7ttLGQ')}}" >
					<div class="social-media">
						<i class="fab fa-youtube"></i>
					</div>
					<a target="_blank" href="{{url::to('https://twitter.com/biologiaaprova')}}" >
					<div class="social-media">
						<i class="fab fa-twitter"></i>
					</div>
					</a>			
				</div>
			</div>
		</div>
		<div class="row justify-content-between align-items-center row-3 mb-3">
			<div class="col-md-7">
				<!-- <a class="link-light" href="{{url::to('termo-uso')}}" target="_blank"><p style="text-align:left">Copyright © 2018-{{date('Y')}} Biologia Aprova. Todos os direitos reservados.</p></a> -->
				<p style="text-align:left">Copyright © 2018-{{date('Y')}} Biologia Aprova. Todos os direitos reservados.</p>
			</div>
			<div class="col-md-3">
				<p style="text-align:right" >Desenvolvido por <a href="{{url::to('https://re9agencia.com.br/index.php')}}"  target="_blank"  ><img class="logo-re9" alt="re9 agência" src="{{url::to('img/logo-menor-re9.png')}}"/></a></p>
			</div>
		</div>
	</div>
</div> 
	
@stop