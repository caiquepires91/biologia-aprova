<style>img {display:block}</style>
<body style='margin: 0px'>
<table valign='top' cellpadding='0' cellmarging='0' cellspacing='0' border='0' width='100%' bgcolor='' align='center' style='border-collapse: collapse; padding: 0px; margin: 0px;'>
<tr style='margin: 0px;'>
	<td valign='top' style='border-collapse: collapse;' height='944'>
	<table cellpadding='0' cellspacing='0' border='0' width='600' align='center' style='border-collapse: collapse; font-family: Verdana; font-size: 12px;line-height:130%'>
		<tr>
			<td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse; width: 72%' >
			<img src='http://www.biologiaaprova.com.br/img/logo_100x100.png' height='80' style='margin-left: 20px;'/></td>
			<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
				<a href='https://twitter.com/biologiaaprova'><img src='http://www.biologiaaprova.com.br/img/ico_twitter_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Twitter' /></a>
			</td>
			<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'><a href='https://www.youtube.com/channel/UCuolE1BPgaa1DWBRh7ttLGQ' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_you_tube_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso canal no YouTube' /></a>
			</td>
			<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
				<a href='https://www.instagram.com/biologiaaprova/' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_instagram_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Instagram' /></a>
			</td>
			<td bgcolor='#FFFFFF' valign='top' style='border-collapse: collapse; width:7%;'>
				<a href='https://www.facebook.com/biologiaaprova/' style='margin-right: 10px; '><img src='http://www.biologiaaprova.com.br/img/ico_facebook_email.png' style='margin-top: 30px; float: left;' border='0' height='22' alt='nosso Facebook' /></a>						
			</td>
		</tr>              
			<tr>
				<td valign='top' bgcolor='#FFFFFF' style='color: #333333; border-collapse: collapse;' colspan='10'>
					<hr style='width: 90%; height: 6px; border: none; background: #002269; float: left; margin: 10px 5%; '>                         
					<p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0px 80px;'>
					Ol&aacute;, <b> {{$nome}}</b>!<br>
					<br>
					Uma solicita&ccedil;&atilde;o para redefini&ccedil;&atilde;o de senha foi realizada. Se voc&ecirc; solicitou a redefini&ccedil;&atilde;o, clique no link abaixo, ou copie e cole na barra de navega&ccedil;&atilde;o do seu navegador <i>web</i>.
					<br>
					<br>
					{{$url}}
					<br>
					<br>
					Atenciosamente,</br>
					Equipe Biologia Aprova</p>
				</td>
			</tr>           
			<tr>
				<td valign='top' bgcolor='#FFFFFF' style='border-collapse: collapse;' height='22' colspan='10'>
					<hr style='width: 90%; height: 6px; border: none; background: #29b372; float: left; margin: 10px 5%; margin-bottom: 0px; '>
				</td>
			</tr>           
			<tr>
				<td valign='top' height='50' style='padding-top: 0px; border-collapse: collapse;' colspan='10'>
					<p style=' line-height: 110%; color: #999999; padding: 5px 60px; margin: 0px; font-size: 10px;'>N&atilde;o responda a esta mensagem. Este e-mail foi enviado por um sistema autom&aacute;tico que n&atilde;o processa respostas.</br>
					Para esclarecer d&uacute;vidas ou enviar sugest&otilde;es, acesse nossa Central de relacionamento, escreva para contato@biologiaaprova.com.br<br><br>
					<a href='http://biologiaaprova.com.br/politicas_privacidade.php' target='blank' style='color: #999999; text-decoration: none;'>Pol&iacute;tica de Privacidade</a></br>
					 <a href='http://biologiaaprova.com.br/termos_uso.php' target='blank' style='color: #999999; text-decoration: none;'>Contrato</a></br></br>
					Copyright &copy; ".date('Y')." Biologia Aprova. Todos os direitos reservados.
					</p>										
				</td>
			</tr>                       
		</table>
	</td>
</tr>
</table>
</body>