<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pagseguro</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

        h1 {
            text-align: center;
            font-family: system-ui;
            margin-bottom: 3rem;
        }

        .wrapper {
            width: 600px;
            display: flex;
            margin: auto;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .box {
            width: 210px;
            background-color: #3c73ff;
            border-radius: 5px;
            padding: 1rem;
            display: flex;
            flex-direction: column;
        }

        .box .descricao-wrapper {
            height: 115px;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .box .descricao {
            color: white;
            font-weight: bold;
            font-size: 25px;
            font-family: sans-serif;
            line-height: 27px;
            text-align: center;
        }

        .box .valor {
            text-align: center;
            font-size: 30px;
            font-family: sans-serif;
            padding: 0.3rem;
            color: #00b46f;
            margin-top: 0px;
            text-shadow: 0 0 black;
            font-weight: bold;
            background: white;
        }

        .box button {
            height: 37px;
            background-color: white;
            margin: auto;
            border-radius: 36px;
            border: unset;
            padding: 0rem 1rem;
            font-weight: bold;
            text-transform: uppercase;
            color: #505050;
            cursor: pointer;
        }

        input {
            margin-bottom: .5rem;
        }

        .row {
            display: flex;
            justify-content: space-evenly;
            column-gap: 20px;
        }

        .row .col {
            width: 100%;
        }

        .row img {
            height: 130px;
            object-fit: contain;
            object-position: center;
        }

        .compras-row {
            width: 100%;
            display: flex;
            flex-direction: column;
            margin-top: 3rem;
        }

        .compras-row .compras-box {
            background-color: #18937c;
            margin: 0.54rem;
            padding: 10px;
            width: 100%;
        }

        .compras-row p {
            color: white;
            margin: 0px;
        }

        .compras-row p span {
            font-weight: bold;
        }
    </style>
</head>

<body class="antialiased">

    <!-- LightBox -->
    <script type="text/javascript"
        src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <div class="wrapper">
        <div class="row mb-5">
            <h1>PAGSEGURO</h1>
        </div>
        <div class="row">
            @foreach ($produtos as $produto)
                <div class="col">
                    <div class="box d-flex flex-direction-column">
                        <img src="{{ url('img/logo_300x300.png') }}" alt="Imagem do produto">
                        <div class="descricao-wrapper">
                            <p class="descricao">{{ $produto->descricao }}</p>
                        </div>
                        <p class="valor">{{ $produto->valor }}</p>
                        <button class="btn mx-auto" onclick="comprar({{ $produto->id }})">Comprar</button>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="compras-row">
            @php
                $metodo = [
                    1 => 'Cartão de crédito',
                    2 => 'Boleto',
                ];

                $status = [
                    1 => 'Aguardando Pagamento',
                    2 => 'Cancelado',
                    3 => 'Pago',
                    6 => 'Devolvido',
                ];
            @endphp
            @foreach ($compras as $compra)
                <div class="compras-box">
                    <p><span>Cliente:</span> {{ $compra->email }}</p>
                    <p><span>Método:</span> {{ $metodo[$compra->metodo] }}</p>
                    <p><span>Status:</span> {{ $status[$compra->status] }}</p>
                    <p><span>Produto:</span> {{ $produtos[$compra->produto_id - 1]->descricao }}</p>
                    <p><span>Valor:</span> {{ $compra->valor }}</p>
                    @if (!empty($compra->boleto))
                        <p><span>Boleto:</span> <a href="{{ $compra->boleto }}" target="_blank">ver</a></p>
                    @endif
                </div>
            @endforeach
        </div>
    </div>

    <script>
        var url_base = "{{ env('PAGSEGURO_BASE_URL') }}";

        const comprar = (id) => {
            const data = {
                "id": id,
            };
        }

        const chamar_lightbox = (code) => {}

        const salvarTransactionCode = (transactionCode) => {
            const data = {
                "transactionCode": transactionCode
            };
        }
    </script>

</body>

</html>
