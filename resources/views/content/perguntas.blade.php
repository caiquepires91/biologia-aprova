<div id="perguntas-section-1" class="row justify-content-center align-items-center">
    <div class="col">
        <h2 class="big-title">{{$titulo}}</h2>
    </div>
</div>
@if ($isSearch)
<div id="faq-search-text" class="row m-md-5 justify-content-center">
    <div class="col-md-8">
        <div style="width: fit-content; margin: auto;">
            <p>resultado para '{{$string}}'...</p>
        </div>      
    </div>
</div>
@endif
<div class="row m-md-5 justify-content-center" style="min-height: 57.3vh;">
    <div class="col-md-10">
        <div class="accordion accordion-flush" id="accordionFlushExample">
            @if ($page == 'perguntas') 
            @forelse ($perguntas as $key=>$pergunta)
            <div class="accordion-item my-md-3" id="faq-{{$pergunta->id}}">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{$pergunta->id}}" aria-expanded="false" aria-controls="flush-collapseOne">
                        <div style="display: flex;">
                            <div class="barra ms-md-1" style="background: #3cab73"></div>
                            <span class="letra" style="color: #3cab73">p.</span>
                            <span class="content  ms-md-4">{{$pergunta->pergunta}}</span>
                        </div>
                    </button>
                </h2>
                <div id="flush-collapse{{$pergunta->id}}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body" style="display: flex;">
                        <div class="barra ms-md-1" style="background: #adabab"></div>
                        <span class="letra my-1" style="color: #adabab">r.</span>
                        <div style="display: inline-flex;">
                            <p class="ms-md-4">{{$pergunta->resposta}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <p style="color: gray; text-align:center">não encontramos o que você está procurando <i class="fas fa-sad-tear"></i></p>
            @endforelse
            @endif
          </div>
    </div>
</div>