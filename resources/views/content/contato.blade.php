<link href="{{url::to('css/contato.css')}}" rel="stylesheet">

<div  class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">contato</h2>
    </div>
</div>

<div class="container" style="height: calc(100vh - 100px);">

<h4 class="contato-subtitle">deixe aqui sua mensagem</h4>

<div id="form_contato">
  <form id="contact_form" name="form_contato" onsubmit="return enviarMensagem(this);">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label for="nome">Nome<span style="color:red;">*</span>:</label>
        <input class="form-control" type="text" id="nome" name="nome" placeholder="Informe seu nome completo..." data-toggle="tooltip" data-placement="bottom" title="Este campo é obrigatório"/>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label for="email">E-mail<span style="color:red;">*</span>:</label>
        <input class="form-control" type="email" id="email" name="email" placeholder="Informe seu e-mail..." data-toggle="tooltip" data-placement="bottom" title="Este campo é obrigatório"/>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label for="assunto">Assunto:</label>
        <select id="assunto" name="assunto" class="selectpicker show-tick form-control">
          <option value="duvida">Dúvida</option>
          <option value="sugestao">Sugestão</option>
          <option value="critica">Crítica</option>
          <option value="elogio">Elogio</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label for="telefone" onkeypress="mask(this,mphone);" >Telefone:</label>
        <input class="form-control" type="text" id="telefone" name="telefone" placeholder="Informe seu telefone..." />
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="form-group">
        <label for="mensagem">Mensagem<span style="color:red;">*</span>:</label>
        <textarea name="mensagem" id="mensagem" class="form-control" placeholder="Informe a sua mensagem..." data-toggle="tooltip" data-placement="bottom" title="Este campo é obrigatório"></textarea>
      </div>
    </div>
  </div>

  <div id="message_btn">
      <button id="send_message" type="submit" style="border: none; background: none;">enviar mensagem<i class="fas fa-paper-plane"></i></button>
  </div>

  <div class="alert alert-success" id="aviso"></div>
  </form>
</div>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

<script>

  $(document).ready(function() {
    $('#telefone').mask("(99) 9999-99999")
    .focusout(function (event) {  
        var target, phone, element;  
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
        phone = target.value.replace(/\D/g, '');
        element = $(target);  
        element.unmask();  
        if(phone.length > 10) {  
            element.mask("(99) 99999-999?9");  
        } else {  
            element.mask("(99) 9999-9999?9");  
        }  
    });
  });

  function enviarMensagem(e) {

      //alert("on submit");

      var nome = $("#nome").val();
      var email = $("#email").val();
      var telefone = $("#telefone").val();
      var assunto = $("#assunto").val();
      var msg = $("#mensagem").val();
      var enviar = true;
      var email_ver = true;

      if(nome.trim() == "") { 
        $('#nome').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show'); 
        enviar = false; 
      }
      $('#email').attr('title', 'Este campo é obrigatório');

      if(email.trim() == "") { 
        $('#email').attr('title', 'Este campo é obrigatório').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show'); 
        enviar = false; 
      } else if(!isEmail(email)){
        $('#email').attr('title', 'E-mail inválido').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show'); 
        enviar = false;
      }

      if(msg.trim() == "") { 
        $('#mensagem').tooltip({placement: 'bottom',trigger: 'manual'}).tooltip('show'); 
        enviar = false; 
      }

      if(enviar) {
        $("#send_message").html('Enviando...');
        $.ajax({
          type:'POST',
          url: "{{url::to('/contato/send')}}",
          data:{'nome':nome, 'email':email, 'assunto':assunto, 'mensagem':msg, 'telefone':telefone},
          success:function(data){
            if(!$("#aviso").hasClass("alert-success")){
              $("#aviso").addClass("alert-success");
            }
            if($("#aviso").hasClass("alert-danger")){
              $("#aviso").removeClass("alert-danger");
            }
            $("#aviso").append("Mensagem enviada com sucesso!").fadeIn(500, function(){
              esconderStatus("#aviso");
            }); 
            //console.log(data);
            //alert("enviou");
            $("#nome").val(""); 
            $("#email").val("");  
            //$("#assunto").picker('val', "duvida");
            $("#mensagem").val("");
            $("#telefone").val("");
            $("#send_message").html('Enviar Mensagem<i class="fas fa-paper-plane"></i>');
          },
          error: function(){
            //alert("ops! deu algum erro");
            if(!$("#aviso").hasClass("alert-danger")){
              $("#aviso").addClass("alert-danger");
            }
            if($("#aviso").hasClass("alert-success")){
              $("#aviso").removeClass("alert-success");
            }
            $("#aviso").append("<strong>Atenção!</strong> Erro ao enviar mensagem.").fadeIn(500, 
              function(){ esconderStatus("#aviso"); }
            );
          }
        });
      } else {
        $("body").animate({
          scrollTop: $(".tooltip-inner").offset().top - 150
        }, 1000);
      }

      return false;
  }

    function mask(o, f) {
    setTimeout(function() {
      var v = mphone(o.value);
      if (v != o.value) {
        o.value = v;
      }
    }, 1);
}

function mphone(v) {
  var r = v.replace(/\D/g, "");
  r = r.replace(/^0/, "");
  if (r.length > 10) {
    r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
  } else if (r.length > 5) {
    r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
  } else if (r.length > 2) {
    r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
  } else {
    r = r.replace(/^(\d*)/, "($1");
  }
  return r;
}

  function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
  }

  $('#nome, #email, #mensagem').on('focus', function(){
    $(this).tooltip('hide');
  });

  function esconderStatus(element) {
  if($(element).is(":visible")){
    set = setTimeout(function(){
      $(element).fadeOut(500, function() {
        $(element).html("");
      });
    }, 3000);
  }
  }

</script>