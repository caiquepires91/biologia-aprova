<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<style>
    #loader-wrapper {
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: #222222e3;
        z-index: 1021;
        display: flex;
    }

    #loader {
        z-index: 1022; /* anything higher than z-index: 1000 of .loader-section */
        margin:auto;
        position: absolute;
        top:50%;
        right: 44%;
    }
    #loader h2 {
        font-size: xxx-large;
        font-weight: 700;
        color: #EEEEEE;
    }

    .has-validation {
        margin-bottom: 34px !important;
    }

    .alert-success {
        color: #f9fafb;
        background-color: #0d6efd;
        border-color: #1267d3;
    }
</style>

<div id="loader-wrapper">
    <div id="loader"><h2>aguarde...</h2></div>
    <div class="loader-section">
    </div>
</div>



<div id="planos-login-section-1" class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">Teste de conhecimento</h2>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-6 mt-md-5 mt-4 mb-md-4">
        <div class="slide-in5 m-auto" style="width:100%">
            <div class="alert alert-success">
                <form  id="cadastro-usuario-simulado-aberto" action="{{url('cadastrar-usuario-simulado-aberto')}}" class="p-0 m-0" novalidate>
                    <div class="col-md-12">
                        <div class="row mb-1">
                            <h2 style="font-size: xx-large;font-weight: 800;">Olá, aluno do Biologia.</h2>
                        </div>
                        <div class="row">
                            <p>Preencha os dados abaixo antes de <strong>continuar  <i class="fas fa-arrow-alt-circle-right"></i></strong>.</p>
                        </div>
                        <div class="row">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-nome" class="form-label">nome completo*:</label>
                                <input type="text" class="form-control" id="cadastrar-nome" name="nome" required aria-describedby="cadastrar-nome-fb">
                                <div id="cadastrar-nome-fb" class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-email" class="form-label">e-mail*:</label>
                                <input type="email" class="form-control" id="cadastrar-email" name="email" required aria-describedby="cadastrar-email-fb">
                                <div id="cadastrar-email-fb" class="invalid-tooltip">
                                    Insira um e-mail válido.
                                </div>
                            </div>
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-telefone" class="form-label">telefone*:</label>
                                <input type="tel" class="form-control" id="cadastrar-telefone" name="telefone" required aria-describedby="cadastrar-telefone-fb">
                                <div id="cadastrar-telefone-fb" class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="slidein5-content row">
                            <div class="col d-flex">
                                <button type="submit" class="ms-auto btn btn-outline-success" style="font-size: larger; font-weight: bolder;" >
                                    Continuar
                                    <i class="fas fa-arrow-alt-circle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-100"></div>
        <div id="warning-box" class="col-12 align-self-center mx-auto mb-5" style="display: none;">
            <p class="warning-text" style="color:red"></p>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        $("#cadastrar-telefone").mask("(99) 99999-9999");
        $("#cadastro-usuario-simulado-aberto").submit(submit_cadastro);
    });

    function submit_cadastro(e) {
        e.preventDefault();
        e.stopPropagation();

        $("input").removeClass("is-invalid");
        $("input").removeClass("is-valid");

        var nome = $("#cadastrar-nome");
        var email = $("#cadastrar-email");
        var telefone = $("#cadastrar-telefone");

        var cancel = false;
        var input = null;

        if (telefone.val() == "") {
            cancel = true;
            input = telefone;
        }
        
        if (!isEmail(email.val())) {
            cancel = true;
            input = email;
        }

        if (nome.val() == "") {
            cancel = true;
            input = nome;
        }

        var formSerialized = $(this).serializeArray();
        
        if (cancel) {
            input.addClass("is-invalid");
            input.focus();
        } else {
            $.post($(this).attr('action'), formSerialized, function(data) {
                if (data.success) {
                    console.log("data user id: " + data.user.id);
                    // let link = `{{url('conta/curso/simulado/resolucao-aberta/123/${data.user.token_id}')}}`;
                    let link = `{{url('conta/curso/simulado/resolucao-aberta/145/${data.user.id}')}}`;
                    if (data.resposta) {
                        link = `/resultado_aberto/${data.user.id}/${data.resposta.total}/${data.resposta.acertos}`;
                    } 
                    window.location.href = link;
                } else {
                    console.log("success false", data.msg);
                    $("#warning-box .warning-text").html(data.msg);
                    $("#warning-box").fadeIn();
                    console.log("cadastro falhou!!");
                }
            })
            .fail(function() {
                $("#warning-box .warning-text").html("Cadastro falhou! Por favor, tente mais tarde.");
                $("#warning-box").fadeIn();
                console.log("cadastro falhou!!")
            });
        }

    }

    function isEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

</script>