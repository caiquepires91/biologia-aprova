<!-- PLANOS DE PAGAMENTO -->
<div class="row">
    <div class="col">
        <div id="planos-section-1" class="row align-items-center">
            <div class="row  justify-content-start align-items-center">
                <div class="col-md-6 bloco">
                    <p class="big-title">vamos começar agora?</p>
                    <p class="subtitle">escolha o seu plano!</p>
                </div>
                <div class="faq-imagem col-md-4 col-lg-2">
                    <div style="width: 100%; height: 100%; display: flex;">
                        <img src="{{ url::to('/img/planos-header.png') }}" alt="biologia aprova - plano de fundo" />
                    </div>
                </div>
            </div>
        </div>

        <div id="planos-section-2" class="row justify-content-center align-items-center">
            <div class="col-md-4">
                <div class="plano position-relative m-md-auto plano-principal">
                    <div class="container plano-header align-items-center"
                        style="height:140px; margin-bottom: -210px;background-image: linear-gradient(to right, rgb(13 110 253) , rgb(65 134 236));">
                        <p class="pt-3 mx-auto">curso</p>
                        <p class="mb-auto mx-auto">completo</p>
                        {{-- <p style="font-size: 30px;font-weight: 300;">+ bônus</p> --}}
                    </div>
                    <div class="plano-body m-md-auto px-5 border bg-light border-2 bloco">
                        <p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 14rem;">assine já!
                        </p>
                        <div id="plano-scroll" class="vantagens mb-3">
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>materiais do curso completo;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>+ conteúdos bônus listados abaixo;
                            </p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>8 Encontros de tira dúvidas;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>4 Simulados TRIEDUC (ENEM), com
                                correção de prova e relatório de desempenho;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>Acesso ao Curso Intensivo para
                                revisão do conteúdo;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>Acesso a 2 aulões ao vivo;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>3 Diagnose de Biologia.</p>
                        </div>
                        <p style="font-size: 1.7rem;">por apenas</p>
                        <span>
                            <h2 class="position-relative">
                                <span class="position-absolute end-100 plano-preco-a">R$</span>
                                21
                                <span class="position-absolute start-100 plano-preco-b">,90</span>
                                <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
                            </h2>
                        </span>
                        {{-- <h2 style="
									margin-top: 9px;
									font-size: 1.8rem;
									line-height: 25px;
									font-weight: 700;
									text-align: center;
									margin-left:0px;
									border: 1px solid;
									width: 100%;
									padding: 5px;
									border-radius: 5px;
									color: #0d6efdc2;
									text-shadow: -3px 2px 2px #0000001f;
									">
								aguarde<br>
								novas<br>
								turmas
								</h2> --}}
                    </div>
                    <div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
                        <button class="green-btn btn rounded-pill" data-plano="5" type="button"
                            onclick="goToLogin(this);" style="font-size: xx-large;">eu quero!</button>
                    </div>
                </div>
            </div>
            @php
                $today = new DateTime('now');
                $target_day = new DateTime(config('constants.outras.deadline_aprova_mais'));
            @endphp
            {{-- <div class="col-md-4 d-none">
					<div class="plano position-relative m-md-auto plano-principal">
						<div class="container plano-header align-items-center">
							<p class="mx-auto" style="padding-top: 1.2rem;">curso completo</p>
						</div>
						<div class="plano-body m-md-auto px-5 border bg-light border-2 bloco" style="height: 720px;">
							<p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 12.2rem;">assine já!</p>
							<div class="vantagens mb-3">
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os assuntos de biologia;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de questões;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e gabaritados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e comentados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para memorização;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
							</div>
							<p style="font-size: 1.7rem;">por apenas</p>
							<span><h2 style="margin-left: 19%;" class="position-relative">
									<span class="position-absolute end-100 plano-preco-a">R$</span>
									25
									<span class="position-absolute start-100 plano-preco-b">,55</span>
									<span class="position-absolute start-100 plano-preco-c">/MÊS</span>
								</h2>
							</span>
						</div>
						<div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
							<button class="green-btn btn rounded-pill" data-plano="6" type="button" onclick="goToLogin(this);" style="font-size: xx-large;">eu quero!</button>
						</div>
					</div>
				</div> --}}
        </div>

        <!-- <div class="row justify-content-center">
    <div class="col-md-3">
     <div class="plano position-relative m-auto" style="width: 272px; margin-bottom: 10% !important;">
      <div class="container d-grid plano-header">
       <h1>uti</h1>
      </div>
      <div class="plano-body come-up m-auto px-5 border bg-light border-2">
       <p style="font-size: larger;">por apenas</p>
       <span><h1 class="position-relative">
         <span id="plano-scroll" class="position-absolute end-100 plano-preco-a">R$</span>
         29
         <span class="position-absolute start-100 plano-preco-b" >,90</span>
         <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
        </h1>
       </span>
       <p>1 mês de acesso ilimitado</p>
      </div>
      <div class="d-grid mx-5" style="margin-top: -20px;">
       <button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();">eu quero!</button>
      </div>
     </div>
    </div>
    <div class="col-md-3">
     <div class="plano position-relative m-auto" style="width: 272px; margin-bottom: 10% !important;">
      <div class="container d-grid plano-header">
       <h1>intensivão</h1>
      </div>
      <div class="plano-body come-up m-auto px-5 border bg-light border-2">
       <p style="font-size: larger;">por apenas</p>
       <span><h1 class="position-relative">
         <span class="position-absolute end-100 plano-preco-a">R$</span>
         24
         <span class="position-absolute start-100 plano-preco-b">,90</span>
         <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
        </h1>
       </span>
       <p>5 meses de acesso ilimitado</p>
      </div>
      <div class="d-grid mx-5" style="margin-top: -20px;">
       <button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();">eu quero!</button>
      </div>
     </div>
    </div>
    <div class="col-md-3">
     <div class="plano position-relative m-auto" style="width: 272px;  margin-bottom: 10% !important;">
      <div class="container d-grid plano-header">
       <h1>CURSO TOTAL</h1>
      </div>
      <div class="plano-body come-up plano-destaque m-auto px-5 border border-2">
       <p style="font-size: larger;">por apenas</p>
       <span><h1 class="position-relative">
         <span class="position-absolute end-100 plano-preco-a">R$</span>
         15
         <span class="position-absolute start-100 plano-preco-b" >,90</span>
         <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
        </h1>
       </span>
       <p>10 meses de acesso ilimitado</p>
      </div>
      <div class="d-grid mx-5" style="margin-top: -20px;">
       <button class="yellow-btn primary-btn btn rounded-pill" type="button" onclick="goToLogin();" >eu quero!</button>
      </div>
     </div>
    </div>
   </div> -->
    </div>
</div>

<div class="mt-5"></div>

<script>
    function goToLogin(elem) {
        var plano = $(elem).data("plano");
        window.location.href = "{{ url::to('planos/choose') }}" + "/" + plano;
    }

    var isInTheViewport = function(elem) {
        var hT = elem.offset().top,
            hH = elem.outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        //console.log ("ht:" + hT + " hH:" + hH + " wH:" + wH + " wS:" + wS);
        return (wS > (hT - wH) && (hT > wS) && (wS + wH > hT + hH));
    };

    $(window).scroll(function() {
        if (isInTheViewport($('#plano-scroll'))) {
            //console.log("in the viewport");
            $('.plano-body').addClass('come-up');
        }
    });
</script>
