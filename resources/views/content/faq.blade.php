<div class="row faq-section-1">
    <!-- mudar o width dessa row de acordo com o width de faq-section-1 pra compensar o overflow -->
    <div class="row justify-content-start align-items-center">
        <div class="col-md-6">
            <div class="faq-bloco">
                <h2>o que você está procurando?</h2>
                <div class="input-group my-5 mx-5 search-shadow" style="width: 80%;">
                    <input id="faq-input" type="text" class="form-control typeahead" placeholder="Pesquisar" aria-label="Pesquisar" aria-describedby="faq-search" autocomplete="off">
                    <button class="btn btn-outline-secondary" type="button" id="faq-search"><i class="fa fa-search"></i></button>
                </div>
                <p>pesquise por respostas em nossa base de conhecimento.</p>
            </div>
        </div>
        <div class="faq-imagem col-md-4 col-lg-2">
            <div style="width: 100%; height: 100%; display: flex;">
                <img src="{{url::to('/img/faq-header.png')}}" alt="biologia aprova - imagem faq"/>    
            </div>
        </div>
    </div>
</div>

@isset($titulo)
    @if(!$isSearch)
<div class="faq-questions-title row justify-content-center align-items-center">
    <div class="col">
        <h2 class="big-title">{{$titulo}}</h2>
    </div>
</div>
<div class="faq-questions-wavy row justify-content-center">
    <div class="col-md-7 mt-2">
        <div class="container wavy-line"><img src="{{url::to('img/long-wavy-line.png')}}" alt="biologia aprova - linha ondulada"></div>
    </div>
</div>
@endif
@endisset
@if ($isSearch)
<div id="faq-search-text" class="row m-md-5 justify-content-center">
    <div class="col-md-8">
        <div style="width: fit-content; margin: auto;">
            <p>resultado para '{{$string}}'...</p>
        </div>      
    </div>
</div>
@endif

@if ($page == 'perguntas')
<div id="section-questions" class="row m-md-5 justify-content-center">
    <div class="col-md-10">
        <div class="accordion accordion-flush" id="accordionFlushExample">
            @forelse ($perguntas as $key=>$pergunta)
            <div class="accordion-item my-md-3" id="faq-{{$pergunta->id}}">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{$pergunta->id}}" aria-expanded="false" aria-controls="flush-collapseOne">
                        <div style="display: flex;">
                            <div class="barra" style="background: #3cab73"></div>
                            <span class="letra" style="color: #3cab73">p.</span>
                            <span class="content  ms-md-4">{{$pergunta->pergunta}}</span>
                        </div>
                    </button>
                </h2>
                <div id="flush-collapse{{$pergunta->id}}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body" style="display: flex;">
                        <div class="barra" style="background: #adabab"></div>
                        <span class="letra my-1" style="color: #adabab">r.</span>
                        <div style="display: inline-flex;">
                            <p class="ms-md-4">{!! $pergunta->resposta !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <p style="color: gray; text-align:center">não encontramos o que você está procurando <i class="fas fa-sad-tear"></i></p>
            @endforelse
          </div>
    </div>
</div>
@endif

<div class="my-container my-md-5  my-lg-5">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="m-5 card-flyer faq-plano position-relative" >
                <i class="fas fa-credit-card"></i>
                <h4>planos e pagamento</h4>
                <p>Consulte nossos planos de assinatura e tire todas  as suas  dúvidas sobre o pagamento.</p>
                <a href="{{url::to('/faq/perguntas/0')}}" class="stretched-link"></a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="m-5 card-flyer faq-plano position-relative" >
                <i class="fas fa-user-circle"></i>
                <h4>cadastro, login e senhas</h4>
                <p>Tire todas suas dúvidas sobre seu perfil de usuário.</p>
                <a href="{{url::to('/faq/perguntas/1')}}" class="stretched-link"></a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="m-5 card-flyer faq-plano position-relative" >
                <i class="fas fa-cogs"></i>
                <h4>funcionalidades da plataforma</h4>
                <p>Fique por dentro de todas as vantagens do Biologia Aprova.</p>
                <a href="{{url::to('/faq/perguntas/2')}}" class="stretched-link"></a>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row"  style="height: auto; background: linear-gradient(44deg, #19d275, #3eb378);">
    <div class="col">
        <div style="height: 120px;">
        </div>
    </div>
</div> -->
<script>

    var path = "{{ route('faq-autocomplete') }}";
    //console.log(path);

    $( document ).ready(function() {
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                //console.log(query);
                return $.get(path, { str: query }, function (data) {
                        //console.log(data);
                        return process(data);
                });
            },
            updater: function(selection){
                //console.log("You selected: " + selection);
            // var str = 'some // slashes', replacement = '\\/'; var replaced = selection.replace(/\//g, replacement);

                //console.log(encodeURIComponent(selection).replace(".", "%2E") + "/");
                //alert("está aqui!");
                window.location="{{url::to('faq-busca')}}" + "\/" + encodeURIComponent(selection).replace(".", "%2E");
            /*  $.get("{{route('faq-busca')}}", {'pergunta':selection}, function (faqs) {
                    //console.log(faqs);
                }); */
            }
        });
    });
 
    $('#faq-search').on('click', function() {
       console.log(document.getElementById("faq-input").value);
       //alert("faq");
       if (document.getElementById("faq-input").value != "") {
            window.location="{{url::to('faq-busca')}}" + "\/" + document.getElementById("faq-input").value;
       }
    });

  
</script>
