@php
$PLANOS = [
    'EXTENSIVO MED' => '190,00',
    'EXTENSIVO ENEM' => '160,00',
    'TESTE GRÁTIS' => '00,00',
    'INTENSIVÃO ENEM' => '250,00',
    'CURSO COMPLETO' => '262,80',
    'CURSO TOTAL' => '43,20',
];
@endphp

<link href="{{ URL::to('/css/conta.css') }}" rel="stylesheet">
<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<!-- Modal -->
<div class="modal fade" id="autorizadoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pagamento Pendente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                O pagamento está pendente. Notificaremos por e-mail assim que recebermos a confirmação.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal - editar foto -->
<div class="modal fade" id="editPhotoModal" tabindex="-1" data-bs-backdrop="static"
    aria-labelledby="editPhotoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form class="needs-validation" id="formEditPhoto" method="post"
                action="{{ url::to('conta/alterar-imagem') }}" enctype="multipart/form-data" novalidate>
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        style="width: 20px;margin-right: unset;"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row mb-4">
                            <div class="col">
                                <div class="mx-auto d-block profile-image"
                                    style="background-image:url({{ url::to('/img/profile/') . '/' . $user->foto }})">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-1" style="display:grid">
                                    <label for="inputEditPhoto" class="form-label" id="labelEditPhoto"
                                        style="margin:auto">selecionar imagem</label>
                                    <input class="form-control form-control-sm" aria-describedby="labelEditPhoto"
                                        id="inputEditPhoto" name="image" type="file" required>
                                    <div id="invalidFBEditPhoto" class="invalid-feedback">
                                        Por favor escolha um arquivo válido.
                                    </div>
                                    <div class="valid-feedback">
                                        Foto alterada com sucesso!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">alterar imagem</button>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    #loader-wrapper {
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: #222222e3;
        z-index: 1021;
        display: flex;
    }

    #loader {
        z-index: 1022;
        /* anything higher than z-index: 1000 of .loader-section */
        margin: auto;
        position: absolute;
        top: 50%;
        right: 44%;
    }

    #loader h2 {
        font-size: xxx-large;
        font-weight: 700;
        color: #EEEEEE;
    }

    .plano-contratado-item {
        margin-bottom: 10px;
        border: 1px #ccc solid;
        padding: 1rem;
    }

    .plano-contratado-item p {
        margin-bottom: 0px;
        font-size: 15px;
    }

    .plano-contratado-item p strong {
        color: #6c6c6c;
    }
</style>

<div id="loader-wrapper">
    <div id="loader">
        <h2>aguarde...</h2>
    </div>
    <div class="loader-section">
    </div>
</div>


<?php
// Navegação
$curso['navegacao'] = [
    [
        'titulo' => 'Área do aluno',
        'icone' => '',
        'url' => url::to('/conta/curso'),
        'active' => false,
    ],
    [
        'titulo' => 'Perfil',
        'icone' => 'fa fa-play',
        'url' => url::to('/conta'),
        'active' => true,
    ],
];
?>

<!-- navegação-->
<div class="plataforma-navegacao margin-header">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-8 col-xl-9">
                <ul class="navegacao">
                    <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                    <?php foreach ($curso["navegacao"] as $nav) { ?>
                    <li class="<?php echo $nav['active'] ? 'active' : ''; ?>">
                        <a href="<?php echo $nav['url']; ?>"><i class="<?php echo $nav['icone']; ?>"></i> <?php echo $nav['titulo']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="col-md-4 col-xl-3">
                <div class="plataforma-progresso">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /navegação -->

<div id="planos-login-section-1" class="row m-0">
    <div class="col">
        <h2 class="big-title">minha área</h2>
    </div>
</div>

<div id="planos-login-section-2" class="container">
    <div class="conta-subcontainer row m-auto justify-content-center align-items-center">
        <div id="user-profile" class="profile col-md-4">
            <div class="conta-block p-md-5" style="background: #3eb378; height: 900px; width: 100%; color: white;">
                <div class="row position-relative">
                    <div class="mx-auto d-block profile-image"
                        style="background-image:url({{ url::to('/img/profile/') . '/' . $user->foto }})">
                    </div>
                    <a id="edit-photo-btn" class="btn btn-light position-absolute" data-bs-toggle="tooltip"
                        data-bs-placement="right" title="Editar foto" href="javascript:callEditPhotoModal()"><i
                            class="fas fa-edit"></i></a>
                </div>
                <p class="nome">{{ $user->nome }}</p>
                <p class="email">{{ $user->email }}</p>
                <div id="info-profile">
                    <p class="titulo">CPF</p>
                    <p id="cpf" class="conteudo">{{ $user->cpf }}</p>
                    <p class="titulo">data de nascimento</p>
                    <p class="conteudo">
                        {{ isset($user->datanascimento) ? date('d/m/Y', strtotime($user->datanascimento)) : '-' }}</p>
                    <p class="titulo">telefone</p>
                    <p class="conteudo">{{ isset($user->telefone) ? $user->telefone : '-' }}</p>
                    <p class="titulo">estado</p>
                    <p class="conteudo">{{ isset($estado->sigla) ? $estado->sigla : '-' }}</p>
                    <p class="titulo">cidade</p>
                    <p class="conteudo">{{ isset($cidade->nome) ? $cidade->nome : '-' }}</p>
                    <p class="titulo">cep</p>
                    <p class="conteudo">{{ $user->cep }}</p>
                </div>
            </div>
        </div>

        <div id="user-settings" class="profile col-md-8 p-md-5">
            <div class="conta-block">
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item m-3" id="info-profile-hide" style="display: none;">
                        <h2 class="accordion-header" id="flush-headingHide">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseHide" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                                <i class="fas fa-user"></i> perfil
                            </button>
                        </h2>
                        <div id="flush-collapseHide" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingHide" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <p class="titulo">CPF</p>
                                <p class="conteudo">{{ $user->cpf }}</p>
                                <p class="titulo">data de nascimento</p>
                                <p class="conteudo">
                                    {{ isset($user->datanascimento) ? date('d/m/Y', strtotime($user->datanascimento)) : '-' }}
                                </p>
                                <p class="titulo">telefone</p>
                                <p class="conteudo">{{ isset($user->telefone) ? $user->telefone : '-' }}</p>
                                <p class="titulo">estado</p>
                                <p class="conteudo">{{ isset($estado->sigla) ? $estado->sigla : '-' }}</p>
                                <p class="titulo">cidade</p>
                                <p class="conteudo">{{ isset($cidade->nome) ? $cidade->nome : '-' }}</p>
                                <p class="titulo">cep</p>
                                <p class="conteudo">{{ $user->cep }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item m-3">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne" aria-expanded="true"
                                aria-controls="flush-collapseOne">
                                <i class="fas fa-user-graduate"></i> minha área
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse show multi-collapse">
                            <div class="accordion-body">
                                <p>Olá, {{ $user->nome }}.</p>
                                <p><strong><i class="fa fa-flask" aria-hidden="true"></i> PLANOS CONTRATADOS</strong>
                                </p>
                                @if (isset($user->plano))

                                    @php
                                        $unidadeTempo = ' meses';
                                        //if ($user->plano->plano === "INTENSIVÃO ENEM")
                                        //$unidadeTempo = " dias";
                                    @endphp

                                    @if ($user->plano->ativo)

                                        @isset($user->planos)
                                            @foreach ($user->planos as $plano)
                                                <div class="plano-contratado-item">
                                                    <p><strong>Plano escolhido: </strong>{{ $plano->plano }}</p>
                                                    <!-- <p><strong>Tempo: </strong> {{ $plano->tempo . $unidadeTempo }}</p> -->
                                                    @if ($plano->status != null)
                                                        <p>
                                                            <strong>Valor:</strong> <span class="valor_plano">
                                                                @if ($plano->cupom != null)
                                                                    <s>{{ "R$" . $PLANOS[$plano->plano] }}</s>
                                                                    <span id="desconto_cartao">
                                                                        R${{ number_format($plano->planoDesconto->valor / 100, 2) }}</span>
                                                                @else
                                                                    {{ "R$" . $PLANOS[$plano->plano] }}
                                                                @endif
                                                            </span>
                                                        </p>
                                                        @if ($plano->cupom != null)
                                                            <p>
                                                                <strong>Cupom:</strong> <span class="valor_plano">
                                                                    @if ($plano->planoDesconto != null)
                                                                        {{ $plano->cupom->cupom }}
                                                                    @endif
                                                                </span>
                                                            </p>
                                                        @endif
                                                    @endif
                                                    <p><strong>Renovações: </strong>{{ $plano->renovacoes - 1 }}</p>
                                                    <p><strong>Iniciado em:
                                                        </strong>{{ date('d-m-Y', strtotime($plano->inicio)) }}</p>
                                                    <p><strong>Finaliza em:
                                                        </strong>{{ date('d-m-Y', strtotime($plano->fim)) }}</p>
                                                </div>
                                            @endforeach
                                        @endisset

                                        <p><a class="outro-plano" data-bs-toggle="collapse"
                                                data-bs-target=".multi-collapse" aria-expanded="false"
                                                aria-controls="flush-collapseOne flush-collapseThree"
                                                role="button">quero renovar ou assinar um novo plano!</a></p>
                                    @elseif ($user->plano->status == 'try')
                                        <h5 style="color: #797979;"><i class="fas fa-laugh-beam"></i>Teste grátis</h5>
                                        <p><strong>Plano escolhido: </strong>{{ $user->plano->plano }}</p>
                                        <p><strong>Tempo: </strong> {{ $user->plano->tempo }} dias</p>
                                        <p><strong>Valor:</strong> <span class="valor_plano">
                                                <s>R$ 19,00/mês</s>
                                            </span></p>
                                        <p><strong>Iniciado em:
                                            </strong>{{ date('d/m/Y', strtotime($user->plano->inicio)) }}</p>
                                        <p><strong>Finaliza em:
                                            </strong>{{ date('d/m/Y', strtotime($user->plano->fim)) }}</p>
                                        <p><a class="outro-plano" data-bs-toggle="collapse"
                                                data-bs-target=".multi-collapse" aria-expanded="false"
                                                aria-controls="flush-collapseOne flush-collapseThree"
                                                role="button">quero escolher um novo plano!</a></p>
                                    @elseif ($user->plano->status == 'authorized')
                                        <h5 style="color: #797979;"><i
                                                class="fas fa-file-invoice-dollar"></i>Pagamento reservado!</h5>
                                        <p><strong>O pagamento foi reservado pela operadora e está pendente.
                                                Notificaremos por e-mail assim que recebermos a confirmação.</strong>
                                        </p>
                                        <p><strong>Plano escolhido: </strong>{{ $user->plano->plano }}</p>
                                        <p><strong>Tempo: </strong> {{ $user->plano->tempo }} {{ $unidadeTempo }}
                                        </p>
                                        <p><strong>Valor:</strong> <span class="valor_plano">
                                                @if ($user->plano->cupom != null)
                                                    <s>{{ "R$" . $PLANOS[$user->plano->plano] }}</s>
                                                    <span
                                                        id="desconto_boleto_1">{{ $user->plano->planoDesconto->valor }}</span>
                                                @else
                                                    {{ "R$" . $PLANOS[$user->plano->plano] }}
                                                @endif
                                            </span></p>
                                    @elseif (isset($user->boleto))
                                        @if ($user->boleto->ativo)

                                            <h5 style="color: #797979;"><i
                                                    class="fas fa-file-invoice-dollar"></i>Boleto pendente!</h5>
                                            <p><strong>Detectamos que você ainda não efetuou o pagamento do plano
                                                    escolhido.</strong></p>
                                            <p>Seu boleto ainda está com o pagamento pendente! Clique no botão abaixo
                                                para imprimi-lo.</p>
                                            <p><strong>Vencimento em:
                                                </strong>{{ date('d-m-Y', strtotime($user->boleto->expirar)) }}</p>
                                            <p><strong>Plano escolhido: </strong>{{ $user->plano->plano }}</p>
                                            <p><strong>Tempo: </strong> {{ $user->plano->tempo }}
                                                {{ $unidadeTempo }}
                                            </p>
                                            <p><strong>Valor:</strong> <span class="valor_plano">
                                                    @if ($user->plano->cupom != null)
                                                        <s>{{ "R$" . $PLANOS[$user->plano->plano] }}</s>
                                                        <span
                                                            id="desconto_boleto_1">{{ $user->plano->planoDesconto->valor }}</span>
                                                    @else
                                                        {{ "R$" . $PLANOS[$user->plano->plano] }}
                                                    @endif
                                                </span></p>
                                            <p style="text-align: center;"><a class="btn btn-outline-dark"
                                                    role="button" href="{{ $user->boleto->boleto }}"
                                                    target="_blank">imprimir boleto</a></p>
                                            <p><strong>Atenção:</strong> o boleto tem vencimento de 2 dias. Caso o
                                                pagamento não seja efetuado em até 2 dias, você deverá efetuar uma nova
                                                compra, clicando em "Quero escolher um novo plano" ou gerando um novo
                                                boleto.</p>
                                            <p>O seu acesso será liberado em até 3 dias úteis, após o pagamento do
                                                boleto. Na compra por cartão de crédito, o acesso é liberado de
                                                imediato.</p>
                                            <p><a class="outro-plano" data-bs-toggle="collapse"
                                                    data-bs-target=".multi-collapse" aria-expanded="false"
                                                    aria-controls="flush-collapseOne flush-collapseThree"
                                                    role="button">quero escolher um novo plano!</a></p>
                                        @else
                                            <h5 style="color: #797979;"><i
                                                    class="fas fa-exclamation-triangle"></i>Aviso!</h5>
                                            <p><strong>Detectamos que você ainda não efetuou o pagamento do plano
                                                    escolhido.</strong></p>
                                            <p>Seu boleto expirou! É necessário gerar um novo boleto, para ter acesso à
                                                plataforma. Clique no botão abaixo para gerá-lo.</p>
                                            <p><strong>Expirou em:
                                                </strong>{{ date('d-m-Y', strtotime($user->boleto->expirar)) }}</p>
                                            <p><strong>Plano escolhido:</strong> {{ $user->plano->plano }}</p>
                                            <p><strong>Tempo:</strong> {{ $user->plano->tempo }}
                                                {{ $unidadeTempo }}
                                            </p>
                                            <p><strong>Valor:</strong> <span class="valor_plano">
                                                    @if ($user->plano->cupom != null)
                                                        <s>{{ "R$" . $PLANOS[$user->plano->plano] }}</s>
                                                        <span
                                                            id="desconto_boleto_2">{{ $user->plano->planoDesconto->valor }}</span>/mês
                                                    @else
                                                        {{ "R$" . $PLANOS[$user->plano->plano] }}
                                                    @endif
                                                </span></p>
                                            <p style="text-align: center;"><button id="pay-button"
                                                    class="btn btn-outline-dark"
                                                    data-url="{{ url::to('pagarme') }}">gerar um novo
                                                    boleto</button>
                                            </p>
                                            <p><strong>Atenção:</strong> o boleto tem vencimento de 2 dias. Caso o
                                                pagamento não seja efetuado em até 2 dias, você deverá efetuar uma nova
                                                compra, clicando em "Quero escolher um novo plano" ou gerando um novo
                                                boleto.</p>
                                            <p>O seu acesso será liberado em até 3 dias úteis, após o pagamento do
                                                boleto. Na compra por cartão de crédito, o acesso é liberado de
                                                imediato.</p>
                                            <p><a class="outro-plano" data-bs-toggle="collapse"
                                                    data-bs-target=".multi-collapse" aria-expanded="false"
                                                    aria-controls="flush-collapseOne flush-collapseThree"
                                                    role="button">quero escolher um novo plano!</a></p>

                                        @endif
                                    @else
                                        <p>Você não possui um plano ainda.</p>
                                        <p><a class="outro-plano" data-bs-toggle="collapse"
                                                data-bs-target=".multi-collapse" aria-expanded="false"
                                                aria-controls="flush-collapseOne flush-collapseThree"
                                                role="button">quero escolher um novo plano!</a></p>
                                    @endif
                                @else
                                    <p>Você não possui um plano ainda.</p>
                                    <p><a class="outro-plano" data-bs-toggle="collapse"
                                            data-bs-target=".multi-collapse" aria-expanded="false"
                                            aria-controls="flush-collapseOne flush-collapseThree" role="button">quero
                                            escolher um novo plano!</a></p>

                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item m-3">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                aria-controls="flush-collapseTwo">
                                <i class="fas fa-lock"></i> alterar senha
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                <form id="alterar-senha-form" class="my-3"
                                    action="{{ url::to('conta/alterar-senha') }}" novalidate>
                                    <div class="mb-3 position-relative  has-validation">
                                        <label for="senha-atual" class="form-label">senha atual</label>
                                        <input type="password" name="senha-atual" class="form-control"
                                            id="senha-atual" autocomplete="password"
                                            aria-describedby="senha-atual-fb">
                                        <div class="valid-tooltip" id="senha-atual-fb">
                                            por favor, digite a senha atual.
                                        </div>
                                    </div>
                                    <div class="mb-3 position-relative  has-validation">
                                        <label for="senha-nova" class="form-label">nova senha</label>
                                        <input type="password" name="senha-nova" class="form-control"
                                            id="senha-nova" autocomplete="new-password"
                                            aria-describedby="senha-nova-fb">
                                        <div class="valid-tooltip" id="senha-nova-fb">
                                            por favor, digite a nova senha.
                                        </div>
                                    </div>
                                    <div class="mb-3 position-relative  has-validation">
                                        <label for="confirma-senha-nova" class="form-label">repetir nova senha</label>
                                        <input type="password" name="confirma-senha-nova" class="form-control"
                                            id="confirma-senha-nova" autocomplete="new-password"
                                            aria-describedby="confirma-senha-nova-fb">
                                        <div class="valid-tooltip" id="confirma-senha-nova-fb">
                                            por favor, digite a mesma senha atual.
                                        </div>
                                    </div>
                                    <div class="w-100"></div>
                                    <div id="warning-box" class="align-self-center mb-3"
                                        style="display: none; font-size: medium;">
                                        <p class="warning-text">Senha atual incorreta!</p>
                                    </div>
                                    <p style="text-align: center;"><button type="submit"
                                            class="btn btn-outline-dark">alterar senha</button></p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item m-3">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree" aria-expanded="false"
                                aria-controls="flush-collapseThree">
                                <i class="fas fa-user-cog"></i> configurações
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse  multi-collapse"
                            aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body pb-3">
                                <p><strong>assine outros planos ou renove suas assinaturas</strong></p>
                                <div class="row">
                                    <div class="col">
                                        <div class="plano-box plano-box-1 position-relative"
                                            style="color: white; background: #0178ca;">
                                            <p class="big-title" style="text-align: center;font-size: xx-large;">
                                                curso<br>completo</p>
                                            <p>por apenas</p>
                                            <div class="plano-body m-auto px-5">
                                                <span>
                                                    <h2 class="position-relative" style="color: white;">
                                                        <span id="plano-scroll"
                                                            class="position-absolute end-100 plano-preco-a">R$</span>
                                                        21
                                                        <span
                                                            class="position-absolute start-100 plano-preco-b">,90</span>
                                                        <span
                                                            class="position-absolute start-100 plano-preco-c">/MÊS</span>
                                                    </h2>
                                                </span>
                                            </div>
                                            <p style="line-height: 17px;padding-top: 12px;">aulas sobre todos os
                                                assuntos de biologia</p>
                                            <a class="stretched-link" href="{{ url::to('planos/choose/5') }}"></a>
                                        </div>
                                    </div>
                                    {{-- <div class="col">
                                        <div class="plano-box plano-box-1 position-relative"  style="color: white; background: #0178ca;">
                                            <p class="big-title" style="text-align: center;font-size: xx-large;" >CURSO TOTAL</p>
                                            <p>por apenas</p>
                                            <div class="plano-body m-auto px-5">
                                                <span>
                                                    <h2 class="position-relative" style="color: white;">
                                                        <span id="plano-scroll" class="position-absolute end-100 plano-preco-a">R$</span>
                                                        7
                                                        <span class="position-absolute start-100 plano-preco-b" >,90</span>
                                                        <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
                                                    </h2>
                                                </span>
                                            </div>
                                            <p>plano adicional</p>
                                            <a class="stretched-link" href="{{url::to('planos/choose/0')}}"></a>
                                        </div>
                                    </div> --}}
                                    {{-- @php
                                    $today = new DateTime("now");
                                    $target_day = new DateTime(config('constants.outras.deadline_aprova_mais'));
                                @endphp
                                <div class="col {{$today <= $target_day ? 'd-none' : ''}}">
                                    <div class="plano-box position-relative">
                                        <p class="big-title" style="text-align: center;font-size: xx-large;margin-bottom: .5rem;" >aprova<br>+</p>
                                        <p>por apenas</p>
                                        <div class="plano-body m-auto px-5">
                                            <span><h2 class="position-relative">
                                                    <span id="plano-scroll" class="position-absolute plano-preco-a" style="right:74%">R$</span>
                                                    3
                                                    <span class="position-absolute plano-preco-b" style="left:74%">,65</span>
                                                    <span class="position-absolute plano-preco-c" style="left:74%">/MÊS</span>
                                                </h2>
                                            </span>
                                        </div>
                                        <p style="line-height: 17px">1 plantão de dúvida/mês e mais!</p>
                                        <a class="stretched-link" href="{{url::to('planos/choose/6')}}"></a>
                                    </div>
                                </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mt-5"div>

    <script>
        var cpf = $("#cpf").html();
        $("#cpf").html(formatarCPF(cpf));

        function formatarCPF(cpfAtual) {
            cpfAtualizado = cpfAtual.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/,
                function(regex, argumento1, argumento2, argumento3, argumento4) {
                    return argumento1 + '.' + argumento2 + '.' + argumento3 + '-' + argumento4;
                });
            return cpfAtualizado;
        }

        function editPhotoError(feedback, input, message) {
            feedback.html(message);
            input.attr('class', 'is-invalid');
        }

        $("#formEditPhoto").submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            //console.log('submit: ' + formData);
            var input = $("#inputEditPhoto");
            var feedback = $("#invalidFBEditPhoto");
            var url = $(this).attr('action');
            //console.log('url: ' + url);

            if ($("#inputEditPhoto").val() != "") {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        //console.log("data: " + data);
                        if (data.success) {
                            input.attr('class', 'is-valid');
                            //console.log("{{ url::to('/img/profile/') . '/' }}" + data.name);
                            $(".profile-image").css("background-image",
                                "url({{ url::to('/img/profile/') . '/' }}" + data.name + ")");
                            //console.log(data.name);
                        } else {
                            editPhotoError(feedback, input, data.message);
                            //console.log('image false');
                        }
                    },
                    error: function(data) {
                        if (data.status = 422) {
                            editPhotoError(feedback, input, "Por favor, selecione uma imagem válida.");
                        } else {
                            editPhotoError(feedback, input, "alterar imagem falhou!");
                        }
                        //console.log(data);
                    }
                });
            } else {
                editPhotoError(feedback, input, "Por favor, selecione uma imagem válida.");
            }

        });

        function callEditPhotoModal() {
            $("#editPhotoModal").modal('toggle');
        }

        @isset($user->plano)
            @if ($user->plano->cupom != null)
                $("#desconto_cartao").html(convertToCurrency(parseInt($("#desconto_cartao").html()) / 100));
                $("#desconto_boleto_1").html(convertToCurrency(parseInt($("#desconto_boleto_1").html()) / 100));
                $("#desconto_boleto_2").html(convertToCurrency(parseInt($("#desconto_boleto_2").html()) / 100));
            @endif
        @endisset

        function convertToCurrency(value) {
            return value.toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL",
                minimumFractionDigits: 2
            })
        }

        function showPlanos() {
            $("#flush-collapseThree").toggle('show');
        }

        $("#alterar-senha-form").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var senhaAtual = $("#senha-atual");
            var senhaNova = $("#senha-nova");
            var confirmaSenhaNova = $("#confirma-senha-nova");

            var cancel = false;
            var input = null;

            if (confirmaSenhaNova.val() != senhaNova.val()) {
                cancel = true;
                input = confirmaSenhaNova;
            }

            if (senhaNova.val() == "") {
                cancel = true;
                input = senhaNova;
            }

            if (senhaAtual.val() == "") {
                cancel = true;
                input = senhaAtual;
            }

            var formSerialized = $(this).serializeArray();

            if (cancel) {
                //console.log("senha cancelada!");
                input.addClass("is-invalid");
            } else {
                $.post($(this).attr('action'), formSerialized, function(data) {
                        // console.log("data: " + data);
                        if (data.success) {
                            //console.log("senha alterada com sucesso!");
                            $("#planos-login-section-2 .warning-text").css('color', 'green !important');
                            $("#planos-login-section-2 .warning-text").html("Senha alterada com sucesso!");
                            $("#planos-login-section-2 #warning-box").fadeIn();
                        } else if (!data.hasSession) {
                            window.location.href = "{{ url::to('planos/login-page') }}";
                        } else if (!data.hasUser) {
                            $("#planos-login-section-2 .warning-text").html(
                                "Sinto muito, algo deu errado. Por favor, tente mais tarde.");
                            $("#planos-login-section-2 #warning-box").fadeIn();
                            //console.log('usuario n encontrado');
                        } else if (!data.hasPass) {
                            $("#planos-login-section-2 .warning-text").html("Senha atual incorreta!");
                            $("#planos-login-section-2 #warning-box").fadeIn();
                            //console.log('senha atual não confere');
                        }
                    })
                    .fail(function() {
                        $("#planos-login-section-2 .warning-text").html(
                            "Sinto muito, algo deu errado. Por favor, tente mais tarde.");
                        $("#planos-login-section-2 #warning-box").fadeIn();
                        //console.log("cadastro falhou!!")
                    });
            }
        });

        /* PAGARME */

        var HAS_CUPOM = false;

        function getPlanoValor() {
            @if (isset($user->plano))
                @isset($user->plano->cupom)
                    if ("{{ $user->plano->cupom != null }}") {
                        return parseInt("{{ $user->plano->planoDesconto->valor }}");
                    } else {
                        if ("{{ $user->plano->plano == 'EXTENSIVO MED' }}") return 19000;
                        else return 26280;
                    }
                @endisset
            @else
                return -1;
            @endif
        }

        function getPlanoId() {
            @if (isset($user->plano))
                return parseInt("{{ $user->plano->plano == 'EXTENSIVO MED' ? 0 : 4 }}");
            @else
                return 4;
            @endif

        }

        function getPlanoCupom() {
            @if (isset($user->plano))
                @isset($user->plano->cupom)
                    if ("{{ $user->plano->cupom != null }}") {
                        HAS_CUPOM = true;
                        var cupom = new Object();
                        cupom.id = parseInt("{{ $user->plano->cupom->id }}");
                        cupom.cupom = "{{ $user->plano->cupom->cupom }}";
                        cupom.data_inicio = "{{ $user->plano->cupom->data_inicio }}";
                        cupom.data_fim = "{{ $user->plano->cupom->data_fim }}";
                        cupom.idPlano = parseInt("{{ $user->plano->cupom->idPlano }}");
                        cupom.tipo_desconto = parseInt("{{ $user->plano->cupom->tipo_desconto }}");
                        cupom.desconto = parseInt("{{ $user->plano->cupom->desconto }}");
                        cupom.updated_at = "{{ $user->plano->cupom->updated_at }}";
                        cupom.created_at = "{{ $user->plano->cupom->created_at }}";
                        return cupom;
                    } else return null;
                @endisset
            @else
                return null;
            @endif
        }

        function error() {
            $('#autorizadoModal modal-title').html("Pagamento falhou!");
            $('#autorizadoModal modal-body').html("Por favor, tente mais tarde.");
            $('#autorizadoModal').modal('show');
        }

        var button = document.querySelector('#pay-button');
        if (button != null) {
            // Abrir o modal ao clicar no botão
            button.addEventListener('click', function() {
                var url = button.getAttribute("data-url");
                //console.log(url);

                var PLANO_ID = getPlanoId();
                var PLANO_NOME =
                    @if ($user->plano != null)
                        "{{ $user->plano->plano }}"
                    @else
                        ""
                    @endif ;
                var PLANO_VALOR = getPlanoValor();
                var CUPOM = getPlanoCupom();

                // inicia a instância do checkout
                var checkout = new PagarMeCheckout.Checkout({
                    encryption_key: "{{ env('PAGARME_CRYPT_KEY') }}",
                    success: function(data) {
                        $("#loader-wrapper").css("display", "block");
                        if (HAS_CUPOM) data.cupom = CUPOM;
                        data.plano_valor = PLANO_VALOR;
                        //console.log("pay success");
                        //console.log(data);
                        $.post(url, data, function(data) {
                                $("#loader-wrapper").css("display", "none");
                                //console.log("resultado do post pagamento");
                                //console.log(data);
                                if (data.success) {
                                    if (data.status == 'paid') {
                                        window.location.href = "{{ url::to('conta/curso') }}";
                                    } else if (data.status == 'waiting_payment') {
                                        window.location.href =
                                            "{{ url::to('pagamento/boleto/') }}/" + data.boleto
                                            .id;
                                    } else if (data.status == 'authorized') {
                                        $('#autorizadoModal modal-title').html(
                                            "Pagamento Pendente");
                                        $('#autorizadoModal modal-body').html(
                                            "O pagamento foi reservado e está pendente. Notificaremos por e-mail assim que recebermos a confirmação."
                                        );
                                        $('#autorizadoModal').modal('show');
                                    }
                                } else {
                                    $('#autorizadoModal modal-title').html("Pagamento falhou!");
                                    $('#autorizadoModal modal-body').html(
                                        "Por favor, tente mais tarde.");
                                    $('#autorizadoModal').modal('show');
                                    //alert("Transação falhou!");
                                }
                            })
                            .fail(function() {
                                $("#loader-wrapper").css("display", "none");
                                error();
                                //console.log("post pagamento falhou!!")
                            });
                    },
                    error: function(err) {
                        $("#loader-wrapper").css("display", "none");
                        error();
                        //console.log(err);
                        //alert(err);
                    },
                    close: function() {
                        $("#loader-wrapper").css("display", "none");
                        //console.log('The modal has been closed.');
                    }
                });

                checkout.open({
                    amount: PLANO_VALOR,
                    customerData: 'true',
                    createToken: 'true',
                    paymentMethods: 'boleto',
                    boletoDiscountPercentage: 0,
                    postbackUrl: "{{ url::to('/postback') }}",
                    costumer: [{
                        name: "{{ $user->nome }}",
                        email: "{{ $user->email }}",
                        documents: [{
                            type: 'CPF',
                            number: '{{ $user->cpf }}'
                        }],
                        phone_numbers: [
                            "{{ $user->telefone }}"
                        ]
                    }],
                    items: [{
                        id: PLANO_ID,
                        title: PLANO_NOME,
                        unit_price: PLANO_VALOR,
                        quantity: 1,
                        tangible: 'false'
                    }],
                });
            });
        }
    </script>
