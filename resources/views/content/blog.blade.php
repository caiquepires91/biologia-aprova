@if ($page == 'news')
<div id="blog-section-2" class="row postition-relative mb-md-5 mb-lg-5 justfy-content-center align-items-center">
        <div class="col">
            <h2 class="big-title">blog</h2>
        </div>
    </div>
@else
<div id="blog-section-1" class="row postition-relative mb-md-5 mb-lg-5 justfy-content-center align-items-center">
        <div class="col">
            <!-- <img class="position-absolute" src="{{url::to('../img/blog-bg.png')}}"/> -->
            <h2 class="big-title">blog</h2>
            <div class="blog-input-group input-group my-5 search-shadow">
                <input id="blog-input-main" type="text" class="form-control typeahead" placeholder="Pesquisar" aria-label="Pesquisar" aria-describedby="blog-search" autocomplete="off">
                <button class="btn btn-outline-secondary" type="button" id="blog-search"><i class="fa fa-search"></i></button>
            </div>
        </div>
       <!--  <div class="blog-container" style="display:none">
            <div class="loader">
                    <i class="fab fa-pagelines position-absolute" style="right:0%"></i>
                    <i class="far fa-newspaper position-absolute"  style="right:5%"></i>                
                    <i class="fas fa-leaf  position-absolute"  style="right:7%"></i>
                    <i class="fas fa-vials  position-absolute"  style=" right:12%"></i>
                    <i class="fab fa-pagelines position-absolute" style="right:15%"></i>
                    <i class="fas fa-microscope  position-absolute"  style="right:20%"></i>
                    <i class="fas fa-temperature-high  position-absolute"  style="right:23%"></i>
                    <i class="fab fa-blogger-b position-absolute "  style="right:27%"></i> 
                    <i class="fas fa-microscope  position-absolute"  style="right:30%"></i>
    
                    <i class="fab fa-pagelines position-absolute" style="right:1%"></i> 
                    <i class="fab fa-blogger-b position-absolute "  style="right:18%"></i>
                    <i class="fab fa-microblog position-absolute"  style="right:25%"></i>
                                   
                    <i class="fas fa-flask position-absolute"  style="left:0%"></i>
                    <i class="fas fa-blog  position-absolute"  style="left:2%"></i>
                    <i class="fas fa-vials  position-absolute"  style=" left:6%"></i>                
                    <i class="fab fa-pagelines position-absolute" style="left:8%"></i> 
                    <i class="fab fa-pagelines position-absolute" style="left:10%"></i>                
                    <i class="fab fa-blogger-b position-absolute "  style="left:13%"></i>
                    <i class="fab fa-microblog position-absolute"  style="left:15%"></i>
                    <i class="fas fa-microscope  position-absolute"  style="left:18%"></i>                
                    <i class="far fa-newspaper position-absolute"  style="left:20%"></i>
                    <i class="fas fa-atom  position-absolute"  style="left:23%"></i>
                    <i class="fas fa-temperature-high  position-absolute"  style="left:27%"></i>                
                    <i class="fas fa-microscope  position-absolute"  style="left:25%"></i>
    
                    <i class="fab fa-pagelines position-absolute" style="right:0%"></i>
                    <i class="far fa-newspaper position-absolute"  style="right:5%"></i>                
                    <i class="fas fa-leaf  position-absolute"  style="right:7%"></i>
                    <i class="fas fa-vials  position-absolute"  style=" right:12%"></i>
                    <i class="fab fa-pagelines position-absolute" style="right:15%"></i>
                    <i class="fas fa-microscope  position-absolute"  style="right:20%"></i>
                    <i class="fas fa-temperature-high  position-absolute"  style="right:23%"></i>
                    <i class="fab fa-blogger-b position-absolute "  style="right:27%"></i> 
                    <i class="fas fa-microscope  position-absolute"  style="right:30%"></i>
    
                    <i class="fab fa-pagelines position-absolute" style="right:1%"></i> 
                    <i class="fab fa-blogger-b position-absolute "  style="right:18%"></i>
                    <i class="fab fa-microblog position-absolute"  style="right:25%"></i>
                                   
                    <i class="fas fa-flask position-absolute"  style="left:0%"></i>
                    <i class="fas fa-blog  position-absolute"  style="left:2%"></i>
                    <i class="fas fa-vials  position-absolute"  style=" left:6%"></i>                
                    <i class="fab fa-pagelines position-absolute" style="left:8%"></i> 
                    <i class="fab fa-pagelines position-absolute" style="left:10%"></i>                
                    <i class="fab fa-blogger-b position-absolute "  style="left:13%"></i>
                    <i class="fab fa-microblog position-absolute"  style="left:15%"></i>
                    <i class="fas fa-microscope  position-absolute"  style="left:18%"></i>                
                    <i class="far fa-newspaper position-absolute"  style="left:20%"></i>
                    <i class="fas fa-atom  position-absolute"  style="left:23%"></i>
                    <i class="fas fa-temperature-high  position-absolute"  style="left:27%"></i>                
                    <i class="fas fa-microscope  position-absolute"  style="left:25%"></i>
          </div>
        </div> -->
</div>
@endif
<div class="my-container">

    @if ($page == 'news')
       <div class="my-breadcrumb row m-md-5">
           <div class="col mx-md-5">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/blog">Blog</a></li>
                    <li class="breadcrumb-item" aria-current="page"><a href="{{url::to('/blog/'.utf8_encode($noticia->categoria))}}">{{$noticia->categoria}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Notícia</li>
                    </ol>
                </nav>
           </div>
       </div>
    @else
    <!-- CATEGORIAS -->
    <div class="categories row justify-content-center">
        <div class="col-md-8">
            <div style="width: fit-content; margin: auto;">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                    <input type="radio" class="btn-check" name="btnradio" id="btnr-todos" autocomplete="off" checked>
                    <label class="btn btn-outline-success" for="btnr-todos"><a href="{{url::to('/blog')}}" class="stretched-link"></a>TODOS</label>
                    @if ($page == 'blog' && !empty($categorias))
                    @foreach( $categorias as $key=>$categoria )
                        <input type="radio" class="btn-check" name="btnradio" id="btnr-{{$key}}" autocomplete="off" @isset($categ) {{ $categ == $categoria->categoria ? 'checked' : ''}} @endisset>
                        <label class="btn btn-outline-success" for="btnr-{{$key}}"><a href="{{url::to('/blog/'.utf8_encode($categoria->categoria))}}" class="stretched-link"></a>{{$categoria->categoria}}</label>
                    @endforeach
                    @endif
                  </div>
            </div>      
        </div>
    </div>
    <div id="search-text" class="row justify-content-center">
        <div class="col-md-8">
            <div style="width: fit-content; margin: auto;">
                <p>buscando 'tex'...</p>
            </div>      
        </div>
    </div>
    @endif
    
    <div class="news row m-md-5">
        <!-- NOTICIAS PRINCIPAIS -->
        <div class="main-news col-md-9">
            @if ($page == 'news')
            <div id="{{$noticia->id}}" class="noticia mx-md-5 mb-md-5">
                <p class="titulo">{{$noticia->titulo}}</p>
                <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> {{$noticia->data_publicacao}}</p>
                <div class="imagem my-md-5" style="background-image: url('../img/blog/{{$noticia->img}}');"></div>
                <p class="conteudo">{!! $noticia->texto !!}<p>
            </div>
            @elseif ($page == 'blog')
            <p id="blog-search-empty" style="color: gray; display:none">não encontramos o que você está procurando <i class="fas fa-sad-tear"></i></p>
            <div id="main-news">
                @foreach ($noticias as $key=>$noticia)
                <div id="{{$noticia->id}}" class="post row mx-md-5 mb-md-5 position-relative">
                <div class="col-md-5">
                    <p class="titulo-mob" style="display:none">{{$noticia->titulo}}</p>
                    <p class="autor-mob"  style="display:none"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em {{date('d/m/Y',strtotime($noticia->data_publicacao))}}</p>
                    <div class="imagem"  style="background-image: url('../img/blog/{{$noticia->img}}');"></div>
                    <a href="{{url::to('/noticia/'.$noticia->id)}}" class="stretched-link"></a>
                </div>
                <div class="col-md-7">
                    <p class="titulo">{{$noticia->titulo}}</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em {{date('d/m/Y',strtotime($noticia->data_publicacao))}}</p>
                    <!-- <p class="conteudo">{{strlen($noticia->texto) > 150 ? substr($noticia->texto,0,150)."..." : $noticia->texto}}</p> -->
                    <p class="conteudo">{{strlen($noticia->texto) > 150 ? substr(strip_tags($noticia->texto),0,150)."..." : strip_tags($noticia->texto)}}</p>
                </div>
                </div>
                @endforeach
                <div id="pagination" class="d-flex m-md-5 justify-content-center">{{$noticias->links('pagination::bootstrap-4')}}</div>
            </div>            
            @endif
        </div>
        <!-- ULTIMAS NOTICIAS -->
        <div class="last-news col-md-3">
            <div class="ultimos-posts">
                <div class="input-group mb-md-3">
                    <input  id="blog-input" type="text" class="typeahead form-control" placeholder="Pesquisar" aria-label="Pesquisar" aria-describedby="blog-search">
                    <button id="blog-search" class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                </div>
                <h4 class="mt-md-5">Últimas Postagens</h4>
                <div class="mx-md-5 mb-md-5 wavy-line">
                    <img src="{{url::to('img/long-wavy-line.png')}}" alt="linha ondulada">
                </div>
                <div id="last-news">
                @if ($page == 'blog' || $page == 'news')
                    @foreach ($ultimasnoticias as $key=>$noticia)
                        <div class="post-menor mb-md-3 position-relative">
                            <div class="imagem" style="background-image: url('../img/blog/{{$noticia->img}}');">
                            </div>
                            <p class="title">{{$noticia->titulo}}</p>
                            <a href="{{url::to('/noticia/'.$noticia->id)}}" class="stretched-link"></a>
                        </div>
                    @endforeach
                @endif
                </div>                
            </div>
        </div>
    </div>
    
    <div class="row my-5">
        <div class="col banner-fundo-container">
            <div class="banner-fundo position-relative" style="background-image: url('../img/banner-ad.png');">
                <button class="btn blog-green-btn rounded-pill position-absolute" type="button" onclick="goToPlanos()">começar agora!</button>
            </div>
        </div>
    </div>

    </div>
</div>

<script>

    function goToPlanos() {
		window.location.href = "{{url::to('/planos')}}";
	}

    var path = "{{ route('blog-autocomplete') }}";
    console.log(path);

    function truncText(text, length) {
        return (text.length > length ? text.substring(0,length) + "..." : text);
    }

    $("#blog-input-main").on("change keyup paste", function(){
        if ($(this).val() == "") {
            $(".categories").fadeIn();
            $("#search-text").fadeOut();
            $("#search-text").find("p")[0].innerHTML = "";
        }
    })

    $( document ).ready(function() {
        $('#blog-input-main.typeahead').typeahead({
            source:  function (query, process) {
                    //console.log(query);
                    $(".categories").css("display", "none");
                    $("#search-text").fadeIn();
                    $("#search-text").css("display", "flex");
                    $("#search-text").find("p")[0].innerHTML = "buscando por '" + query + "'...";

                return $.get(path, { str: query }, function (data) {
                        console.log(data);
                        var jsonObjects = data.data;
                        if (!jsonObjects.length) {
                            $("#blog-search-empty").fadeIn();
                        } else {
                            $("#blog-search-empty").fadeOut();
                        }
                        document.getElementById("main-news").innerHTML = '';
                        jsonObjects.forEach(jsonObj => {
                            var divPrincipal = document.createElement("div");
                            var divLev11 = document.createElement("div");
                            var divLev12 = document.createElement("div");
                            var divLev2 = document.createElement("div");
                            var parag1 = document.createElement("p");
                            var parag2 = document.createElement("p");
                            var parag3 = document.createElement("p");
                            var parag4 = document.createElement("p");
                            var parag5 = document.createElement("p");
                            var link = document.createElement("a");
                            
                            link.setAttribute("href", "{{url::to('/noticia/')}}/" + jsonObj.id);
                            link.setAttribute("class", "stretched-link");
                            parag1.setAttribute("class", "titulo-mob");
                            parag1.setAttribute("style", "display:none");
                            parag1.innerHTML = jsonObj.titulo;
                            parag2.setAttribute("class", "autor-mob");
                            parag2.setAttribute("style", "display:none");
                            parag2.innerHTML =  "<i class='far fa-clock' style='color: #3eb378'></i> Postado em " + jsonObj.data_publicacao;
                            divLev2.setAttribute("class", "imagem");
                            divLev2.setAttribute("style", "background-image: url('../img/blog/" + jsonObj.img + "')");

                            divLev11.setAttribute("class", "col-md-5");
                            divLev11.appendChild(parag1);
                            divLev11.appendChild(parag2);
                            divLev11.appendChild(divLev2);
                            divLev11.appendChild(link);

                            parag3.setAttribute("class", "titulo");
                            parag3.innerHTML = jsonObj.titulo;
                            parag4.setAttribute("class", "autor");
                            parag4.innerHTML =  "<i class='far fa-clock' style='color: #3eb378'></i> Postado em " + jsonObj.data_publicacao;
                            parag5.setAttribute("class", "conteudo");

                            parag5.innerHTML = truncText(jsonObj.texto, 150);

                            divLev12.setAttribute("class", "col-md-7");
                            divLev12.appendChild(parag3);
                            divLev12.appendChild(parag4);
                            divLev12.appendChild(parag5);

                            divPrincipal.setAttribute("class", "post row mx-md-5 mb-md-5 position-relative");
                            divPrincipal.setAttribute("id", jsonObj.id);
                            divPrincipal.appendChild(divLev11);
                            divPrincipal.appendChild(divLev12);
                            document.getElementById("main-news").appendChild(divPrincipal);
                        });

                        //console.log(jsonObjects.links);

                    /*   document.getElementById("main-news").appendChild(
                            "<div id='pagination' class='d-flex m-md-5 justify-content-center'>"
                            +jsonObjects.noticias +
                            "</div>"
                            ); */
                        //return process(data);
                });
            }
        });

        $('input.typeahead').typeahead({
            source:  function (query, process) {
                //console.log(query);
                return $.get(path, { str: query }, function (data) {
                        //console.log(data);
                        document.getElementById("last-news").innerHTML = '';
                        data.data.forEach(jsonObj => {
                            console.log(jsonObj);
                            var divPrincipal = document.createElement("div");
                            var divFilha = document.createElement("div");
                            var parag = document.createElement("p");
                            var link = document.createElement("a");
                            link.setAttribute("href", "{{url::to('/noticia/')}}/" + jsonObj.id);
                            link.setAttribute("class", "stretched-link");
                            parag.setAttribute("class", "title");
                            parag.innerHTML = jsonObj.titulo;
                            divFilha.setAttribute("class", "imagem");
                            divFilha.setAttribute("style", "background-image: url('../img/blog/" + jsonObj.img + "')");
                            divPrincipal.setAttribute("class", "post-menor mb-md-3 position-relative");
                            divPrincipal.appendChild(divFilha);
                            divPrincipal.appendChild(parag);
                            divPrincipal.appendChild(link);
                            document.getElementById("last-news").appendChild(divPrincipal);
                        });
                        //return process(data);
                });
            }
        });
    });

   
    $('#blog-search').on('click', function() {
        console.log(document.getElementById('blog-input').value);
        $.get("{{route('blog-busca')}}", {'titulo':document.getElementById('blog-input').value}, function (noticias) {
                console.log(noticias);
            });
    });
    
</script>
