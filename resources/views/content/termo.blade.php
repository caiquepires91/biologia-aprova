<link href="{{URL::to('/css/termo.css')}}" rel="stylesheet">
<div id="planos-login-section-1" class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">termo de uso</h2>
    </div>
</div>
<div id="corpo">    	
        <div class="corpo2">
        	<div class="container">
            <div class="col-xs-12">
              <p style="text-align:right;"><strong class="atualizacao_d">Última atualização: 29 de junho de 2017</strong></p>
              <p>
                Este Termo de Uso apresenta as “Condições Gerais” aplicáveis ao uso dos serviços oferecidos por Central de Concursos do Vale do São Francisco LTDA-ME, pessoa jurídica de direito privado, sociedade limitada, inscrita do CNPJ sob o nº 17.764.041/0001-57, doravante denominada Biologia Aprova, através do site <a href="http://www.biologiaaprova.com.br/">www.biologiaaprova.com.br</a>.
              </p>
              <p>
                Qualquer pessoa, doravante denominada “PARTICIPANTE”, que pretenda utilizar qualquer serviço ou informação do Biologia Aprova deverá aceitar este Termo de Uso, e todas as demais políticas incorporados ao mesmo por referência e princípios que o regem.
              </p>
              <p>
                A ACEITAÇÃO DO TERMO DE USO É ABSOLUTAMENTE INDISPENSÁVEL À UTILIZAÇÃO DO BIOLOGIA APROVA E SEUS SERVIÇOS.
              </p>
              <p>
                <strong>Cláusula Primeira (Do Objeto).</strong> A empresa Biologia Aprova oferece um conjunto de ferramentas, recursos e funcionalidades web, composto de vídeo-aulas, e-books, fórum de dúvidas, voltados à preparação do PARTICIPANTE na disciplina de Biologia, direcionadas ao ENEM, vestibulares e assuntos do Ensino Médio.
              </p>
              <p class="ident">
                I – Visitante – Pessoa não cadastrada. Navega pelas informações do Biologia Aprova estando restritas todas as ferramentas, recursos e funcionalidades de aprendizagem e colaboração.
              </p>
              <p class="ident">
                II – Cadastrado – Pessoa que realizou o procedimento de cadastro. Navega pelas informações do Biologia Aprova estando restrita uma parte das ferramentas, recursos e funcionalidades de aprendizagem e colaboração.
              </p>
              <p class="ident">
                III – Assinante – Pessoa que aderiu a um dos planos de assinatura. Navega pelas informações do Biologia Aprova estando liberadas para as ferramentas, recursos e funcionalidades de aprendizagem e colaboração, podendo optar pelo acesso aos e-books e/ou vídeo-aulas.
              </p>
              <p>
                <strong>Cláusula Terceira (Do Cadastro)</strong>
              </p>
              <p>
                <strong>3.1.</strong> Os serviços do Biologia Aprova estão disponíveis apenas para as pessoas que tenham capacidade legal para contrair as obrigações constantes deste termo. Não podendo utilizá-los, assim, pessoas que não gozem dessa capacidade, inclusive menores de 14 anos de idade.
              </p>
              <p>
                <strong>3.2.</strong> Não é permitido que uma pessoa tenha mais de um cadastro ativo.
              </p>
              <p>
                <strong>3.3.</strong> Sendo identificada a situação descrita no parágrafo anterior, o Biologia Aprova poderá excluir definitivamente todos os cadastros e as informações do Participante.
              </p>
              <p>
                <strong>3.4.</strong> O cadastro é de uso pessoal e intransferível, não sendo permitido seu uso por mais de uma pessoa, conta compartilhada. A conexão simultânea de dois ou mais usuários com o mesmo login sujeitará o infrator a ter seu acesso bloqueado.
              </p>
              <p>
                <strong>3.5.</strong> Cabe ao PARTICIPANTE a correta identificação no seu ato de cadastro no site. Em caso de inexatidão ou falsidade das informações prestadas pelo participante em seu cadastro, o Biologia Aprova se reserva o direito de bloquear o acesso do usuário ao site.
              </p>
              <p>
                <strong>Cláusula Quarta (Das Responsabilidades)</strong>
              </p>
              <p>
                <strong>4.1.</strong> O PARTICIPANTE é responsável por todo conteúdo enviado e toda e qualquer atividade que ocorra dentro de seu login.
              </p>
              <p>
                <strong>4.2.</strong> O PARTICIPANTE se compromete a não publicar qualquer material cujo conteúdo:
              </p>
              <p class="ident">
                I – Contenha difamações, ofensas, divulgação de informações particulares de terceiros ou que seja obsceno, pornográfico, racista, preconceituoso, abusivo ou ameaçador;
              </p>
              <p class="ident">
                II – Infrinja quaisquer direitos de propriedade intelectual ou quaisquer direitos de terceiros, incluindo direitos autorais, conexos ou de marca;
              </p>
              <p class="ident">
                III – Contenha violação da lei vigente;
              </p>
              <p class="ident">
                IV – Incentive à pratica de atos ilegais;
              </p>
              <p class="ident">
                V – Contenha propaganda para captação de recursos ou oferta de bens ou serviços;
              </p>
              <p>
                <strong>4.3.</strong> Ao Biologia Aprova se reserva o direito de rever, editar, rejeitar ou excluir qualquer conteúdo disponibilizado, a seu exclusivo critério, mas não assume qualquer obrigação ou responsabilidade de fazê-lo.
              </p>
              <p>
                <strong>4.4.</strong> O PARTICIPANTE é responsável por zelar sua conta de acesso, notificando o Biologia Aprova caso perceba algum uso indevido da mesma.
              </p>
              <p>
                <strong>4.5.</strong> Cabe ao Biologia Aprova, pelo portal de domínio www.biologiaaprova.com.br, fornecer plataforma de ensino onde serão disponibilizados os recursos mencionados na Cláusula Primeira, com a responsabilidade de oferecer os serviços de apoio administrativo inerentes à atividade.
              </p>
              <p>
                <strong>4.6.</strong> Ao PARTICIPANTE é proibido manipular ou adulterar o conteúdo do material didático fornecido pelo Biologia Aprova.
              </p>

              <p>
                <strong>Cláusula Quinta (Da Violação No Sistema ou Base de Dados).</strong> Não é permitida a utilização de nenhum dispositivo, software, ou outro recurso que venha a interferir ou interagir com as ferramentas, funcionalidades, contas ou bancos de dados do Biologia Aprova.
              </p>

              <p>
                <strong>Parágrafo Único.</strong> Qualquer intromissão, tentativa de, ou atividade que viole ou contrarie as leis de direito de propriedade intelectual e/ou as proibições estipuladas neste Termo de Uso, tornarão o responsável passível das ações legais pertinentes, bem como o bloqueio do cadastro, sendo ainda responsável pelas indenizações por eventuais danos causados.
              </p>

              <p>
                <strong>Cláusula Sexta (Da Exclusão).</strong> O PARTICIPANTE poderá a qualquer tempo excluir sua conta, ficando registrada a sua colaboração no período de uso.
              </p>

              <p>
                <strong>Parágrafo Primeiro.</strong> No caso do PARTICIPANTE permanecer mais de 6 (seis) meses sem utilizar os serviços, ao Biologia Aprova se reserva o direito de excluir arquivos históricos do participante.
              </p>

              <p>
                <strong>Parágrafo Segundo.</strong> Em caso de violação deste termo de uso o Biologia Aprova se reserva o direito a exclusão da conta do participante a qualquer momento.
              </p>

              <p>
                <strong>Cláusula Sétima (Da Modificação do Termo de Uso).</strong> O Biologia Aprova poderá modificar, a qualquer tempo, este Termo de Uso, visando seu aprimoramento. O novo Termo de Uso entrará em vigor a partir de sua publicação no site.
              </p>

              <p>
                <strong>Parágrafo Único.</strong> No prazo de 48 (quarenta e oito) horas contadas a partir da publicação, o PARTICIPANTE deverá comunicar-se por e-mail, caso não concorde com o Termo de Uso alterado.
              </p>

              <p>
                <strong>Cláusula Oitava (Da Propriedade Intelectual).</strong> O uso comercial da expressão Biologia Aprova como marca, nome empresarial ou nome de domínio, bem como os conteúdos das telas relativas aos serviços do Biologia Aprova assim como os programas, bancos de dados, redes e arquivos, que permitem ao PARTICIPANTE acesse e use sua conta, são de propriedade do Biologia Aprova e estão protegidos pelas leis e tratados internacionais de direito autoral, marcas, patentes, modelos e desenhos industriais. O uso indevido e a reprodução total ou parcial dos referidos conteúdos são proibidos, salvo a autorização expressa do Biologia Aprova.
              </p>

              <p>
                <strong>Parágrafo Único.</strong> O Biologia pode possuir links com outros sites na internet o que não significa que esses sites sejam de propriedade ou operados pelo Biologia Aprova. Não possuindo controle sobre esses sites, o Biologia Aprova não será responsável pelos conteúdos, práticas e serviços ofertados nos mesmos. A presença de links para outros sites não implica relação de sociedade, de supervisão, de cumplicidade ou solidariedade do Biologia Aprova para com esses sites e seus conteúdos.
              </p>

              <p>
                <strong>Cláusula Oitava.</strong> (Do foro e Legislação Aplicável). Todos os itens deste Termo de Uso estão regidos pelas leis vigentes na República Federativa do Brasil. Para todos os assuntos referentes à sua interpretação e cumprimento, as partes se submeterão ao Foro da Cidade de Petrolina-PE, exceção feita a reclamações apresentadas por PARTICIPANTES que se enquadrem no conceito legal de consumidores, que poderão submeter ao foro de seu domicílio.
              </p>

            </div>
          </div>
        </div>
<div>