<link href="{{URL::to('/css/curso/plataforma.css?v=1.3')}}" rel="stylesheet">

<?php
    $curso["navegacao"] = [
        [
            'titulo' => "Área do aluno",
            'icone' => "fa fa-play",
            'url' => url::to('/conta/curso'),
            'active' => true,
        ],
    ];

    $musicas_intensivao = [6, 31, 7, 26, 10, 11, 21, 8, 19];
    $areas_intensivao = [15, 16, 19];
    $areas_simulados_intensivao = [17];
    $simulados_intensivao = [138, 139, 141];
    $areas_simulados_ocultas = [15, 16, 17, 18, 19];
    $areas_ebooks_intensivao = [18];    
    $extra = 20;

    $dueTime = date("Y-m-d H:i:s", strtotime("2021-10-21 00:00"));
    $now = date("Y-m-d H:i:s");
    if ($now >= $dueTime) {
        $simulados_intensivao = [138, 139, 141, 142];
    }

    //$areas_ebooks_intensivao = [18, 19, 20, 21, 22, 23, 24];

    $status_aula_ao_vivo = [
        "<span class='status-aovivo' style='background: black'>Agendada</span>",
        "<span class='status-aovivo' style='background: blue'>Em breve</span>",
        "<span class='status-aovivo' style='background: red'>Ao vivo</span>",
        "<span class='status-aovivo' style='background: green;'>Realizada</span>"
    ];

    $start_delay = 'PT0H30M'; // 30min.
    $server_ahead_time = 'PT3H0M'; // 3h.
    $server_now_ahead_time = 'PT2H30M'; // 2h30min.

    $delay = "-3 hours";

    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
?>

<style>

    #ebookModal .modal-title {
        font-weight: 700;
        font-size: large;
    }

    #ebookModal .modal-sobre {
        margin-top: 1rem;
    }

    #ebookModal .modal-sobre p {
        text-align: justify;
        font-family: 'Nunito';
    }

    #ebookModal .modal-footer {
        width: 80%;
        margin: auto;
        font-family: 'Lufga';
        font-weight: 700;
    }

    #ebookModal .modal-footer button {
        margin:auto;
        width: 80%;
        font-family: 'Lufga';
        font-weight: bold;
    }

    .status-aovivo {
        font-size: small;
        color: white;
        padding: 0px 5px;
        margin: 0px 5px;
        border-radius: 2px;
        width: 72px;
        text-align: center;
    }

    .plataforma-planos ul li {
        border-radius: 8px;
        background-color: rgba(255, 255, 255, 0.15);
        margin-top: 5px;
        color: white;
        margin-left: auto;
        font-family: 'Nunito';
        font-weight: 500;
        font-size: 14px;
        margin-right: auto;
        padding: 7px 20px 7px 15px;
        display: flex!important;
        justify-content: space-around;
        transition: 0.5s ease;
        cursor: pointer;
    }

    .plataforma-planos ul li:hover {
        color:#073982;
        background-color: rgba(255, 255, 255, 68%);
        font-weight: 900;
    }

</style>

<!-- E-book Modal -->
<div class="modal fade" id="ebookModal" tabindex="-1"  data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="ebookModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ebookModalLabel">E-book</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="row modal-img">
                <center><img src="#" alt="biologia aprova - imagem do e-book" /></center>                 
            </div>
            <div class="row modal-sobre">
                <p></p>
            </div> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-src="" data-id="" data-bs-dismiss="modal">acesse</button>
        </div>
      </div>
    </div>
</div>

<div class="plataforma-navegacao margin-header">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <ul class="navegacao">
                        <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                        <?php foreach ($curso["navegacao"] as $nav) { ?>
                        <li class="<?php echo $nav["active"] ? "active" : ""; ?>">
                            <a href="<?php echo $nav["url"]; ?>"><i class="<?php echo $nav["icone"]; ?>"></i> <?php echo $nav["titulo"]; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-md-4 col-xl-3">
                    <div class="plataforma-progresso">
                        <p style="margin-bottom:0">Progresso</p>
                        <!-- <div class="barra-progresso">
                            <div style="{{'width:' .  $qtdVideosAssistidos*100/$qtdVideos . '%'}}"></div>
                        </div> -->
                        <div class="progresso-bar" data-porcentagem="{{$qtdVideosAssistidos*100/$qtdVideos}}" style="width: 200px; height: 5px; margin-left: 10px; border-radius: 5px; display: flex; overflow: hidden;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /navegação -->

<!-- header -->
<div class="plataforma-header" style="background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118));">
    <div class="container-lg">
        <div class="plataforma-header-content">
            <!-- perfil -->
            <div class="plataforma-perfil">
                <div class="plataforma-perfil-foto profile-picture" style="background-image:url({{url::to('/img/profile/') . '/' . $user->foto}})"></div>
                <div class="plataforma-perfil-dados">
                    <p class="opacity-1">Bem-vindo(a) de volta, </p>
                    <h2>
                        @isset ($user)
                        {{$user->nome}}
                        @endisset
                        <a href="{{url::to('conta')}}" class="alterar-dados" title="Alterar dados"><i class="fas fa-pencil-alt"></i></a>
                    </h2>
                    <!-- <p class="opacity-5">Último acesso: 13/01/2019 às 14:40</p> -->
                </div>
            </div> <!-- /perfil -->

           <div>
                <!-- plano atual -->
                <div class="plataforma-plano">
                    <p>Plano ativo</p>
                    @isset($user->plano)
                    <h2>{{$user->plano->plano}}</h2>
                    <p>Expira em <strong>{{date("d/m/Y", strtotime($user->plano->fim))}}</strong></p>
                    @endisset
                </div>
                @if ($user->planos)
                <div class="plataforma-planos">
                    <ul>
                        @foreach ($user->planos as $plano)
                            @if ($user->plano->id == $plano->id)
                                @continue
                            @endif
                            <li data-plano="{{$plano}}" onclick="trocarPlano({{$plano->id}});">{{$plano->plano}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!-- /plano atual -->
           </div>
        </div>
    </div>
</div>
<!-- /header -->

<style>

    .plataforma-playlist {
        width: 300px;
        height: 120px;
        display: flex;
        border: 1px solid white;
        border-radius: 15px;
        margin-top: -40px;
        background: linear-gradient(to right, #0d6efd 50% , white 50%);
        background-size: 200% 100%;
    background-position:left bottom;
        padding: 1rem;
        cursor:pointer;
        box-shadow: rgb(0 0 0 / 15%) 1.95px 1.95px 2.6px;
        transition:all .5s ease;
    }

    .plataforma-playlist:hover{
      box-shadow: none;
      background-position:right bottom;
      box-shadow: 0px 0px 15px background;
    }

    .plataforma-playlist:hover .playlist-img-container {
      background-color: rgb(13 110 253);
      transition: background-color 500ms linear;
    }

    .plataforma-playlist:hover .playlist-img-container i {
        color: white;
        transition: color 500ms linear;
    }

    .hide-i {
        display: block;
        opacity: 1;
        animation: hide .25s cubic-bezier(0.375, 0.150, 0.690, 1.650);
    }

    .show-i {
        display: none;
        opacity: 0;
        animation: show .25s .25s cubic-bezier(0.375, 0.150, 0.690, 1.650);
    }

    .plataforma-playlist:hover  p {
        color:#069775;
        transition: color 500ms linear;
    }

    .plataforma-playlist:hover h2 {
        color:black;
        transition: color 500ms linear;
    }

    .plataforma-playlist .playlist-img-container {
        width: 70px;
        height: 70px;
        border-radius: 50px;
        display: flex;
        background: white ;
        box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;
        margin-top:auto;
        margin-bottom:auto;
        margin-right: 1rem;
    }

    .plataforma-playlist .playlist-img-container i {
        margin: auto;
        color: #069775;
        font-size: x-large;
    }

    .plataforma-playlist .playlist-img-container .play {
       display: none  !important;
    }

    .playlist-text-container {
        margin: auto 0px auto 0px;
    }

    .plataforma-playlist h2 {
        font-size: 1.2rem;
        font-weight: bold;
        font-family: 'Lufga';
        color: white;
    }

    .plataforma-playlist p {
        font-weight: 600;
        font-family: 'Lufga';
        font-size: .9rem;
        color: white;
    }

    @keyframes show {
        from {
            display: none;
            opacity: 0;
            transform:scale(.8);
        }
        to {
            display: block;
            opacity: 1;
            transform: scale(1);
        }
    }

    @keyframes hide {
        from {
            display: block;
            opacity: 1;
            transform: scale(1);
        }
        to {
            display: none;
            opacity: 0;
            transform:scale(.8);
        }
    }

    #musica-link {
        display: flex;
        cursor: pointer;
    }

    .plataforma-musica-categoria .song-status {
        visibility: hidden;
        color: #198754 !important;
    }

</style>

<!-- conteúdo -->
<div class="container-lg base_url data_container" data-url="{{url::to('/')}}" data-plano="{{$user->plano->plano}}">


@if ($user->plano->plano === "EXTENSIVO MED")
@isset($playlists)
<div class="row mb-3">
    @foreach($playlists as $key=>$playlist)
    <div class="col-md-4 col-lg-3">
        <div class="plataforma-playlist" data-playlist="{{$playlist}}" onclick="goToPlaylist(this);">
            <div class="playlist-img-container">
                <i class="fas fa-stethoscope stethoscopo"></i>
                <i class="fas fa-play play"></i>
            </div>
            <div class="playlist-text-container">
                <h2>Playlist</h2>
                <p>{{$playlist->nome}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endisset
@endif

<div class="row plataforma-row">
    <div class="col-12">
        <div class="plataforma-content" >
            <div class="plataforma-content-header">
                <h2>Conteúdo do curso</h2>
                <ul>
                    @isset ($qtdVideos)
                        @if ($user->plano->plano === "INTENSIVÃO ENEM")
                            <li id="qtd_aulas"></li> <li id="qtd_simulados"></li>
                        @else
                            <li>{{$qtdVideos}} {{$qtdVideos == 1 ? "aula" : "aulas"}}</li>
                        @endif
                    @endisset
                </ul>
            </div>

            <!-- <p class="m-5 text-center">Nenhum conteúdo disponível no momento.</p> -->

            <div class="row">
                <div class="col-lg-8">
                    <div class="plataforma-categorias">
                    @isset($areas)
                        @foreach ($areas as $area)
                                
                            @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                @if (!in_array($area->id, $areas_intensivao))
                                    @continue
                                @endif
                            @else
                                @if (!count($area->videos))
                                    @continue
                                @endif
                                @if (in_array($area->id, $areas_intensivao))
                                    @continue
                                @endif
                            @endif
                                <!-- categoria -->
                            <div class="plataforma-categoria" data-id="{{$area->id}}">
                                <div class="categoria-header">
                                    <h2>{{$area->nome}}</h2>
                                    <ul>
                                        @if ($area->id == 16)
                                            <li>6 plantões</li>
                                        @else
                                            <li>{{count($area->videos)}} {{count($area->videos) == 1 ? "aula" : "aulas" }}</li>
                                        @endif
                                        <li></li>
                                    </ul>
                                    <i class="fa fa-chevron-down categoria-close"></i>
                                </div>
                                <div class="categoria-body">

                                    @if ($area->id == 16)
                                        
                                    <div  class="categoria-video" data-link="https://meet.google.com/kyw-xwuy-kkh" onclick="goTo(this);">
                                        <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                        </div>
                                        <div>
                                            <p>Encontro 1 Intensivão Biologia</p>
                                            <p style="font-size: 12px;">Segunda-feira, 11/10 · 21h às 22h</p>
                                        </div>
                                        <i class="fas fa-clipboard-list categoria-play"></i>
                                    </div>

                                        <div  class="categoria-video" data-link="https://meet.google.com/wae-ynyc-wak" onclick="goTo(this);">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                            </div>
                                            <div>
                                                <p>Encontro 2 Intensivão Biologia</p>
                                                <p style="font-size: 12px;">Segunda-feira, 18/10 · 21h às 22h</p>
                                            </div>
                                            <i class="fas fa-clipboard-list categoria-play"></i>
                                        </div>

                                        <div  class="categoria-video"  data-link="https://meet.google.com/yah-twci-zbk" onclick="goTo(this);">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                            </div>
                                            <div>
                                                <p>Encontro 3 Intensivão Biologia</p>
                                                <p style="font-size: 12px;">Segunda-feira, 25/10 · 21h às 22h</p>
                                            </div>
                                            <i class="fas fa-clipboard-list categoria-play"></i>
                                        </div>

                                        <div  class="categoria-video"  data-link="https://meet.google.com/ebb-hsdq-naq" onclick="goTo(this);">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                            </div>
                                            <div>
                                                <p>Encontro 4 Intensivão Biologia</p>
                                                <p style="font-size: 12px;">Segunda-feira, 01/10 · 21h às 22h</p>
                                            </div>
                                            <i class="fas fa-clipboard-list categoria-play"></i>
                                        </div>

                                        <div  class="categoria-video"  data-link="https://meet.google.com/jmt-fttb-unw" onclick="goTo(this);">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                            </div>
                                            <div>
                                                <p>Encontro 5 Intensivão Biologia</p>
                                                <p style="font-size: 12px;">Segunda-feira, 08/10 · 21h às 22h</p>
                                            </div>
                                            <i class="fas fa-clipboard-list categoria-play"></i>
                                        </div>

                                        <div  class="categoria-video"  data-link="https://meet.google.com/bqm-ugjv-ccy" onclick="goTo(this);">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/logo_100x100.png')}}');">
                                            </div>
                                            <div>
                                                <p>Encontro 6 Intensivão Biologia (Plantão Bônus)</p>
                                                <p style="font-size: 12px;">Segunda-feira, 15/10 · 21h às 22h</p>
                                            </div>
                                            <i class="fas fa-clipboard-list categoria-play"></i>
                                        </div>
                                    @else
                                        <!-- VIDEOS -->
                                        @foreach ($area->videos as $video)
                                            @php
                                                if ($video->tipo == 2) {
                                                    $inicio = date("Y-m-d H:i:s", strtotime($video->data_inicio));
                                                    $breve = date("Y-m-d H:i:s", strtotime("-30 minutes",  strtotime($inicio)));
                                                    $fim = date("Y-m-d H:i:s", strtotime($video->data_final));
                                                    $now = date("Y-m-d H:i:s");

                                                    $inicio = date("Y-m-d H:i:s", strtotime($delay, strtotime($inicio)));
                                                    $breve = date("Y-m-d H:i:s", strtotime($delay,  strtotime($breve)));
                                                    $fim = date("Y-m-d H:i:s", strtotime($delay,  strtotime($fim)));
                                                    $now = date("Y-m-d H:i:s");

                                                    //echo $now;

                                                    $status = "";
                                                    if ($now < $inicio) {
                                                        if ($now >= $breve) {
                                                            $status = $status_aula_ao_vivo[1];
                                                        }
                                                        else {
                                                            $status = $status_aula_ao_vivo[0];
                                                        }
                                                    }
                                                    elseif ($now >= $inicio) {
                                                        if ($now < $fim) {
                                                            $status = $status_aula_ao_vivo[2];
                                                        }
                                                        else {
                                                            $status = $status_aula_ao_vivo[3];
                                                        }
                                                    }
                                                }
                                            @endphp
                                            <div data-id="{{$video->id}}" data-plano="{{$user->plano->id}}" class="categoria-video video {{!$video->liberado ? 'video-nao-liberado' : '' }} {{$video->assistido ? 'assistido' : ''}}">
                                                <div class="video-placeholder" style="background-image:url('{{url::to('/') .'/'. $video->img}}');">
                                                    <!-- <div class="video-duracao">duração?</div> -->
                                                </div>
                                                <div>
                                                    <p>Aula {{$video->aula}} - {{$video->nome }}</p>
                                                    @if ($video->tipo == 2)
                                                    <p style="font-size: 12px;">{{date("d/m/Y H:i", strtotime($inicio))}} - {{ date("H:i", strtotime($fim))}}</p>
                                                    @endif
                                                </div>
                                                
                                                <span class="categoria-play">{!! $video->tipo == 2 ? $status : ""!!} <i class="far fa-play-circle"></i></span>

                                                @if (!$video->liberado)
                                                <div class="video-liberacao">
                                                    <p>Este vídeo será liberado em <strong>data de liberação?</strong>.</p>
                                                </div>
                                                @endif
                                            </div>
                                        @endforeach
                                        <!-- QUESTIONARIOS -->
                                        @isset($area->questionarios)
                                        @foreach ($area->questionarios as $questionario)
                                        <div data-id="{{$questionario->id}}" data-plano="{{$user->plano->id}}" data-questionario=true class="categoria-video questionario {{!$questionario->ativo ? 'video-nao-liberado' : '' }} {{$questionario->realizado ? 'assistido' : ''}}">
                                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/quiz.jpg')}}');">
                                            </div>
                                            <p>Quiz - {{$questionario->nome}}
                                                @if(!$questionario->realizado && strtotime($questionario->data) > strtotime("2021-03-25") )
                                                <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>
                                                @endif
                                            </p>
                                            <i class="fas fa-clipboard-list categoria-play"></i>

                                            @if (!$questionario->ativo)
                                            <div class="video-liberacao">
                                                <p>Este qustinario ainda não está liberado.</p>
                                            </div>
                                            @endif
                                        </div>
                                        @endforeach
                                        @endisset
                                    @endif
                                </div>
                            </div> <!-- categoria -->
                        @endforeach
                    @endisset
                    </div>
                </div>  

                <div class="col-lg-4">

                    @php
						$now = date('Y-m-d');
						$dueTime = date('Y-m-d', strtotime("+7 days", strtotime($user->plano->inicio)));
					@endphp 

                    @if (true) <!-- $now >= $dueTime -->
                     <!-- musicas -->
                     <div class="plataforma-card">
                        <h2><i class="fas fa-music" style="color:rgb(4 150 118)"></i> Músicas</h2>

                        <div class="plataforma-simulados-categorias" style="margin-top: 10px;">
                           
                            <!-- categoria -->
                            <div class="plataforma-simulado-categoria plataforma-musica-categoria">
                                <div class="categoria-header">
                                    <h2>Músicas Gerais</h2>
                                    @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                        <small>9 músicas</small>
                                    @else
                                    <small>31 músicas</small>
                                    @endif
                                    <i class="fa fa-chevron-down categoria-close"></i>
                                </div>
                                
                                @php

                                    // $filtered_songs = [];
                                    // $count = 1;
                                    // foreach ($songs as $key=>$song) {
                                    //     if (in_array($song->id, $musicas_intensivao)) {
                                    //         $song->id = $count;
                                    //         $count++;
                                    //         $filtered_songs[] = $song;
                                    //     }
                                    // }

                                    // //dd($user->plano->plano);

                                    // if ($user->plano->plano === "INTENSIVÃO ENEM") {
                                    //     unset($songs);
                                    //     $songs = $filtered_songs;
                                    // }

                                    // $filtered = $songs->filter(function ($song, $key) {
                                    //     //return in_array($song->id, $musicas_intensivao);
                                    //     return in_array($song->id, $musicas_intensivao) ? true : false;
                                    // });

                                    // dd($filtered);

                                @endphp

                                <div class="categoria-body">
                                    <ul class="plataforma-card-simulados data_song" data-songs="@isset($songs) {{$songs}} @endisset">
                                        @isset($songs)
                                        @php $index = 1; @endphp
                                        @foreach ($songs as $key=>$song)
                                            @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                                @if (!in_array($song->id, $musicas_intensivao))
                                                    @continue
                                                @else
                                                    @php
                                                        $song->id = $index;
                                                        $index++;
                                                    @endphp
                                                @endif
                                            @endif
                                            <li id="musica-{{$song->id}}" data-id="musica-{{$key}}" title="{{$song->nome}}">    
                                                <div id="musica-link" onclick="playSong(this);" data-id={{$song->id}} data-song="{{$song}}">
                                                <!-- <div id="musica-link" onclick="playSong(this);" data-id={{$song->id}} data-song="{{$song}}"> -->
                                                    <div class="simulado-status"><i class="fas fa-music"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>{{$song->nome}}</p>
                                                        <span class="song-status"></span>
                                                    </div>
                                                </div>
                                            </li>
                                           
                                        @endforeach
                                        @endisset
                                    </ul>
                                </div>
                            </div> <!-- /categoria -->
                        </div>
                    </div>
                    <!-- /musicas -->
                    @endif
                    
                    <!-- e-books -->
                    <div class="plataforma-card">
                        <h2><i class="fas fa-book" style="color:rgb(4 150 118)"></i> Materiais e E-Books</h2>

                        <div class="plataforma-simulados-categorias" style="margin-top: 10px;">

                        @isset($ebooks)
                         <!-- categoria -->
                       
                            @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                <!-- <div class="categoria-fixed">
                                    <h2>Itensivão de Biologia</h2>
                                    <a class="link-light stretched-link" href="{{url('arquivos\intensivao_enem_2021.pdf')}}" target="_blank"><small>Acessar</small></a>
                                </div> -->
                                @foreach ($areas as $area)
                                    @if (!in_array($area->id, $areas_ebooks_intensivao))
                                        @continue
                                    @endif
                                    @isset($area->ebooks)
                                    <div class="plataforma-simulado-categoria">
                                    <div class="categoria-header">
                                        <h2>{{$area->nome}}</h2>
                                        <!-- <small>{{count($area->ebooks)}} e-book{{count($area->ebooks) == 1 ? '' : 's'}}</small> -->
                                        <i class="fa fa-chevron-down categoria-close"></i>
                                    </div>
                                    <div class="categoria-body">
                                        <ul class="plataforma-card-simulados sem-categoria">
                                            <li>    
                                                <a id="ebook-link"  href="{{url('arquivos\intensivao_enem_2021.pdf')}}" target="_blank">
                                                    <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>Apostila da Aula 01</p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>    
                                                <a id="ebook-link"  href="{{url('arquivos\apostila_aula2.pdf')}}" target="_blank">
                                                    <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>Apostila da Aula 02</p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>    
                                                <a id="ebook-link"  href="{{url('arquivos\mapaconceitual_aula2.pdf')}}" target="_blank">
                                                    <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>Mapa Conceitual da Aula 02</p>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>    
                                                <a id="ebook-link"  href="{{url('arquivos\apostila_aula_3.pdf')}}" target="_blank">
                                                    <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>Apostila da Aula 03</p>
                                                    </div>
                                                </a>
                                            </li>
                                            @php
                                                $dueTime = date("Y-m-d H:i:s", strtotime("2021-10-21 00:00"));
                                                $now = date("Y-m-d H:i:s");
                                            @endphp
                                            @if ($now >= $dueTime)
                                                <li>    
                                                    <a id="ebook-link"  href="{{url('arquivos\apostila_aula_4_5.pdf')}}" target="_blank">
                                                        <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                        <div class="simulado-titulo">
                                                            <p>Apostila das Aulas 04 e 05</p>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endif
                                            @foreach ($area->ebooks as $key=>$ebook)
                                            <li data-id="{{$ebook->id}}" title="{{$ebook->titulo}}">    
                                                <a id="ebook-link" href="#" onclick="showEbook('{{json_encode($ebook)}}'); return false;">
                                                    <div class="simulado-status"><i class="fas fa-book"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>{{$ebook->nome}}</p>
                                                        <!-- <span>Disponibilizado em {{date('d/m/Y', strtotime($ebook->data))}}</span> -->
                                                    </div>
                                                    <div class="simulado-icone">
                                                        @if ($ebook->visualizado)
                                                        <i class="fa fa-eye"></i>
                                                        @else
                                                        <i class="fa fa-eye" style="color:#37343599"></i>
                                                        @endif
                                                    </div>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    </div>
                                    @endisset
                                @endforeach
                            @else
                            <div class="plataforma-simulado-categoria">
                            <div class="categoria-header">
                                <h2>E-books Gerais</h2>
                                <small>{{count($ebooks)}} e-book{{count($ebooks) == 1 ? '' : 's'}}</small>
                                <i class="fa fa-chevron-down categoria-close"></i>
                            </div>
                            <div class="categoria-body">
                                <ul class="plataforma-card-simulados sem-categoria">
                                    @foreach ($ebooks as $key=>$ebook)
                                    <li data-id="$ebook->id" title="{{$ebook->titulo}}">    
                                        <a id="ebook-link" href="#" onclick="showEbook('{{json_encode($ebook)}}'); return false;">
                                            <div class="simulado-status"><i class="fas fa-book"></i></div>
                                            <div class="simulado-titulo">
                                                <p>{{$ebook->nome}}</p>
                                                <!-- <span>Disponibilizado em {{date('d/m/Y', strtotime($ebook->data))}}</span> -->
                                            </div>
                                            <div class="simulado-icone">
                                                @if ($ebook->visualizado)
                                                <i class="fa fa-eye"></i>
                                                @else
                                                <i class="fa fa-eye" style="color:#37343599"></i>
                                                @endif
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            </div>
                            @endif
                         <!-- /categoria -->
                        @endisset

                        </div>
                    </div><!-- /e-books -->

                    <!-- simulados -->
                    <div class="plataforma-card">
                        <h2><i class="fas fa-file-alt" style="color:rgb(4 150 118)"></i> Simulados</h2>
                        <div class="plataforma-simulados-categorias simulado-list" style="margin-top: 10px;">
                            @isset ($areas)

                            <!-- lista as categorias extras -->
                            @foreach ($areas as $area)

                                @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                    @continue
                                @else 
                                    @if (!count($area->simulados))
                                        @continue
                                    @endif
                                @endif

                                @if ($area->id != $extra)
                                    @continue
                                @else

                                <!-- categoria geral-->
                                <div class="plataforma-simulado-categoria">
                                    <div class="categoria-header">
                                        <h2>{{$area->nome}}</h2>
                                        @if ($user->plano->plano !== "INTENSIVÃO ENEM")
                                        <small>{{count($area->simulados)}} simulado{{count($area->simulados) == 1 ? '' : 's'}}</small>
                                        @else
                                        <small>1 Simulado</small>
                                        @endif
                                        <i class="fa fa-chevron-down categoria-close"></i>
                                    </div>
                                    <div class="categoria-body">
                                        <ul class="plataforma-card-simulados">
                                        @isset ($area->simulados)
                                        @foreach ($area->simulados as $key=>$simulado)
                                        @if ($simulado->idArea == $extra)

                                                {{$iniciado = ($simulado->data_inicio && !$simulado->data_realizacao)}}
                                                <?php $simulado_icone_status = "far fa-file-alt" ?>
                                                @if ($simulado->realizado)
                                                <?php $simulado_icone_status = "fas fa-check" ?>
                                                @endif


                                            <li class="simulado-item" data-id="{{$simulado->id}}" title="{{$simulado->nome}}">
                                                <a href="{{url::to('/conta/curso/simulado') .'/'. $simulado->id .'/'. $user->plano->id}}">
                                                    <div class="simulado-status {{ $iniciado ? 'iniciado' : ''}} {{$simulado->realizado ? 'realizado' : ''}}"><i class="far {{$simulado_icone_status}}"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>{{$simulado->nome}}
                                                            @if(!$simulado->realizado && strtotime($simulado->data) > strtotime("2021-03-25") )
                                                            <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>
                                                            @endif
                                                        </p>
                                                        
                                                        @if ($iniciado)
                                                        <span>Iniciado em {{date("d/m/Y", strtotime($simulado->data_inicio))}}</span>
                                                        @elseif ($simulado->realizado)
                                                        <span>Realizado em {{date("d/m/Y", strtotime($simulado->dataRealizado))}}</span>
                                                        @else
                                                        <!-- <span>Disponibilizado em {{date("d/m/Y", strtotime($simulado->data))}}</span> -->
                                                        @endif
                                                    </div>
                                                    <div class="simulado-icone">
                                                        @if ($simulado->visualizado)
                                                        <i class="fa fa-eye"></i>
                                                        @else
                                                        <i class="fa fa-eye" style="color:#37343599"></i>
                                                        @endif
                                                    </div>
                                                </a>
                                            </li> 

                                        @endif
                                        @endforeach
                                        @endisset
                                        </ul>
                                    </div>
                                </div> <!-- /categoria geral -->
                                @endif
                            @endforeach

                            @foreach ($areas as $area)

                                @if (!count($area->simulados))
                                    @continue
                                @endif
                                
                                @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                    @if (!in_array($area->id, $areas_simulados_intensivao)) 
                                        @continue
                                    @endif
                                @else
                                    @if (in_array($area->id, $areas_simulados_intensivao) || in_array($area->id, $areas_simulados_ocultas)) 
                                        @continue
                                    @endif  
                                    @if (!count($area->simulados)) 
                                        @continue
                                    @endif
                                @endif

                                @if ($area->id == $extra)
                                    @continue
                                @endif

                                <!-- categoria -->
                                <div class="plataforma-simulado-categoria">
                                    <div class="categoria-header">
                                        <h2>{{$area->nome}}</h2>
                                        @if ($user->plano->plano !== "INTENSIVÃO ENEM")
                                        <small>{{count($area->simulados)}} simulado{{count($area->simulados) == 1 ? '' : 's'}}</small>
                                        @endif
                                        <i class="fa fa-chevron-down categoria-close"></i>
                                    </div>
                                    <div class="categoria-body">
                                        <ul class="plataforma-card-simulados">
                                        @isset ($area->simulados)
                                        @foreach ($area->simulados as $simulado)

                                                {{$iniciado = ($simulado->data_inicio && !$simulado->data_realizacao)}}
                                                <?php $simulado_icone_status = "far fa-file-alt" ?>
                                                @if ($simulado->realizado)
                                                <?php $simulado_icone_status = "fas fa-check" ?>
                                                @endif

                                                @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                                    @if (!in_array($simulado->id, $simulados_intensivao))
                                                        @continue
                                                    @endif
                                                @else
                                                    @if (in_array($simulado->id, $simulados_intensivao))
                                                        @continue
                                                    @endif
                                                @endif

                                            <li data-id="{{$simulado->id}}" title="{{$simulado->nome}}">
                                                <a href="{{url::to('/conta/curso/simulado') .'/'. $simulado->id .'/'. $user->plano->id}}">
                                                    <div class="simulado-status {{ $iniciado ? 'iniciado' : ''}} {{$simulado->realizado ? 'realizado' : ''}}"><i class="far {{$simulado_icone_status}}"></i></div>
                                                    <div class="simulado-titulo">
                                                        <p>{{$simulado->nome}}
                                                            @if(!$simulado->realizado && strtotime($simulado->data) > strtotime("2021-03-25") )
                                                            <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>
                                                            @endif
                                                        </p>
                                                        
                                                        @if ($iniciado)
                                                        <span>Iniciado em {{date("d/m/Y", strtotime($simulado->data_inicio))}}</span>
                                                        @elseif ($simulado->realizado)
                                                        <span>Realizado em {{date("d/m/Y", strtotime($simulado->dataRealizado))}}</span>
                                                        @else
                                                        <!-- <span>Disponibilizado em {{date("d/m/Y", strtotime($simulado->data))}}</span> -->
                                                        @endif
                                                    </div>
                                                    <div class="simulado-icone">
                                                        @if ($simulado->visualizado)
                                                        <i class="fa fa-eye"></i>
                                                        @else
                                                        <i class="fa fa-eye" style="color:#37343599"></i>
                                                        @endif
                                                    </div>
                                                </a>
                                            </li> 
                                        @endforeach
                                        @endisset
                                        </ul>
                                    </div>
                                </div>
                                <!-- /categoria -->
                            @endforeach
                            @endisset
                        </div>

                    </div> <!-- /simulados -->
                </div>  
            </div>
        </div>
    </div>
</div>
</div> <!-- /conteúdo -->

<div class="mt-5"></div>

 <!-- Scripts -->
 <script type="text/javascript">
    var is_home = true;
    var ultimo_video_assistido = new Object();
    ultimo_video_assistido.id_categoria = "{{$user->ultimoVideoAssistido != null ? $user->ultimoVideoAssistido->idArea : 1 }}";
    var base_url = $(".base_url").data('url');
   
    var PLAYER = null;
    var CURRENT_TRACK = -1;
    var PLAYER_DESTROIED = true;

    var musicas_intensivao = [6, 31, 7, 26, 10, 11, 21, 8, 19];
    var is_intensivao_enem = "{{$user->plano->plano === 'INTENSIVÃO ENEM'}}";

    $(document).ready(function(){

        // botão troca plano
        $(".plataforma-planos li").hover(function() {
            $(this).html("IR PARA ESSE PLANO");
        }, function () {
            let plano = $(this).data("plano");
            $(this).html(plano.plano);
        });

        // count simulados
        var simulado_list_el = $(".simulado-list").find("li.simulado-item");
        console.log("simulado_list", simulado_list_el.length);
        $("#qtd_simulados").text(`${simulado_list_el.length} Simulados`);

        // handle playlists
        var plano = $(".data_container").data('plano');
        var hasPlaylists = "{{ ($playlists != null) && (count($playlists) > 0) }}";
        if (!hasPlaylists) $(".plataforma-row").css('margin-top', '-40px');

        // show "novo" for categoria (simulado).
        var categorias = $(".plataforma-simulados-categorias").find(".plataforma-simulado-categoria");
        categorias.each(function (index) {
            var categoria = $( this );
            var novo = categoria.find(".novo-simulado").html();
            //console.log("novo: " + novo + (novo != null));
            if (novo != null) {
                categoria.find("h2").append(' <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>');
            }
        });

        // show "novo" for categoria (video).
        var categorias_videos = $(".plataforma-categorias").find(".plataforma-categoria");
        categorias_videos.each(function (index) {
            var categoria = $( this );
            var novo = categoria.find(".novo-simulado").html();
            //console.log("novo: " + novo + (novo != null));
            if (novo != null) {
                categoria.find("h2").append(' <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>');
            }
        });

        // $(".plataforma-playlist").hover(mouseIn, mouseOut);
    });

    /* plano */
    function trocarPlano(id) {
        let link = `{{url('conta/plano/trocar/${id}')}}`;
        window.open(link, "_self");
    }
    /* /plano */

    /* Songs */

    function playSong(e) {
        var song = $(e).data('song');
        console.log("song", song);
        var song_index = song.id - 1;
        CURRENT_TRACK = song.id;
        console.log("current track: " + CURRENT_TRACK);
        console.log("current index: " + song_index);
        $("#player-wrapper").css('visibility', 'visible');
        if (PLAYER == null || PLAYER_DESTROIED) {
            setPlayer(song_index);
        }
        else PLAYER.switchTrack(song_index, startPlaying=true);
    }

    function setPlayer(song_id) {

        var song_array = getSongsArray();
        PLAYER = new Calamansi(document.querySelector('#calamansi-player-1'), {
            skin: "{{url::to('assets/calamansi/skins/calamansi-compact')}}",
            playlists: {
                'Biologia Aprova': song_array,
            },
            defaultAlbumCover: "{{url::to('assets/calamansi/skins/default-album-cover.png')}}",
        });

        console.log("PLAYER", PLAYER);

        PLAYER.on('playlistLoaded', function (player) {
            console.log('playlistLoaded', player);
            PLAYER.switchTrack(song_id, startPlaying=true);
            PLAYER_DESTROIED = false;
            $("#player-wrapper").css('visibility', 'visible');
        });

        PLAYER.on('play', function (player) {
            var index = player.currentTrack().info.songId;
            console.log('play', player);
            //updateDOMSongSatus(index, "tocando...");
        });

        PLAYER.on('pause', function (player) {
            var index = player.currentTrack().info.songId;
            console.log('pause', player.currentTrack().info);
            //updateDOMSongSatus(index, "pausada");
        });

        PLAYER.on('trackSwitched', function (player) {
            var index = player.currentTrack().info.songId;
            console.log('pause', player);
            //updateDOMSongSatus(index, "tocando...");
        });

        $("#player-close").click(function(e){
            PLAYER.destroy();
            PLAYER_DESTROIED = true;
            $(".song-status").html("");
            $("#player-wrapper").css('visibility', 'hidden');
        });

    }

    function updateDOMSongSatus(index, message) {
        console.log("updateDOMSongSatus song index: " + index);
        $(".song-status").html("");
        var status_el =  $("#musica-" + index + " .song-status");
        status_el.html(message);
        status_el.css('visibility', 'visible');
    }

    function getSongsArray() {
        var songs = $(".data_song").data('songs');
        var song_array = [];
        console.log("getsongsarray songs", songs);
        $.each(JSON.parse(songs), function(i,song) {
            if (is_intensivao_enem) {   
                if (musicas_intensivao.includes(song.id)) {
                    console.log("song.id", song.id);
                    song_array.push({source: base_url + '/musicas/' + song.src, info:{songId:song.id}});
                }
            }
            else {
                song_array.push({source: base_url + '/musicas/' + song.src, info:{songId:song.id}});
            }
            //song_array.push({source: base_url + '/musicas/' + song.src, info:{songId:song.id}});

        });
        console.log("getsongsarray songs array", song_array);
        return song_array;
    }

    /* /Songs */

    /* E-books */

    function goToEbook(elem) {

        markEbookAsSeen($(elem).data('id'));

        var src = $(elem).data('src');
        window.open(
            "{{url::to('/arquivos/ebooks') . '/'}}" + src,
            '_blank' // <- This is what makes it open in a new window.
        );
    }

    function markEbookAsSeen(id) {
        console.log(id);
        var url = "{{url::to('ebook/markAsSeen')}}";
        $.post(url, {'idEbook': id}, function(data) {
            //console.log(data);
            if (data.success) {
                console.log("ebook " + data.ebook.nome + " visualizado.");
            } else {
                console.log(data.message);
            }
        })
        .fail(function() {
            console.log("markEbookAsSeen falhou!");
        });
    }

    function showEbook(ebook) {
        var ebook = JSON.parse(prepareStringForJson(ebook));
        //console.log(ebook == null);
        //console.log("ebook: " + ebook.src);
        $('#ebookModal .modal-title').html(ebook.titulo);
        $('#ebookModal .modal-body .modal-img img').attr('src', "{{url::to('img/ebooks/capas') . '/'}}" + ebook.img);
        $('#ebookModal .modal-body .modal-sobre p').html(ebook.sobre);
        $('#ebookModal .modal-footer button').data("id", ebook.id);
        $('#ebookModal .modal-footer button').data("src", ebook.src);
        $('#ebookModal .modal-footer button').attr("onclick", 'goToEbook(this)');
        $('#ebookModal').modal('show');
    }

    /* / E-books */

    function goToPlaylist(e) {
        var playlist = $(e).data("playlist");
        var url = "{{url::to('conta/curso/aula')}}" + "/-1?tipo=playlist_med&playlist=" + playlist.id;
        window.location.href = url;
        //console.log("clicked!");
    }

    function prepareStringForJson(string) {
         // preserve newlines, etc - use valid JSON
         string = string.replace(/\\n/g, "\\n")  
                    .replace(/\\'/g, "\\'")
                    .replace(/\\"/g, '\\"')
                    .replace(/\\&/g, "\\&")
                    .replace(/\\r/g, "\\r")
                    .replace(/\\t/g, "\\t")
                    .replace(/\\b/g, "\\b")
                    .replace(/\\f/g, "\\f");
        // remove non-printable and other non-valid JSON chars
        string = string.replace(/[\u0000-\u0019]+/g,"");
        return string;
    }

    function goTo(e) {
        let link = $(e).data('link');
        console.log(link);
        window.open(link, "_target");
    }

</script>

<script src="{{URL::to('/js/plataforma.js?v=1.0')}}"></script>
<script src="{{URL::to('/js/progressbar.js?v=1.0')}}"></script>