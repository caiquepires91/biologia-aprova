<style>

#erro p {
    font-size: xxx-large;
    text-align: center;
}

#erro .row {
    height: 100vh;
    margin-top: 8rem;
}

#erro .content {
    font-family: 'Lufga';
    color: #a0a0a0;
    text-align: center;
    font-weight: 600;
}

</style>

<div  class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">{{$subject}}</h2>
    </div>
</div>
<div id="erro" class="container">
    <div class="row justify-content-center">
        <div class="mx-auto content">
            <p><i class="fas fa-times-circle"></i></p>
            <p>{{$message}}</p>
        </div>
    </div>
</div>