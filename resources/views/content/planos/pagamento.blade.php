<!-- Modal -->
<div class="modal fade" id="autorizadoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pagamento Pendente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                O pagamento está pendente. Notificaremos por e-mail assim que recebermos a confirmação.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dados-cartao" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Dados do titular do cartão</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="dados-cartao-form" action="{{ url::to('planos/atualizar-dados-cartao') }}"
                name="form_dados_cartao" class="p-0 m-0" novalidate>
                <div class="modal-body">
                    <p>Antes de realizar o pagamento precisamos de alguns dados.</p>
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-cpf-cartao" class="form-label">CPF*:</label>
                                <input type="text" class="form-control" id="cadastrar-cpf-cartao" name="cpf-cartao"
                                    value="{{ $user->cpf_cartao }}" aria-describedby="cadastrar-cpf-cartao-fb">
                                <div id="cadastrar-cpf-cartao-fb" class="invalid-tooltip ">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-email" class="form-label">e-mail*:</label>
                                <input type="email" class="form-control" id="cadastrar-email-cartao"
                                    autocomplete="username" value="{{ $user->email_cartao }}" name="email-cartao"
                                    aria-describedby="cadastrar-email-cartao-fb">
                                <div id="cadastrar-email-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-endereco" class="form-label">endereço*:</label>
                                <input type="text" class="form-control" id="cadastrar-endereco" autocomplete="street"
                                    name="endereco" value="{{ $user->endereco }}"
                                    aria-describedby="cadastrar-endereco-fb">
                                <div id="cadastrar-endereco-fb" class="invalid-tooltip">
                                    Por favor, digite um endereço válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-numero" class="form-label">num.*:</label>
                                <input type="text" class="form-control" id="cadastrar-numero"
                                    autocomplete="street_number" value="{{ $user->numero }}" name="numero"
                                    aria-describedby="cadastrar-numero-fb">
                                <div id="cadastrar-numero-fb" class="invalid-tooltip">
                                    Por favor, digite um número válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-estado" class="form-label">estado*:</label>
                                <select type="text" class="form-select" id="cadastrar-estado" data-tipo="estado"
                                    name="estado" value="{{ $user->estado }}"
                                    aria-describedby="cadastrar-estado-fb"></select>
                                <div id="cadastrar-estado-fb" class="invalid-tooltip ">
                                    Esse campo é obrigatório.
                                </div>
                            </div>

                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-bairro" class="form-label">bairro*:</label>
                                <input type="text" class="form-control" id="cadastrar-bairro"
                                    autocomplete="neighborhood" value="{{ $user->bairro }}" name="bairro"
                                    aria-describedby="cadastrar-bairro-fb">
                                <div id="cadastrar-bairro-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-2 has-validation position-relative">
                                <label for="cadastrar-cidade" class="form-label">cidade*:</label>
                                <select type="text" class="form-select" id="cadastrar-cidade" data-tipo="cidade"
                                    autocomplete="neighborhood" name="cidade" value="{{ $user->cidade }}"
                                    aria-describedby="cadastrar-cidade-fb"></select>
                                <div id="cadastrar-cidade-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                            <div class="has-validation position-relative">
                                <label for="cadastrar-cep" class="form-label">CEP*:</label>
                                <input type="text" class="form-control" id="cadastrar-cep" name="cep"
                                    value="{{ $user->cep }}" aria-describedby="cadastrar-cep-fb">
                                <div id="cadastrar-cep-fb" class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="dados-cartao-form-btn" type="submit" class="btn btn-secondary">CONTINUAR</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /Modal -->

<style>
    #loader-wrapper {
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: #222222e3;
        z-index: 1021;
        display: flex;
    }

    #loader {
        z-index: 1022;
        /* anything higher than z-index: 1000 of .loader-section */
        margin: auto;
        position: absolute;
        top: 50%;
        right: 44%;
    }

    #loader h1 {
        font-size: xxx-large;
        font-weight: 700;
        color: #EEEEEE;
    }

    #pagarme-checkout-amount-information {
        font-size: 18px;
        font-weight: bolder;
        background: #4848eb !important;
        border-radius: 5px;
        padding: 1rem;
    }
</style>

<div id="loader-wrapper">
    <div id="loader">
        <h1>aguarde...</h1>
    </div>
    <div class="loader-section">
    </div>
</div>

<div id="planos-login-section-1" class="row">
    <div class="col"
        style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h1 class="big-title" style="margin: auto;"><span style="font-weight:400;">checkout</span> <i
                class="fas fa-chevron-right" style="font-size: x-large;"></i> <span
                style="font-weight:400;">registro</span> <i class="fas fa-chevron-right"
                style="font-size: x-large;"></i> <strong>pagamento</strong></h1>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center align-items-center mx-md-5 p-2 pagamento-info-box">
        <div class="col-md-7 content">
            <p class="big-title"><i class="fas fa-info-circle"></i> informações sobre sua compra</p>
            <div class="row mt-3">
                <div id="plano-type" style="background: #ffffffe3;color: #5a6066; margin-left:15px; padding: 10px;"
                    class="col-md-5 col-sm-12">plano escolhido
                    <hr class="m-2"><strong>CURSO COMPLETO</strong>
                </div>
                <!-- <div id="plano-duration" class="col-md col-sm-12">duração: 7 dias</div> -->
                <div id="plano-price" style="background: #ffffffe3;color: #5a6066;margin-left:15px; padding: 10px;"
                    class="col-md-5 col-sm-12">valor
                    <hr class="m-2"><strong>R$ 262,80</strong>
                </div>
            </div>
            <div>
                <p class="mt-4" id="plano-renew"></p>
            </div>
            @isset($boleto)
                <p class="big-title" style="font-size: large; padding-top: 2rem; line-height: 1rem; font-weight: 500;"><i
                        class="fas fa-file-invoice-dollar"></i> boleto pendente!</p>
                <div class="row">
                    <a class="text-white" href="{{ url::to('pagamento/boleto') . '/' . $boleto->id }}">visualizar</a>
                </div>
            @endisset
        </div>
        <div class="col-md-5 mt-3 content align-self-start" style="display: flex;">
            <select id="pagamento-plano-select"
                data-plano="{{ app('request')->session()->has('plano')? app('request')->session()->get('plano'): 3 }}"
                class="form-select" aria-label="planos de pagamento" style="width: 60%;margin-left: auto;">
                <!-- <option value="0" @if (app('request')->session()->has('plano')) {{ app('request')->session()->get('plano') == 0? 'selected': '' }} @else "selected" @endif>plano extensivo med</option>
                <option value="1" @if (app('request')->session()->has('plano')) {{ app('request')->session()->get('plano') == 1? 'selected': '' }} @endif >plano extensivo enem</option> -->
                {{-- <option value="3" @if (app('request')->session()->has('plano')) {{app('request')->session()->get('plano') == 3 ? 'selected' : ''}} @endif "selected" >plano intensivão enem</option> --}}
                <option value="5"
                    @if (app('request')->session()->has('plano')) {{ app('request')->session()->get('plano') == 5? 'selected': '' }} @endif>
                    CURSO COMPLETO</option>
                {{-- @php
                  	$today = new DateTime("now");
					$target_day = new DateTime(config('constants.outras.deadline_aprova_mais'));
                @endphp
                @if ($today > $target_day)
                <option value="6" @if (app('request')->session()->has('plano')) {{app('request')->session()->get('plano') == 6 ? 'selected' : ''}} @endif >CURSO TOTAL</option>
                @endif --}}
            </select>
        </div>
    </div>
</div>
<div class="container pagamento">
    <div class="row justify-content-center user-box">
        <div class="content col-md-6 p-md-5">
            <div id="login-block" class="block" style="margin-left:auto">
                <p>tenho um cupom de desconto</p>
                <div style="display: flex;">
                    <form id="cupom-form" action="{{ url::to('pagamento/cupom') }}">
                        <div class="mb-3 has-validation position-relative">
                            <label for="pagamento-cupom" class="form-label">número do cupom*:</label>
                            <input type="text" class="form-control" id="pagamento-cupom"
                                placeholder="xxxx-xxxx-xxxx-xxxx">
                            <div id="pagamento-cupom-fb" class="invalid-tooltip"
                                aria-describedby="pagamento-cupom-fb">
                                cupom inválido.
                            </div>
                            <div id="pagamento-cupom-fb-valid" class="valid-tooltip"
                                aria-describedby="pagamento-cupom-fb-valid">
                                cupom válido!
                            </div>
                        </div>
                        <div class="img-container" style="padding: 15px; display: flex;">
                            <img id="cupom-img" src="{{ url::to('/img/pagamento/cartao_cupom.png') }}"
                                alt="biologia aprova - cupom" />
                        </div>
                        <div class="mb-3">
                            <p class="normal-text">Informe o número do cupom exatamente como ele está escrito.</p>
                        </div>
                        <button id="cupom-btn" type="submit" class="btn btn-primary" style="margin-top: auto;">
                            <div class="row">
                                <div class="col-6" style="text-align: left;">validar cupom</div>
                                <div class="col-6" style="text-align: right;"><i
                                        class="fas fa-long-arrow-alt-right"></i></div>
                            </div>
                        </button>
                    </form>
                    <div class="m-auto d-flex mt-3 row" id="desconto"
                        style="display:none;font-size: large;font-weight: bold;">
                        {{-- <div class="col"> --}}
                        {{-- <span class='mx-auto'><i class='fas fa-gifts'></i> Você possui um</span> --}}
                        {{-- <p style="ont-size: 1.7rem;
                            color: #6c6e71;
                            font-weight: 400;
                            margin-bottom: 0;
                            text-align: center;
                            line-height: unset;
                            background: unset;">desconto de</p>
                            <span>
                                <h2 class="position-relative" style="width: fit-content;font-size: 7.5rem;font-weight: 700;top:-33px;color: #0d6efd">
                                    50
                                    <span class="position-absolute start-100 plano-preco-b" style="font-size: xx-large;font-weight: 700;top: 14%;">%</span>
                                </h2>
                            </span> --}}
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="content col-md-6 p-md-5">
            <div class="block">
                <p>forma de pagamento</p>
                <div style="display: flex;">
                    <form id="cartao-form">
                        <div class="mb-3">
                            <div class="position-relative">
                                <label><i class="far fa-credit-card"></i> pague com cartão de crédito.</label>
                                <div class="img-container">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <!-- <input class="form-check-input mt-0" type="radio" value="" id="pagamento-cartao" name="pagamento-radio"  onClick="mostrarDiv('cartao')" aria-describedby="pagamento-cartao-fb" checked>
                                <label class="form-check-label" for="pagamento-cartao">
                                    pagar com cartão de crédito.
                                </label>
                                <div id="pagamento-cartao-fb" class="invalid-tooltip">
                                    Leia os termos de utilização antes de continuar.
                                </div> -->
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="position-relative">
                                <label><i class="fas fa-file-invoice"></i> pague com boleto.</label>
                                <div class="img-container">
                                    <img id="boleto-img" src="{{ url::to('/img/pagamento/boleto.png') }}"
                                        alt="biologia aprova - boleto bancário" />
                                </div>

                                <!--  <input class="form-check-input mt-0" type="radio" value="" id="pagamento-boleto" name="pagamento-radio"  onClick="mostrarDiv('boleto')"  aria-describedby="pagamento-boleto-fb">
                                <label class="form-check-label" for="pagamento-boleto">
                                    pagar com boleto.
                                </label>
                                <div id="pagamento-boleto-fb" class="invalid-tooltip">
                                    Leia os termos de utilização antes de continuar.
                                </div> -->
                            </div>
                        </div>
                        <div class="mb-3">
                            <p class="normal-text">Você poderá visualizar ou imprimir após a finalização da compra. A
                                data de vencimento é de 2 dias corridos após a conclusão da compra. Após esta data, o
                                boleto perderá a validade.</p>
                        </div>
                        <div>
                            <button id="pay-button" class="btn btn-primary" data-url="{{ url::to('pagarme') }}"
                                style="background-color: #3eb378; margin-top:auto">
                                <div class="row">
                                    <div id="pay-button-name" class="col-6" style="text-align: left;">continuar
                                    </div>
                                    <div class="col-6" style="text-align: right;"><i
                                            class="fas fa-long-arrow-alt-right"></i></div>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<script>
    var HAS_CUPOM = false;
    var CUPOM = null;
    var PLANO_ID = 0;
    var PLANO_VALOR = 19000;
    var PLANO_NOME = 'EXTENSIVO MED';
    const PAGARME_URL = "{{ url::to('pagarme') }}";
    const PLANOS = @json($user->planos, JSON_PRETTY_PRINT);
    var usuario = @json($user, JSON_PRETTY_PRINT);
    var estados = @json(isset($estados) ? $estados : [], JSON_PRETTY_PRINT);

    $(document).ready(function() {
        initPlano();

        $("#cadastrar-cpf-cartao").mask('000.000.000-00', {
            reverse: true
        });
        $("#cadastrar-cep").mask('00000-000', {
            reverse: true
        });
        $('#pagamento-cupom').mask("AAAA-SSSS-AAAA-SS00");

        $("#pagamento-plano-select").change(function() {
            /* reset cupom ao mudar de plano */
            HAS_CUPOM = false;
            $("#pagamento-cupom").val("");
            $("#pagamento-cupom").prop("disabled", false);
            $("#cupom-form button").prop("disabled", false);
            $("#pagamento-cupom").removeClass("is-valid");
            $("#pagamento-cupom").removeClass("is-invalid");
            /* /reset cupom ao mudar de plano */

            selected = $("#pagamento-plano-select option:selected").val();
            var plano = getPlanoById(parseInt(selected));
            mostraPlano(plano);
        });

        $("#cupom-form").submit(
            function(e) {
                e.preventDefault();
                e.stopPropagation();

                var numero = $("#pagamento-cupom").val().replace(/-/g, "");
                if (numero == "") {
                    $("#pagamento-cupom").addClass("is-invalid");
                }

                var plano = $("#pagamento-plano-select").val();

                console.log("link cupom", $(this).attr('action') + '/' + numero + '/' + plano);
                $.get($(this).attr('action') + '/' + numero + '/' + plano, function(data) {
                        //console.log(data);
                        if (data.success) {
                            setPlanoByCupom(data.cupom);
                            //console.log(data.cupom);
                            CUPOM = data.cupom;
                            $("#pagamento-cupom").prop("disabled", true);
                            //$("#pagamento-cupom-fb-valid").show();
                            $("#cupom-form button").prop("disabled", true);
                            $("#pagamento-cupom").addClass("is-valid");
                        } else {
                            $("#pagamento-cupom-fb").html(data.message);
                            $("#pagamento-cupom").addClass("is-invalid");
                        }
                    })
                    .fail(function() {
                        $("#pagamento-cupom-fb").html("Validação falhou! Por favor, tente mais tarde.");
                        $("#pagamento-cupom").addClass("is-invalid");
                        //console.log("cupom falhou!!");
                    });
            }
        );

        $("#cartao-form").submit(
            function(e) {
                e.preventDefault();
                e.stopPropagation();
            }
        );

        $("#dados-cartao-form").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var estado = $("#cadastrar-estado");
            var cidade = $("#cadastrar-cidade");
            var endereco = $("#cadastrar-endereco");
            var numero = $("#cadastrar-numero");
            var bairro = $("#cadastrar-bairro");
            var cep = $("#cadastrar-cep");
            var email_cartao = $("#cadastrar-email-cartao");
            var cpf_cartao = $("#cadastrar-cpf-cartao");

            var cancel = false;
            var input = null;

            if (bairro.val() == "") {
                cancel = true;
                input = bairro;
            }

            if (cidade.val() == "" || !cidade.val()) {
                cancel = true;
                input = cidade;
            }

            if (estado.val() == "") {
                cancel = true;
                input = estado;
            }

            if (numero.val() == "") {
                cancel = true;
                input = numero;
            }

            if (endereco.val() == "") {
                cancel = true;
                input = endereco;
            }

            if (!isEmail(email_cartao.val())) {
                cancel = true;
                input = email_cartao;
            }

            if (!isValidCPF(cpf_cartao.val())) {
                cancel = true;
                input = cpf_cartao;
            }

            var formSerialized = $(this).serializeArray();

            if (cancel) {
                input.addClass("is-invalid");
                input.focus();
            } else {
                $.post($(this).attr('action'), formSerialized, function(data) {
                        console.log("data: " + data);
                        if (data.success) {
                            usuario = data.user;
                            $("#modal-dados-cartao").modal("hide");
                            assinar();
                        } else {
                            $("#warning-text").html(
                                "Atualização dos dados falhou! Por favor, tente mais tarde.");
                            $("#warning-box").fadeIn();
                            console.log("cadastro falhou!!");
                        }
                    })
                    .fail(function() {
                        $("#warning-text").html(
                            "Atualização dos dados falhou! Por favor, tente mais tarde.");
                        $("#warning-box").fadeIn();
                        console.log("cadastro falhou!!")
                    });
            }

        });

        $("#cadastrar-estado").change(function() {
            //console.log($("select option:selected").val());
            $.post("{{ url::to('planos/cidades') }}", {
                "estado": $("select option:selected").val()
            }, function(data) {
                //console.log(data);
                if (data.success) {
                    loadCidades(data.cidades);
                    if (usuario.estado) $(`#cadastrar-cidade`).val(usuario.cidade).change();
                    //console.log(data.cidades);
                } else {
                    console.log('cidades n encontradas!');
                }
            }).fail(function() {
                console.log('cidades falhou!');
            });
        });

        $("#dados-cartao-form-btn").click(function() {
            $("#dados-cartao-form").submit();
        });

        $('input').on('input', function() {
            $(this).removeClass('is-invalid');
            $(this).removeClass('is-valid');
        });

        var button = document.querySelector('#pay-button');
        button.addEventListener('click', assinar);

        hasDesconto(usuario.email);

    });

    function initPlano() {
        var plano_escolhido = $("#pagamento-plano-select").data("plano");
        var plano = getPlanoById(parseInt(plano_escolhido));
        mostraPlano(plano);
    }

    function setPlanoByCupom(cupom) {
        HAS_CUPOM = true;
        PLANO_ID = cupom.idPlano;
        var plano = getPlanoById(cupom.idPlano);
        PLANO_NOME = plano.nome;
        var valor_inicial = plano.valor;
        /* type: 0 - valor, 1 - % */
        if (cupom.tipo_desconto) {
            PLANO_VALOR = valor_inicial - (valor_inicial * cupom.desconto / 100);
        } else {
            PLANO_VALOR = valor_inicial - cupom.desconto;
        }

        plano['valor'] = PLANO_VALOR;
        console.log("PLANO_VALOR pos-cupom:" + PLANO_VALOR);
        mostraPlano(plano);
    }

    function getPlanoById(selected) {
        var plano = [];
        switch (selected) {
            case 0:
                plano["val"] = 0;
                plano["nome"] = "EXTENSIVO MED";
                plano["duracao"] = "10";
                plano["valor"] = 19000;
                break;
            case 1:
                plano["val"] = 1;
                plano["nome"] = "EXTENSIVO ENEM";
                plano["duracao"] = "10";
                plano["valor"] = 16000;
                break;
            case 3:
                plano["val"] = 3;
                plano["nome"] = "INTENSIVÃO ENEM";
                plano["duracao"] = "4";
                plano["valor"] = 25000;
                break;
            case 5:
                plano["val"] = 5;
                plano["nome"] = "CURSO COMPLETO";
                plano["duracao"] = "12";
                plano["valor"] = 26280;
                break;
            case 6:
                plano["val"] = 6;
                plano["nome"] = "CURSO TOTAL";
                plano["duracao"] = "12";
                plano["valor"] = 4370;
                break;
        }
        //console.log(plano.nome);
        return plano;
    }

    function mostraPlano(plano) {
        let timeConst = " meses";
        let plano_nome = "<strong>" + plano.nome.toLowerCase() + "</strong>";
        let plano_valor = convertToCurrency(plano.valor / 100);
        let today = new Date();
        let target_day = new Date("{{ config('constants.outras.deadline_aprova_mais') }}");
        console.log("target day", target_day);

        // if (plano.val == 3)
        //     timeConst = " dias";

        // plano CURSO COMPLETO
        if (plano.val == 5 && today <= target_day) {
            plano_nome +=
                "<br>(oferta disponível até {{ date('d/m', strtotime(config('constants.outras.deadline_aprova_mais'))) }})";
            plano_valor += " + <s>R$ 43,20</s> = R$ " + plano_valor;
            // plano_valor += " + R$ 43,20 = <s>R$357,60</s> R$ 374,88";
        }

        $("#pay-button-name").html("continuar");
        $("#plano-renew").html("");
        PLANOS.forEach(plano_el => {
            if (plano_el.plano == plano.nome) {
                let plano_fim = new Date(plano_el.fim);
                let numberOfDaysToAdd = 365;
                let someDate = new Date();
                let result = someDate.setDate(plano_fim.getDate() + numberOfDaysToAdd);
                let nova_data = new Date(new Date(plano_el.fim).getTime() + 365 * 24 * 60 * 60 * 1000);
                $("#plano-renew").html(
                    `<i class="fa fa-star" style="color:#3eb378" aria-hidden="true"></i> Plano já contratado. Expira em <strong>${nova_data.getDate() < 10 ? '0' + nova_data.getDate() : nova_data.getDate()}/${plano_fim.getUTCMonth() < 10 ? '0' + plano_fim.getUTCMonth() : plano_fim.getUTCMonth() }/${plano_fim.getUTCFullYear()}</strong><br><i class="fa fa-star" style="color:#e6ce23" aria-hidden="true"></i>  <strong>Renove</strong> o plano e tenha <strong>acesso</strong> até <strong>${nova_data.getDate() < 10 ? '0' + nova_data.getDate() : nova_data.getDate()}/${nova_data.getUTCMonth() < 10 ? '0' + nova_data.getUTCMonth() : nova_data.getUTCMonth() }/${nova_data.getUTCFullYear()}</strong>`
                );
                $("#pay-button-name").html("renovar");
            }
        });

        $("#plano-type").html("plano escolhido<hr class='my-2'>" + plano_nome);
        $("#plano-duration").html("duração:<br><strong>" + plano.duracao + timeConst + "</strong>");
        $("#plano-price").html("valor<hr class='my-2'><strong>" + plano_valor + "</strong>");

        // if (plano.val == 3)
        //     $("#plano-price").html("valor:<br><strong>" + convertToCurrency(plano.valor/100) + "</strong>");
        // else
        //     $("#plano-price").html("valor:<br><strong>" + convertToCurrency(plano.valor/100) + "</strong>");
    }

    function convertToCurrency(value) {
        return value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
            minimumFractionDigits: 2
        })
    }

    function cleanPhone(value) {
        return value.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
    }

    function cleanCEP(value) {
        return value.trim().replace("-", "");
    }

    function mostrarDiv(valor) {
        if (valor == 'boleto') {
            $('#pag_cartao').css('display', 'none');
            $('#pag_boleto').css('display', 'block');
        }
        if (valor == 'cartao') {
            $('#pag_cartao').css('display', 'block');
            $('#pag_boleto').css('display', 'none');
        }
    }

    function aguard() {
        // console.log("aguardando...");
    }

    function error() {
        $('#autorizadoModal .modal-title').html("Pagamento falhou!");
        $('#autorizadoModal .modal-body').html("Por favor, tente mais tarde.");
        $('#autorizadoModal').modal('show');
    }

    function isEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function isValidCPF(cpf) {
        if (typeof cpf !== "string") return false
        cpf = cpf.replace(/[\s.-]*/igm, '')
        if (
            !cpf ||
            cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999"
        ) {
            return false
        }
        var soma = 0
        var resto
        for (var i = 1; i <= 9; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i)
        resto = (soma * 10) % 11
        if ((resto == 10) || (resto == 11)) resto = 0
        if (resto != parseInt(cpf.substring(9, 10))) return false
        soma = 0
        for (var i = 1; i <= 10; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i)
        resto = (soma * 10) % 11
        if ((resto == 10) || (resto == 11)) resto = 0
        if (resto != parseInt(cpf.substring(10, 11))) return false
        return true
    }

    function loadEstados(estados) {
        $.each(estados, function(index, item) {
            var option = document.createElement("option");
            option.text = item.sigla;
            option.value = item.cod_estados;
            $("#cadastrar-estado").append(option);
        });
        $("#cadastrar-estado").prop('disabled', false);
    }

    function loadCidades(cidades) {
        $("#cadastrar-cidade").empty();
        $("#cadastrar-cidade").append('<option selected>Selecione a cidade</option>');
        $.each(cidades, function(index, item) {
            //console.log("texto: " + item.sigla);
            //console.log("cod: " + item.cod_estados);
            var option = document.createElement("option");
            option.setAttribute('data-cep', item.cep);
            option.text = item.nome;
            option.value = item.cod_cidades;
            $("#cadastrar-cidade").append(option);
        });
        $("#cadastrar-cidade").prop('disabled', false);
    }

    function assinar() {

        // console.log("Pagar", usuario);

        if (
            !usuario.email_cartao ||
            !usuario.cpf_cartao ||
            !usuario.estado ||
            !usuario.cidade ||
            !usuario.bairro ||
            !usuario.endereco ||
            !usuario.numero
        ) {
            loadEstados(estados);
            console.log('estado', usuario.estado);
            console.log('cidade', usuario.cidade);
            $("#modal-dados-cartao").modal("show");
            if (usuario.estado) $(`#cadastrar-estado`).val(usuario.estado).change();
            return;
        }

        // verifica qual plano foi selecionado
        var plano = $("#pagamento-plano-select").val();

        if (HAS_CUPOM) {
            setPlanoByCupom(CUPOM);
        } else if (plano == 0) {
            PLANO_NOME = 'EXTENSIVO MED';
            PLANO_ID = 0;
            PLANO_VALOR = 19000;
        } else if (plano == 1) {
            PLANO_NOME = 'EXTENSIVO ENEM';
            PLANO_ID = 1;
            PLANO_VALOR = 16000;
        } else if (plano == 3) {
            PLANO_NOME = 'INTENSIVÃO ENEM';
            PLANO_ID = 3;
            PLANO_VALOR = 25000;
        } else if (plano == 5) {
            PLANO_NOME = 'CURSO COMPLETO';
            PLANO_ID = 5;
            PLANO_VALOR = 26280;
        } else if (plano == 6) {
            // 0 - extensivo med
            PLANO_NOME = 'APROVA +';
            PLANO_ID = 6;
            PLANO_VALOR = 4320;
        }

        var checkout = new PagarMeCheckout.Checkout({
            encryption_key: "{{ env('PAGARME_CRYPT_KEY') }}",
            success: function(data) {
                console.log("checkout data", data);
                if (HAS_CUPOM) data.cupom = CUPOM;
                // adicionar sinalizador do cupom.
                data.plano_valor = PLANO_VALOR;
                console.log("pre post checkout", PAGARME_URL);
                console.log("data pre post checkout", data);
                $("#loader-wrapper").css("display", "block");
                $.post(PAGARME_URL, data, function(data) {
                        console.log("data result post pos checkout", data);
                        $("#loader-wrapper").css("display", "none");
                        if (data.success) {
                            if (data.status == 'paid') {
                                window.location.href = "{{ url::to('conta/curso') }}";
                            } else if (data.status == 'waiting_payment') {
                                window.location.href = "{{ url::to('pagamento/boleto/') }}/" + data
                                    .boleto.id;
                            } else if (data.status == 'authorized') {
                                $('#autorizadoModal modal-title').html("Pagamento Pendente");
                                $('#autorizadoModal modal-body').html(
                                    "O pagamento foi reservado e está pendente. Notificaremos por e-mail assim que recebermos a confirmação."
                                );
                                $('#autorizadoModal').modal('show');
                            }
                        } else {
                            $("#loader-wrapper").css("display", "none");
                            error();
                            alert("Transação falhou!");
                        }
                    })
                    .fail(function() {
                        $("#loader-wrapper").css("display", "none");
                        error();
                        console.log("post pagamento falhou!!");
                    });
            },
            error: function(err) {
                $("#loader-wrapper").css("display", "none");
                console.log(err);
                error();
            },
            close: function() {
                $("#loader-wrapper").css("display", "none");
                //alert("on pay modal colsed ");
                //console.log('The modal has been closed.');
            }
        });

        var phonenumber = cleanPhone("+55{{ $user->telefone }}");
        var cep = cleanCEP(usuario.cep);

        checkout.open({
            amount: PLANO_VALOR,
            // headerText: "É importante utilizar o mesmo e-mail que está logado na conta. Total: {price_info}",
            customerData: 'false',
            createToken: 'true',
            customer_editable: 'true',
            paymentMethods: 'boleto,credit_card',
            boletoDiscountPercentage: 0,
            postbackUrl: "{{ url::to('/postback') }}",
            maxInstallments: 12,
            defaultInstallments: 1,
            freeInstallments: 12,
            customer: {
                external_id: usuario.id,
                name: usuario.nome,
                type: 'individual',
                country: 'br',
                email: usuario.email_cartao,
                documents: [{
                    type: 'cpf',
                    number: usuario.cpf_cartao,
                }, ],
                phone_numbers: [phonenumber],
            },
            billing: {
                name: 'Ciclano de Tal',
                address: {
                    country: 'br',
                    state: usuario.estado,
                    city: usuario.cidade,
                    neighborhood: usuario.bairro,
                    street: usuario.endereco,
                    street_number: usuario.numero,
                    zipcode: cep
                }
            },
            items: [{
                id: PLANO_ID,
                title: PLANO_NOME,
                unit_price: PLANO_VALOR,
                quantity: 1,
                tangible: 'false'
            }],
        });
    }

    function hasDesconto(email) {
        if (parseInt("{{ isset($link_desconto) ? $link_desconto : 0 }}")) {
            console.log("é um link desconto");
            $("#pagamento-cupom").val("ESCO-LAPU-BLIC-AA22");
            $("#cupom-btn").click();
            $("#cupom-form").hide();
            $("#desconto").html(
                "<span class='mx-auto'><i class='fas fa-gifts'></i> Você ganhou um desconto de 50%</span>");
            $("#desconto").show();
            return;
        }
        let action = "/has-desconto";
        let data = {
            email: email
        };
        $.post(action, data, function(data) {
                // console.log("hasDesconto: " + data);
                if (data.success) {
                    // console.log("resposta desconto", data.resposta);
                    if (data.resposta) {
                        $("#pagamento-cupom").val("ESCO-LAPU-BLIC-AA22");
                        $("#cupom-btn").click();
                        $("#cupom-form").hide();
                        $("#desconto").html(
                            "<span class='mx-auto'><i class='fas fa-gifts'></i> Você ganhou um desconto de 50%</span>"
                        );
                        $("#desconto").show();
                    }
                } else {
                    console.log(data.msg);
                }
            })
            .fail(function() {
                console.log("verificação de desconto falhou!");
            });
    }
</script>
