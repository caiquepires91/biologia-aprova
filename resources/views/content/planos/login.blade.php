@php
$now = new DateTime('now');
// now 2:15 ahead local
$dueTime = new DateTime('2021-10-06 23:59');
@endphp

<style>
    #loader-wrapper {
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: #222222e3;
        z-index: 1021;
        display: flex;
    }

    #loader {
        z-index: 1022;
        /* anything higher than z-index: 1000 of .loader-section */
        margin: auto;
        position: absolute;
        top: 50%;
        right: 44%;
    }

    #loader h1 {
        font-size: xxx-large;
        font-weight: 700;
        color: #EEEEEE;
    }

    #cartao-img {
        width: 60px !important;
        margin: 12px auto !important;
    }

    .plano-title p {
        font-size: 20px;
        text-align: center;
        margin: 0;
    }
</style>

<div id="loader-wrapper">
    <div id="loader">
        <h1>aguarde...</h1>
    </div>
    <div class="loader-section">
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="recuperarModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="recuperarModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="recuperarModalLabel" style="font-family: 'Lufga';font-weight: 700;">
                    Recuperar Senha</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="recuperar-senha" name="recuperar_senha_email" action="{{ url::to('/recuperar-senha') }}">
                <div class="modal-body">
                    <div class="m-3 has-validation position-relative">
                        <label for="email-recuperar" class="form-label">e-mail:</label>
                        <input type="email" name="email" class="form-control" id="email-recuperar"
                            aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text">um link será enviado para esse endreço de e-mail.</div>
                        <div id="email-recuperar-fb" class="invalid-tooltip">
                            Por favor, digite um e-mail válido.
                        </div>
                    </div>
                    <div id="recuperar-warning-box" class="m-3" style="display: block;">
                        <p class="recuperar-warning-text"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" style="font-weight: 600;">recuperar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    .info-box .content {
        width: 480px;
    }

    @media only screen and (max-width: 600px) {

        /* For phones: */
        .info-box .content {
            width: 100%;
        }
    }
</style>

<div id="planos-login-section-1" class="row">
    <div class="col"
        style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">checkout</h2>
    </div>
</div>

{{-- @if ($now <= $dueTime) --}}
@if (true)
    <div class="container">
        <div class="row">
            <p class="mt-5 mb-0 text-center"><span class="plano-title" style="font-size:1.6rem">CURSO COMPLETO +
                    BÔNUS<br></span> <span style="margin-bottom: .6rem;color: black;font-size: 28px;line-height: 38px;">
                    módulo bônus para inscrições até
                    {{ date('d/m', strtotime(config('constants.outras.deadline_aprova_mais'))) }} </span></p>
            <div class="container wavy-line mb-5 mt-2"><img src="{{ url::to('img/long-wavy-line.png') }}"
                    alt="biologia aprova - linha ondulada"></div>
        </div>
    </div>
@endif

<div class="container">
    <div id="cadastro-box" class="row justify-content-center user-box" style="display: none;">
        <div class="col-md-8 col-12 pe-md-5">
            <form id="cadastro-form" action="{{ url::to('planos/cadastrar') }}" name="form_cadastro" class="p-0 m-0"
                novalidate>
                <div id="cadastro-processo" class="block row justify-content-center w-md-100" style="max-width:unset">
                    <div class="col-md-12 p-0">
                        <p>dados cadastrais</p>
                    </div>
                    <div class="row p-3">
                        <div class="col-md-12">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-nome" class="form-label">nome completo*:</label>
                                <input type="text" class="form-control" id="cadastrar-nome" name="nome"
                                    aria-describedby="cadastrar-nome-fb">
                                <div id="cadastrar-nome-fb" class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-cpf" class="form-label">CPF*:</label>
                                <input type="text" class="form-control" id="cadastrar-cpf" name="cpf"
                                    aria-describedby="cadastrar-cpf-fb">
                                <div id="cadastrar-cpf-fb" class="invalid-tooltip">
                                    Por favor, digite um CPF válido.
                                </div>
                            </div>
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-telefone" class="form-label">telefone*:</label>
                                <input type="text" class="form-control" id="cadastrar-telefone" name="telefone"
                                    aria-describedby="cadastrar-telefone-fb">
                                <div id="cadastrar-telefone-fb" class="invalid-tooltip ">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-md-3">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-email" class="form-label">e-mail*:</label>
                                <input type="email" class="form-control" id="cadastrar-email"
                                    autocomplete="username" name="email" aria-describedby="cadastrar-email-fb">
                                <div id="cadastrar-email-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                        </div>
                        <div class="row mb-md-3">
                            <div class="col-md-6">
                                <div class="mb-3 has-validation position-relative">
                                    <label for="cadastrar-pass" class="form-label">senha*:</label>
                                    <input type="password" class="form-control" id="cadastrar-pass"
                                        autocomplete="new-password" name="senha"
                                        aria-describedby="cadastrar-pass-fb">
                                    <div id="cadastrar-pass-fb" class="invalid-tooltip">
                                        Esse campo é obrigatório.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3 has-validation position-relative">
                                    <label for="cadastrar-confirmar-pass" class="form-label">confirmar senha*:</label>
                                    <input type="password" class="form-control" id="cadastrar-confirmar-pass"
                                        name="confirmaSenha" autocomplete="new-password"
                                        aria-describedby="cadastrar-confirmar-pass-fb">
                                    <div id="cadastrar-confirmar-pass-fb" class="invalid-tooltip">
                                        As senhas não combinam.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span style="font-size: 25px;font-weight: 700;">dados do titular do cartão</span>
                        <div class="w-100 mb-md-2"></div>
                        <div class="col-md-6">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-cpf-cartao" class="form-label">CPF*:</label>
                                <input type="text" class="form-control" id="cadastrar-cpf-cartao"
                                    name="cpf-cartao" aria-describedby="cadastrar-cpf-cartao-fb">
                                <div id="cadastrar-cpf-cartao-fb" class="invalid-tooltip ">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-email" class="form-label">e-mail*:</label>
                                <input type="email" class="form-control" id="cadastrar-email-cartao"
                                    autocomplete="username" name="email-cartao"
                                    aria-describedby="cadastrar-email-cartao-fb">
                                <div id="cadastrar-email-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-endereco" class="form-label">endereço*:</label>
                                <input type="text" class="form-control" id="cadastrar-endereco"
                                    autocomplete="street" name="endereco" aria-describedby="cadastrar-endereco-fb">
                                <div id="cadastrar-endereco-fb" class="invalid-tooltip">
                                    Por favor, digite um endereço válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-numero" class="form-label">num.*:</label>
                                <input type="text" class="form-control" id="cadastrar-numero"
                                    autocomplete="street_number" name="numero"
                                    aria-describedby="cadastrar-numero-fb">
                                <div id="cadastrar-numero-fb" class="invalid-tooltip">
                                    Por favor, digite um número válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-md-5">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-estado" class="form-label">estado*:</label>
                                <select type="text" class="form-select" id="cadastrar-estado" name="estado"
                                    aria-describedby="cadastrar-estado-fb"></select>
                                <div id="cadastrar-estado-fb" class="invalid-tooltip ">
                                    Esse campo é obrigatório.
                                </div>
                            </div>

                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-bairro" class="form-label">bairro*:</label>
                                <input type="text" class="form-control" id="cadastrar-bairro"
                                    autocomplete="neighborhood" name="bairro"
                                    aria-describedby="cadastrar-bairro-fb">
                                <div id="cadastrar-bairro-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-md-5">
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-cidade" class="form-label">cidade*:</label>
                                <select type="text" class="form-select" id="cadastrar-cidade"
                                    autocomplete="neighborhood" name="cidade"
                                    aria-describedby="cadastrar-cidade-fb"></select>
                                <div id="cadastrar-cidade-fb" class="invalid-tooltip">
                                    Por favor, digite um e-mail válido.
                                </div>
                            </div>
                            <div class="mb-3 has-validation position-relative">
                                <label for="cadastrar-cep" class="form-label">CEP*:</label>
                                <input type="text" class="form-control" id="cadastrar-cep" name="cep"
                                    aria-describedby="cadastrar-cep-fb">
                                <div id="cadastrar-cep-fb" class="invalid-tooltip">
                                    Esse campo é obrigatório.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 align-self-center mb-4">
                            <p class="normal-text">Para realizar o cadastro é necessário preencher todos os campos com
                                asterísco (*) e concordar com os termos de utilização.</p>
                            <div class="position-relative">
                                <input class="form-check-input" type="checkbox" value=""
                                    id="cadastrar-checkbox" aria-describedby="cadastrar-check-fb">
                                <label class="form-check-label" for="cadastrar-check" style="line-height: 30px;">
                                    Li e aceito os <a href="{{ url::to('termo-uso') }}" target="_blank">termos de
                                        utilização</a>.
                                </label>
                                <div id="cadastrar-check-fb" class="invalid-tooltip">
                                    Leia os termos de utilização antes de continuar.
                                </div>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div id="warning-box" class="col-12 align-self-center mb-5" style="display: none;">
                            <p class="warning-text" style="color:red">Endereço de e-mail já possui cadastro! Clique <a
                                    href="{{ url::to('planos/login-page') }}">aqui</a> para fazer login.</p>
                        </div>
                    </div>
                </div>
                <div id="cadastro-sucesso" class="block row justify-content-center" style="display: none;">
                    <div class="col-md-12 mb-5 p-0 ">
                        <p>dados cadastrais</p>
                    </div>
                    <div class="col-md-12 mb-5 ">
                        <p class="cadastro-sucesso-titulo">cadastro realizado!</p>
                        <p class="cadastro-sucesso-subtitulo" style="font-size: x-large;">clique em
                            <strong>continuar</strong> para fazer login.
                        </p>
                    </div>
                    <div class="col-md-4 mb-5">
                        <button type="button position-relative" class="btn btn-primary">
                            <div class="row">
                                <div class="col-6" style="text-align: left;">continuar</div>
                                <div class="col-6" style="text-align: right;"><i
                                        class="fas fa-long-arrow-alt-right"></i></div>
                            </div>
                            <a class="stretched-link" href="{{ url::to('planos/login-page') }}"></a>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="content col-md-4">
            <div class="block">
                <p class="mb-0">formas de pagamento</p>
                <div class="mb-md-3 d-md-block d-none">
                    <div id="valor-box" class="container m-auto plano-title"
                        style="display: none;color: rgb(255 255 255);padding: 0px;">
                        <div
                            style="margin: 0px -16px;padding:unset;background: #3eb378;box-shadow: rgb(0 0 0 / 15%) 1.95px 1.95px 7px 2px;">
                            <span
                                style="font-size:15px;height:unset; line-height:unset; background:unset; color: #f9fafbed; margin-left: 138px;">apenas</span>
                            <p id="texto-valor"
                                style="font-size:60px; margin:0px;margin-bottom: 0px;text-shadow: -3px 2px 3px #06060687;">
                                <span style="font-size:20px">R$</span>262<span style="font-size:20px">,80</span>
                            </p>
                            <p style="font-size:15px;color: #f9fafbed; height:auto; line-height:unset;">(em até 12x sem
                                juros)</p>
                        </div>
                        {{-- <p class="mb-0 text-center"><span class="plano-title" style="font-size:40px;color: #084298;border: solid 1px #00255417;text-shadow: -3px 2px 3px #06060687;padding: .2rem 2rem;"><span style="font-size:20px">Apenas</span> R$262,80 <span  style="font-size:20px">(em até 12x sem juros)</span></p> --}}
                    </div>
                </div>
                <div class="d-flex p-3" style="flex-direction: column;">
                    <div class="mb-3">
                        <div class="position-relative">
                            <div class="row justify-content-center" style="padding: 10px 20px;">
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/visa.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/master.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/american.jpg') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/aura.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img"
                                        src="{{ url::to('/img/pagamento/cartoes/banescard.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/cabal.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/credz.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/diners.jpg') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/discover.jpg') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/elo.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/jcb.jpg') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/sorocred.jpg') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/cartoes/hiper.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                                <div class="col-3">
                                    <img id="cartao-img" src="{{ url::to('/img/pagamento/boleto.png') }}"
                                        alt="biologia aprova - cartão de crédito" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <p class="normal-text" style="text-align: justify !important;font-size: 12px !important;">No
                            caso de pagamento por boleto, você poderá visualizá-lo ou imprimi-lo após a finalização da
                            compra. A data de vencimento é de 2 dias corridos após a conclusão da compra. Após essa
                            data, o boleto perderá a valdiade.</p>
                    </div>
                    <div id="section-cupom">
                        <div id="desconto"></div>
                        <form id="cupom-form" action="{{ url::to('pagamento/cupom') }}"
                            style="padding: unset; min-height: unset; margin: unset;">
                            <div class="mb-1 has-validation position-relative">
                                <label for="pagamento-cupom" class="form-label">tenho um cupom:</label>
                                <input type="text" class="form-control" id="pagamento-cupom"
                                    placeholder="xxxx-xxxx-xxxx-xxxx">
                                <div id="pagamento-cupom-fb" class="invalid-tooltip"
                                    aria-describedby="pagamento-cupom-fb">
                                    cupom inválido.
                                </div>
                                <div id="pagamento-cupom-fb-valid" class="valid-tooltip"
                                    aria-describedby="pagamento-cupom-fb-valid">
                                    cupom válido!
                                </div>
                                <small class="f-1">Informe o número do cupom exatamente como ele está
                                    escrito.</small>
                            </div>
                            <button id="cupom-btn" type="submit" class="btn btn-primary" style="margin-top: 1rem;">
                                <div class="row">
                                    <div class="col-6" style="text-align: left;">validar cupom</div>
                                    <div class="col-6" style="text-align: right;"><i style="margin-top: 3px;"
                                            class="fas fa-ticket-alt"></i></div>
                                </div>
                            </button>
                        </form>
                    </div>
                    <div class="mt-3">
                        <p
                            style="margin: 0px -16px;border-radius: unset;background-color: #ccc;color: #545353;font-size:large">
                            <i class="fas fa-user-shield"></i> COMPRA 100% SEGURA
                        </p>
                    </div>
                    <div>
                        <!-- <button id="pay-button" class="btn btn-primary" data-url="{{ url::to('pagarme') }}"  style="background-color: #3eb378; margin-top:auto">
                                <div class="row">
                                <div class="col-6"  style="text-align: left;">continuar</div>
                                <div class="col-6" style="text-align: right;"><i class="fas fa-long-arrow-alt-right"></i></div>
                                </div>
                            </button> -->
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <button id="btn-assinar" class="btn btn-primary block" style="height: 60px;min-height: unset;">
                    <div class="row justify-content-center">
                        <span class="m-auto fs-3">pagar</span>
                    </div>
                </button>
                <button id="pay-button" class="d-none" data-url="{{ url::to('pagarme') }}"
                    data-redirect="{{ url::to('conta/curso') }}"></button>
            </div>
        </div>
    </div>
</div>

<!-- <div class="container">
    <div id="video-box" class="row">
        <div class="col" style=" display: flex!important;flex-direction: column;">
            <p class="plano-title mt-5 text-center">Gabaritando Biologia - Intensivão Enem</p>
            <div class="container wavy-line mb-4 mt-1"><img src="{{ url::to('img/long-wavy-line.png') }}"  alt="biologia aprova - linha ondulada"></div>
            <div style="position:relative;padding-bottom:56.25%;">
                <iframe style="width: 80%;height: 80%;position: absolute;left: 50%;top: 0;transform: translateX(-50%);" class="m-auto" width="100%" height="100%" src="https://www.youtube.com/embed/OXqpTWaeORE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <hr style="margin-top: 3.5rem;">
        </div>
    </div>
</div> -->

@if ($now <= $dueTime)
    <div class="container">
        <div id="video-box" class="row">
            <div class="col" style=" display: flex!important;flex-direction: column;">
                <!-- <p class="mt-5 mb-0 text-center"><span class="plano-title" style="font-size:1.6rem">Gabaritando Biologia -</span> <span style="margin-bottom: .6rem;color: black;font-size: 28px;line-height: 38px;"> Intensivão Enem </span></p>
            <div class="container wavy-line mb-5 mt-2"><img src="{{ url::to('img/long-wavy-line.png') }}"  alt="biologia aprova - linha ondulada"></div> -->
                <div class="d-none d-md-flex">
                    <iframe class="m-auto"width="860" height="555"
                        src="https://www.youtube.com/embed/OXqpTWaeORE" title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                    <hr style="margin-top: 3.5rem;">
                </div>
                <div>
                    <iframe class="m-auto d-md-none" width="100%" height="250"
                        src="https://www.youtube.com/embed/OXqpTWaeORE" title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                    <p class="mt-5 mb-0 text-center"><span class="plano-title"
                            style="font-size:40px;color:#0aab6c"><span style="font-size:20px">Apenas</span> R$250,00
                            <span style="font-size:20px">(em até 12x sem juros)</span></p>
                    <div class="aprovado-btn row justify-content-center mt-md-3 mb-md-3">
                        <div class="col d-flex">
                            <a class="btn blue-btn rounded-pill m-auto" type="button" href="#login-box"
                                style="width: 200px;font-size: 25px;">assine já!</a>
                        </div>
                    </div>
                    <hr style="margin-top: 3.5rem;">
                </div>
            </div>
        </div>
    </div>

    <!-- INFO BOX -->
    <div class="row" id="info-box">
        <div class="col-md-6 p-md-5 pb-md-0">
            <div class="content ms-md-auto">
                <p class="plano-title" style="text-align: left;">Todo o conteúdo que você precisa saber para gabaritar
                    biologia estará disponível para você.</p>
                <div class="wavy-line wavy-line-login mt-3 mb-3"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
                <div class="plano-vantagens-login mb-4 mx-4 mx-md-0">
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Matéria organizada de forma sequencial,
                        lógica;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Metodologia didática viva, divertida e
                        exemplificativa para você assimilar o conteúdo;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Plantões tira dúvidas ao vivo para se
                        certificar que você aprendeu;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Material de auxílio para revisão rápida e
                        eficiente;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Paródias que vão grudar biologia na sua
                        cabeça;</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 p-md-5 pb-md-0">
            <div class="content me-auto">
                <p class="plano-title" style="text-align: left;">Como e quando o curso será entregue:</p>
                <div class="wavy-line wavy-line-login mt-3 mb-3"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
                <div class="plano-vantagens-login mb-4 mx-4 mx-md-0">
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>7 aulas AO VIVO de teoria + resolução de
                        questões, sendo elas às quintas-feiras de 07/10 a 11/11 de 19h a 21h20min e uma aula extra no
                        dia 16/10 de 14h a 16h30min;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Gravadas integral das aulas
                        disponibilizada na plataforma do Biologia Aprova em até 2 dias úteis;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Resumos, mapas mentais e paródias
                        disponibilizados na plataforma do Biologia Aprova semanalmente, de acordo com a matéria
                        abordada;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Plantões tira dúvidas às segundas-feiras
                        de 11/10 a 08/11 de 21h as 22h; </p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Plantão bônus surpresa – em data provável
                        de 15/11 de 21h as 22h;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Acesso à plataforma até 31/01/2022.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col">
                <hr style="margin-top: 2rem; margin-left: 1rem; margin-right: 1rem;">
            </div>
        </div>
    </div>
@endif

<!-- LOGIN BOXES -->
<div id="login-box" class="row justify-content-center user-box">

    <div class="col-md-6 p-md-5 content">
        {{-- <div class="block" style="{{ $now > $dueTime ? 'margin:auto' : 'margin-left:auto'}} "> --}}
        <div class="block" style="margin-left:auto">
            <p>já sou cadastrado</p>
            <form id="login-form" action="{{ url::to('planos/login') }}" name="form_login">
                <div class="mb-3 position-relative">
                    <label for="login-email" class="form-label">digite seu e-mail*:</label>
                    <input type="email" name="email" class="form-control" autocomplete="username"
                        id="login-email" aria-describedby="login-email-fb">
                    <div id="login-email-fb" class="invalid-tooltip">
                        Por favor, digite um e-mail válido.
                    </div>
                </div>
                <div class="mb-3 position-relative">
                    <label for="login-pass" class="form-label">digite sua senha*:</label>
                    <input type="password" name="password" autocomplete="current-password" class="form-control"
                        id="login-pass" aria-describedby="login-pass-fb">
                    <div id="login-pass-fb" class="invalid-tooltip">
                        E-mail ou senha não conferem.
                    </div>
                </div>
                <div class="mb-3" style="display: flex;">
                    <a href="javascript:recuperarModal();"><span><i class="fas fa-key"></i> esqueci minha
                            senha</span></a>
                </div>
                <button type="submit" class="btn btn-primary">
                    <div class="row">
                        <div class="col-6" style="text-align: left;">entrar</div>
                        <div class="col-6" style="text-align: right;"><i class="fas fa-long-arrow-alt-right"></i>
                        </div>
                    </div>
                </button>
            </form>
        </div>
    </div>

    {{-- @if ($now <= $dueTime) --}}
    @if (true)
        <div class="col-md-6 p-md-5 content">
            <div id="signup-block" class="block">
                <p>sou novo aqui</p>
                <form id="signup-from" action="{{ url::to('planos/cadastrar-page') }}" name="form_go_to_cadastro"
                    novalidate>
                    <div class="mb-3 position-relative">
                        <label for="signup-email" class="form-label">digite seu e-mail*:</label>
                        <input type="email" name="email" class="form-control" id="signup-email"
                            autocomplete="username" required>
                        <div id="signup-email-fb" class="invalid-tooltip">
                            Por favor, digite um e-mail válido.
                        </div>
                    </div>
                    <div style="margin-bottom: 8.85rem;">
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <div class="row">
                            <div class="col-6" style="text-align: left;">continuar</div>
                            <div class="col-6" style="text-align: right;"><i
                                    class="fas fa-long-arrow-alt-right"></i>
                            </div>
                    </button>
                </form>
            </div>
        </div>
    @endif
</div>

<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<script>
    var pay_button = document.querySelector('#pay-button');
    const PAGARME_URL = pay_button.getAttribute("data-url");
    const REDIRECT_SUCCESS = pay_button.getAttribute("data-redirect");
    const POSTBACK_URL = "{{ url::to('/postback') }}";
    const REDIRECT_BOLETO = "{{ url::to('pagamento/boleto/') }}/";
    const API_KEY = "{{ env('PAGARME_CRYPT_KEY') }}";
    var HAS_CUPOM = false;
    var CUPOM = null;
    var PLANO_ID = 5;
    var PLANO_VALOR = 26280;
    var PLANO_NOME = 'CURSO COMPLETO';
    var FLAG_DESCONTO = false;
    var usuario = null;
    var desconto = "{{ isset($desconto) ? $desconto : null }}";
    // console.log("desconto na sessão", desconto);

    const URLS = {
        pagarme: PAGARME_URL,
        redirect_success: REDIRECT_SUCCESS,
        postback: POSTBACK_URL,
        redirect_boleto: REDIRECT_BOLETO
    }

    // 5 - CURSO COMPLETO
    var PLANO = {
        id: 5,
        nome: 'CURSO COMPLETO',
        valor: 26280
    }

    $(document).ready(function() {

        $("#cadastrar-cpf").mask('000.000.000-00', {
            reverse: true
        });
        $("#cadastrar-cpf-cartao").mask('000.000.000-00', {
            reverse: true
        });
        $("#cadastrar-nascimento").mask('00/00/0000', {
            reverse: true
        });
        $("#cadastrar-telefone").mask("(99) 99999-9999");
        $("#cadastrar-cep").mask('00000-000', {
            reverse: true
        });
        $('#pagamento-cupom').mask("AAAA-SSSS-AAAA-SS00");

        $("#btn-assinar").click(() => {
            $("#cadastro-form").submit();
        });

        $("#cadastrar-estado").change(function() {
            //console.log($("select option:selected").val());
            $.post("{{ url::to('planos/cidades') }}", {
                "estado": $("select option:selected").val()
            }, function(data) {
                //console.log(data);
                if (data.success) {
                    loadCidades(data.cidades);
                    //console.log(data.cidades);
                } else {
                    console.log('cidades não encontradas!');
                }
            }).fail(function() {
                console.log('cidades falhou!');
            });
        });

        $('input').on('input', function() {
            $(this).removeClass('is-invalid');
            $(this).removeClass('is-valid');

            if (parseInt("{{ isset($link_desconto) ? $link_desconto : 0 }}")) {
                console.log("é um link desconto");
                $("#pagamento-cupom").val("ESCO-LAPU-BLIC-AA22");
                $("#cupom-btn").click();
                $("#cupom-form").hide();
                $("#desconto").html("<span>Você ganhou um desconto de 50%. Aproveite!</span>");
                $("#texto-valor").html(
                    "<span style='font-size:20px'>R$</span>131<span style='font-size:20px'>,40</span>"
                );
            }
        });

        $("#cadastro-form").submit(
            function(e) {
                e.preventDefault();
                e.stopPropagation();
                //console.log($("#signup-email").val());
                //console.log("ation: " + $(this).attr('action'));

                var nome = $("#cadastrar-nome");
                var cpf = $("#cadastrar-cpf");
                var estado = $("#cadastrar-estado");
                var cep = $("#cadastrar-cep");
                var senha = $("#cadastrar-pass");
                var email = $("#cadastrar-email");
                var nascimento = $("#cadastrar-nascimento");
                var cidade = $("#cadastrar-cidade");
                var telefone = $("#cadastrar-telefone");
                var confirmaSenha = $("#cadastrar-confirmar-pass");
                var endereco = $("#cadastrar-endereco");
                var numero = $("#cadastrar-numero");
                var bairro = $("#cadastrar-bairro");
                var estado = $("#cadastrar-estado");
                var email_cartao = $("#cadastrar-email-cartao");
                var cpf_cartao = $("#cadastrar-cpf-cartao");
                var checkbox = $("#cadastrar-checkbox");

                var cancel = false;
                var input = null;

                if (!checkbox.is(":checked")) {
                    cancel = true;
                    input = checkbox;
                }

                if (bairro.val() == "") {
                    cancel = true;
                    input = bairro;
                }

                if (cidade.val() == "" || !cidade.val()) {
                    cancel = true;
                    input = cidade;
                }

                if (estado.val() == "") {
                    cancel = true;
                    input = estado;
                }

                if (numero.val() == "") {
                    cancel = true;
                    input = numero;
                }

                if (endereco.val() == "") {
                    cancel = true;
                    input = endereco;
                }

                if (!isEmail(email_cartao.val())) {
                    cancel = true;
                    input = email_cartao;
                }

                if (!isValidCPF(cpf_cartao.val())) {
                    cancel = true;
                    input = cpf_cartao;
                }

                if (senha.val() != confirmaSenha.val()) {
                    cancel = true;
                    input = confirmaSenha;
                }

                if (senha.val() == "") {
                    cancel = true;
                    input = senha;
                }

                if (telefone.val() == "") {
                    cancel = true;
                    input = telefone;
                }

                if (cep.val() == "") {
                    cancel = true;
                    input = cep;
                }

                // if (cidade.val() == "") {
                //     cancel = true;
                //     input = cidade;
                // }

                // if (estado.val() == "") {
                //     cancel = true;
                //     input = estado;
                // }

                // if (!isValidDate(nascimento.val())) {
                //     cancel = true;
                //     input = nascimento;
                // }

                if (!isValidCPF(cpf.val())) {
                    cancel = true;
                    input = cpf;
                }

                if (!isEmail(email.val())) {
                    cancel = true;
                    input = email;
                }

                if (nome.val() == "") {
                    cancel = true;
                    input = nome;
                }

                var next_route = "@php
                    if (isset($next_route)) {
                        echo $next_route;
                    } else {
                        echo '';
                    }
                @endphp";

                var formSerialized = $(this).serializeArray();
                formSerialized.push({
                    name: 'next_route',
                    value: next_route
                });

                if (cancel) {
                    input.addClass("is-invalid");
                    input.focus();
                } else {
                    $.post($(this).attr('action'), formSerialized, function(data) {
                            // console.log("data: " + data);
                            if (data.success) {
                                if (data.wasCadastrado) {
                                    $("#warning-box").fadeIn();
                                } else {
                                    usuario = data.user;
                                    hasDesconto(data.user.email, data.user, PLANO, API_KEY, URLS,
                                        HAS_CUPOM ? CUPOM : null);
                                }
                            } else {
                                $("#warning-text").html(
                                    "Cadastro falhou! Por favor, tente mais tarde.");
                                $("#warning-box").fadeIn();
                                console.log("cadastro falhou!!");
                            }
                        })
                        .fail(function() {
                            $("#warning-text").html("Cadastro falhou! Por favor, tente mais tarde.");
                            $("#warning-box").fadeIn();
                            console.log("cadastro falhou!!")
                        });
                }

            }
        );

    });

    function recuperarModal() {
        $("#recuperarModal").modal('show');
    }

    $("#recuperar-senha").submit(function(e) {
        e.preventDefault();
        e.stopPropagation();

        $("#recuperarModal .modal-footer button").html("caregando...");

        var url = $(this).attr('action');

        var email = $("#email-recuperar");

        if (email.val() == "") {
            email.addClass("is-invalid");
            email.focus();
        } else {
            var formSerialized = $(this).serializeArray();
            $.post($(this).attr('action'), formSerialized, function(data) {
                    // console.log("data: " + data.data.url + " " + data.data.nome);
                    if (data.success) {
                        $(".recuperar-warning-text").css('color', 'green');
                        $(".recuperar-warning-text").html(data.message);
                        $("#recuperar-warning-box").fadeIn().delay(4000).fadeOut(2000);
                    } else {
                        $(".recuperar-warning-text").css('color', 'red');
                        $(".recuperar-warning-text").html(data.message);
                        $("#recuperar-warning-box").fadeIn().delay(4000).fadeOut(2000);
                        //console.log("cadastro falhou!!");
                    }
                })
                .fail(function() {
                    $(".recuperar-warning-text").css('color', 'red');
                    $(".recuperar-warning-text").html("Ops! Algo deu errado. Por favor, tente mais tarde.");
                    $("#recuperar-warning-box").fadeIn().delay(4000).fadeOut(2000);
                    //console.log("cadastro falhou!!")
                }).always(function() {
                    $("#recuperarModal .modal-footer button").html("recuperar");
                });
        }

    });

    $("#login-form").submit(
        function(e) {
            e.preventDefault();
            e.stopPropagation();

            // console.log("ation: " + $(this).attr('action'));

            var email = $("#login-email").val();
            var pass = $("#login-pass").val();
            var next_route = "@php
                if (isset($next_route)) {
                    echo $next_route;
                } else {
                    echo '';
                }
            @endphp";

            if (parseInt("{{ isset($link_desconto) ? $link_desconto : 0 }}")) {
                console.log("link desconto aqui");
                next_route = "desconto";
            }

            //alert(next_route);
            //console.log(email);
            //console.log(pass);

            if (isEmail(email)) {
                $.post($(this).attr('action'), {
                        'email': email,
                        'senha': pass,
                        'next_route': next_route
                    }, function(data) {
                        if (data.success) {
                            if (data.next_route) {
                                // console.log("link route", "{{ url::to('/') }}" + '/' + data.next_route);
                                window.location.href = "{{ url::to('/') }}" + '/' + data.next_route;
                                return;
                            }
                            if (data.user.plano != null) {
                                if (data.user.plano.status == 'paid' || data.user.plano.status == 'try' || data
                                    .user.plano.status == null) window.location.href =
                                    "{{ url::to('conta/curso') }}";
                                else window.location.href = "{{ url::to('conta') }}";
                            } else {
                                window.location.href = "{{ url::to('conta') }}";
                            }
                        } else {
                            $("#login-pass").addClass('is-invalid');
                        }
                    })
                    .fail(function() {
                        console.log("login falhou!!")
                    });
            } else {
                $("#login-email").addClass('is-invalid');
            }
        }
    );

    $("#signup-from").submit(
        function(e) {
            e.preventDefault();
            e.stopPropagation();
            //console.log($("#signup-email").val());
            // console.log("ation: " + $(this).attr('action'));
            var email = $("#signup-email").val();
            // console.log(email);

            if (isEmail(email)) {
                $.post($(this).attr('action'), {
                        'email': $("#signup-email").val()
                    }, function(data) {
                        //console.log("data: " + data);
                        if (data.success) {
                            $('#video-box').fadeOut();
                            $('#info-box').fadeOut();
                            $('#login-box').fadeOut();
                            $('#planos-login-section-1 .big-title').html(
                                '<span style="font-weight:400;">checkout</span> <i class="fas fa-chevron-right" style="font-size: x-large;"></i> <strong>registro</strong>'
                            );
                            loadEstados(data.estados);
                            $('#valor-box').fadeIn();
                            $('#cadastro-box').fadeIn();
                            $("#cadastrar-email").val(data.email);
                        } else {
                            //console.log(data.message);
                            $("#signup-email-fb").html(data.message);
                            $("#signup-email").addClass('is-invalid');
                            //alert(data.success);
                        }
                    })
                    .fail(function() {
                        //console.log("post falhou!!");
                        $("#signup-email-fb").html("Ops! Algo deu errado, por favor, tente mais tarde.");
                        $("#signup-email").addClass('is-invalid');
                    });
            } else {
                $("#signup-email").addClass('is-invalid');
            }
        }
    );

    $("#cupom-form").submit(
        function(e) {
            e.preventDefault();
            e.stopPropagation();

            var numero = $("#pagamento-cupom").val().replace(/-/g, "");
            if (numero == "") {
                $("#pagamento-cupom").addClass("is-invalid");
            }

            var plano = PLANO.id;

            $.get($(this).attr('action') + '/' + numero + '/' + plano, function(data) {
                    //console.log(data);
                    if (data.success) {
                        setPlanoByCupom(data.cupom);
                        // console.log(data.cupom);
                        CUPOM = data.cupom;
                        $("#pagamento-cupom").prop("disabled", true);
                        //$("#pagamento-cupom-fb-valid").show();
                        $("#cupom-form button").prop("disabled", true);
                        $("#pagamento-cupom").addClass("is-valid");
                        // sync assinatura
                        if (FLAG_DESCONTO) {
                            FLAG_DESCONTO = false;
                            assinar(usuario, PLANO, API_KEY, URLS, HAS_CUPOM ? CUPOM : null);
                        }
                    } else {
                        $("#pagamento-cupom-fb").html(data.message);
                        $("#pagamento-cupom").addClass("is-invalid");
                    }
                })
                .fail(function() {
                    $("#pagamento-cupom-fb").html("Validação falhou! Por favor, tente mais tarde.");
                    $("#pagamento-cupom").addClass("is-invalid");
                    //console.log("cupom falhou!!");
                });
        }
    );

    function setPlanoByCupom(cupom) {
        HAS_CUPOM = true;
        PLANO.id = cupom.idPlano;
        var plano = getPlanoById(cupom.idPlano);
        PLANO.nome = plano.nome;
        var valor_inicial = plano.valor;
        if (cupom.tipo_desconto) {
            PLANO.valor = valor_inicial - (valor_inicial * cupom.desconto / 100);
        } else {
            PLANO.valor = valor_inicial - cupom.desconto;
        }

        plano['valor'] = PLANO.valor;
        // console.log("PLANO_VALOR pos-cupom:" + PLANO.valor);
    }

    function getPlanoById(selected) {
        var plano = [];
        switch (selected) {
            case 0:
                plano["val"] = 0;
                plano["nome"] = "EXTENSIVO MED";
                plano["duracao"] = "10";
                plano["valor"] = 19000;
                break;
            case 1:
                plano["val"] = 1;
                plano["nome"] = "EXTENSIVO ENEM";
                plano["duracao"] = "10";
                plano["valor"] = 16000;
                break;
            case 3:
                plano["val"] = 3;
                plano["nome"] = "INTENSIVÃO ENEM";
                plano["duracao"] = "4";
                plano["valor"] = 25000;
                break;
            case 5:
                plano["val"] = 5;
                plano["nome"] = "CURSO COMPLETO";
                plano["duracao"] = "12";
                plano["valor"] = 26280;
                break;
            case 6:
                plano["val"] = 6;
                plano["nome"] = "CURSO TOTAL";
                plano["duracao"] = "12";
                plano["valor"] = 9480;
                break;
        }
        return plano;
    }

    function loadEstados(estados) {
        $.each(estados, function(index, item) {
            var option = document.createElement("option");
            option.text = item.sigla;
            option.value = item.cod_estados;
            $("#cadastrar-estado").append(option);
        });
        $("#cadastrar-estado").prop('disabled', false);
    }

    function loadCidades(cidades) {
        $("#cadastrar-cidade").empty();
        $("#cadastrar-cidade").append('<option selected>Selecione a cidade</option>');
        $.each(cidades, function(index, item) {
            //console.log("texto: " + item.sigla);
            //console.log("cod: " + item.cod_estados);
            var option = document.createElement("option");
            option.setAttribute('data-cep', item.cep);
            option.text = item.nome;
            option.value = item.cod_cidades;
            $("#cadastrar-cidade").append(option);
        });
        $("#cadastrar-cidade").prop('disabled', false);
    }

    function isEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function isValidCPF(cpf) {
        if (typeof cpf !== "string") return false
        cpf = cpf.replace(/[\s.-]*/igm, '')
        if (
            !cpf ||
            cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999"
        ) {
            return false
        }
        var soma = 0
        var resto
        for (var i = 1; i <= 9; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i)
        resto = (soma * 10) % 11
        if ((resto == 10) || (resto == 11)) resto = 0
        if (resto != parseInt(cpf.substring(9, 10))) return false
        soma = 0
        for (var i = 1; i <= 10; i++)
            soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i)
        resto = (soma * 10) % 11
        if ((resto == 10) || (resto == 11)) resto = 0
        if (resto != parseInt(cpf.substring(10, 11))) return false
        return true
    }

    function isValidDate(dateString) {
        // First check for the pattern
        if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    }

    function hasDesconto(email, user, plano, api_key, urls, cupom) {
        let action = "/has-desconto";
        let data = {
            email: email
        };
        $.post(action, data, function(data) {
                // console.log("hasDesconto: " + data);
                if (data.success) {
                    // console.log("resposta desconto", data.resposta);
                    if (data.resposta) {
                        FLAG_DESCONTO = true;
                        $("#pagamento-cupom").val("ESCO-LAPU-BLIC-AA22");
                        $("#cupom-btn").click();
                        $("#cupom-form").hide();
                        $("#desconto").html("<span>Você ganhou um desconto de 50%. Aproveite!</span>");
                        $("#desconto").show();
                        alert("Você possui um desconto de 50%");
                    } else {
                        if (parseInt("{{ isset($link_desconto) ? $link_desconto : 0 }}")) {
                            console.log("com desconto");
                            $("#pagamento-cupom").val("ESCO-LAPU-BLIC-AA22");
                            $("#cupom-btn").click();
                            $("#cupom-form").hide();
                            $("#desconto").html(
                                "<span class='mx-auto'><i class='fas fa-gifts'></i> Você ganhou um desconto de 50%</span>"
                            );
                            $("#desconto").show();
                        }
                    }
                } else {
                    console.log(data.msg);
                    assinar(user, plano, api_key, urls, cupom);
                }
            })
            .fail(function() {
                console.log("verificação de desconto falhou!");
            });
    }
</script>

<script src="{{ url('js/pagamento.js') }}"></script>
