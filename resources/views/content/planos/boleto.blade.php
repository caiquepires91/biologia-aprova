<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<!-- Modal -->
<div class="modal fade" id="autorizadoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pagamento Pendente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                O pagamento está pendente. Notificaremos por e-mail assim que recebermos a confirmação.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<style>
    #loader-wrapper {
        display: none;
    }

    #loader-wrapper .loader-section {
        position: fixed;
        top: 0;
        width: 100%;
        height: 100%;
        background: #222222e3;
        z-index: 1021;
        display: flex;
    }

    #loader {
        z-index: 1022;
        /* anything higher than z-index: 1000 of .loader-section */
        margin: auto;
        position: absolute;
        top: 50%;
        right: 44%;
    }

    #loader h2 {
        font-size: xxx-large;
        font-weight: 700;
        color: #EEEEEE;
    }
</style>

<div id="loader-wrapper">
    <div id="loader">
        <h2>aguarde...</h2>
    </div>
    <div class="loader-section">
    </div>
</div>

<div id="planos-login-section-1" class="row">
    <div class="col"
        style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">finalizar compra</h2>
    </div>
</div>
<div class="row justify-content-center" style="height: 90vh;">
    <div class="col-md-8 m-5">
        <div class="slide-in5">
            <div class="alert alert-success">
                <div class="col-md-12">
                    <div class="row mb-3">
                        <h2 style="font-size: xx-large;font-weight: 800;"><i class="fa fa-info-circle"></i> Informações
                            sobre sua compra</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <p>Plano escolhido: <span
                                    id="plano_escolhido_3"><strong>{{ $user->plano->plano }}</strong></span></p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            @php
                                $unidadeTempo = ' meses';
                                //if ($user->plano->plano === "INTENSIVÃO ENEM")
                                //$unidadeTempo = " dias";
                            @endphp
                            <!-- <p>Duração: <span id="duracao_plano_3"><strong>{{ $user->plano->tempo }} {{ $unidadeTempo }}</strong></span></p> -->
                        </div>
                    </div>
                    <div class="row">
                        <p>Forma de pagamento: <strong>Boleto bancário</strong></p>
                    </div>
                    <div class="row">
                        <p>Vencimento em: <strong>{{ date('d-m-Y', strtotime($boleto->expirar)) }}</strong></p>
                    </div>
                    <div class="row">
                        <p>Valor: <span id="valor_plano_3"><strong>
                                    @if ($boleto->cupom != null)
                                        <s>
                                            {{ $user->plano->plano == 'EXTENSIVO MED' ? "R$ 19,00/mês" : ($user->plano->plano == 'EXTENSIVO ENEM' ? "R$ 16,00/mês" : ($user->plano->plano == 'CURSO TOTAL' ? "R$ 43,20" : "R$ 262,80")) }}</s>
                                        <span id="desconto">{{ $user->plano->planoDesconto->valor }}</span>
                                    @else
                                        {{ $user->plano->plano == 'EXTENSIVO MED' ? "R$ 19,00/mês" : ($user->plano->plano == 'EXTENSIVO ENEM' ? "R$ 16,00/mês" : ($user->plano->plano == 'CURSO TOTAL' ? "R$ 43,20" : "R$ 262,80")) }}
                                    @endif
                                </strong>
                            </span></p>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="row" style="display:none;">
                        <select id="selecionar_plano_3" name="selecionar_plano_3" class="selectpicker show-tick"
                            title="Alterar plano">
                            <option value="0"
                                data-subtext="10 meses de acesso" "{{ $user->plano->plano == 'EXTENSIVO MED' ? 'selected' : '' }}">
                                plano extensivo med</option>
                            <option value="1"
                                data-subtext="10 meses de acesso" "{{ $user->plano->plano == 'EXTENSIVO ENEM' ? 'selected' : '' }}">
                                plano extensivo enem</option>
                        </select>
                    </div>
                    <style>
                        .ajust_bt_pagarme input {
                            background: #FFF;
                            border: 1px solid #FFF;
                            width: 100%;
                            height: 30px;
                            margin-top: 20px;
                        }

                        .ajust_bt_pagarme input:hover {
                            background: #F9F9F9;
                            border: 1px solid #FFF;
                        }
                    </style>
                    <div class="row" style="padding: 0px 30%;">
                        <button id="pay-button" class="btn btn-primary" data-url="{{ url::to('pagarme') }}"
                            style="background-color: #3eb378; font-size: larger; font-weight: bolder;">
                            <div class="row">
                                <div class="col-9" style="text-align: left;">pagar com cartão</div>
                                <div class="col-3" style="text-align: right;"><i
                                        class="fas fa-long-arrow-alt-right"></i></div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
            <div class="slidein5-content row">
                <div class="col-lg-8">
                    <button class="imprimir_boleto btn btn-outline-success" onClick="goTo('{{ $boleto->boleto }}');"
                        style="font-size: larger; font-weight: bolder;">
                        <i class="fa fa-print"></i>
                        Imprimir boleto
                    </button>
                </div>
                <div class="col-lg-4">
                    <a href=""></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var usuario = @json($user, JSON_PRETTY_PRINT);
    var HAS_CUPOM = false;

    $(document).ready(function() {
        console.log('usuario', usuario);

        @if ($boleto->cupom != null)
            $("#desconto").html(convertToCurrency(parseInt($("#desconto").html()) / 100));
        @endif

        $("#pay-button").on('click', function(e) {
            var url = this.getAttribute("data-url");
            var plano_id = getPlanoId();
            var plano_nome = "{{ $user->plano->plano }}";
            var plano_valor = getPlanoValor();
            var CUPOM = getPlanoCupom();

            var checkout = new PagarMeCheckout.Checkout({
                encryption_key: "{{ env('PAGARME_CRYPT_KEY') }}",
                success: function(data) {
                    $("#loader-wrapper").css("display", "block");
                    if (HAS_CUPOM) data.cupom = CUPOM;
                    data.plano_valor = plano_valor;
                    //console.log("pay success");
                    //console.log(data);
                    $.post(url, data, function(data) {
                            $("#loader-wrapper").css("display", "none");
                            //console.log("resultado do post pagamento");
                            //console.log(data);
                            if (data.success) {
                                if (data.status == 'paid') {
                                    window.location.href =
                                        "{{ url::to('conta/curso') }}";
                                } else if (data.status == 'waiting_payment') {
                                    window.location.href =
                                        "{{ url::to('pagamento/boleto/') }}/" + data
                                        .boleto.id;
                                } else if (data.status == 'authorized') {
                                    $('#autorizadoModal modal-title').html(
                                        "Pagamento Pendente");
                                    $('#autorizadoModal modal-body').html(
                                        "O pagamento foi reservado e está pendente. Notificaremos por e-mail assim que recebermos a confirmação."
                                    );
                                    $('#autorizadoModal').modal('show');
                                }
                            } else {
                                error();
                                //alert("Transação falhou!");
                            }
                        })
                        .fail(function() {
                            $("#loader-wrapper").css("display", "none");
                            error();
                            //console.log("post pagamento falhou!!");
                        });
                },
                error: function(err) {
                    $("#loader-wrapper").css("display", "none");
                    error();
                    //console.log(err);
                    //alert(err);
                },
                close: function() {
                    $("#loader-wrapper").css("display", "none");
                    //console.log('The modal has been closed.');
                }
            });

            var phonenumber = cleanPhone("+55{{ $user->telefone }}");

            checkout.open({
                amount: plano_valor,
                // headerText: "É importante utilizar o mesmo e-mail que está logado na conta. Total: {price_info}",
                customerData: 'false',
                createToken: 'true',
                customer_editable: 'true',
                paymentMethods: 'boleto,credit_card',
                boletoDiscountPercentage: 0,
                postbackUrl: "{{ url::to('/postback') }}",
                maxInstallments: 12,
                defaultInstallments: 1,
                freeInstallments: 12,
                customer: {
                    external_id: usuario.id,
                    name: usuario.nome,
                    type: 'individual',
                    country: 'br',
                    email: usuario.email_cartao,
                    documents: [{
                        type: 'cpf',
                        number: usuario.cpf_cartao,
                    }, ],
                    phone_numbers: [phonenumber],
                },
                billing: {
                    name: usuario.nome,
                    address: {
                        country: 'br',
                        state: usuario.estado,
                        city: usuario.cidade,
                        neighborhood: usuario.bairro,
                        street: usuario.endereco,
                        street_number: usuario.numero,
                        zipcode: cleanCEP(usuario.cep)
                    }
                },
                items: [{
                    id: plano_id,
                    title: plano_nome,
                    unit_price: plano_valor,
                    quantity: 1,
                    tangible: 'false'
                }],
            });
        });
    })

    function goTo(link) {
        window.open(link, '_blank');
    }

    function convertToCurrency(value) {
        return value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
            minimumFractionDigits: 2
        })
    }

    function getPlanoValor() {
        @if ($user->plano->cupom != null)
            return parseInt("{{ $user->plano->planoDesconto->valor }}");
        @else
            if ("{{ $user->plano->plano == 'EXTENSIVO MED' }}") return 19000;
            else if ("{{ $user->plano->plano == 'EXTENSIVO ENEM' }}") return 16000;
            else if ("{{ $user->plano->plano == 'CURSO COMPLETO' }}") return 26280;
            else if ("{{ $user->plano->plano == 'CURSO TOTAL' }}") return 4370;
            else return 26280;
        @endif
    }

    function getPlanoId() {
        return parseInt(
            "{{ $user->plano->plano == 'EXTENSIVO MED' ? 0 : ($user->plano->plano == 'EXTENSIVO ENEM' ? 1 : 5) }}"
        );
    }

    function getPlanoCupom() {
        @if ($user->plano->cupom)
            if ("{{ $user->plano->cupom != null }}") {
                HAS_CUPOM = true;
                var cupom = new Object();
                cupom.id = parseInt("{{ $user->plano->cupom->id }}");
                cupom.cupom = "{{ $user->plano->cupom->cupom }}";
                cupom.data_inicio = "{{ $user->plano->cupom->data_inicio }}";
                cupom.data_fim = "{{ $user->plano->cupom->data_fim }}";
                cupom.idPlano = parseInt("{{ $user->plano->cupom->idPlano }}");
                cupom.tipo_desconto = parseInt("{{ $user->plano->cupom->tipo_desconto }}");
                cupom.desconto = parseInt("{{ $user->plano->cupom->desconto }}");
                cupom.updated_at = "{{ $user->plano->cupom->updated_at }}";
                cupom.created_at = "{{ $user->plano->cupom->created_at }}";
                return cupom;
            } else return null;
        @else
            return null;
        @endif
    }

    function error() {
        $('#autorizadoModal modal-title').html("Pagamento falhou!");
        $('#autorizadoModal modal-body').html("Por favor, tente mais tarde.");
        $('#autorizadoModal').modal('show');
    }

    function cleanPhone(value) {
        return value.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
    }

    function cleanCEP(value) {
        return value.trim().replace("-", "");
    }
</script>
