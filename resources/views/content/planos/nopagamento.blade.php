
<!-- Modal -->
<div class="modal fade" id="autorizadoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pagamento Pendente</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            O pagamento está pendente. Notificaremos por  e-mail assim que recebermos a confirmação.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
</div>

  <style>
      #loader-wrapper {
          display: none;
      }

      #loader-wrapper .loader-section {
            position: fixed;
            top: 0;
            width: 100%;
            height: 100%;
            background: #222222e3;
            z-index: 1021;
            display: flex;
        }

        #loader {
            z-index: 1022; /* anything higher than z-index: 1000 of .loader-section */
            margin:auto;
            position: absolute;
            top:50%;
            right: 44%;
        }
        #loader h1 {
            font-size: xxx-large;
            font-weight: 700;
            color: #EEEEEE;
        }
  </style>

<div id="loader-wrapper">
    <div id="loader"><h1>aguarde...</h1></div>
    <div class="loader-section">
    </div>
</div>

<div id="planos-login-section-1" class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h1 class="big-title" style="margin: auto;">pagamento</h1>
    </div>
</div>
<div class="container" style="
    height: calc(100vh - 340px);
">
    <div class="row justify-content-center align-items-center mx-md-5 p-2 pagamento-info-box">
        <div class="col-md-7 content">
            <p class="big-title" style="text-align:center" ><i class="fas fa-info-circle"></i> aguarde novas turmas</p>
        </div>
    </div>
</div>

<script src="https://assets.pagar.me/checkout/1.1.0/checkout.js"></script>

<script>

    var HAS_CUPOM = false;
    var CUPOM = null;
    var PLANO_ID = 0;
    var PLANO_VALOR = 19000;
    var PLANO_NOME = 'EXTENSIVO MED';

    initPlano();

    function initPlano() {
        var plano_escolhido = $("#pagamento-plano-select").data("plano");
        var plano = getPlanoById(parseInt(plano_escolhido));
        mostraPlano(plano);
    }

    function setPlanoByCupom(cupom) {
        HAS_CUPOM = true;
        PLANO_ID = cupom.idPlano;
        var plano = getPlanoById(cupom.idPlano);
        PLANO_NOME = plano.nome;
        var valor_inicial = plano.valor;
        /* type: 0 - valor, 1 - % */
        if (cupom.tipo_desconto) {
            PLANO_VALOR = valor_inicial - (valor_inicial * cupom.desconto/100);
        } else {
            PLANO_VALOR = valor_inicial - cupom.desconto;
        }

        plano['valor'] = PLANO_VALOR;
        //console.log("PLANO_VALOR pos-cupom:" + PLANO_VALOR);
        mostraPlano(plano);
    }

    function getPlanoById(selected) {
        var plano = [];
        switch(selected) {
            case 0:
                plano["nome"] = "EXTENSIVO MED";
                plano["duracao"] = "10";
                plano["valor"] = 19000;
                break;
            case 1:
                plano["nome"] = "EXTENSIVO ENEM";
                plano["duracao"] = "10";
                plano["valor"] = 16000;
                break;
        }
        //console.log(plano.nome);
        return plano;
    }

    function mostraPlano(plano) {
        $("#plano-type").html("plano escolhido: " + plano.nome.toLowerCase());
        $("#plano-duration").html("duração: " + plano.duracao + " meses");
        $("#plano-price").html("valor: " + convertToCurrency(plano.valor/1000) + "/mês"); 
    }

    function convertToCurrency(value) {
        return value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
            minimumFractionDigits: 2
        })
    }

    $("#pagamento-plano-select").change(function(){
        /* reset cupom ao mudar de plano */
        HAS_CUPOM = false;
        $("#pagamento-cupom").val("");
        $("#pagamento-cupom").prop("disabled", false);
        $("#cupom-form button").prop("disabled", false);
        $("#pagamento-cupom").removeClass("is-valid");
        $("#pagamento-cupom").removeClass("is-invalid");
         /* /reset cupom ao mudar de plano */

        selected = $("#pagamento-plano-select option:selected").val();
        var plano = getPlanoById(parseInt(selected));
        mostraPlano(plano);
    });

    $('#pagamento-cupom').mask("AAAA-SSSS-AAAA-SS00");

    $("#cupom-form").submit(
        function (e) {
            e.preventDefault();
            e.stopPropagation();

            var numero = $("#pagamento-cupom").val().replace(/-/g,"");
            if (numero == "") {
                $("#pagamento-cupom").addClass("is-invalid");
            }

            var plano = $("#pagamento-plano-select").val();
            
            //console.log(numero);

            $.get($(this).attr('action') + '/' + numero + '/' + plano, function(data) {
                //console.log(data);
                if (data.success) {
                    setPlanoByCupom(data.cupom);
                    //console.log(data.cupom);
                    CUPOM = data.cupom;
                    $("#pagamento-cupom").prop("disabled", true);
                    //$("#pagamento-cupom-fb-valid").show();
                    $("#cupom-form button").prop("disabled", true);
                    $("#pagamento-cupom").addClass("is-valid");
                } else {
                    $("#pagamento-cupom-fb").html(data.message);
                    $("#pagamento-cupom").addClass("is-invalid");
                }
            })
            .fail(function() {
                $("#pagamento-cupom-fb").html("Validação falhou! Por favor, tente mais tarde.");
                $("#pagamento-cupom").addClass("is-invalid");
                //console.log("cupom falhou!!");
            });
        }
    );

    $("#cartao-form").submit(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
        }
    );

    function mostrarDiv(valor) {
        if (valor == 'boleto') {
            $('#pag_cartao').css('display', 'none');
            $('#pag_boleto').css('display', 'block');
        }
        if (valor == 'cartao') {
            $('#pag_cartao').css('display', 'block');
            $('#pag_boleto').css('display', 'none');
        }
    }

    function aguard() {
      // console.log("aguardando...");
    }

    function error() {
        $('#autorizadoModal .modal-title').html("Pagamento falhou!");
        $('#autorizadoModal .modal-body').html("Por favor, tente mais tarde.");
        $('#autorizadoModal').modal('show');
    }

    $('input').on('input', function() {
        $(this).removeClass('is-invalid');
        $(this).removeClass('is-valid');
    });

    function fillOutCheckout() {
        $("#pagarme-modal-box-buyer-name").val("{{$user->nome}}");
    }

    var button = document.querySelector('#pay-button');
    var url = button.getAttribute("data-url");
    //console.log(url);
    // Abrir o modal ao clicar no botão
    button.addEventListener('click', function() {
        // verifica qual plano foi selecionado
       var plano = $("#pagamento-plano-select").val();

       if (HAS_CUPOM) {
            setPlanoByCupom(CUPOM);
       } else if (plano == 0) {
           // 0 - extensivo med
           PLANO_NOME = 'EXTENSIVO MED';
           PLANO_ID = 0;
           PLANO_VALOR = 19000;
       } else {
           // 1 - extensivo enem
            PLANO_NOME = 'EXTENSIVO ENEM';
           PLANO_ID = 1;
           PLANO_VALOR = 16000;
       }

       fillOutCheckout();

        var checkout = new PagarMeCheckout.Checkout({
            encryption_key: "{{env('PAGARME_CRYPT_KEY')}}",
            success: function(data) {
                if (HAS_CUPOM) data.cupom = CUPOM;
                // adicionar sinalizador do cupom.
                data.plano_valor = PLANO_VALOR;
                //console.log("pay success");
                //console.log(data);
                //console.log(url);
                $("#loader-wrapper").css("display", "block");
                $.post(url, data, function(data) {
                        $("#loader-wrapper").css("display", "none");
                        //alert("on post result");
                        //console.log("resultado do post pagamento");
                        //console.log("dados boleto:" + JSON.stringify(data));
                        if (data.success) {
                            if (data.status == 'paid') {
                                window.location.href = "{{url::to('conta/curso')}}";
                            } else if (data.status == 'waiting_payment') {
                                window.location.href = "{{url::to('pagamento/boleto/')}}/" + data.boleto.id;
                            } else if (data.status == 'authorized') {
                                $('#autorizadoModal modal-title').html("Pagamento Pendente");
                                $('#autorizadoModal modal-body').html("O pagamento foi reservado e está pendente. Notificaremos por e-mail assim que recebermos a confirmação.");
                                $('#autorizadoModal').modal('show');
                            }
                        } else {
                            error();
                            //alert("Transação falhou!");
                        }
                })
                .fail(function() {
                    $("#loader-wrapper").css("display", "none");
                    error();
                    //console.log("post pagamento falhou!!");
                });
            },
            error: function(err) {
                $("#loader-wrapper").css("display", "none");
                //console.log(err);
                error();
                alert(err);
            },
            close: function() {
                $("#loader-wrapper").css("display", "none"); 
                alert("on pay modal colsed ");
                //console.log('The modal has been closed.');
            }
        });

        checkout.open({
            amount: PLANO_VALOR,
            customerData: 'true',
            createToken: 'true',
            paymentMethods: 'boleto,credit_card',
            boletoDiscountPercentage: 0,
            postbackUrl: "{{url::to('/postback')}}",
            costumer: [{
                name : "{{$user->nome}}",
                email:"{{$user->email}}",
                documents: [{
                    type: 'CPF',
                    number: '{{$user->cpf}}'
                }],
                phone_numbers: [
                    "{{$user->telefone}}"
                ]
            }],
            items: [{
                id: PLANO_ID,
                title: PLANO_NOME,
                unit_price: PLANO_VALOR,
                quantity: 1,
                tangible: 'false'
            }],
        });
    });
</script>