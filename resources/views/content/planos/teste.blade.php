<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ops!</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            Algo não saiu como esperado. Por favor, tente mais tarde.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>

<style>
    .teste {
        height: 100vh;
    }
</style>

<div id="planos-login-section-1" class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">teste grátis</h2>
    </div>
</div>
<div class="row mb-5 justify-content-center teste" >
<div class="col-md-4 " style="display:flex">
    <button id="teste-btn" data-url="{{url::to('try')}}" class="btn btn-primary blue-btn rounded-pill" style="margin:auto; margin-top: 5rem;">7 dias de teste grátis</button>
</div>
</div>

<script>
    $("#teste-btn").on('click', function() {
        var url = $('#teste-btn').data('url');
        console.log(url);
        $.post(url, {}, function(data) {
            console.log(data);
            if (data.success) {
                if (data.user.plano.status == 'try') window.location.href = "{{url::to('conta/curso')}}";
            } else {
                $('#infoModal modal-body').html(data.message);
                $('#infoModal').modal('show');
            }         
        })
        .fail(function() {
            $('#infoModal modal-body').html("Algo não saiu como esperado. Por favor, tente mais tarde.");
            $('#infoModal').modal('show');
            console.log("post falhou!!")
        });
    });
</script>