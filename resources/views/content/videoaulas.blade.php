<button id="to-top-btn" class="btn blue-btn rounded-pill fixed-bottom" onclick="topFunction()" ><i class="fas fa-chevron-up"></i></button>
<div id="video-section-1" class="row postition-relative justfy-content-center align-items-center">
    <div class="col">
        <!-- <img class="position-absolute" src="{{url::to('../img/blog-bg-2.jpg')}}"/> -->
        <div id="video-bg" class="position-absolute">
           <!--  <video autoplay loop muted autoPlay class="bg_video">
                <source src="videos/bg.webm" type="video/webm">
                <source src="{{url::to('img/biology-bg-2.mp4')}}" type="video/mp4">
            </video> -->
            <iframe  id="1" src="https://player.vimeo.com/video/516859047?autoplay=1&loop=1&autopause=0&background=1&muted=1"  frameborder="0" width="582" height="425" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
        <h2 class="big-title">videoaulas</h2>
    </div>
</div>

<div id="video-section-2" style="display: none;"></div>

<!-- <div class="row justfy-content-center align-items-center sticky-top" style="background: #3eb378;top: 83.08px;margin: 0;margin-right: 17px;" id="video-section-2">
    <div id="videos-bar" class="col" style="display: flex; overflow-x:scroll; padding:0">
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" style="padding: 0px 6px;padding-top: 4px;">
            <div class="btn-group" role="group" aria-label="First group" style="margin: 4px auto;">
                <button type="button" class="btn btn-success" data-scroll="video-content-1" onclick="scrollToElement(this)" >Introdução a origem da vida</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-2" onclick="scrollToElement(this)" >Evolução</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-3" onclick="scrollToElement(this)" >Biologia molecular</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-4" onclick="scrollToElement(this)" >Citologia</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-5" onclick="scrollToElement(this)" >Bioenergética</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-6" onclick="scrollToElement(this)" >Citogenética</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-7" onclick="scrollToElement(this)" >Embriologia e Histologia</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-8" onclick="scrollToElement(this)" >Sistemática e Taxonomia</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-9" onclick="scrollToElement(this)" >Microbiologia e Endemias</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-10" onclick="scrollToElement(this)" >Botânica</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-11" onclick="scrollToElement(this)" >Zoologia</button>
                <button type="button" class="btn btn-success" data-scroll="video-content-12" onclick="scrollToElement(this)" >Ecologia</button>  
                <button type="button" class="btn btn-success" data-scroll="video-content-13" onclick="scrollToElement(this)" >Genética</button>  
                <button type="button" class="btn btn-success" data-scroll="video-content-14" onclick="scrollToElement(this)" >Fisiologia</button>  
            </div>
          </div>
    </div>
</div> -->

<div id="videos-sidebar" class="sticky-top">
    <div class="sidebar-top">
    </div>
    <div class="sidebar-header">
        <div class="sidebar-header-icon"><i class="fas fa-microscope"></i></div>
        <div class="sidebar-header-title"><p>categorias</p></div>
    </div>
    <div class="sidebar-content">
       <ul>
           @foreach($areas as $key=>$area)
           <li id="sidebar-item-{{$key + 1}}" >
               <!-- <div class="sidebar-item-icon"><img src="{{url::to('img/videos/v'. ($key + 1) . '.png')}}"/></div> -->
               <button type="button" class="sidebar-btn" data-scroll="video-content-{{$key + 1}}" onclick="scrollToElement(this)" >{{$area}}</button></li>
           @endforeach
       </ul>
    </div>
</div>
<div id="videos-sidebar-btn"><i class="fas fa-chevron-right"></i></div>

<div class="my-video-container position-relative">
    @php
        $COLORS = [
            "#6799fb87",
            "#ff66007a",
            "#100e6e7a",
            "#cb359985",
            "#fca7238a",
            "#802ff07d",
            "#9a01cd85",
            "#03cbfa8c",
            "#8080808c",
            "#37c83687",
            "#ffcc008a",
            "#37ca9896",
            "#ce2c6185",
            "#fe000085"
        ]
    @endphp
   <!--  <div class="video-bg">
        <iframe style="height:100%; width:100%" width="560" height="315" src="https://www.youtube.com/embed/-MKapbz0GI?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div> -->
    @isset($videosPorArea)
    @forelse ($videosPorArea as $key=>$videos)
        <div style="display: none;" >{{$id = $key + 1}}</div> 
        @if ($key % 2 == 0)
        <div class="video-padding-container"  id="video-content-{{$id}}" data-highlight="sidebar-item-{{$key + 1}}" style="background-color: {{$COLORS[$key]}}">
            <div class="videoaulas-container-impar videoaulas-container position-relative">
                <div class="iframe-container position-absolute" style="border-color: #175075">
                    <div id="carousel-{{$id}}" class="carousel slide position-relative">
                        <div class="carousel-inner" style="margin: auto;">
                            @forelse($videos as $keyVideo=>$video)
                            <div class="carousel-item {{ $keyVideo ? '' : 'active' }} position-relative" data-title="{{$video->nome}}">
                                @if($video->privado)
                                    @if (isset($user))
                                        @if ( $user->plano->ativo && ($user->plano->status == "paid" || $user->plano->status == "try" || $user->plano->status == null))
                                        @else
                                        <div class="mask_video"><p>Assine já</p></div>
                                        @endif
                                    @else
                                    <div class="mask_video"><p>Assine já</p></div>
                                    @endif
                                @endif
                                <div class="vi-lazyload" data-id="{{(int) filter_var($video->src, FILTER_SANITIZE_NUMBER_INT)}}" data-thumb="1" data-logo="0"></div>
                                <!-- <iframe class="d-block w-100"  src="{{$video->src}}" width="530" height="300" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe> -->
                            </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="control position-absolute">
                            <div class="prev" style="width: 70px;">
                                <button class="carousel-control-prev video-slide-control" type="button" data-bs-target="#carousel-{{$id}}"  data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true" style="background: unset;"><i class="fas fa-chevron-left"></i></span>
                                    <span class="visually-hidden">Previous</span>
                                  </button>
                            </div>
                              <div class="indicators" style="width: 400px; display: flex;">
                                <div class="carousel-indicators">
                                    @forelse ($videos as $keyVideo=>$video)
                                    <button type="button" data-bs-target="#carousel-{{$id}}" data-bs-slide-to="{{$keyVideo}}" class="{{ $keyVideo ? '' :'active' }}" aria-current="true"></button>
                                    @empty
                                    @endforelse
                                </div>
                              </div>
                              <div class="next"  style="width: 70px;">
                                <button class="carousel-control-next video-slide-control" type="button" data-bs-target="#carousel-{{$id}}"  data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"  style="background: unset;"><i class="fas fa-chevron-right"></i></span>
                                    <span class="visually-hidden">Next</span>
                                  </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-container-impar position-absolute">
                    <img src="{{url::to('img/videos/v'. $id . '.png')}}" alt="biologia aprova - logo {{$areas[$key]}}"/>
                    <p class="big-title">{{$areas[$key]}}</p>
                </div>
            </div>
        </div>
        @else
        <div class="video-padding-container"  id="video-content-{{$id}}" data-highlight="sidebar-item-{{$key + 1}}"  style="background-color: {{$COLORS[$key]}}" >
            <div class="videoaulas-container-par my-auto  videoaulas-container position-relative">
                <div class="iframe-container position-absolute" style="border-color: #175075">
                    <div id="carousel-{{$id}}" class="carousel slide position-relative">
                        <div class="carousel-inner" style="margin: auto;">
                            @forelse($videos as $keyVideo=>$video)
                            <div class="carousel-item {{ $keyVideo ? '' : 'active' }} position-relative" data-title="{{$video->nome}}">
                                @if($video->privado)
                                    @if (isset($user))
                                        @if ( $user->plano->ativo && ($user->plano->status == "paid" || $user->plano->status == "try"))
                                        @else
                                        <div class="mask_video"><p>Assine já</p></div>
                                        @endif
                                    @else
                                    <div class="mask_video"><p>Assine já</p></div>
                                    @endif
                                @endif
                                <div class="vi-lazyload" data-id="{{(int) filter_var($video->src, FILTER_SANITIZE_NUMBER_INT)}}" data-thumb="3" data-logo="0"></div>
                                <!-- <iframe class="d-block w-100"  src="{{$video->src}}" width="530" height="300" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe> -->
                            </div>
                            @empty
                            @endforelse
                        </div>
                        <div class="control position-absolute">
                            <div class="prev" style="width: 70px;">
                                <button class="carousel-control-prev video-slide-control" type="button" data-bs-target="#carousel-{{$id}}"  data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true" style="background: unset;"><i class="fas fa-chevron-left"></i></span>
                                    <span class="visually-hidden">Previous</span>
                                  </button>
                            </div>
                              <div class="indicators">
                                <div class="carousel-indicators">
                                    @forelse ($videos as $keyVideo=>$video)
                                    <button type="button" data-bs-target="#carousel-{{$id}}" data-bs-slide-to="{{$keyVideo}}" class="{{ $keyVideo ? '' :'active' }}" aria-current="true"></button>
                                    @empty
                                    @endforelse
                                </div>
                              </div>
                              <div class="next"  style="width: 70px;">
                                <button class="carousel-control-next video-slide-control" type="button" data-bs-target="#carousel-{{$id}}"  data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"  style="background: unset;"><i class="fas fa-chevron-right"></i></span>
                                    <span class="visually-hidden">Next</span>
                                  </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="desktop text-container-par position-absolute">
                    <img src="{{url::to('img/videos/v'. $id . '.png')}}" alt="biologia aprova - logo {{$areas[$key]}}"/>
                    <p class="big-title">{{$areas[$key]}}</p>
                </div>
                <div class="text-container-par-mob position-absolute">
                    <p class="big-title">{{$areas[$key]}}</p>
                    <img src="{{url::to('img/videos/v'. $id . '.png')}}" alt="biologia aprova - logo {{$areas[$key]}}"/>
                </div>
            </div>
        </div>
        @endif
    @empty
    @endforelse
    @endisset
</div>

<script>

    setTimeout(openNav, 2000);
    setTimeout(closeNav, 5000);

    $("#videos-sidebar ul li").on("click", function() {
        //console.log("clicked");
        $("#videos-sidebar ul .selected").removeClass('selected');
        $(this).addClass('selected');
    });

    var isOpen = false;
    $("#videos-sidebar-btn").on("click", function () {
        if (isOpen) {
            closeNav();
            isOpen = false;
        } else {
            openNav();
            isOpen = true;
        } 
    });

    function openNav() {
        document.getElementById("videos-sidebar").style.width = "280px";
        document.getElementById("videos-sidebar-btn").style.left = "280px";
        $("#videos-sidebar-btn").html('<i class="fas fa-chevron-left"></i>');
    }

    function closeNav() {
        document.getElementById("videos-sidebar").style.width = "0";
        document.getElementById("videos-sidebar-btn").style.left = "0";
        $("#videos-sidebar-btn").html('<i class="fas fa-chevron-right"></i>');
    }

    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }

    /* function scrollToElement(element) {
        //console.log("clicados");
        var targetId = element.getAttribute("data-scroll");
        //console.log("targetId: " + targetId);
        var targetElement = document.getElementById(targetId);
        targetElement.scrollIntoView();
        targetElement.scrollTop -= 90;
    } */

    function scrollToElement(element) {
        //console.log("clicados");
        var targetId = element.getAttribute("data-scroll");
        //console.log("targetId: " + targetId);
        var element = document.getElementById(targetId);
        const yOffset = -120; 
        const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
        window.scrollTo({top: y, behavior: 'smooth'});
    }


    
    var isInTheViewport = function (elem) {
        var hT = elem.offset().top,
                hH = elem.outerHeight(),
                wH = $(window).height(),
                wS = $(this).scrollTop();
                //console.log ("ht:" + hT + " hH:" + hH + " wH:" + wH + " wS:" + wS);
        return  (wS > (hT-wH) && (hT > wS) && (wS+wH > hT+hH));
    };

    function highlightItem(element) {
        if (isInTheViewport(element)) {
            $("#videos-sidebar ul .selected").removeClass('selected');
            //console.log( element.data("highlight"));
            $("#" + element.data("highlight")).addClass('selected');
        }	
    }

    var videoElements = $(".my-video-container").find(".video-padding-container");

    $(window).scroll(function() {
        videoElements.each(function(index, elem) {
            var jElem = $(elem);
            //highlightItem(jElem);
        });
    });		

    $(function() {
        /* 	document.getElementsByClassName("container-fluid").style= "overflow: unset !important";
        $(".container-fluid").css("overflow-x", "unset !important;"); */
        
        $(window).on('scroll', function() {
            var defaultTopPadding = 84; //84px;
            var scrollTop = $(this).scrollTop();
            var topDistance = $('#video-content-1').offset().top;
            var isOnTop = false;
            //console.log("scrol:" + scrollTop);
            //console.log("top dist:" + topDistance);
            //console.log("diferenca: " +  (topDistance - scrollTop));
            if ( topDistance - scrollTop < defaultTopPadding ) {
                isOnTop = true;
            } else {
                isOnTop = false;
            }
            //console.log($(".navbar").height());
            if (isOnTop) $('#to-top-btn').fadeIn();
            else  $('#to-top-btn').fadeOut();
        });

      /*   vimeoPlayers = [];
		var isVideoPlaying = false;
		var children = $(".my-video-container").find("iframe");
		children.each(function() {
			//console.log("iframe " + $(this)[0].getAttribute('id'));
			iframe = $(this)[0];
			videoId = $(this)[0].getAttribute('id');
			vimeoPlayers[videoId] = new Vimeo.Player(iframe);
		});

        console.log("vimeo", vimeoPlayers);

        $(".carousel").bind('slid.bs.carousel', function(){
            currentIndex = $(this).find('.carousel-item.active').index();
            currentTitle = $('.carousel-item.active').data("title");
            console.log("current index:" + currentIndex);
            console.log("current title:" + currentTitle);
            vimeoPlayers[currentIndex].getVideoTitle().then(function(title) {
                console.log('title:', title);
            });
        }); */
       
    });

  
</script>
