<div class="my-container">
    <div class="row mx-md-5 mt-md-5 mb-md-1">
        <div class="col">
            <h2 style="text-align: center;">blog ({{$page == 'news' ? 'yes' : 'no'}})</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div style="width: fit-content; margin: auto;">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                  </div>
            </div>      
        </div>
    </div>
    
    <div class="row m-md-5">
        <div class="col-md-9">
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="post row m-md-5">
                <div class="col-md-5">
                    <div class="imagem"></div>
                </div>
                <div class="col-md-7">
                    <p class="titulo">As 10 melhores faculdades de Medicina do país</p>
                    <p class="autor"><i class="far fa-clock" style="color: #3eb378;"></i> Postado em 07/12/2020 às 14:12</p>
                    <p class="conteudo">O Enem está chegando, outros vestibulares também, e você já está se preparando há algum tempo para enfrentar estas provas, né? Com os estudos que você tem mantido (principalmente se for aluno Biologia Aprova)...</p>
                </div>
            </div>
            <div class="row m-md-5">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                        </a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
            </div>
        </div>
        <div class="col-md-3">
            <div class="ultimos-posts">
                <div class="input-group mb-md-3">
                    <input type="text" class="form-control" placeholder="Pesquisar" aria-label="Pesquisar" aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
                </div>
                <h4 class="mt-md-5 mb-md-4">Últimas Postagens</h4>
                <img class="wavy-line px-md-5" src="img/long-wavy-line.png"  alt="biologia aprova - linha ondulada">
                <div class="post-menor mb-md-3">
                    <div class="imagem">
                    </div>
                    <p class="title">As 10 melhores faculdades de Medicina do país</p>
                </div>
                <div class="post-menor mb-md-3">
                    <div class="imagem">
                    </div>
                    <p class="title">As 10 melhores faculdades de Medicina do país</p>
                </div>
                <div class="post-menor mb-md-3">
                    <div class="imagem">
                    </div>
                    <p class="title">As 10 melhores faculdades de Medicina do país</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row m-md-5">
        <div class="col">
            <div class="banner-fundo"></div>
        </div>
    </div>
</div>