<style>
    .bricks--component {
        width: 100% !important;
    }
</style>

<div class="my-home-container">
    <!-- PROFESSORA 1 -->
    <div id="section-1" class="section-1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col_bloco_1 col-md-5 mb-md-5 mb-lg-5 position-relative">
                    <div class="d-flex w-100 h-100 justify-content-center mt-4 align-items-center">
                        <div class="bloco_1 m-auto bloco"
                            style="display: flex;
						justify-content: space-around;
						flex-direction: column;
						align-content: flex-end;">
                            <p class="big-title-lp mx-2 mb-md-2 p1">Gabaritando Biologia</p>
                            <!-- <p class="big-title-lp-2 mx-2 p2">Intensivão do ENEM</p> -->
                            {{-- <p class="mx-2" style="    margin-bottom: .6rem;
								color: #198754;
								font-size: 38px;
								line-height: 38px;">INTENSIVÃO ENEM</p> --}}
                            <p class="mx-2"
                                style="    margin-bottom: .6rem;color: #198754;font-size: 38px;line-height: 38px;">CURSO
                                COMPLETO</p>
                            {{-- <p class="mx-2" style="margin-bottom: .6rem;font-size: 21px;line-height: 25px;color: #7e7e7e;">Inclui conteúdos Bônus até {{date('d/m',strtotime(config('constants.outras.deadline_aprova_mais')))}}</p> --}}
                            {{-- <p class="mx-2" style="margin-bottom: .6rem;font-size: 21px;line-height: 25px;color: #7e7e7e;">Inscrições Encerradas</p> --}}
                            {{-- <p class="mx-2" style="margin-bottom: .6rem;font-size: 24px;font-weight: 800;line-height: 25px;color: #01253d;">Aguarde Turmas de Revisão</p> --}}
                            <!-- <p class="mx-2" style="margin-bottom: .6rem;font-size: 21px;line-height: 25px;color: #7e7e7e;">Inscrições de 30/09 a 06/10</p> -->
                            <a href="#section-5" class="btn blue-btn rounded-pill"
                                style="padding: 10px 20px;margin: auto;width: 200px;">assine já!</a>
                            <!-- <p class="mx-2" style="margin-bottom: .6rem; font-size: 0.9rem;">dia 30/09 às 20h35min</p> -->
                            <form id="landing-pange-form" class="mx-2">
                                <!-- <div class="mb-2">
        <label for="input-nome" class="form-label">Nome*</label>
        <input type="text" class="form-control" id="input-nome" aria-describedby="nomeHelp">
        </div> -->
                                <!-- <div class="my-4 mx-5" style="height: 60px;">
         <label for="input-email" class="ms-3 form-label">Informe seu melhor email</label>
         <input type="email" name="email" class="form-control" id="input-email" aria-describedby="emailHelp">
         <div id="emailHelp" class="text-center form-text text-danger" style="display: none;">Informe um email válido!</div>
        </div> -->
                                {{-- <div class="mb-3 form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">De acordo com as Leis 12.965/2014 e 13.709/2018, que regulam o uso da Internet e o tratamento de dados pessoais no Brasil, autorizo Biologia Aprova a enviar notificações por e-mail ou outros meios e concordo com sua Política de Privacidade.</label>
									<label class="form-check-label-2">Ao informar meus dados, eu concordo com a <a href="" target="_blank">Política de Privacidade</a> e com os <a href="{{url::to('termo-uso')}}" target="_blank">Termos de Uso</a>.</label>
								</div> --}}
                                <div role="main" id="vagas-esgotadas-c5a718a853d7a0d3e9a1"></div>
                                <!-- <div class="d-grid my-3">
         <button class="btn btn-primary rounded-pill green-btn" style="padding: 0px;
									max-width: unset;
									line-height: 17px;
									width: 100%;
									height: 54px;
									font-size: 1rem;" type="button" onclick="handl_gabaritando_click();">quero me inscrever<br>gratuitamente neste evento</button>
        </div> -->
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="bloco_2 position-relative">
                        <img class="position-absolute bg_banner_top bg-transition"
                            src="{{ url::to('img/bg_banner_1.png') }}" alt="biologia aprova - fundo" />
                        <img class="position-absolute" id="professora_1" src="{{ url::to('img/professora.png') }}"
                            alt="biologia aprova - professora mary ann" />
                        <img class="position-absolute el_circle circle-transition"
                            src="{{ url::to('img/circle.png') }}" alt="biologia aprova - círculo" />
                        <img class="position-absolute" id="el_wavy_1" src="{{ url::to('img/wavy-line.png') }}"
                            alt="biologia aprova - linha ondulada" />
                        <img class="position-absolute name-transition" id="el_bean_1"
                            src="{{ url::to('img/bean.png') }}" alt="biologia aprova - nome" />
                        <img class="position-absolute" id="el_bean_2" src="{{ url::to('img/bean.png') }}"
                            alt="biologia aprova - jujuba" />
                        <img class="position-absolute" id="el_seta_v" src="{{ url::to('img/seta_vertical.png') }}"
                            alt="biologia aprova - setas verticais" />
                        <div class="line-2 slide-bar position-absolute"></div>
                        <p class="position-absolute name-transition-1 bg_line_top"
                            style="text-align: center; color:white; font-weight: 700;">Prof<sup>a</sup>. Mary Ann
                            Saraiva</p>
                        <p class="position-absolute bg_line_top name-transition-2"
                            style="text-align: center; color:white; background-color: #01253d; font-weight: 700;">
                            Prof<sup>a</sup>. Mary Ann Saraiva</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ITENS -->
    <!-- <div class="section-3" id="itens-scroll">
  <div class="container">
   <div class="row justify-content-center home-column-item">
    <div class="col-md-12">
     <div style="display: flex">
     <img  src="{{ url::to('img/item.png') }}" alt="biologia aprova - item 1">
     <p>O workshop Gabaritando Biologia acontecerá no dia 30/09 às 20:35 e será 100% online e 100% gratuito.</p>
     </div>
    </div>
    <div class="col-md-12">
     <div style="display: flex">
     <img  style="animation-delay: .2s;" src="{{ url::to('img/item.png') }}"  alt="biologia aprova - item 2">
     <p style="animation-delay: .4s;">Nele eu vou ensinar o exato passo a passo de como você pode se preparar para GABARITAR Biologia no ENEM.</p>
     </div>
    </div>
    <div class="col-md-12">
     <div style="display: flex">
     <img  style="animation-delay: .2s;" src="{{ url::to('img/item.png') }}"  alt="biologia aprova - item 2">
     <p style="animation-delay: .4s;">Eu vou te guiar nesse processo e te mostrar exatamente o que estudar nesta reta final.</p>
     </div>
    </div>
   </div>
  </div>
 </div> -->

    <!-- O QUE É -->
    <div id="section-3" class="section-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 bloco">
                    <p class="big-title">o que é o Biologia Aprova?</p>
                </div>
            </div>
            <div class="row my-2 justify-content-center">
                <div class="col-md-7">
                    <div class="container wavy-line"><img src="{{ url::to('img/long-wavy-line.png') }}"
                            alt="biologia aprova - linha ondulada"></div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="bloco-texto">
                        <p class="text-content" style="text-align: center;">Biologia Aprova é uma plataforma de ensino
                            de Biologia para ENEM 100% on-line que oferece conteúdo teórico e prático totalmente focado
                            na preparação do vestibulando e cuidadosamente pensada para quem quer GABARITAR Biologia no
                            ENEM.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- PROFESSORA 2 -->
    <div id="section-2" class="section-2" style="margin-bottom: 3.5rem;">
        <div class="row position-relative">
            <div class="parallax shape-1">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-5 col-xxl-3"></div>
                        <div class="col-md-7">
                            <div class="bloco-texto">
                                <h2 class="big-title">quem é Mary Ann Saraiva?</h2>
                                <div id="professora-scroll" class="line my-2"
                                    style="width: 17%; border-color: #01253d; opacity: 0;"></div>
                                <p
                                    style="margin-top: 30px;font-size: 18px;font-family: 'Nunito';line-height: 28px; text-align: justify;">
                                    Mary Ann Saraiva é apaixonada por biologia e leciona a matéria há mais de 30 anos.
                                    Sua especialidade é ensinar jovens aspirantes a universitários a como se preparar
                                    para dominar o assunto. Ela acredita num modelo de ensino divertido, didático e
                                    descomplicado no qual os alunos aprendem de maneira descontraída e retém o
                                    conhecimento por muito mais tempo. Seu maior objetivo é ajudar estudantes e
                                    vestibulandos a gabaritar biologia no ENEM.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="circle position-absolute">
                </div>
                <img id="professora_2" class="position-absolute" src="{{ url::to('img/professora_2.png') }}"
                    alt="biologia aprova - professora Mary Ann" style="opacity: 0;" />
            </div>
        </div>
    </div>

    <!-- PLANOS DE PAGAMENTO -->
    <div id="section-5" class="section-5 container" style="margin-bottom: 5%;">
        <div class="row justify-content-center align-items-center">
            <div class="desktop col-md-6">
                <p class="plano-title" style="text-align: left;">Todo o conteúdo que você precisa saber para gabaritar
                    biologia estará disponível para você.</p>
                <div class="wavy-line wavy-line-alt mt-3 mb-3"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
                <div class="plano-vantagens mb-4">
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Aulas sobre todos os assuntos de biologia;
                    </p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Matéria organizada de forma sequencial,
                        lógica;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Metodologia didática viva, divertida e
                        exemplificativa para você assimilar o conteúdo;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Material de auxílio para revisão rápida e
                        eficiente;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Paródias que vão grudar biologia na sua
                        cabeça.</p>
                </div>
                <br>
                {{-- <p class="plano-title"  style="text-align: left;font-weight: 400;font-size: xx-large;">Bônus <span style="font-size: initial;color: #0aab6c;font-style: initial;">(para inscrições até {{date('d/m',strtotime(config('constants.outras.deadline_aprova_mais')))}})</span></p>
					<div class="wavy-line wavy-line-alt mt-3 mb-3"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada"></div>
					<div class="plano-vantagens mb-4">
						<p style="font-size: 1rem;"><i class="fas fa-check"></i>1 plantão (1h) de dúvida por mês com tema (total de 8 encontros);</p>
						<p style="font-size: 1rem;"><i class="fas fa-check"></i>4 simulados tipo Enem por ano (novidade TriEduc) com correção das provas e relatórios de desempenho;</p>
						<p style="font-size: 1rem;"><i class="fas fa-check"></i>1 acesso ao Intensivo por partes para revisão do conteúdo;</p>
						<p style="font-size: 1rem;"><i class="fas fa-check"></i>2 aulões ao vivo de revisão;</p>
						<p style="font-size: 1rem;"><i class="fas fa-check"></i>3 Diagnose de biologia: simulados para identificar dificuldades e quais assuntos precisam ser revistos.</p>
					</div> --}}
            </div>
            <div class="mob col-sm-12" style="display:none">
                <p class="plano-title" style="text-align: left;">Todo o conteúdo que você precisa saber para gabaritar
                    biologia estará disponível para você.</p>
                <div class="wavy-line mt-3 mb-3"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
                <div class="plano-vantagens mb-4">
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Aulas sobre todos os assuntos de biologia;
                    </p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Matéria organizada de forma sequencial,
                        lógica;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Metodologia didática viva, divertida e
                        exemplificativa para você assimilar o conteúdo;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Material de auxílio para revisão rápida e
                        eficiente;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>Paródias que vão grudar biologia na sua
                        cabeça.</p>
                </div>
                <br>
                <p class="plano-title" style="text-align: left;font-weight: 400;font-size: xx-large;">Bonus <span
                        style="font-size: initial;color: #0aab6c;font-style: initial;">(para inscrições até
                        {{ date('d/m', strtotime(config('constants.outras.deadline_aprova_mais'))) }})</span></p>
                <div class="wavy-line mt-3 mb-3"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
                <div class="plano-vantagens mb-4">
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>1 plantão (1h) de dúvida por mês com tema
                        (total de 8 encontros);</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>4 simulados tipo Enem por ano (novidade
                        TriEduc) com correção das provas e relatórios de desempenho;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>1 acesso ao Intensivo por partes para
                        revisão do conteúdo;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>2 aulões ao vivo de revisão;</p>
                    <p style="font-size: 1rem;"><i class="fas fa-check"></i>3 Diagnose de Biologia: simulados para
                        identificar dificuldades e quais assuntos precisam ser revistos.</p>
                </div>
            </div>
            <div class="col-md-5 plano-container">
                <div class="plano position-relative m-md-auto plano-principal">
                    <div class="container plano-header align-items-center mt-5">
                        <p class="pt-3 mx-auto">Curso</p>
                        <p class="mb-auto mx-auto">Completo</p>
                    </div>
                    <div class="plano-body m-auto px-5 border bg-light border-2 bloco">
                        <p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 12rem;">assine já!
                        </p>
                        <div id="plano-scroll" class="vantagens mb-3">
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;</p>
                            {{-- <p style="font-size: 1rem;"><i class="fas fa-check"></i>plantão MED ao vivo toda semana;</p> --}}
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os assuntos de
                                biologia;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de questões;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e
                                gabaritados;</p>
                            {{-- <p style="font-size: 1rem;"><i class="fas fa-check"></i>videoaulas aprofundadas;</p> --}}
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e
                                comentados;</p>
                            {{-- <p style="font-size: 1rem;"><i class="fas fa-check"></i>cronograma de estudo;</p> --}}
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para memorização;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
                        </div>
                        <p style="font-size: 1.7rem;">por apenas</p>
                        <span>
                            <h2 class="position-relative">
                                <span class="position-absolute end-100 plano-preco-a">R$</span>
                                21
                                <span class="position-absolute start-100 plano-preco-b">,90</span>
                                <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
                            </h2>
                        </span>
                    </div>
                    <div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
                        <button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();"
                            style="font-size: xx-large;">eu quero!</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-none">
                <div class="plano position-relative m-md-auto plano-principal"
                    style="width: 272px; margin-bottom: 10% !important;">
                    <div class="container plano-header align-items-center"
                        style="background-image: linear-gradient(to right, rgb(13 110 253) , rgb(65 134 236));">
                        <p class="pt-3 mx-auto">curso</p>
                        <p class="mb-auto mx-auto">completo</p>
                    </div>
                    <div class="plano-body m-auto px-5 border bg-light border-2 bloco" style="height: 686px;">
                        <p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 14.2rem;"></p>
                        <div class="vantagens mb-3">
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>10 meses de acesso;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e
                                gabaritados;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeo resoluções de questões;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>videoaulas aprofundadas;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e
                                comentados;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>cronograma de estudo;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas;</p>
                            <p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
                        </div>
                        <p style="font-size: 1.7rem;">por apenas</p>
                        <span>
                            <h2 class="position-relative">
                                <span class="position-absolute end-100 plano-preco-a">R$</span>
                                21
                                <span class="position-absolute start-100 plano-preco-b">,90</span>
                                <span class="position-absolute start-100 plano-preco-c">/MÊS</span>
                            </h2>
                        </span>
                    </div>
                    <div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
                        <button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();"
                            style="font-size: xx-large;">eu quero!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <!-- <div class="line"></div> -->
    </div>

    <!-- DEPOIMENTOS -->
    <div id="section-6" class="section-6">
        <div class="row justify-content-center">
            <div class="col-md-7 bloco">
                <p class="big-title">cases de sucesso</p>
            </div>
        </div>
        <div class="row my-2 justify-content-center">
            <div class="col-md-7 mb-5">
                <div class="container wavy-line"><img src="{{ url::to('img/long-wavy-line.png') }}"
                        alt="biologia aprova - linha ondulada"></div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div id="homeBannerBottom" class="carousel slide" data-bs-ride="carousel">
                    <ol class="carousel-indicators">
                        @isset($depoimentos)
                            @forelse ($depoimentos as $key => $depoimento)
                                <li data-bs-target="#homeBannerBottom" data-bs-slide-to="{{ $key }}"
                                    class="{{ $key ? '' : 'active' }}"></li>
                            @empty
                            @endforelse
                        @endisset
                    </ol>
                    <div class="carousel-inner">
                        @isset($depoimentos)
                            @forelse ($depoimentos as $key=>$depoimento)
                                <div class="carousel-item {{ $key ? '' : 'active' }}">
                                    <div class="row">
                                        <div class="col-md-3 p-0">
                                            <div class="image"
                                                style="background-image:url('img/{{ $depoimento->img }}')">
                                            </div>
                                            <div class="image-mobile">
                                                <div style="background-image:url('img/{{ $depoimento->img }}');"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-9 depoimento">
                                            <p>{{ $depoimento->depoimento }}</p>
                                            <br>
                                            <h3>{{ $depoimento->nome }}</h3>
                                            <p style="font-size: small;">{{ $depoimento->vestibular }}</p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="aprovado-btn row justify-content-center mb-md-5">
        <div class="col d-flex">
            <a class="btn blue-btn rounded-pill m-auto" type="button" href="#section-5"
                style="padding: 10px 20px;width: 325px;">quero assinar</a>
        </div>
    </div>

    <!-- FAQ -->
    <!-- <div id="section-questions">
   <div class="container">
    <div class="row justify-content-center">
    <div class="col-md-10">
     <div class="faq-questions-title row justify-content-center align-items-center">
      <div class="col">
       <h2 class="big-title">F.A.Q. - perguntas frequentes</h2>
      </div>
     </div>
     <div class="faq-questions-wavy row justify-content-center">
      <div class="col-md-7 mt-2 mb-5">
       <div class="container wavy-line"><img src="{{ url::to('img/long-wavy-line.png') }}" alt="biologia aprova - linha ondulada"></div>
      </div>
     </div>
     <div class="accordion accordion-flush" id="accordionFlushExample">
      @forelse ($perguntas as $key=>$pergunta)
<div class="accordion-item my-md-3" id="faq-{{ $pergunta->id }}">
       <h2 class="accordion-header" id="flush-headingOne">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{ $pergunta->id }}" aria-expanded="false" aria-controls="flush-collapseOne">
         <div style="display: flex;">
          <div class="barra" style="background: #3cab73"></div>
          <span class="letra" style="color: #3cab73">p.</span>
          <span class="content  ms-md-4">{{ $pergunta->pergunta }}</span>
         </div>
        </button>
       </h2>
       <div id="flush-collapse{{ $pergunta->id }}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
        <div class="accordion-body" style="display: flex;">
         <div class="barra" style="background: #adabab"></div>
         <span class="letra my-1" style="color: #adabab">r.</span>
         <div style="display: inline-flex;">
          <p class="ms-md-4">{!! $pergunta->resposta !!}</p>
         </div>
        </div>
       </div>
      </div>
@empty
      <p style="color: gray; text-align:center">não encontramos o que você está procurando <i class="fas fa-sad-tear"></i></p>
@endforelse
     </div>
    </div>
   </div>
   </div>
  </div> -->
</div>

<script type="text/javascript"
    src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>

<script type="text/javascript">
    new RDStationForms('vagas-esgotadas-c5a718a853d7a0d3e9a1', 'UA-113092297-1').createForm();
    // $("#conversion-form-vagas-esgotadas").bind('ajax:complete', handl_gabaritando_click);

    const BASE_URL = "{{ url('/') }}";

    function goToArticle(elem) {
        var link = $(elem).data("link");
        window.open(link, '_blank');
    }

    function goToPlanos() {
        window.location.href = "{{ url::to('/#section-5') }}";
    }

    function goToLogin() {
        if (
            @if (isset($user->plano))
                1
            @else
                0
            @endif )
            window.location.href = "{{ url::to('/planos/choose/5') }}";
        else
            window.location.href = "{{ url::to('/planos/login-page') }}";
    }

    function handl_gabaritando_click() {

        let email = $("input[name='email'").val();
        var concordo = false;
        if ($('#exampleCheck').is(":checked")) {
            concordo = true;
        }

        console.log("email", email);
        console.log("concordo", concordo);

        alert("teste");

        // if(!isEmail(email)) {
        // 	$("#landing-pange-form #input-email").focus();
        // 	$("#emailHelp").fadeIn();
        // 	return;
        // }

        $.ajax({
            type: 'POST',
            url: BASE_URL + "/lp/save",
            data: {
                email: email,
                concordo: concordo
            },
            dataType: 'JSON'
        }).done((res) => {
            //console.log("resp", res);
            if (res.success)
                window.open('https://conteudo.biologiaaprova.com.br/insc_intensivo_202109', '_blank');
            else
                console.log("algo errado!");
        }).fail((err) => console.log(err));

    }

    function isEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    var isInTheViewport = function(elem) {
        if (!elem.offset())
            return;

        var hT = elem.offset().top,
            hH = elem.outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        //console.log ("ht:" + hT + " hH:" + hH + " wH:" + wH + " wS:" + wS);
        return (wS > (hT - wH) && (hT > wS) && (wS + wH > hT + hH));
    };

    function isInViewport(element) {
        var rect = element.getBoundingClientRect();
        var html = document.documentElement;
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || html.clientHeight) &&
            rect.right <= (window.innerWidth || html.clientWidth)
        );
    }

    $(window).scroll(function() {

        if (isInTheViewport($('#professora-scroll'))) {
            $('#professora_2').addClass('show-up');
            $('.line').addClass('grow-right');
        }

        if (isInTheViewport($('#itens-scroll'))) {
            var itens = document.querySelectorAll('.home-column-item img');
            itens.forEach(element => {
                element.setAttribute('class', 'show-up');
                console.log(element);
            });

            var paragraphs = document.querySelectorAll('.home-column-item p');
            paragraphs.forEach(element => {
                element.setAttribute('class', 'slide-left');
                console.log(element);
            });
        }

        if (isInTheViewport($('#video-scroll'))) {
            if (!isVideoPlaying) $('.section-4 #hv-carousel').addClass('float');
        } else {
            $('.section-4 #hv-carousel').removeClass('float');
        }

        if (isInTheViewport($('#plano-scroll'))) {
            $('.plano-body').addClass('come-up');
        } else {}

    });

    $(document).ready(function() {

        //$(".bricks-form__submit").on("click", handl_gabaritando_click);

        function onPlay() {
            isVideoPlaying = true;
            $('.section-4 #hv-carousel').removeClass('float');
        }

        function onPause() {
            $('.section-4 #hv-carousel').removeClass('float');
            isVideoPlaying = false;
        }

        function onEnded() {
            $('.section-4 #hv-carousel').addClass('float');
            isVideoPlaying = false;
        }

        vimeoPlayers = [];
        var isVideoPlaying = false;
        var children = $(".home-2-video").find("iframe");
        children.each(function() {
            //console.log("iframe " + $(this)[0].getAttribute('id'));
            iframe = $(this)[0];
            videoId = $(this)[0].getAttribute('id');
            vimeoPlayers[videoId] = new Vimeo.Player(iframe);
            vimeoPlayers[videoId].on('play', onPlay);
            vimeoPlayers[videoId].on('pause', onPause);
            vimeoPlayers[videoId].on('ended', onEnded);
        });

        $("#hv-carousel").bind('slid.bs.carousel', function() {
            //currentIndex = $('.carousel-item.active').index();
            currentTitle = $('.carousel-item.active').data("title");
            $(".home-2-text p").fadeOut(function() {
                $(".home-2-text p").html(currentTitle);
                //$(".home-2-text p").html(currentTitle.replace(/ /g, "<br>"));
                $(".home-2-text p").fadeIn();
            });
        });

        $("#input-email").on('focus', (e) => {
            console.log("estou aqui");
            $("#emailHelp").fadeOut();
        })
    });
</script>
