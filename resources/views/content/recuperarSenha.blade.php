<style>
.recuperar-senha-box .warning-text {
    color: green;
}
</style>
<div  class="row">
    <div class="col" style="height: 84px; background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118)); color: white; display: flex;">
        <h2 class="big-title" style="margin: auto;">redefinir senha</h2>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center mt-5"style="height: 100vh">
        <div class="recuperar-senha-box" style="width:400px">
            <form id="recuperar-senha-form" action="{{url::to('redefinir')}}" name="form_recuperar_senha">
                <div class="mb-3 has-validation position-relative">
                    <label for="nova-senha" class="form-label" id="label-nova-senha">nova senha*:</label>
                    <input type="password" name="nova_senha"  class="form-control" autocomplete="new-password" id="nova-senha" aria-describedby="label-nova-senha">
                    <div id="login-email-fb" class="invalid-tooltip">
                        Por favor, digite uma senha válida.
                    </div>
                </div>
                <div class="mb-3 has-validation position-relative">
                    <label for="confirmar-nova-senha" class="form-label" id="label-confirmar">confirmar nova senha*:</label>
                    <input type="password" name="confirmar_nova_senha" autocomplete="new-password" class="form-control" id="confirmar-nova-senha" aria-describedby="label-confirmar">
                    <div id="login-pass-fb" class="invalid-tooltip">
                        As senhas não combinam.
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" style="width: 100%;">
                    <div class="row" >
                    <div id="btn-text" class="col-6"  style="text-align: left;font-weight: 600;">redefinir senha</div>
                    <div class="col-6" style="text-align: right;"><i class="fas fa-long-arrow-alt-right"></i></div>
                    </div>
                </button>
                <div id="warning-box" class="align-self-center mt-3" style="display: none;">
                    <p class="warning-text"></p>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

    $('input').on('input', function() {
        $(this).removeClass('is-invalid');
        $(this).removeClass('is-valid');
    });

    $("#recuperar-senha-form").submit(function(e){
        e.preventDefault();
        e.stopPropagation();

        $("#recuperar-senha-form #btn-text").html("aguarde...");

        var url = $(this).attr('action');

        var senha = $("#nova-senha");
        var confirmaSenha = $("#confirmar-nova-senha");

        var cancel = false;
        var input = null;

        if (senha.val() != confirmaSenha.val()) {
                cancel = true;
                input = confirmaSenha;
            }

        if (senha.val() == "") {
            cancel = true;
            input = senha;
        }

        var formSerialized = $(this).serializeArray();
        formSerialized.push({name: 'token', value: "{{$token}}"});
           
        if (cancel) {
            input.addClass("is-invalid");
            input.focus();
        } else {
            $.post($(this).attr('action'), formSerialized, function(data) {
                console.log("data: " + data.message);
                if (data.success) {
                    $(".warning-text").css('color', 'green');
                    $(".warning-text").html(data.message);
                    $("#warning-box").fadeIn();
                } else {
                    $(".warning-text").css('color', 'red');
                    $(".warning-text").html(data.message);
                    $("#warning-box").fadeIn();
                    //console.log("cadastro falhou!!");
                }
            })
            .fail(function() {
                $(".warning-text").css('color', 'red');
                $(".warning-text").html("Redefinição de senha falhou! Por favor, tente mais tarde.");
                $("#warning-box").fadeIn();
                //console.log("cadastro falhou!!")
            })
            .always(function(){
                $("#recuperar-senha-form #btn-text").html("redefinir senha");
            });
        }

    });
</script>