<div class="my-home-container">
	<!-- PROFESSORA 1 -->
	<div class="section-1">
		<div class="row justify-content-center">
			<div class="col_bloco_1 col-md-5 mb-md-5 mb-lg-5 position-relative">
				<div class="bloco_1 bloco name-transition-1">
					<p class="big-title mx-2 p1">aprovar você no vestibular</p>
					<p class="big-title mx-2 p2" style="font-weight: 500;">é o nosso propósito!</p>
					<div class="d-grid my-3"><button onclick="goToPlanos();" class="btn btn-primary rounded-pill d-grid green-btn" style="padding: 0px;font-size: 1.4rem;" type="button">quero ser aprovado!</button></div>
				</div>
				<div class="bloco_1_2 name-transition-2 position-absolute">
					<p class="big-title mx-2 text-border pb-3">+1500</p>
					<p class="big-title mx-2">aprovados em medicina</p>
					<div class="d-grid my-2 me-md-5 me-lg-5"><button onclick="goToPlanos();" class="btn btn-primary rounded-pill d-grid green-btn" style="padding: 0px;font-size: 1.4rem;" type="button">quero ser aprovado!</button></div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="bloco_2 position-relative">
					<img class="position-absolute bg_banner_top bg-transition" src="{{url::to('img/bg_banner_1.png')}}"  alt="biologia aprova - fundo"/>
					<img class="position-absolute img-transition" id="professora_1" src="{{url::to('img/professora.png')}}"  alt="biologia aprova - professora mary ann"/>
					<img class="position-absolute el_circle circle-transition" src="{{url::to('img/circle.png')}}"  alt="biologia aprova - círculo"/>
					<img class="position-absolute" id="el_wavy_1" src="{{url::to('img/wavy-line.png')}}"  alt="biologia aprova - linha ondulada"/>
					<img class="position-absolute name-transition" id="el_bean_1" src="{{url::to('img/bean.png')}}"  alt="biologia aprova - nome"/>
					<img class="position-absolute" id="el_bean_2" src="{{url::to('img/bean.png')}}"  alt="biologia aprova - jujuba"/>
					<img class="position-absolute" id="el_seta_v" src="{{url::to('img/seta_vertical.png')}}"  alt="biologia aprova - setas verticais"/>
					<div class="line-2 slide-bar position-absolute"></div>
					<p class="position-absolute name-transition-1 bg_line_top" style="text-align: center; color:white; font-weight: 700;" >Prof<sup>a</sup>. Mary Ann Saraiva</p>	
					<p class="position-absolute bg_line_top name-transition-2" style="text-align: center; color:white; background-color: #01253d; font-weight: 700;" >Ariomar Jefferson</p>
				</div>
			</div>
		</div>
	</div>
		
		<!-- PROFESSORA 2 -->
		<div class="section-2" style="margin-bottom: 180px;">
		<div class="row position-relative">
			<div class="parallax shape-1">
				<div class="row justify-content-center">
					<div class="col-md-5 col-xxl-3"></div>
					<div class="col-md-5">
						<div class="bloco-texto">
							<h2 style="line-height: 38px; font-weight: 700; font-size: 2.5rem;">estude com a melhor professora de Biologia do Brasil</h2>
							<div id="professora-scroll" class="line my-2" style="width: 17%; border-color: #01253d; opacity: 0;"></div>
							<p style="margin-top: 30px;font-size: x-large; font-family: 'Nunito'; line-height: 28px;">mais de 30 anos de experiência, didática incrível e mais de 12 mil alunos aprovados no Enem e vestibulares.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="circle position-absolute">
				</div>
				<img id="professora_2" class="position-absolute" src="{{url::to('img/professora_2.png')}}" alt="biologia aprova - professora Mary Ann" style="opacity: 0;"/>
			</div>
		</div>
		</div>
		
		<!-- ITENS -->
		<div class="section-3">
		<div class="row justify-content-center">
			<div class="col-md-7 bloco">
				<p class="big-title">por que 12 mil alunos escolheram o Biologia Aprova?</p>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-7 mb-5">
				<div id="itens-scroll" class="container mt-3 wavy-line"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada"></div>
			</div>
		</div>
		
		<div class="row justify-content-center home-column-item">
			<div class="col-md-4">
				<div style="display: flex">
				<img  src="{{url::to('img/item.png')}}" alt="biologia aprova - item 1">
				<p>acesso ilimitado ao conteúdo da plataforma</p>
				</div>
			</div>
			<div class="col-md-4">
				<div style="display: flex">
				<img  style="animation-delay: .2s;" src="{{url::to('img/item.png')}}"  alt="biologia aprova - item 2">
				<p style="animation-delay: .4s;">mais de 300 videoulas aprofundadas e resolução de questões em vídeos</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center home-column-item">
			<div class="col-md-4">
				<div style="display: flex">
				<img  style="animation-delay: .4s;" src="{{url::to('img/item.png')}}"  alt="biologia aprova - item 3">
				<p style="animation-delay: .6s;">mais de 100 simulados comentados e gabaritados</p>
				</div>
			</div>
			<div class="col-md-4">
				<div style="display: flex">
				<img  style="animation-delay: .6s;" src="{{url::to('img/item.png')}}"  alt="biologia aprova - item 4">
				<p style="animation-delay: .8s;">e-books e músicas exclusivas</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center home-column-item">
			<div class="col-md-4">
				<div style="display: flex">
				<img style="animation-delay: .8s;" src="{{url::to('img/item.png')}}"  alt="biologia aprova - item 5">
				<p style="animation-delay: 1s;">exercícios resolvidos e comentados</p>
				</div>
			</div>
			<div class="col-md-4">
				<div style="display: flex">
				<img  style="animation-delay: 1s;" src="{{url::to('img/item.png')}}"  alt="biologia aprova - item 6">
				<p style="animation-delay: 1.2s;">material de apoio e muito mais!</p>
				</div>
			</div>
		</div>
		</div>
		
		<!-- VÍDEO -->
		<div id="section-4" class="section-4">
		<div id="row-section" class="row position-relative">
			<div class="home-2-video">
				<div style="display: flex">
						<div id="hv-carousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
							<div class="carousel-inner">
								@isset($videos)
								@foreach ($videos as $key=>$video)
								<div class="carousel-item {{$key ? '': 'active'}}" data-title="{{$video->area->nome}}">
										<iframe id="{{$key}}" class="d-block w-100" src="{{$video->src}}" width="582" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
								</div>
								@endforeach
								@endisset
							</div>
							<button class="carousel-control-prev-icon position-absolute btn"  aria-hidden="true" type="button" data-bs-target="#hv-carousel"  data-bs-slide="prev" >
							</button>
							<button class="carousel-control-next-icon position-absolute btn" aria-hidden="true" type="button" data-bs-target="#hv-carousel"  data-bs-slide="next">
							</button>
						</div>
				</div>
			</div>
			<div class="home-2 mt-5 justify-content-center parallax shape-2">
				<div class="row justify-content-center">
					<div class="col-md-8 mt-md-5 mt-sm-4 bloco">
						<p class="big-title" style="color: white !important;">assista algumas videoaulas grátis ;)</p>
						<div id="video-scroll" class="container mt-md-3 wavy-line"><img src="{{url::to('img/long-wavy-line-dark.png')}}"  alt="biologia aprova - linha ondulada escura"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="home-2-text">
							<div style="min-height: 160px; display:flex">
								<p>Introdução e Origem da Vida</p>
							</div>
							<img class="ms-auto mb-3" src="{{url::to('img/seta_horizontal.png')}}" alt="biologia aprova - setas para a direita"/>
							<button class="btn blue-btn rounded-pill" type="button" style="font-size:large; width:230px; height:45px;margin-left: auto;"><a class="stretched-link"   href="{{url::to('/videoaulas')}}"></a>ver todas as videoaulas</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<!-- REPORTAGENS -->
		<!-- <div class="section-7 container" style="display: none;">
			<div class="row justify-content-center">
				<div class="col-md-7 bloco">
					<p class="big-title">onde a gente já apareceu</p>
				</div>
			</div>
			<div class="row justify-content-center" style='margin-bottom:2rem;'>
				<div class="col-md-7">
					<div class="container mt-3 wavy-line"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada"></div>
				</div>
			</div>
				<div class="row reportagem-content justify-content-center">
					<div class="col-md-4 col-lg-3 align-self-center">
						<div class="card-wrapper" onclick="goToArticle(this);" data-link="https://g1.globo.com/pe/petrolina-regiao/noticia/2019/09/03/frio-nas-manhas-do-vale-do-sao-francisco-atrapalha-a-pescaria-artesanal.ghtml">
							<h2>globo</h2>
							<hr/>
							<div class="reportagem-img" style="background-image: url({{url::to('img/g1.png')}})"></div>
							<div  id="reportagem-scroll"></div>
						</div>
					</div>
					<div class="col-md-4 col-lg-3 align-self-center">
						<div class="card-wrapper" onclick="goToArticle(this);" data-link="https://globoplay.globo.com/v/7895795">
							<h2>mais você</h2>
							<hr/>
							<div class="reportagem-img" style="background-image: url({{url::to('img/maisvc.png')}})"></div>
						</div>
					</div>
					<div class="w-100"></div>
					<div class="col-md-4 col-lg-3 align-self-center">
						<div class="card-wrapper" onclick="goToArticle(this);" data-link="https://globoplay.globo.com/v/8747303">
							<h2>GRTV1</h2>
							<hr/>
							<div class="reportagem-img" style="background-image: url({{url::to('img/gr1.jpg')}})"></div>
						</div>
					</div>
					<div class="col-md-4 col-lg-3 align-self-center">
						<div class="card-wrapper" onclick="goToArticle(this);" data-link="https://www.carlosbritto.com/professora-mary-ann-alerta-populacao-para-manter-os-cuidados-contra-covid-19-com-a-reabertura-do-comercio">
							<h2>carlos britto</h2>
							<hr/>
							<div class="reportagem-img" style="background-image: url({{url::to('img/carlos_brito.png')}})"></div>
						</div>
					</div>
				</div>
		</div> -->
		
			<!-- REPORTAGENS -->
			<div class="section-7 container">
				<div class="row justify-content-center">
					<div class="col-md-7 bloco">
						<p class="big-title">onde a gente já apareceu</p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-7">
						<div class="container mt-3 wavy-line"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada"></div>
					</div>
				</div>
				<!-- <div style="width: 800px; margin: auto;" > -->
					<div class="reportagem-content-alt row justify-content-center">
							<div class="col-md-2 col-lg-2 reportagem-col">
								<div class="card-reportagem" onclick="goToArticle(this);" data-link="https://g1.globo.com/pe/petrolina-regiao/noticia/2019/09/03/frio-nas-manhas-do-vale-do-sao-francisco-atrapalha-a-pescaria-artesanal.ghtml">
									<img class="reportagem-img-alt" src="{{url::to('img/logo-g1.jpg')}}"/>
								</div>
							</div>
							<div class="col-md-2 col-lg-2 reportagem-col">
								<div class="card-reportagem" onclick="goToArticle(this);" data-link="https://globoplay.globo.com/v/7895795">
									<img class="reportagem-img-alt" src="{{url::to('img/maisvc.png')}}" style="max-height: 88px;"/>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 reportagem-col">
								<div class="card-reportagem" onclick="goToArticle(this);" data-link="https://globoplay.globo.com/v/8747303">
									<img class="reportagem-img-alt" src="{{url::to('img/grtv.png')}}"/>
								</div>
							</div>
							<div class="col-md-4 col-lg-3 reportagem-col">
								<div class="card-reportagem" onclick="goToArticle(this);" data-link="https://www.carlosbritto.com/professora-mary-ann-alerta-populacao-para-manter-os-cuidados-contra-covid-19-com-a-reabertura-do-comercio">
									<img class="reportagem-img-alt" src="{{url::to('img/carlos_brito.png')}}"/>
								</div>
							</div>
					</div>
				<!-- </div> -->
			</div>
		
		<!-- PLANOS DE PAGAMENTO -->
		<div id="section-5" class="section-5 container" style="margin-bottom: 5%;">
			<div class="row justify-content-center align-items-center">
				<div class="desktop col-md-3" >
					<p class="big-title" style="text-align: left;">vamos</p>
					<p class="big-title" style="text-align: left;">começar</p>
					<p class="big-title" style="text-align: left;">agora?</p>
					<p class="subtitle" style="text-align: left; margin: 0;">escolha o</p>
					<p class="subtitle" style="text-align: left;">seu plano!</p>
					<div class="wavy-line mt-3 mb-5"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada" style="width: 195px; margin:0"></div>
				</div>
				<div class="mob col-sm-12" style="display:none">
					<p class="big-title" style="text-align: center;">vamos começar agora?</p>
					<p class="subtitle" style="text-align: center;">escolha o seu plano!</p>
					<div class="wavy-line mt-3 mb-5"><img src="{{url::to('img/long-wavy-line.png')}}" alt="biologia aprova - linha ondulada" style="width: 195px;"></div>
				</div>
				<div class="col-md-5 plano-container">
					<div class="plano position-relative m-md-auto plano-principal">
						<div class="container plano-header align-items-center">
							<p class="pt-3 mx-auto">extensivo</p>
							<p class="mb-auto mx-auto">med</p>
						</div>
						<div class="plano-body m-auto px-5 border bg-light border-2 bloco">
							<p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 12rem;">mais escolhido!</p>
							<div id="plano-scroll" class="vantagens mb-3">
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>10 meses de acesso;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>plantão MED ao vivo toda semana;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e gabaritados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeo resoluções de questões;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>videoaulas aprofundadas;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e comentados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>cronograma de estudo;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
							</div>
							<h2 style="
									margin-top: 9px;
									font-size: 1.8rem;
									line-height: 25px;
									font-weight: 700;
									text-align: center;
									margin-left:0px;
									border: 1px solid;
									width: 100%;
									padding: 5px;
									border-radius: 5px;
									color: #0d6efdc2;
									text-shadow: -3px 2px 2px #0000001f;
									">
								aguarde<br>
								novas<br>
								turmas
								</h2>
							<!-- <p style="font-size: 1.7rem;">por apenas</p>
							<span><h2 class="position-relative">
									<span class="position-absolute end-100 plano-preco-a">R$</span>
									19
									<span class="position-absolute start-100 plano-preco-b">,00</span>
									<span class="position-absolute start-100 plano-preco-c">/MÊS</span>
								</h2>
							</span> -->
						</div>
						<div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
							<button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();" style="font-size: xx-large;">eu quero!</button>
						</div>
					</div>
				</div>
				<div class="col-md-4 d-none">
					<div class="plano position-relative m-md-auto plano-principal" style="width: 272px; margin-bottom: 10% !important;">
						<div class="container plano-header align-items-center" style="background-image: linear-gradient(to right, rgb(13 110 253) , rgb(65 134 236));">
							<p class="pt-3 mx-auto">extensivo</p>
							<p class="mb-auto mx-auto">enem</p>
						</div>
						<div class="plano-body m-auto px-5 border bg-light border-2 bloco" style="height: 686px;">
							<p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 14.2rem;"></p>
							<div class="vantagens mb-3">
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>10 meses de acesso;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e gabaritados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeo resoluções de questões;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>videoaulas aprofundadas;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e comentados;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>cronograma de estudo;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas;</p>
								<p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
							</div>
							<p style="font-size: 1.7rem;">por apenas</p>
							<span><h2 class="position-relative">
									<span class="position-absolute end-100 plano-preco-a">R$</span>
									16
									<span class="position-absolute start-100 plano-preco-b">,00</span>
									<span class="position-absolute start-100 plano-preco-c">/MÊS</span>
								</h2>
							</span>
						</div>
						<div class="d-grid mx-4" style="margin-top: -30px;width: inherit;">
							<button class="green-btn btn rounded-pill" type="button" onclick="goToLogin();" style="font-size: xx-large;">eu quero!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="line"></div>
		</div>
		 
		<!-- DEPOIMENTOS -->
		<div class="section-6" style="margin-top: 8%;margin-bottom: 4%;">
			<div class="row justify-content-center">
				<div class="col-md-7 bloco">
					<p class="big-title">quem faz, aprova!<img src="{{url::to('img/cup.jpg')}}"  alt="biologia aprova - taça" style="margin-bottom: 27px;"/></p>
					<p class="subtitle-2" style="margin-top: -20px;">veja o que os alunos falam sobre nós</p>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-7 mb-5">
					<div class="container wavy-line"><img src="{{url::to('img/long-wavy-line.png')}}"  alt="biologia aprova - linha ondulada"></div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div id="homeBannerBottom" class="carousel slide" data-bs-ride="carousel">
						<ol class="carousel-indicators">
							@isset ($depoimentos)
							@forelse ($depoimentos as $key => $depoimento)
							<li data-bs-target="#homeBannerBottom" data-bs-slide-to="{{$key}}" class="{{$key ? '': 'active'}}"></li>
							@empty
							@endforelse
							@endisset
						</ol>
						<div class="carousel-inner">
							@isset ($depoimentos)
							@forelse ($depoimentos as $key=>$depoimento)
							<div class="carousel-item {{$key ? '': 'active'}}">
								<div class="row">
									<div class="col-md-3 p-0">
										<div class="image" style="background-image:url('img/{{$depoimento->img}}')">
										</div>
										<div class="image-mobile">
											<div  style="background-image:url('img/{{$depoimento->img}}');"></div>
										</div>
									</div>
									<div class="col-md-9 depoimento">
										<p>{{$depoimento->depoimento}}</p>
											<br>
										<h3>{{$depoimento->nome}}</h3>
										<p style="font-size: small;">{{$depoimento->vestibular}}</p>
									</div>
								</div>
							</div>
							@empty
							@endforelse
							@endisset
						</div>
					</div>
				</div>		
			</div>
		</div>
		
		<div class="aprovado-btn row justify-content-center mb-md-5">
			<div class="col">
				<button class="btn blue-btn rounded-pill" type="button" onclick="goToPlanos()" >quero ser aprovado também!</button>
			</div>
		</div>
</div>

<script>

		function goToArticle(elem) {
			var link = $(elem).data("link");
			window.open(link, '_blank');
		}

		function goToPlanos() {
			window.location.href = "{{url::to('/#section-5')}}";
		}

		function goToLogin() {
			window.location.href = "{{url::to('/planos/login-page')}}";
		}

	$( document ).ready(function() {
		function onPlay() {
			isVideoPlaying = true;
			$('.section-4 #hv-carousel').removeClass('float');
		}

		function onPause() {
			$('.section-4 #hv-carousel').removeClass('float');
			isVideoPlaying = false;	
		}

		function onEnded() {
			$('.section-4 #hv-carousel').addClass('float');
			isVideoPlaying = false;	
		}

		vimeoPlayers = [];
		var isVideoPlaying = false;
		var children = $(".home-2-video").find("iframe");
		children.each(function() {
			//console.log("iframe " + $(this)[0].getAttribute('id'));
			iframe = $(this)[0];
			videoId = $(this)[0].getAttribute('id');
			vimeoPlayers[videoId] = new Vimeo.Player(iframe);
			vimeoPlayers[videoId].on('play', onPlay);
			vimeoPlayers[videoId].on('pause', onPause);
			vimeoPlayers[videoId].on('ended', onEnded);
		});

		$("#hv-carousel").bind('slid.bs.carousel', function(){
			//currentIndex = $('.carousel-item.active').index();
			currentTitle = $('.carousel-item.active').data("title");
			$(".home-2-text p").fadeOut(function(){
				$(".home-2-text p").html(currentTitle);
				//$(".home-2-text p").html(currentTitle.replace(/ /g, "<br>"));
				$(".home-2-text p").fadeIn();
			});
			//console.log("current index:" + currentIndex);
			//console.log("current title:" + currentTitle);
			/* vimeoPlayers[currentIndex].getVideoTitle().then(function(title) {
				console.log('title:', title);
			}); */
		});

	/* 	var iframe = document.querySelector('iframe');
		var player = new Vimeo.Player(iframe);
		var isVideoPlaying = false;

		player.on('play', onPlay);
		player.on('pause', onPause);
		player.on('ended', onEnded); */
		
		var isInTheViewport = function (elem) {
			var hT = elem.offset().top,
					hH = elem.outerHeight(),
					wH = $(window).height(),
					wS = $(this).scrollTop();
					//console.log ("ht:" + hT + " hH:" + hH + " wH:" + wH + " wS:" + wS);
			return  (wS > (hT-wH) && (hT > wS) && (wS+wH > hT+hH));
		};

		function isInViewport(element) {
			var rect = element.getBoundingClientRect();
			var html = document.documentElement;
			return (
				rect.top >= 0 &&
				rect.left >= 0 &&
				rect.bottom <= (window.innerHeight || html.clientHeight) &&
				rect.right <= (window.innerWidth || html.clientWidth)
			);
		}

		$(window).scroll(function() {

			if (isInTheViewport($('#professora-scroll'))) {
				$('#professora_2').addClass('show-up');
				$('.line').addClass('grow-right');
			}

			if (isInTheViewport($('#itens-scroll'))) {
				var itens = document.querySelectorAll('.home-column-item img');
				itens.forEach(element => {
					element.setAttribute('class', 'show-up');
					console.log(element);
				});

				var paragraphs = document.querySelectorAll('.home-column-item p');
				paragraphs.forEach(element => {
					element.setAttribute('class', 'slide-left');
					console.log(element);
				});	
			}

			if (isInTheViewport($('#video-scroll'))) {
				if (!isVideoPlaying) $('.section-4 #hv-carousel').addClass('float');
			} else {
				$('.section-4 #hv-carousel').removeClass('float');
			}

			/* var rep_trigger_point = document.getElementById('reportagem-scroll');
			if (isInViewport(rep_trigger_point)) {
				console.log("REPORTAGEM at view");
				$('.section-7 .card-wrapper').addClass('animate-reportagem-card');
				$('.section-7 hr').addClass('animate-reportagem-hr');
				$('.section-7 .reportagem-img').addClass('animate-reportagem-img');
			} else {
			} */

			if (isInTheViewport($('#plano-scroll'))) {
				$('.plano-body').addClass('come-up');
			} else {
			}

		});		
	});

</script>