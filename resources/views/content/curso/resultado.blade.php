<?php
    
    // Navegação
    $curso["navegacao"] = [
        [
            'titulo' => 'Área do aluno',
            'icone' => "fa fa-play",
            'url' => url::to('/conta/plano/trocar/' . $user->plano->id),
            'active' => false,
        ], [
            'titulo' => $simulado->nome,
            'icone' => "fa fa-file-text",
            'url' => url::to('/conta/curso/simulado/resultado') .'/'. $simulado->id,
            'active' => true,
        ],
    ];

    // Questões
    // $simulado["questoes"] = Plataforma\Simulados\get_questoes($simulado["id"]);

    // Simulados
    // $simulados = Plataforma\Simulados\buscar();

    $letters = ["a", "b", "c", "d", "e"];

    // echo $user->simulado;
    // echo $user->simulado->acertos;
    // echo $simulado->questoes;
    // exit;

    // Respostas do usuário
    // $questoes_respostas = array_column($simulado_realizado["questoes"]["respostas"], "id_alternativa");
?>
   
<!-- plataforma -->
<link href="{{URL::to('/css/curso/plataforma.css?v=1.4')}}" rel="stylesheet">
<link href="{{URL::to('/css/curso/jquery.rateyo.min.css')}}" rel="stylesheet">
<!-- Bootstrap -->
<link rel="stylesheet" href="{{url::to('assets/lib/bootstrap/css/bootstrap.min.css')}}">
<!-- FontAwesome -->
<link rel="stylesheet" href="{{url::to('assets/lib/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!-- Slick -->
<link rel="stylesheet" href="{{url::to('assets/lib/slick/slick.css')}}">
<!-- Aos -->
<link rel="stylesheet" href="{{url::to('assets/lib/aos/dist/aos.css')}}">
<!-- Main CSS -->
<!-- Custom CSS -->
<link rel="stylesheet" href="{{url::to('assets/css/custom.css')}}">
<!-- /plataforma -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

<style>
    .form-check-label.checked {
        font-weight: bold;
        /* color: #073982; */
    }
    .form-check-label p {
        display: inline-block;
    }
    .form-check-label.correta {
        font-weight: bold;
        color: green;
    }

    .plataforma-simulado-questao p {
        word-break: break-word;
    }

    .plataforma-navegacao {
        font-family: 'Lufga';
    }

    p {
        margin:0;
    }
</style>

 <!-- navegação-->
 <div class="plataforma-navegacao margin-header">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-8 col-xl-9">
                <ul class="navegacao">
                    <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                    <?php foreach ($curso["navegacao"] as $nav) { ?>
                    <li class="<?php echo $nav["active"] ? "active" : ""; ?>">
                        <a href="<?php echo $nav["url"]; ?>"><i class="<?php echo $nav["icone"]; ?>"></i> <?php echo $nav["titulo"]; ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="col-md-4 col-xl-3">
                <div class="plataforma-progresso">
                    <p>Progresso</p>
                    <!-- <div class="barra-progresso">
                    <div style="{{'width:' . $user->simuladosRealizados*100/$qtdSimulados  . '%'}}"></div>
                    </div> -->
                    <div class="progresso-bar" data-porcentagem="{{$user->simuladosRealizados*100/$qtdSimulados}}" style="width: 200px; height: 5px; margin-left: 10px; border-radius: 5px; display: flex; overflow: hidden;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- /navegação -->

    <!-- header -->
<div class="plataforma-header" style="background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118));">
    <div class="container-lg">
        <div class="plataforma-header-content">
            <!-- perfil -->
            <div class="plataforma-perfil">
                <div class="plataforma-perfil-foto  profile-picture" style="background-image:url({{url::to('/img/profile/') . '/' . $user->foto}})"></div>
                <div class="plataforma-perfil-dados">
                    <p class="opacity-1">Bem-vindo(a) de volta,</p>
                    <h2>
                        @isset ($user)
                            {{$user->nome}}
                        @endisset
                        <a href="{{url::to('conta')}}" class="alterar-dados" title="Alterar dados"><i class="fas fa-pencil-alt"></i></a>
                    </h2>
                    <!-- <p class="opacity-5">Último acesso: 13/01/2019 às 14:40</p> -->
                </div>
            </div> <!-- /perfil -->

            <!-- plano atual -->
            <div class="plataforma-plano">
                <p>Plano ativo</p>
                @isset($user->plano)
                <h2>{{$user->plano->plano}}</h2>
                <p>Expira em <strong>{{date("d/m/Y", strtotime($user->plano->fim))}}</strong></p>
                @endisset
            </div> <!-- /plano atual -->
        </div>
    </div>
</div>
<!-- /header -->

    <!-- conteúdo -->
    <div class="container-lg">
        <div class="row plataforma-row">
            <div class="col-12">
                <div class="plataforma-content">
                    <div class="plataforma-content-header">
                        <h2>Simulado</h2>
                        <ul>
                            @isset ($qtdSimulados)
                            <li>{{$qtdSimulados}} {{$qtdSimulados == 1 ? "simulado" : "simulados"}}</li>
                            @endisset
                            @isset ($user)
                            <li>{{$user->simuladosRealizados != null ? $user->simuladosRealizados : '0'}} realizados</li>
                            @endisset
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="plataforma-simulado-header header-topo">
                                <h2>
                                    <i class="fa fa-edit"></i> {{$simulado->nome}}<br />
                                    <p>Realizado em {{date("d/m/Y \à\s H:i", strtotime($user->simulado->data))}}</p>

                                    <div style="width: 75%; margin: 20px auto 0; border-top: 4px solid #cacaca; padding-top: 20px;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Acertos</p>
                                                <canvas id="meu_desempenho_acertos" width="100%"></canvas>
                                            </div>
                                            <div class="col-md-4">
                                                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Nota</p>
                                                <canvas id="meu_desempenho_nota" width="100%"></canvas>
                                            </div>
                                            <div class="col-md-4">
                                                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Tempo</p>
                                                <canvas id="meu_desempenho_tempo" width="100%"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                            </div>

                            @isset ($simulado->perguntas)
                            @foreach ($simulado->perguntas as $questionKey=>$questao)
                           <!--  {{'resposta: ' . $simulado->gabarito[$questionKey]}} -->
                            <div class="plataforma-simulado-questao" data-id="{{$questao->id}}">
                                <div class="plataforma-simulado-header">
                                    <h2>
                                        Questão {{intval($questionKey) + 1}}
                                    </h2>
                                </div>

                                {!! $questao->texto !!}

                                <div class="questao-alternativas">
                                    @isset ($questao->alternativas)
                                    @foreach ($questao->alternativas as $key=>$alternativa)
                                   <!--  {{'alternativa: ' . $key}} -->
                                    <div class="form-check">
                                        <label class="form-check-label {{ $key == $user->simulado->resposta[$questionKey] ? 'checked' : ''}}  {{$key == $simulado->gabarito[$questionKey] -1 ? 'correta' : ''}}">
                                            <input type="radio" class="form-check-input" name="questoes[{{$questao->id}}]" value="{{$key+1}}" disabled {{ $key == $user->simulado->resposta[$questionKey] -1 ? 'checked' : ''}}> {{$letters[$key]}}) {!!$alternativa->texto!!}
                                            @if ($key == $user->simulado->resposta[$questionKey] -1)
                                                {!! $key == $simulado->gabarito[$questionKey] -1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times" style="color: red;"></i>' !!}
                                                @php
                                                    $isWrong = false;
                                                    $flag = true;
                                                    if ($key != $simulado->gabarito[$questionKey] -1 && $flag) {
                                                        $isWrong = true;
                                                        $flag = false;
                                                    }
                                                @endphp
                                            @endif
                                        </label>
                                    </div>
                                    @endforeach
                                    @endisset
                                </div>

                                <div class="my-3 p-3 questao-comentario" style="{{  $isWrong ? 'background: #b33e3e42;' : 'background: #3eb37830;' }} border-radius: 6px;">
                                    <span><b>Comentário:</b></span>
                                    <span style="font-size:12px;">{{$isWrong}} {!! $questao->comentario !!}</span>
                                </div>
                            </div>
                            @endforeach
                            @endisset

                            <div class="plataforma-simulado-footer my-3">
                                <button type="button" class="button button-primaria" onClick="goBack();">Voltar</button>
                            </div>
                          
                        </div>  

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /conteúdo -->

    <div class="mt-5"div>

    <script src="{{url::to('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url::to('assets/lib/aos/dist/aos.js')}}"></script>
    <script src="{{url::to('assets/lib/slick/slick.min.js')}}"></script>
    <script src="{{url::to('assets/lib/bootstrap/js/bootstrap.min.js')}}>"></script>
    <script src="{{url::to('assets/js/jquery.mask.min.js')}}"></script>
    <script src="{{url::to('assets/js/moment.js')}}"></script>
    <script src="{{url::to('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{url::to('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{url::to('assets/js/login.js')}}"></script>
    <script src="{{url::to('assets/js/main.js')}}"></script>
    <script src="{{url::to('assets/js/custom.js')}}"></script>  

    <script src="{{URL::to('/js/jquery.iframetracker.min.js')}}"></script>
    <script src="{{URL::to('/js/jquery.rateyo.min.js')}}"></script>
    <script src="{{URL::to('/js/easytimer.min.js')}}"></script>
    <script src="{{URL::to('/js/plataforma.js?v=1.0')}}"></script>
    <script src="{{URL::to('/js/progressbar.js?v=1.0')}}"></script>
    <script src="{{URL::to('/js/simulado.js?v=1.0')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>


    <script type="text/javascript">
        var is_home = false;

        $( document ).ready(function() {
            // $(".plataforma-row").css('margin-top', '-40px');
        });
        
        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
            //Get ctx from string
            var ctx = chart.chart.ctx;
            
                    //Get options from the center object in options
            var centerConfig = chart.config.options.elements.center;
            var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
            var color = centerConfig.color || '#000';
            var sidePadding = centerConfig.sidePadding || 20;
            var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
            //Start with a base font of 30px
            ctx.font = "30px " + fontStyle;
            
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
            var stringWidth = ctx.measureText(txt).width;
            var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

            // Find out how much the font can grow in width.
            var widthRatio = elementWidth / stringWidth;
            var newFontSize = Math.floor(30 * widthRatio);
            var elementHeight = (chart.innerRadius * 2);

            // Pick a new font size so it will not be larger than the height of label.
            var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
            ctx.font = "bold " + fontSizeToUse+"px " + fontStyle;
            ctx.fillStyle = color;
            
            //Draw text in center
            ctx.fillText(txt, centerX, centerY);
                }
            }
        });

        const media_geral_options = {
            cutoutPercentage: 70,
            responsive: true,
            legend: false,
            animation: {
                animateRotate: true,
            },
            elements: {
                center: {
                    text: '',
                    sidePadding: 40,
                }
            }
        };
        
        @isset ($user->simulado)

            // ────────────────────────────────────────────────────────────────────────────────
            // ────────────────────────────── Meu desempenho ──────────────────────────────────
            // ────────────────────────────────────────────────────────────────────────────────

           

            // Acertos
            media_geral_options.elements.center.text = '{{ $user->simulado->acertos . "/" . $simulado->questoes }}';
            new Chart(document.getElementById('meu_desempenho_acertos'), {
                type: 'doughnut',
                data: {
                    labels: ['Acertos', 'Erros'],
                    datasets: [{
                        data: [{{$user->simulado->acertos}}, {{$simulado->questoes}} - {{$user->simulado->acertos}}],
                        backgroundColor: ['#073982', '#e0e0e0'],
                    }],
                },
                options: media_geral_options,
            });

            // Nota
            <?php
                $meu_desempenho_nota = $user->simulado->acertos * (1000 / $simulado->questoes);
            ?>
            media_geral_options.elements.center.text = '<?php echo $meu_desempenho_nota < 1000 ? number_format($meu_desempenho_nota, 1, ",", ".") : $meu_desempenho_nota; ?>';
            new Chart(document.getElementById('meu_desempenho_nota'), {
                type: 'doughnut',
                data: {
                    labels: ['Nota', ''],
                    datasets: [{
                        data: [<?php echo $meu_desempenho_nota < 1000 ? number_format($meu_desempenho_nota, 2) : $meu_desempenho_nota; ?>, <?php echo (1000 - $meu_desempenho_nota) < 1000 ? number_format(1000 - $meu_desempenho_nota, 2) : 1000 - $meu_desempenho_nota; ?>],
                        backgroundColor: ['#073982', '#e0e0e0'],
                    }],
                },
                options: media_geral_options,
            });

            // Tempo
            media_geral_options.elements.center.text = '{{intval($user->simulado->tempo / 100 / 60) > 0 ? intval($user->simulado->tempo / 100 / 60) : number_format($user->simulado->tempo / 100 / 60, 1)}} min';
            new Chart(document.getElementById('meu_desempenho_tempo'), {
                type: 'doughnut',
                data: {
                    labels: ['Tempo'],
                    datasets: [{
                        data: [{{intval($user->simulado->tempo / 100 / 60) > 0 ? intval($user->simulado->tempo / 100 / 60) : number_format($user->simulado->tempo / 100 / 60, 1)}}],
                        backgroundColor: ['#073982', '#e0e0e0'],
                    }],
                },
                options: media_geral_options,
            });

        @endisset

        var imagens = document.querySelectorAll(".plataforma-simulado-questao img");
        for (i=0; i < imagens.length; i++) {
            let src =  imagens[i].getAttribute('src');
            let substring = 'http';
            let hasSubstring = src.search(substring);
            if (hasSubstring < 0)
                imagens[i].setAttribute('src', "{{url::to('/')}}/" + imagens[i].getAttribute('src'));
        }

        function goBack() {
            window.location.href="{{url::to('/conta/curso/simulado') .'/'. $simulado->id .'/'. $user->plano->id}}";
        }

    </script>