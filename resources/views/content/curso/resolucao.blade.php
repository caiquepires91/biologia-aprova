<?php
    // Navegação
    $curso["navegacao"] = [
        [
            'titulo' => 'Área do aluno',
            'icone' => "fa fa-play",
            'url' => url::to('/conta/plano/trocar/' . $user->plano->id),
            'active' => false,
        ], [
            'titulo' => $simulado->nome,
            'icone' => "fa fa-file-text",
            'url' => url::to('/conta/curso/simulado/resolucao') .'/'. $simulado->id,
            'active' => true,
        ],
    ];

    // Questões
    // simulado->questoes

    // Realizado
    /*$simulado_realizado = Plataforma\Simulados\_busca_realizacao_simulado($id_simulado, $_SESSION["plataforma"]["id_usuario"]);
    if (!$simulado_realizado || !is_null($simulado_realizado["data_realizacao"])) {
        $simulado_realizado = Plataforma\Simulados\registrar_realizacao($simulado["id"]);
    }

    // Simulados
    $simulados = Plataforma\Simulados\buscar();
    */

    $letters = ["a", "b", "c", "d", "e"];
?>

<!-- plataforma -->
<link href="{{URL::to('/css/curso/plataforma.css?v=1.4')}}" rel="stylesheet">
<link href="{{URL::to('/css/curso/jquery.rateyo.min.css')}}" rel="stylesheet">

<!-- FontAwesome -->
<link rel="stylesheet" href="{{url::to('assets/lib/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!-- Slick -->
<link rel="stylesheet" href="{{url::to('assets/lib/slick/slick.css')}}">
<!-- Aos -->
<link rel="stylesheet" href="{{url::to('assets/lib/aos/dist/aos.css')}}">
<!-- Main CSS -->
<!-- Custom CSS -->
<link rel="stylesheet" href="{{url::to('assets/css/custom.css')}}">
<!-- /plataforma -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

<style>
    .form-check-label.checked {
        font-weight: bold;
        color: #073982;
    }
    .form-check-label p {
        display: inline-block;
    }
    .plataforma-simulado-questao p {
        word-break: break-word;
    }

    .form-check-input:checked {
        background-color: #049676;
        border-color: #049676;
    }

    .plataforma-navegacao {
        font-family: 'Lufga';
    }

    p {
        margin:0;
    }

</style>

    <!-- navegação-->
    <div class="plataforma-navegacao margin-header">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <ul class="navegacao">
                        <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                        <?php foreach ($curso["navegacao"] as $nav) { ?>
                        <li class="<?php echo $nav["active"] ? "active" : ""; ?>">
                            <a href="<?php echo $nav["url"]; ?>"><i class="<?php echo $nav["icone"]; ?>"></i> <?php echo $nav["titulo"]; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-md-4 col-xl-3">
                    <div class="plataforma-progresso">
                        <p>Progresso</p>
                       <!--  <div class="barra-progresso">
                        <div style="{{'width:' . $user->simuladosRealizados*100/$qtdSimulados  . '%'}}"></div>
                        </div> -->
                        <div class="progresso-bar" data-porcentagem="{{$user->simuladosRealizados*100/$qtdSimulados}}" style="width: 200px; height: 5px; margin-left: 10px; border-radius: 5px; display: flex; overflow: hidden;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /navegação -->

    <!-- header -->
<div class="plataforma-header" style="background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118));">
    <div class="container-lg">
        <div class="plataforma-header-content">
            <!-- perfil -->
            <div class="plataforma-perfil">
                <div class="plataforma-perfil-foto profile-picture" style="background-image:url({{url::to('/img/profile/') . '/' . $user->foto}})"></div>
                <div class="plataforma-perfil-dados">
                    <p class="opacity-1">Bem-vindo(a) de volta,</p>
                    <h2>
                        @isset ($user)
                        {{$user->nome}}
                        @endisset
                        <a href="{{url::to('conta')}}" class="alterar-dados" title="Alterar dados"><i class="fas fa-pencil-alt"></i></a>
                    </h2>
                    <!-- <p class="opacity-5">Último acesso: 13/01/2019 às 14:40</p> -->
                </div>
            </div> <!-- /perfil -->

            <!-- plano atual -->
            <div class="plataforma-plano">
                <p>Plano ativo</p>
                @isset($user->plano)
                <h2>{{$user->plano->plano}}</h2>
                <p>Expira em <strong>{{date("d/m/Y", strtotime($user->plano->fim))}}</strong></p>
                @endisset
            </div> <!-- /plano atual -->
        </div>
    </div>
</div>
<!-- /header -->

    <!-- conteúdo -->
    <div class="container-lg base_url" url-data="{{url::to('/')}}"> 
        <div class="row plataforma-row">
            <div class="col-12">
                <div class="plataforma-content">
                    <div class="plataforma-content-header">
                        <h2>Simulado</h2>
                        <ul>
                            @if ($user->plano->plano === "INTENSIVÃO ENEM")
                                <li>2 simulados</li>
                            @else
                                @isset ($qtdSimulados)
                                <li>{{$qtdSimulados}} {{$qtdSimulados == 1 ? "simulado" : "simulados"}}</li>
                                @endisset
                                @isset ($user)
                                <li>{{$user->simuladosRealizados != null ? $user->simuladosRealizados : '0'}} realizados</li>
                                @endisset
                            @endif
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form id="form_simulado" action="{{url::to('/simulado/finalizar')}}" method="POST">
                            <input type="hidden" name="id" value="{{$simulado->id}}">
                            <input type="hidden" name="id_realizacao" value="{{$simulado->id}}">
                            <input type="hidden" id="simulado_duracao" name="duracao" value="00:00:00">

                            <div class="plataforma-simulado-header header-topo">
                                <h2>
                                    <i class="fa fa-edit"></i> {{$simulado->nome}}<br />
                                    <!-- <p>Cadastrado em {{date("d/m/Y \à\s H:i", strtotime($simulado->data))}}</p> -->
                                </h2>
                            </div>
                            
                            @isset ($simulado->perguntas)
                            @foreach ($simulado->perguntas as $key=>$questao)
                            <div class="plataforma-simulado-questao" data-id="{{$questao->id}}">
                                <div class="plataforma-simulado-header">
                                    <h2>
                                        Questão {{intval($key) + 1}}
                                    </h2>
                                </div>

                                {!! $questao->texto !!}

                                <div class="questao-alternativas">
                                    @isset ($questao->alternativas)
                                    @foreach ($questao->alternativas as $key=>$alternativa)
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="questoes[{{$questao->id}}]" value="{{$key+1}}" required> {{$letters[$key]}}) {!!$alternativa->texto!!}
                                        </label>
                                    </div>
                                    @endforeach
                                    @endisset
                                </div>
                            </div>
                            @endforeach
                            @endisset
                            

                            <div class="plataforma-simulado-footer">
                                <button type="submit" class="button button-primaria my-3">Finalizar simulado</button>
                            </div>
                            
                            </form>
                        </div>  

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /conteúdo -->

    <div class="mt-5"div>

    <div class="simulado-contagem">
        <i class="fa fa-clock-o"></i>
        <p>00:00:00</p>
    </div>
    
    <script src="{{url::to('assets/lib/aos/dist/aos.js')}}"></script>
    <script src="{{url::to('assets/lib/slick/slick.min.js')}}"></script>
    <script src="{{url::to('assets/js/moment.js')}}"></script>
    <script src="{{url::to('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{url::to('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{url::to('assets/js/login.js')}}"></script>
    <script src="{{url::to('assets/js/main.js')}}"></script>
    <script src="{{url::to('assets/js/custom.js')}}"></script>  

    <script src="{{URL::to('/js/jquery.iframetracker.min.js')}}"></script>
    <script src="{{URL::to('/js/jquery.rateyo.min.js')}}"></script>
    <script src="{{URL::to('/js/easytimer.min.js')}}"></script>
    <script src="{{URL::to('/js/plataforma.js')}}"></script>
    <script src="{{URL::to('/js/progressbar.js')}}"></script>
    <script src="{{URL::to('/js/simulado.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <script type="text/javascript">
        var is_home = false;
        var timer = new easytimer.Timer();
        timer.start();
        timer.addEventListener('secondsUpdated', function (e) {
            $('.simulado-contagem p').html(timer.getTimeValues().toString());
            $('#simulado_duracao').val(timer.getTimeValues().toString());
        });

        $( document ).ready(function() {
            //$(".plataforma-row").css('margin-top', '-40px');

            var imagens = document.querySelectorAll(".plataforma-simulado-questao img");
            for (i=0; i < imagens.length; i++) {
                let src =  imagens[i].getAttribute('src');
                console.log("img", src);
                let substring = 'http';
                let hasSubstring = src.search(substring);
                if (hasSubstring < 0)
                    imagens[i].setAttribute('src', "{{url::to('/')}}/" + imagens[i].getAttribute('src').replace("../", ""));
                
                let substring_alt = "https://biologiaaprova.com.br/./uploads/simulado/imgs/";
                let correct_string = "https://admin.biologiaaprova.com.br/production/uploads/simulado/imgs/";
                let hasSubstring_alt = src.search(substring_alt);
                if (hasSubstring_alt < 0)
                    imagens[i].setAttribute('src', imagens[i].getAttribute('src').replace(substring_alt, correct_string));
            }
        });

        // centrarlizar imagens de questões não centralizadas.
        var span_parents = $(".plataforma-simulado-questao").find("img");
        console.log("span: " + span_parents.length);
        for (i = 0; i < span_parents.length; i++) {
            console.log("<center>" + $(span_parents[i]) + "</center>");
            $(span_parents[i]).parent().html("<center>" + $(span_parents[i]).parent().html() + "</center>");
        }
       
        $("#form_simulado").submit(function(e){
            e.preventDefault();
            e.stopPropagation();

            var gabarito = "{{$simulado->gabarito}}";
            var correctAnswers = gabarito.split(',');

            var userAnswer = '';
            var numCorrect = 0;
            var userAnswersString = '';

            var alternativas = $("#form_simulado").children('.plataforma-simulado-questao');
            for (var i=0; i < alternativas.length; i++) {
                label =  alternativas[i].querySelector('.checked');
                checkedInput = alternativas[i].querySelector("input[name='questoes[" + $(alternativas[i]).data('id') + "]']:checked");
                userAnswer = (checkedInput||{}).value;
                if (i) userAnswersString = userAnswersString + ',' + userAnswer;
                else userAnswersString = userAnswersString + userAnswer;
                console.log("resposta user:" + userAnswer);
                console.log("resposta correta:" + correctAnswers[i]);
                if (userAnswer == correctAnswers[i]) {
                    numCorrect++;
                    label.style.color = 'green';
                } else {
                    label.style.color = 'red';
                }
            }

            var idSimulado = $("#form_simulado").find("input[name='id']").val();
            var tempo = $("#form_simulado").find("input[name='duracao']").val();
            var userId = "{{$user->id}}";

            console.log("simulado id: " + idSimulado);
            console.log("tempo: " + strTimeToSec(tempo));
            console.log("respostas corretas: " + numCorrect);
            console.log("respostas string: " + userAnswersString);

            var formData = new FormData();
            formData.append('idUser', userId);
            formData.append('idSimulado', idSimulado);
            formData.append('respostas', userAnswersString);
            formData.append('acertos', numCorrect);
            formData.append('tempo', strTimeToSec(tempo));

            // console.log("link: " + $(this).attr('action'));

            $.ajax({
                url: $(this).attr('action'),
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    console.log(data);
                    if (data.success) {
                        window.location.href="{{url::to('/') . '/conta/curso/simulado/resultado/' . $simulado->id . '/' . $user->plano->id}}";
                        console.log(data.simuladoRealizado);
                    } else {
                        console.log("finalização falhou!!");
                    }
                }
            });

        });

        function strTimeToSec(time) {
            timeArray = time.split(':');
            hour = timeArray[0] * 60 * 60;
            min = timeArray[1] * 60;
            sec = timeArray[2];
            return hour + min + sec;
        }

    </script>