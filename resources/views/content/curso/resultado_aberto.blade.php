<style>
	.plano:hover {
		animation: unset !important;
	}
	.errada::before {
        content: "✖";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-right: 4px;
        color: red;
        font-weight: bold;
    }

	.correta::before {
        content: "✔";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-right: 4px;
        color: green;
        font-weight: bold;
    }

	.titulo-simulado {
		margin-top: 85px;
	}

	.plano-row {
		margin-top: 100px;margin-bottom: 100px;
	}

	@media (max-width: 991.98px) { 
		.plano-principal .plano-header {
			margin-left: -9px;
		}

		.titulo-simulado {
			margin-top: unset;
		}
		
		.plano-row {
			margin-top: 73px;margin-bottom: 20px;
		}

	}

	@media (max-width: 575.98px) {
		.plano-row {
			margin-top: 20px;margin-bottom: 20px;
		}
	 }

</style>
<div class="m-md-3 mt-3" style="width: 100%;">
	<p class="{{ $acertos/$total >= 0.7 ? "correta" : "errada"}}" style="text-align: center "><strong>{{ $acertos/$total >= 0.7 ? "Parabéns" : "Que pena"}}, você acertou {{$acertos}} {{$acertos > 1 ? "questões" : "questão"}} de um total de {{$total}}.</strong></p>
</div>
<!-- PLANO 50% -->
<div class="row plano-row">
	<div class="col">
			<div id="planos-section-2" class="row justify-content-center align-items-center">
				<div class="col-md-12">
					<div class="plano position-relative m-md-auto plano-principal d-none d-md-block" style="width: 700px; transition: none;">
						<div class="container plano-header align-items-center" style="width:768px">
							<p class="pt-3 mx-auto">curso completo</p>
							<p class="mb-auto mx-auto">+ bônus</p>
						</div>
						<div class="plano-body m-md-auto w-100 pt-5 px-5 border bg-light border-2 bloco">
							<p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 12rem;">Você acaba de ganhar 50% para assinar o nosso plano!</p>
							<div class="row">
								<div class="col-md-7">
									<div id="plano-scroll" class="vantagens mb-3">
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os assuntos de biologia;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de questões;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e gabaritados;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e comentados;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para memorização;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
									</div>
								</div>
								<div class="col-md-5">
									<p style="font-size: 1.2rem;">de 12x <s style="font-size: 1.7rem;">R$21,90</s> por</p>
									<span>
										<h2 class="position-relative" style="margin-left: 24%">
											<span class="position-absolute end-100" style="top: 29%;font-size: 16px;color: gray;">12x</span>
											<span class="position-absolute end-100 plano-preco-b" style="top: 42%;">R$</span>
											10
											<span class="position-absolute start-100 plano-preco-b" style="top: 42%;">,95</span>
										</h2>
									</span>
								</div>
							</div>
						</div>
						<div class="d-flex" style="margin-top: -83px;width: inherit;">
							<button class="green-btn btn rounded-pill mx-auto" data-plano="5" type="button" onclick="goToLogin(this);" style="font-size: xx-large; width:228px">eu quero!</button>
						</div>
					</div>


					<div class="plano position-relative m-0 plano-principal d-md-none" style="width: 100%; transition: none;margin-bottom: 0px;">
						<p style="font-size: 1.5rem;color: #212529;text-align: center;">Você acaba de ganhar 50% para assinar o nosso plano!</p>
						<div class="container plano-header align-items-center" style="height:auto;padding-bottom:15px;width:100%;margin-bottom: unset; z-index: 5; width: 100%; margin-left: unset;">
							<p class="pt-3 mx-auto">curso completo</p>
							<p class="mb-auto mx-auto">+ bônus</p>
						</div>
						<div class="plano-body m-md-auto w-100 border bg-light border-2 bloco" style="padding-top:4%">
							<div class="row">
								<div class="col-md-7">
									<div id="plano-scroll" class="vantagens mb-3">
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os assuntos de biologia;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de questões;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e gabaritados;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos e comentados;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para memorização;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
										<p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
									</div>
								</div>
								<div class="col-md-5">
									<p style="font-size: 1.2rem;">de 12x <s style="font-size: 1.7rem;">R$21,90</s> por</p>
									<span>
										<h2 class="position-relative" style="margin-left: 24%">
											<span class="position-absolute end-100" style="top: 29%;font-size: 16px;color: gray;">12x</span>
											<span class="position-absolute end-100 plano-preco-b" style="top: 42%;">R$</span>
											10
											<span class="position-absolute start-100 plano-preco-b" style="top: 42%;">,95</span>
										</h2>
									</span>
								</div>
							</div>
						</div>
						<div class="d-flex" style="margin-top: -70px;width: inherit;">
							<button class="green-btn btn rounded-pill mx-auto" data-plano="5" type="button" onclick="goToLogin(this);" style="font-size: xx-large; width:228px">eu quero!</button>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

<div class="mt-5"></div>

<script>

	function goToLogin(elem) {
		var plano = $(elem).data("plano");
		window.location.href = "{{url::to('planos/choose')}}" + "/" + plano + "/50";
	}

	var isInTheViewport = function (elem) {
		var hT = elem.offset().top,
				hH = elem.outerHeight(),
				wH = $(window).height(),
				wS = $(this).scrollTop();
				//console.log ("ht:" + hT + " hH:" + hH + " wH:" + wH + " wS:" + wS);
		return  (wS > (hT-wH) && (hT > wS) && (wS+wH > hT+hH));
	};

	$(window).scroll(function() {
		if (isInTheViewport($('#plano-scroll'))) {
			//console.log("in the viewport");
			$('.plano-body').addClass('come-up');
		}
	});
</script>