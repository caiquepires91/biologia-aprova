<?php
 

    // Navegação
    $curso["navegacao"] = [
        [
            'titulo' => 'Área do aluno',
            'icone' => "fa fa-play",
            'url' => url::to('/conta/plano/trocar/' . $user->plano->id),
            'active' => false,
        ], [
            'titulo' => $simulado->nome,
            'icone' => "fa fa-file-text",
            'url' => url::to('/conta/curso/simulado/') .'/'. $simulado->id . '/' . $user->plano->id,
            'active' => true,
        ],
    ];

    $areas_simulados_intensivao = [17];
    $simulados_intensivao = [138, 139, 141];
    $musicas_intensivao = [6, 31, 7, 26, 10, 11, 21, 8, 19];
    $areas_intensivao = [15, 16, 19, 17];
    $areas_simulados_ocultas = [15, 16, 17, 18, 19];
    $extra = 20;

    $dueTime = date("Y-m-d H:i:s", strtotime("2021-10-21 00:00"));
    $now = date("Y-m-d H:i:s");
    if ($now >= $dueTime) {
        $simulados_intensivao = [138, 139, 141, 142];
    }
?>


<style>
    .plataforma-perfil-dados p, .plataforma-plano p {
        margin:0 !important;
    }

    .plataforma-navegacao {
        font-family: 'Lufga';
    }

    .plataforma-content {
        margin-top: unset !important;
    }

</style>

    <!-- plataforma -->
    <link href="{{URL::to('/css/curso/plataforma.css?v=1.4')}}" rel="stylesheet">
    <link href="{{URL::to('/css/curso/jquery.rateyo.min.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{url::to('assets/lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{url::to('assets/lib/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!-- Slick -->
    <link rel="stylesheet" href="{{url::to('assets/lib/slick/slick.css')}}">
    <!-- Aos -->
    <link rel="stylesheet" href="{{url::to('assets/lib/aos/dist/aos.css')}}">
    <!-- Main CSS -->
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{url::to('assets/css/custom.css')}}">
    <!-- /plataforma -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

    <!-- navegação-->
    <div class="plataforma-navegacao margin-header">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <ul class="navegacao">
                        <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                        <?php foreach ($curso["navegacao"] as $nav) { ?>
                        <li class="<?php echo $nav["active"] ? "active" : ""; ?>">
                            <a href="<?php echo $nav["url"]; ?>"><i class="<?php echo $nav["icone"]; ?>"></i> <?php echo $nav["titulo"]; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-md-4 col-xl-3">
                    <div class="plataforma-progresso">
                        <p>Progresso</p>
                        <!-- <div class="barra-progresso">
                            <div style="{{'width:' . $user->simuladosRealizados*100/$qtdSimulados  . '%'}}"></div>
                        </div> -->
                        <div class="progresso-bar" data-porcentagem="{{$user->simuladosRealizados*100/$qtdSimulados}}" style="width: 200px; height: 5px; margin-left: 10px; border-radius: 5px; display: flex; overflow: hidden;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /navegação -->

    <!-- header -->
<div class="plataforma-header" style="background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118));">
    <div class="container-lg">
        <div class="plataforma-header-content">
            <!-- perfil -->
            <div class="plataforma-perfil">
                <div class="plataforma-perfil-foto  profile-picture" style="background-image:url({{url::to('/img/profile/') . '/' . $user->foto}})"></div>
                <div class="plataforma-perfil-dados">
                    <p class="opacity-1">Bem-vindo(a) de volta,</p>
                    <h2>
                    @isset ($user)
                        {{$user->nome}}
                        @endisset
                        <a href="{{url::to('conta')}}" class="alterar-dados" title="Alterar dados"><i class="fas fa-pencil-alt"></i></a>
                    </h2>
                    <!-- <p class="opacity-5">Último acesso: 13/01/2019 às 14:40</p> -->
                </div>
            </div> <!-- /perfil -->

            <!-- plano atual -->
            <div class="plataforma-plano">
                <p>Plano ativo</p>
                @isset($user->plano)
                <h2>{{$user->plano->plano}}</h2>
                <p>Expira em <strong>{{date("d/m/Y", strtotime($user->plano->fim))}}</strong></p>
                @endisset
            </div> <!-- /plano atual -->
        </div>
    </div>
</div>
<!-- /header -->

    <!-- conteúdo -->
    <div class="container-lg">
        <div class="row plataforma-row">
            <div class="col-12">
                <div class="plataforma-content py-3" data-simulado="{{$simulado->id}}" data-area="{{$simulado->idArea}}" data-questoes="{{$simulado->questoes}}">
                    <!-- var $simulado está sendo modificada em algum momento - garantir qtdquestoes n mude / TODO:investigar -->
                    @php
                        $qtdQuestoes = $simulado->questoes;
                    @endphp
                    <div class="plataforma-content-header">
                        <h2>Simulado - {{$qtdQuestoes}} {{$qtdQuestoes > 1 ? 'questões' : 'questão'}}</h2>
                        <ul>
                            @isset ($qtdSimulados)
                            <li>{{$qtdSimulados}} {{$qtdSimulados == 1 ? "simulado" : "simulados"}}</li>
                            @endisset
                            @isset ($user)
                            <li>{{$user->simuladosRealizados != null ? $user->simuladosRealizados : '0'}} realizados</li>
                            @endisset
                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-lg-8">
                            <div class="plataforma-simulado-header">
                                <h2>
                                    <i class="fa fa-edit"></i> {{$simulado->nome}}<br />
                                    <!-- <p>Cadastrado em {{date("d/m/Y \à\s H:i", strtotime($simulado->data))}}</p> -->
                                </h2>
                            </div>

                            @if ($user->simulado == null)
                            <p style="text-align: center" ><strong>Você ainda não realizou este simulado!</strong></p>
                            @endif

                            <div class="row justify-content-center">
                            <div class="col-md-4" style="display:flex">
                                <a href="{{url::to('/') . '/conta/curso/simulado/resolucao/' . $simulado->id . "/" . $user->plano->id}}" class="button button-primaria" style="padding: 10px 25px; font-size: 1rem;margin: auto; font-family:'Lufga';">Iniciar simulado</a>
                            </div>
                           <!--  <div class="col-md-4">
                                <a data-target="#modal_orientacoes" data-toggle="modal" class="button button-secundaria" style="padding: 5px 20px; font-size: 1rem; margin: 5px 0 0; color: #049676; font-family: 'Lufga'; border-color: #a6c5d1; background-color: #dae1ec;"><i class="fa fa-info-circle"></i> Orientações</a>
                            </div> -->
                            </div>

                            @isset ($user->simulado)
                            @if ($user->simulado->realizado)
                                <!-- Meu desempenho -->
                            <div class="plataforma-simulado-header" style="margin-top: 40px;">
                                <h2>
                                    Meu desempenho<br />
                                    <p>Realizado em {{date("d/m/Y \à\s H:i", strtotime($user->simulado->data))}}</p>
                                </h2>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Acertos</p>
                                    <canvas id="meu_desempenho_acertos" width="100%"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Nota</p>
                                    <canvas id="meu_desempenho_nota" width="100%"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Tempo</p>
                                    <canvas id="meu_desempenho_tempo" width="100%"></canvas>
                                </div>
                            </div>

                            <div class="row justify-content-center align-items-center">
                                <div class="col-3 my-3" style="display:flex">
                                    <a href="{{url::to('/') . '/conta/curso/simulado/resultado/' . $simulado->id . "/" . $user->plano->id}}" class="button button-primaria" style="margin:auto ;">Ver resultado</a>
                                </div>
                            </div>
                            @endif
                            @endisset
                            
                           

                            <!-- Média geral -->
                            <div class="plataforma-simulado-header" style="margin-top: 40px;">
                                <h2>
                                    Média geral<br />
                                    <p>Desde o início</p>
                                </h2>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Acertos</p>
                                    <canvas id="media_geral_acertos" width="100%"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Nota</p>
                                    <canvas id="media_geral_nota" width="100%"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Tempo</p>
                                    <canvas id="media_geral_tempo" width="100%"></canvas>
                                </div>
                            </div>
                        </div>  

                        {{-- {{dd($playlists)}} --}}

                        <div class="col-lg-4">
                            <!-- SIMULADOS -->
                            <div class="plataforma-card">
                                <h2><i class="fa fa-file-text" style="color:rgb(4 150 118)"></i> Simulados</h2>
                                <div class="plataforma-simulados-categorias" style="margin-top: 10px;">
                                @isset ($playlists)
                                @foreach ($playlists as $playlist)
                                    <div class="plataforma-simulado-categoria">
                                        <div class="categoria-header"  data-area="{{$playlist->id}}">
                                            <h2>{{$playlist->nome}}</h2>
                                            <small>{{count($playlist->simulados)}} simulado{{count($playlist->simulados) == 1 ? '' : 's'}}</small>
                                            <i class="fa fa-chevron-down categoria-close"></i>
                                        </div>
                                        <div class="categoria-body">
                                            <ul class="plataforma-card-simulados">
                                            @isset ($playlist->simulados)
                                            @foreach ($playlist->simulados as $simulado)

                                                {{$iniciado = ($simulado->data_inicio && !$simulado->data_realizacao)}}
                                                <?php $simulado_icone_status = "far fa-file-alt" ?>
                                                @if ($simulado->realizado)
                                                <?php $simulado_icone_status = "fas fa-check" ?>
                                                @endif

                                                <li data-simulado="{{$simulado->id}}" data-area="$area->id" title="{{$simulado->nome}}">
                                                    <a href="{{url::to('/conta/curso/simulado') .'/'. $simulado->id .'/'. $user->plano->id}}">
                                                        <div class="simulado-status {{ $iniciado ? 'iniciado' : ''}} {{$simulado->realizado ? 'realizado' : ''}}"><i class="{{$simulado_icone_status}}" ></i></div>
                                                        <div class="simulado-titulo">
                                                            <p>{{$simulado->nome}}
                                                            @if(!$simulado->realizado && strtotime($simulado->data) > strtotime("2021-03-25") )
                                                            <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>
                                                            @endif
                                                            </p>
                                                            
                                                            <!-- @if ($iniciado)
                                                            <span>Iniciado em {{date("d/m/Y", strtotime($simulado->data_inicio))}}</span>
                                                            @elseif ($simulado->realizado)
                                                            <span>Realizado em {{date("d/m/Y", strtotime($simulado->dataRealizado))}}</span>
                                                            @else
                                                            <span>Disponibilizado em {{date("d/m/Y", strtotime($simulado->data))}}</span>
                                                            @endif -->
                                                        </div>
                                                        <div class="simulado-icone">
                                                            @if ($simulado->visualizado)
                                                            <i class="fa fa-eye"></i>
                                                            @else
                                                            <i class="fa fa-eye" style="color:#37343599"></i>
                                                            @endif
                                                        </div>
                                                    </a>
                                                </li> 
                                            @endforeach

                                            @endisset
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                                @endisset
                                </div>
                            </div>
                            <!-- /SIMULADOS -->
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /conteúdo -->

    <div class="mt-5"div>

    <!-- Modal - Orientações -->
    <div class="modal fade" id="modal_orientacoes" tabindex="-1" role="dialog" aria-labelledby="modal_orientacoes_label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="modal_orientacoes_label">Instruções para os simulados</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="margin-bottom: 10px;"><strong>Olá, aluno Somma!</strong></p>
                    <p style="margin-bottom: 10px;"><strong>Leia com atenção as instruções:</strong></p>
                    <p style="margin-bottom: 10px;">1. Cada simulado contém 10 questões envolvendo os tópicos já estudados, visto nos vídeos da plataforma;</p>
                    <p style="margin-bottom: 5px;">2. Sugestão de tempo para resolução para cada simulado será:</p>
                    <ul>
                        <li style="list-style-type: square; margin-left: 50px;">Simulado 1: tempo total são 60 minutos</li>
                        <li style="list-style-type: square; margin-left: 50px;">Simulado 2: tempo total são 55 minutos</li>
                        <li style="list-style-type: square; margin-left: 50px;">Simulado 3: tempo total são 50 minutos</li>
                        <li style="list-style-type: square; margin-left: 50px;">Simulado 4: tempo total são 45 minutos</li>
                        <li style="list-style-type: square; margin-left: 50px;">Simulado 5: tempo total são 40 minutos</li>
                    </ul>
                    <p style="margin-bottom: 10px;"><i>Em simulados posteriores iremos trabalhar com 30 minutos, sendo, em média, 3 minutos por questão, tempo médio que você dispõe na prova do ENEM.</i></p>
                    <p style="margin-bottom: 10px;">3. Após responder cada simulado, verifique seu desempenho;</p>
                    <p style="margin-bottom: 5px;">4. Identifique as questões/assuntos específicos que errou. Em seguida:</p>
                    <ul style="margin-bottom: 10px;">
                        <li style="list-style-type: square; margin-left: 50px;">Assista novamente a aula referente ao vídeo/conteúdo da questão. Anotando cuidadosamente cada passo;</li>
                        <li style="list-style-type: square; margin-left: 50px;">Refaça a questão quantas vezes for necessário.</li>
                    </ul>
                    <p style="margin-bottom: 5px;">5. Ao fazer o simulado, você só tem a ganhar:</p>
                    <ul style="margin-bottom: 10px;">
                        <li style="list-style-type: square; margin-left: 50px;">Verifica sua aprendizagem;</li>
                        <li style="list-style-type: square; margin-left: 50px;">Aprimora o tempo para resolução de questões;</li>
                        <li style="list-style-type: square; margin-left: 50px;">Desenvolve técnicas para turbinar seus cálculos;</li>
                        <li style="list-style-type: square; margin-left: 50px;">Se eventualmente você erra uma questão, tem a oportunidade de solidificar sua aprendizagem, pois fará a revisão da aula/conteúdo necessário.</li>
                    </ul>
                    <p style="text-align: right;"><strong>Arrebente nos simulados!</strong></p>
                    <p style="text-align: right;">Prof. Aristóteles Alves Feitosa e equipe SOMMA.</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm" data-dismiss="modal" style="font-weight: 600;">Ok</button>
                </div>
            </div>
        </div>
    </div> <!-- /Modal - Orientações -->

    <script src="{{url::to('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{url::to('assets/lib/aos/dist/aos.js')}}"></script>
    <script src="{{url::to('assets/lib/slick/slick.min.js')}}"></script>
    <script src="{{url::to('assets/lib/bootstrap/js/bootstrap.min.js')}}>"></script>
    <script src="{{url::to('assets/js/jquery.mask.min.js')}}"></script>
    <script src="{{url::to('assets/js/moment.js')}}"></script>
    <script src="{{url::to('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{url::to('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{url::to('assets/js/login.js')}}"></script>
    <script src="{{url::to('assets/js/main.js')}}"></script>
    <script src="{{url::to('assets/js/custom.js')}}"></script>  
    
    <script src="{{URL::to('/js/jquery.iframetracker.min.js')}}"></script>
    <script src="{{URL::to('/js/jquery.rateyo.min.js')}}"></script>
    
    <script src="{{URL::to('/js/plataforma.js')}}"></script>
    <script src="{{URL::to('/js/progressbar.js')}}"></script>
    <script src="{{URL::to('/js/simulado.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <script type="text/javascript">
        var is_home = false;

        $( document ).ready(function() {
            $(".plataforma-row").css('margin-top', '-40px');
            //expandirSimuladoAtual();
        });

        // adiciona novo para a categoria caso algum filho possua a tag.
        var categorias = $(".plataforma-simulados-categorias").find(".plataforma-simulado-categoria");
        categorias.each(function (index) {
            var categoria = $( this );
            var novo = categoria.find(".novo-simulado").html();
            //console.log("novo: " + novo + (novo != null));
            if (novo != null) {
                categoria.find("h2").append(' <span class="badge rounded-pill bg-success novo-simulado" style="color:white">novo</span>');
            }
        });

        function expandirSimuladoAtual() {
            var open = $(".plataforma-simulado-categoria.show");
            //console.log("aberto: " + open.find("h1").html());
            if (open != null) {
                open.find('.categoria-body').slideToggle(function (elemento) {
                return open.toggleClass('show');
                });   
            }

            var idArea = $(".plataforma-content").data("area");
            var header =  $(".categoria-header[data-area=" + idArea + "]");
            var categoria = header.parent();
            //categoria.parent().addClass('show');
            console.log("esse simulado area: " + idArea);
            console.log("linha: " + header.find("h2").html());

            if (categoria.hasClass('show')) {
                categoria.find('.categoria-body').slideToggle(function (elemento) {
                return categoria.toggleClass('show');
                });
            } else {
                categoria.toggleClass('show');
                categoria.find('.categoria-body').slideToggle();
            }
        }

        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
            //Get ctx from string
            var ctx = chart.chart.ctx;
            
                    //Get options from the center object in options
            var centerConfig = chart.config.options.elements.center;
            var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
            var color = centerConfig.color || '#000';
            var sidePadding = centerConfig.sidePadding || 20;
            var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
            //Start with a base font of 30px
            ctx.font = "30px " + fontStyle;
            
                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
            var stringWidth = ctx.measureText(txt).width;
            var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

            // Find out how much the font can grow in width.
            var widthRatio = elementWidth / stringWidth;
            var newFontSize = Math.floor(30 * widthRatio);
            var elementHeight = (chart.innerRadius * 2);

            // Pick a new font size so it will not be larger than the height of label.
            var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
            ctx.font = "bold " + fontSizeToUse+"px " + fontStyle;
            ctx.fillStyle = color;
            
            //Draw text in center
            ctx.fillText(txt, centerX, centerY);
                }
            }
        });

        const media_geral_options = {
            cutoutPercentage: 70,
            responsive: true,
            legend: false,
            animation: {
                animateRotate: true,
            },
            elements: {
                center: {
                    text: '',
                    sidePadding: 40,
                }
            }
        };

        @isset ($user->simulado)
        @if ($user->simulado->realizado)

        // ────────────────────────────────────────────────────────────────────────────────
        // ────────────────────────────── Meu desempenho ──────────────────────────────────
        // ────────────────────────────────────────────────────────────────────────────────

        var qtdQuestoes = $(".plataforma-content").data("questoes");
        //console.log(qtdQuestoes);

        //var $simulado está sendo modificada em algum momento - garantir qtdquestoes n mude / TODO:investigar
        @php
            $simulado->questoes = $qtdQuestoes;
        @endphp

        // console.log({{ $simulado->questoes}});
       // console.log({{ $user->simulado->tempo}});

        // Acertos
        media_geral_options.elements.center.text = '{{ $user->simulado->acertos . "/" . $simulado->questoes }}';
        new Chart(document.getElementById('meu_desempenho_acertos'), {
            type: 'doughnut',
            data: {
                labels: ['Acertos', 'Erros'],
                datasets: [{
                    data: [{{$user->simulado->acertos}}, {{$simulado->questoes}} - {{$user->simulado->acertos}}],
                    backgroundColor: ['#073982', '#e0e0e0'],
                }],
            },
            options: media_geral_options,
        });

        // Nota
        <?php
            $meu_desempenho_nota = $user->simulado->acertos * (1000 / $simulado->questoes);
        ?>
        media_geral_options.elements.center.text = '<?php echo $meu_desempenho_nota < 1000 ? number_format($meu_desempenho_nota, 1, ",", ".") : $meu_desempenho_nota; ?>';
        new Chart(document.getElementById('meu_desempenho_nota'), {
            type: 'doughnut',
            data: {
                labels: ['Nota', ''],
                datasets: [{
                    data: [<?php echo $meu_desempenho_nota < 1000 ? number_format($meu_desempenho_nota, 2) : $meu_desempenho_nota; ?>, <?php echo (1000 - $meu_desempenho_nota) < 1000 ? number_format(1000 - $meu_desempenho_nota, 2) : 1000 - $meu_desempenho_nota; ?>],
                    backgroundColor: ['#073982', '#e0e0e0'],
                }],
            },
            options: media_geral_options,
        });

        // Tempo
        media_geral_options.elements.center.text = '{{intval($user->simulado->tempo/ 100 / 60) > 0 ? intval($user->simulado->tempo/ 100 / 60) : number_format($user->simulado->tempo/ 100 / 60, 1)}} min';
        new Chart(document.getElementById('meu_desempenho_tempo'), {
            type: 'doughnut',
            data: {
                labels: ['Tempo'],
                datasets: [{
                    data: [{{intval($user->simulado->tempo / 100 / 60) > 0 ? intval($user->simulado->tempo/ 100 / 60) : number_format($user->simulado->tempo/ 100 / 60, 1)}}],
                    backgroundColor: ['#073982', '#e0e0e0'],
                }],
            },
            options: media_geral_options,
        });

        // ────────────────────────────────────────────────────────────────────────────────
        // ─────────────────────────────── Média geral ────────────────────────────────────
        // ────────────────────────────────────────────────────────────────────────────────

        console.log("{{'acertos: '. $acertos . '/questoes: '. $simulado->questoes}}");
        // Acertos
        media_geral_options.elements.center.text = '{{number_format($acertos, 1) . "/" . $simulado->questoes}}';
        new Chart(document.getElementById('media_geral_acertos'), {
            type: 'doughnut',
            data: {
                labels: ['Acertos', 'Erros'],
                datasets: [{
                    data: [{{number_format($acertos, 1)}}, {{$simulado->questoes - number_format($acertos, 1)}}],
                    backgroundColor: ['#45da3d', '#e0e0e0'],
                }],
            },
            options: media_geral_options,
        });

        // Nota
        <?php
            $media_geral_nota = $acertos * (1000 / $simulado->questoes);
        ?>

        console.log("{{'acertos: '. $acertos . '/questoes: '. $simulado->questoes  . '/media geral: '. $media_geral_nota}}");
        media_geral_options.elements.center.text = '<?php echo $media_geral_nota < 1000 ? number_format($media_geral_nota, 1, ",", ".") : $media_geral_nota; ?>';
        new Chart(document.getElementById('media_geral_nota'), {
            type: 'doughnut',
            data: {
                labels: ['Nota', ''],
                datasets: [{
                    data: [<?php echo $media_geral_nota < 1000 ? number_format($media_geral_nota, 2) : $media_geral_nota; ?>, <?php echo (1000 - $media_geral_nota) < 1000 ? number_format(1000 - $media_geral_nota, 2) : 1000 - $media_geral_nota; ?>],
                    backgroundColor: ['#45da3d', '#e0e0e0'],
                }],
            },
            options: media_geral_options,
        });

        // Tempo
        media_geral_options.elements.center.text = '{{intval( $tempos /1000 / 60) > 0 ? intval( $tempos /1000 / 60) : number_format($tempos /1000 / 60, 1)}} min';
        new Chart(document.getElementById('media_geral_tempo'), {
            type: 'doughnut',
            data: {
                labels: ['Tempo'],
                datasets: [{
                    data: [{{intval($simulado->tempos / 60) > 0 ? intval($tempos/1000 / 60) : number_format($tempos/1000 / 60, 1)}}],
                    backgroundColor: ['#45da3d'],
                }],
            },
            options: media_geral_options,
        });

        @endif
        @endisset
    </script>