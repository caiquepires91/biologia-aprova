@php
    $areas_intensivao = [15, 19];
    $delay = "-3 hours";
@endphp

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v10.0&appId=166218681998316&autoLogAppEvents=1" nonce="7micjaFt"></script>

<!-- plataforma -->
<link href="{{URL::to('/css/curso/plataforma.css?v=1.4')}}" rel="stylesheet">
<link href="{{URL::to('/css/curso/jquery.rateyo.min.css')}}" rel="stylesheet">
<!-- FontAwesome -->
<link rel="stylesheet" href="{{url::to('assets/lib/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!-- Slick -->
<link rel="stylesheet" href="{{url::to('assets/lib/slick/slick.css')}}">
<!-- Aos -->
<link rel="stylesheet" href="{{url::to('assets/lib/aos/dist/aos.css')}}">
<!-- Main CSS -->
<!-- Custom CSS -->
<link rel="stylesheet" href="{{url::to('assets/css/custom.css')}}">
<!-- /plataforma -->

<style>
    .container-lg {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    .aula-documento-item {
        display: flex;
        align-items: center;
        width: 100%;
        font-size: x-large;
        font-family: 'Nunito';
        color: #373435;
        border-radius: 5px;
        padding: 14px 13px 12px;
        border: 2px solid #032c67;
        font-weight: bold;
        transition: all .3s ease-in-out;
        cursor: pointer;
        margin-top: 10px;
    }

    .aula-documento-icone {
        padding: 0 5px;
        margin-right: 10px;
        display: flex;
        align-items: center;
        margin-left: auto;
    }

    .aula-documento-titulo {
        margin-right: auto;
    }

    .aula-documento-titulo p {
        margin: 0 !important;
        padding: 0 !important;
    }

    .embed-responsive {
        position: relative;
        display: block;
        width: 100%;
        padding: 0;
        overflow: hidden;
    }

    .embed-responsive-16by9::before {
        padding-top: 56.25%;
    }
    .embed-responsive::before {
        display: block;
        content: "";
    }
    .embed-responsive .embed-responsive-item, .embed-responsive embed, .embed-responsive iframe, .embed-responsive object, .embed-responsive video {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 0;
    }

    @media (min-width: 576px) {
        .container-lg {
            max-width: 540px;
        }
    }
    @media (min-width: 768px) {
        .container-lg {
            max-width: 720px;
        }
    }
    @media (min-width: 992px) {
        .container-lg {
            max-width: 960px;
        }
    }
    @media (min-width: 1200px) {
        .container-lg {
            max-width: 1150px;
        }
    }
    @media (min-width: 1300px) {
        .container-lg {
            max-width: 1250px;
        }
    }
</style>

<?php
    // Navegação
    $curso["navegacao"] = [
        [
            'titulo' => 'Área do aluno',
            'icone' => "",
            'url' => url::to('/conta/plano/trocar/' . $user->plano->id),
            'active' => false,
        ], [
            'titulo' => $aula->nome . " " .  $user->plano->plano,
            'icone' => "fa fa-play",
            'url' => url::to('/conta/curso/aula/') .'/'. $aula->id . '/' . $user->plano->id . "?tipo=" . $tipo,
            'active' => true,
        ],
    ];
?>

<!-- navegação-->
<div class="plataforma-navegacao margin-header">
        <div class="container-lg">
            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <ul class="navegacao">
                        <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                        <?php foreach ($curso["navegacao"] as $nav) { ?>
                        <li class="<?php echo $nav["active"] ? "active" : ""; ?>">
                            <a href="<?php echo $nav["url"]; ?>"><i class="<?php echo $nav["icone"]; ?>"></i> <?php echo $nav["titulo"]; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-md-4 col-xl-3">
                    <div class="plataforma-progresso">
                        <p>Progresso</p>
                       <!--  <div class="barra-progresso">
                            <div style="{{'width:' .  $qtdVideosAssistidos*100/$qtdVideos . '%'}}"></div>
                        </div> -->
                        <div class="progresso-bar" data-porcentagem="{{$qtdVideosAssistidos*100/$qtdVideos}}" style="width: 200px; height: 5px; margin-left: 10px; border-radius: 5px; display: flex; overflow: hidden;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /navegação -->

    <div class="plataforma-video-content base_url" data-aula="{{$aula->id}}" data-url="{{url::to('/')}}">
        <div class="plataforma-video-menu-roll"><i class="fa fa-chevron-left"></i></div>
        <div class="plataforma-video-menu" style="height: 100%;">
            @isset ($playlists)
            @foreach ($playlists as $playlist)

            <!-- CATEGORIA -->
             <div class="plataforma-video-categoria">
                <div class="categoria-header">
                    <div>
                        <h2>{{$playlist->nome}}</h2>
                        <ul>
                            <li>{{count($playlist->videos)}}  {{count($playlist->videos) == 1 ? 'aula' : 'aulas'}}</li>
                            <li></li>
                        </ul>
                    </div>
                    <i class="fa fa-chevron-down categoria-close"></i>
                </div>
                <div class="categoria-body">
                    @foreach ($playlist->videos as $video)
                        @if ($video->tipo == config('constants.video.link'))
                            <div  class="categoria-video" data-link="{{$video->src}}" onclick="goTo(this);">
                                <div class="video-placeholder" style="background-image:url('{{url::to($video->img)}}');">
                                </div>
                                <div>
                                    <p>{{$video->nome}}</p>
                                    <p style="font-size: 12px;">{{date('l', strtotime($delay, strtotime($video->data_inicio)))}}, {{date('d/m', strtotime($delay, strtotime($video->data_inicio)))}} · {{date('H', strtotime($delay, strtotime($video->data_inicio)))}}h às {{date('H', strtotime($delay, strtotime($video->data_fim)))}}h</p>
                                </div>
                                <i class="fas fa-link categoria-play"></i>
                            </div>
                        @else
                        <div data-id="{{$video->id}}"  data-plano="{{$user->plano->id}}" data-playlist="{{$tipo == 'playlist_med' ? true : false}}" class="categoria-video video {{!$video->liberado ? 'video-nao-liberado' : '' }} {{$video->assistido ? 'assistido' : ''}}">
                            <div class="video-placeholder" style="background-image:url('{{url::to('/') .'/'. $video->img}}');">
                                <!-- <div class="video-duracao">duração?</div> -->
                            </div>
                            <p>Aula {{$video->aula}} - {{$video->nome}}</p>
                            <i class="fa fa-play-circle-o categoria-play"></i>

                            @if (!$video->liberado)
                            <div class="video-liberacao">
                                <p>Este vídeo será liberado em <strong>data de liberação?</strong>.</p>
                            </div>
                            @endif
                        </div>
                        @endif
                    @endforeach
                    <!-- QUESTIONARIOS -->
                    @isset($playlist->questionarios)
                    @foreach ($playlist->questionarios as $questionario)
                        <div data-id="{{$questionario->id}}" data-plano="{{$user->plano->id}}" data-questionario=true class="categoria-video questionario {{!$questionario->ativo ? 'video-nao-liberado' : '' }} {{$questionario->realizado ? 'assistido' : ''}}">
                            <div class="video-placeholder" style="background-image:url('{{url::to('/img/quiz.jpg')}}');">
                            </div>
                            <p>Quiz - {{$questionario->nome}}</p>
                            <i class="fas fa-clipboard-list categoria-play"></i>

                            @if (!$questionario->ativo)
                            <div class="video-liberacao">
                                <p>Este qustinario ainda não está liberado.</p>
                            </div>
                            @endif
                        </div>
                    @endforeach
                    @endisset
                    <!-- /QUESTIONARIOS -->
                </div>
            </div>
            <!-- /CATEGORIA -->
            @endforeach
            @endisset
        </div>

        <div class="plataforma-video-conteudo">
           @if ($tipo == 'questionario')

           @php
           $simulado = $aula;
           $letters = ["a", "b", "c", "d", "e"];
           @endphp

           <style>
            .form-check-label.checked {
                font-weight: bold;
                color: #073982;
            }
            .form-check-label p {
                display: inline-block;
            }
            .plataforma-simulado-questao p {
                word-break: break-word;
            }
        
            .form-check-input:checked {
                background-color: #049676;
                border-color: #049676;
            }

            .form-check-label.correta {
                font-weight: bold;
                color: green;
            }

            .button.button-primaria:disabled, .button.button-primaria:disabled {
                background-color: #a6c5d1;
                color: #373435;
            }
        
            p {
                margin:0;
            }

            .plataforma-navegacao {
                font-family: 'Lufga';
            }
        

            .plataforma-navegacao .plataforma-progresso p {
                font-size: .85rem;
                color: white;
                margin-bottom: 4px;
                font-weight: 500;
            }
        
            </style>

            
            @isset ($user->simulado)
            <!-- Meu desempenho -->
           <!--  <div class="plataforma-simulado-header" style="margin-top: 40px;">
            <h2>
                Meu desempenho<br />
                <p>Realizado em {{date("d/m/Y \à\s H:i", strtotime($user->simulado->data))}}</p>
            </h2>
            </div>

            <div class="row">
            <div class="col-md-4">
                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Acertos</p>
                <canvas id="meu_desempenho_acertos" width="100%"></canvas>
            </div>
            <div class="col-md-4">
                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Nota</p>
                <canvas id="meu_desempenho_nota" width="100%"></canvas>
            </div>
            <div class="col-md-4">
                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">Tempo</p>
                <canvas id="meu_desempenho_tempo" width="100%"></canvas>
            </div>
            </div>

            <div class="row justify-content-center align-items-center">
            <div class="col-3 my-3" style="display:flex">
                <a href="{{url::to('/') . '/conta/curso/simulado/resultado/' . $simulado->id . '?tipo=' . $tipo }}" class="button button-primaria" style="margin:auto ;">Ver resultado</a>
            </div>
            </div> -->
            @endisset


            <div class="row">
                <div class="col-lg-12">
                <form id="form_simulado" action="{{url::to('/simulado/finalizar')}}" method="POST">
                    <input type="hidden" name="id" value="{{$simulado->id}}">
                    <input type="hidden" name="id_realizacao" value="{{$simulado->id}}">
                    <input type="hidden" id="simulado_duracao" name="duracao" value="00:00:00">

                    <div class="plataforma-simulado-header header-topo">
                        <h2>
                            <i class="fa fa-edit"></i> {{$simulado->nome}}<br />
                            <!-- <p>Cadastrado em {{date("d/m/Y \à\s H:i", strtotime($simulado->data))}}</p> -->
                        </h2>
                    </div>
                    
                    @isset ($simulado->perguntas)
                    @foreach ($simulado->perguntas as $key=>$questao)
                    <div class="plataforma-simulado-questao" data-id="{{$questao->id}}">
                        <div class="plataforma-simulado-header">
                            <h2>
                                Questão {{intval($key) + 1}}
                            </h2>
                        </div>

                        {!! $questao->texto !!}

                        <div class="questao-alternativas">
                            @isset ($questao->alternativas)
                            @foreach ($questao->alternativas as $key=>$alternativa)
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="questoes[{{$questao->id}}]" value="{{$key+1}}" required> {{$letters[$key]}}) {!!$alternativa->texto!!}
                                </label>
                            </div>
                            @endforeach
                            @endisset
                        </div>
                    </div>
                    @endforeach
                    @endisset
                    

                    <div class="plataforma-simulado-footer">
                        <button type="submit" class="button button-primaria my-3">Finalizar quiz</button>
                    </div>
                    
                    </form>
                </div>  
            </div>
           <!--  <div class="simulado-contagem">
                <i class="fa fa-clock-o"></i>
                <p>00:00:00</p>
            </div> -->
           @else 
           <div class="video-embed" style="{{$aula->tipo == 2 ? 'width: 90%' : ''}}">
                <div class="d-flex">
                    <div class="embed-responsive embed-responsive-16by9" style="box-shadow: 0 0 30px rgba(0, 0, 0, .1); border-radius: 5px; overflow: hidden;">
                        <iframe id="iframe_video" class="embed-responsive-item" data-url="{{$aula->src}}" allowfullscreen src="{{$aula->src}}"></iframe>
                    </div>
                    @isset($aula->src_chat)
                        <iframe src="{{$aula->src_chat}}" width="500" height="100" frameborder="0"></iframe>
                    @endisset
                </div>

                @isset($documentos)
                <div class="row" style="margin-top: 10px;">
                    @foreach($documentos as $documento)
                    <div class="{{count($documentos) > 1 ? 'col-md-6' : 'col-md-12' }}">
                        <a href="{{url::to('/') .'/'. $documento->url}}" target="_blank" data-id="{{$documento->id}}" class="aula-documento-item">
                            <div class="aula-documento-icone">
                                <i class="fa fa-download"></i>
                            </div>
                            <div class="aula-documento-titulo">
                                <p>{{$documento->nome}}</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                @endisset
            </div>

            <div class="video-separator"></div>

            <div class="video-informacoes">
                <h2>Aula {{$aula->aula}} - {{$aula->nome}}</h2>

                <div class="video-avaliacao">
                    <!-- <p>Avalie esta aula:</p> -->
                    <!-- codigo avaliacao -->
                </div>
            </div>

            <div class="row">
                <div class="col-lg-9 video-detalhes">
                    <!-- add descriçao video -->
                </div>
            </div>

            <div class="video-separator"></div>

            <div style="width: 100%;">
                <div class="fb-comments" data-href="{{url::to('/') . '/conta/curso/aula/'. $aula->id}}" data-width="100%" data-numposts="5"></div>
            </div>
           @endif
        </div>
    </div>

    <div class="bg-responsivo"></div>

    <div id="loading_overlay" class="loading">
        <div class="loading-icon">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
            <span class="sr-only">Carregando...</span>
        </div>
        <p>Carregando...</p>
    </div>

    
    <script src="{{url::to('assets/lib/aos/dist/aos.js')}}"></script>
    <script src="{{url::to('assets/lib/slick/slick.min.js')}}"></script>
    <script src="{{url::to('assets/js/moment.js')}}"></script>
    <script src="{{url::to('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{url::to('assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{url::to('assets/js/login.js')}}"></script>
    <script src="{{url::to('assets/js/main.js')}}"></script>
    <script src="{{url::to('assets/js/custom.js')}}"></script>  
    
    <script src="{{URL::to('/js/jquery.iframetracker.min.js')}}"></script>
    <script src="{{URL::to('/js/jquery.rateyo.min.js')}}"></script>
    <script src="{{URL::to('js/plataforma.js?v=1.0')}}"></script>
    <script src="{{URL::to('/js/progressbar.js?v=1.0')}}"></script>
    <script src="{{URL::to('/js/easytimer.min.js')}}"></script>
    <script src="{{URL::to('/js/simulado.js?v=1.0')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <!-- Scripts -->
    <script type="text/javascript">
        var is_home = false;
        var id_aula = $(".plataforma-video-content").data("aula");
        var tipo_aula = "{{$tipo}}";
        var video_visualizado = false;

        // console.log(tipo_aula);

        var iframe =  $('#iframe_video');
        if ("{{$tipo}}" != "questionario") {
            var video = new Vimeo.Player(iframe);
            video.on('play', function() {
               // console.log("video played!");

                var formData = new FormData();
                formData.append("idUser", "{{$user->id}}");
                formData.append("id_video", id_aula);
                //console.log("{{$user->id}}");
                //console.log(id_aula);

                if (!video_visualizado) {
                    $.post("{{url::to('aula/play')}}", {idUser:"{{$user->id}}", id_video:id_aula}, function(data) {
                        //console.log("data: " + data.success);
                        if (data.success) {
                            video_visualizado = true;
                            //console.log("video " + data.videoUsuario.id + " marcadao como assistido!");
                        } else {
                            //console.log("marcar como assistido falhou!!");
                        }
                    })
                    .fail(function() {
                        //console.log("marcar como asssistido falhou!!")
                    });
                }
            });
        }
      
    </script>

<!-- Questionario -->
<script type="text/javascript">

    var timer = new easytimer.Timer();

    $(document).ready(function() {
        var imagens = document.querySelectorAll(".plataforma-simulado-questao img");
        for (i=0; i < imagens.length; i++) {
            imagens[i].setAttribute('src', "{{url::to('/')}}/" + imagens[i].getAttribute('src'));
        }

        timer.start();
        timer.addEventListener('secondsUpdated', function (e) {
            $('.simulado-contagem p').html(timer.getTimeValues().toString());
            $('#simulado_duracao').val(timer.getTimeValues().toString());
        });
        
        $("#form_simulado").submit(function(e){
            e.preventDefault();
            e.stopPropagation();

            var gabarito = "{{$simulado->gabarito}}";
            var correctAnswers = gabarito.split(',');

            var userAnswer = '';
            var numCorrect = 0;
            var userAnswersString = '';

            var alternativas = $("#form_simulado").children('.plataforma-simulado-questao');
            for (var i=0; i < alternativas.length; i++) {
                label =  alternativas[i].querySelector('.checked');
                checkedInput = alternativas[i].querySelector("input[name='questoes[" + $(alternativas[i]).data('id') + "]']:checked");
                //console.log("input[name='questoes[" + $(alternativas[i]).data('id') + "]']");
                inputs = $(alternativas[i]).find("input");
                //console.log("input correto: " + $(inputs[correctAnswers[i] - 1]).val());
                $(inputs[correctAnswers[i] - 1]).parent().addClass('correta');
                $(inputs[correctAnswers[i] - 1]).prop('checked', true);
                //console.log("input " + i + " size: " + inputs.length);
                userAnswer = (checkedInput||{}).value;
                if (i) userAnswersString = userAnswersString + ',' + userAnswer;
                else userAnswersString = userAnswersString + userAnswer;
                //console.log("resposta user:" + userAnswer);
                //console.log("resposta correta:" + correctAnswers[i]);
                if (userAnswer == correctAnswers[i]) {
                    numCorrect++;
                    iconRight = document.createElement("i");
                    iconRight.setAttribute('class', 'fa fa-check');
                    label.append(iconRight);
                    label.style.color = 'green';
                } else {
                    iconWrong = document.createElement("i");
                    iconWrong.setAttribute('class', 'fa fa-times');
                    label.append(iconWrong);
                    label.style.color = 'red';
                }
            }

            var idSimulado = $("#form_simulado").find("input[name='id']").val();
            var tempo = "00:00:00";
            var userId = "{{$user->id}}";

            var formData = new FormData();
            formData.append('idUser', userId);
            formData.append('idSimulado', idSimulado);
            formData.append('respostas', userAnswersString);
            formData.append('acertos', numCorrect);
            formData.append('tempo', strTimeToSec(tempo));

            $.ajax({
                url: $(this).attr('action'),
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    if (data.success) {
                        $(".button.button-primaria").prop('disabled', true);
                    } else {
                        console.log("finalização falhou!!");
                    }
                    $(".questao-alternativas input").prop('disabled', true);
                }
            });

        });
    });

    function strTimeToSec(time) {
        timeArray = time.split(':');
        hour = timeArray[0] * 60 * 60;
        min = timeArray[1] * 60;
        sec = timeArray[2];
        return hour + min + sec;
    }

    function goTo(e) {
        let link = $(e).data('link');
        window.open(link, "_target");
    }

</script>
    