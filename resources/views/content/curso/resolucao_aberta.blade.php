<?php

// Navegação
$curso['navegacao'] = [
    [
        'titulo' => $simulado->nome,
        'icone' => 'fa fa-file-text',
        'url' => url::to('/conta/curso/simulado/resolucao-aberta') . '/' . $simulado->id . '/' . $user_aberto->id,
        'active' => true,
    ],
];

$letters = ['a', 'b', 'c', 'd', 'e'];

?>

<!-- plataforma -->
<link href="{{ URL::to('/css/curso/plataforma.css?v=1.4') }}" rel="stylesheet">
<link href="{{ URL::to('/css/curso/jquery.rateyo.min.css') }}" rel="stylesheet">

<!-- FontAwesome -->
<link rel="stylesheet" href="{{ url::to('assets/lib/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!-- Slick -->
<link rel="stylesheet" href="{{ url::to('assets/lib/slick/slick.css') }}">
<!-- Aos -->
<link rel="stylesheet" href="{{ url::to('assets/lib/aos/dist/aos.css') }}">
<!-- Main CSS -->
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ url::to('assets/css/custom.css') }}">
<!-- /plataforma -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">

<style>
    .form-check-label.checked {
        font-weight: bold;
        color: #073982;
    }

    .form-check-label p {
        display: inline-block;
    }

    .plataforma-simulado-questao p {
        word-break: break-word;
    }

    .form-check-input:checked {
        background-color: #049676;
        border-color: #049676;
    }

    .plataforma-navegacao {
        font-family: 'Lufga';
    }

    p {
        margin: 0;
    }

    .questao-alternativas .correta {
        color: green !important;
        font-weight: bold !important;
    }

    .questao-alternativas .correta::after {
        content: "✔";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-left: 4px;
        color: green;
        font-family: 'Montserrat', sans-serif;
        font-weight: bold;
    }

    .questao-alternativas .errada {
        color: red !important;
        font-weight: bold !important;
    }

    .questao-alternativas .errada::after {
        content: "✖";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-left: 4px;
        color: red;
        font-family: 'Montserrat', sans-serif;
        font-weight: bold;
    }

    .plano:hover {
        animation: unset !important;
    }

    .errada::before {
        content: "✖";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-right: 4px;
        color: red;
        font-weight: bold;
    }

    .correta::before {
        content: "✔";
        display: inline-block;
        font-size: 1rem;
        line-height: 1.2rem;
        margin-right: 4px;
        color: green;
        font-weight: bold;
    }

    .titulo-simulado {
        margin-top: 85px;
    }

    .plano-row {
        margin-top: 100px;
        margin-bottom: 100px;
    }

    @media (max-width: 991.98px) {
        .plano-principal .plano-header {
            margin-left: -9px;
        }

        .titulo-simulado {
            margin-top: unset;
        }

        .plano-row {
            margin-top: 73px;
            margin-bottom: 20px;
        }

    }

    @media (max-width: 575.98px) {
        .plano-row {
            margin-top: 20px;
            margin-bottom: 20px;
        }
    }
</style>

<!-- navegação-->
<div class="plataforma-navegacao margin-header">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-8 col-xl-9">
                <ul class="navegacao">
                    <li><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>
                    <?php foreach ($curso["navegacao"] as $nav) { ?>
                    <li class="<?php echo $nav['active'] ? 'active' : ''; ?>">
                        <a href="<?php echo $nav['url']; ?>"><i class="<?php echo $nav['icone']; ?>"></i> <?php echo $nav['titulo']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="col-md-4 col-xl-3">
                <div class="plataforma-progresso">
                    {{-- <p>Progresso</p> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /navegação -->

<!-- header -->
<div class="plataforma-header"
    style="background-image: linear-gradient(to right, rgb(110, 179, 0) , rgb(4, 150, 118));">
    <div class="container-lg">
        <div class="plataforma-header-content">
            <!-- perfil -->
            <div class="plataforma-perfil">
                <div class="plataforma-perfil-foto profile-picture"
                    style="background-image:url({{ url::to('/img/profile/') . '/' . 'perfil.jpg' }})"></div>
                <div class="plataforma-perfil-dados">
                    <p class="opacity-1">Bem-vindo(a),</p>
                    <h2>
                        @if ($user_aberto)
                            {{ $user_aberto->nome }}
                        @endif
                    </h2>
                    <!-- <p class="opacity-5">Último acesso: 13/01/2019 às 14:40</p> -->
                </div>
            </div> <!-- /perfil -->

            <!-- plano atual -->
            @isset($user)
                <div class="plataforma-plano">
                    <p>Plano ativo</p>
                    @isset($user->plano)
                        <h2>{{ $user->plano->plano }}</h2>
                        <p>Expira em <strong>{{ date('d/m/Y', strtotime($user->plano->fim)) }}</strong></p>
                    @endisset
                </div>
            @endisset
            <!-- /plano atual -->
        </div>
    </div>
</div>
<!-- /header -->

<!-- conteúdo -->
<div class="container-lg base_url" url-data="{{ url::to('/') }}">
    <div class="row plataforma-row">
        <div class="col-12">
            <div class="plataforma-content">
                <div class="plataforma-content-header">
                    <h2>Simulado</h2>
                    <ul>
                        @if ($user && $user->plano->plano === 'INTENSIVÃO ENEM')
                            <li>2 simulados</li>
                        @else
                            @isset($qtdSimulados)
                                <li>{{ $qtdSimulados }} {{ $qtdSimulados == 1 ? 'simulado' : 'simulados' }}</li>
                            @endisset
                            @isset($user)
                                <li>{{ $user->simuladosRealizados != null ? $user->simuladosRealizados : '0' }} realizados
                                </li>
                            @endisset
                        @endif
                    </ul>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_simulado" action="{{ url::to('/simulado/finalizar') }}" method="POST">
                            <input type="hidden" name="id" value="{{ $simulado->id }}">
                            <input type="hidden" name="id_realizacao" value="{{ $simulado->id }}">
                            <input type="hidden" id="simulado_duracao" name="duracao" value="00:00:00">

                            <div class="plataforma-simulado-header header-topo">
                                <h2>
                                    <i class="fa fa-edit"></i> {{ $simulado->nome }}<br />
                                    <!-- <p>Cadastrado em {{ date('d/m/Y \à\s H:i', strtotime($simulado->data)) }}</p> -->
                                </h2>
                            </div>

                            @isset($simulado->perguntas)
                                @foreach ($simulado->perguntas as $key => $questao)
                                    <div class="plataforma-simulado-questao" data-id="{{ $questao->id }}">
                                        <div class="plataforma-simulado-header">
                                            <h2>
                                                Questão {{ intval($key) + 1 }}
                                            </h2>
                                        </div>

                                        {!! $questao->texto !!}

                                        <div class="questao-alternativas">
                                            @isset($questao->alternativas)
                                                @foreach ($questao->alternativas as $key => $alternativa)
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input"
                                                                name="questoes[{{ $questao->id }}]"
                                                                value="{{ $key + 1 }}" required> {{ $letters[$key] }})
                                                            {!! $alternativa->texto !!}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endisset
                                        </div>
                                        <div class="my-3 p-3 questao-comentario"
                                            style=" background: #3eb37830; border-radius: 6px; display:none">
                                            <span><b>Comentário:</b></span>
                                            <span style="font-size:12px;">{!! $questao->comentario !!}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endisset


                            <div id="btn_finalizar_wrapper" class="plataforma-simulado-footer">
                                <button type="submit" class="button button-primaria my-3">Finalizar simulado</button>
                            </div>

                        </form>
                        <div id="btn_continuar_wrapper" class="plataforma-simulado-footer" style="display: none">
                            <button class="button button-primaria my-3" onclick="continuar();">Continuar</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- PLANO 50% -->
    <div class="row plano-row" id="info-desconto" style="display:none">
        <div class="col">
            <div id="planos-section-2" class="row justify-content-center align-items-center">
                <div class="col-md-12">
                    <div class="plano position-relative m-md-auto plano-principal d-none d-md-block"
                        style="width: 700px; transition: none;">
                        <div class="container plano-header align-items-center" style="width:768px">
                            <p class="pt-3 mx-auto">CURSO TOTAL</p>
                            <p class="mb-auto mx-auto">+ bônus</p>
                        </div>
                        <div class="plano-body m-md-auto w-100 pt-5 px-5 border bg-light border-2 bloco">
                            <p style="font-size: 1.5rem;margin: -2.2rem;color: #212529;padding-bottom: 12rem;">Você
                                acaba de ganhar 50% para assinar o nosso plano!</p>
                            <div class="row">
                                <div class="col-md-7">
                                    <div id="plano-scroll" class="vantagens mb-3">
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;
                                        </p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os
                                            assuntos de biologia;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de
                                            questões;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e
                                            gabaritados;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos
                                            e comentados;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para
                                            memorização;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <p style="font-size: 1.2rem;">de 12x <s style="font-size: 1.7rem;">R$21,90</s> por
                                    </p>
                                    <span>
                                        <h2 class="position-relative" style="margin-left: 24%">
                                            <span class="position-absolute end-100"
                                                style="top: 29%;font-size: 16px;color: gray;">12x</span>
                                            <span class="position-absolute end-100 plano-preco-b"
                                                style="top: 42%;">R$</span>
                                            10
                                            <span class="position-absolute start-100 plano-preco-b"
                                                style="top: 42%;">,95</span>
                                        </h2>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex" style="margin-top: -83px;width: inherit;">
                            <button class="green-btn btn rounded-pill mx-auto" data-plano="5" type="button"
                                onclick="goToLogin(this);" style="font-size: xx-large; width:228px">eu quero!</button>
                        </div>
                    </div>


                    <div class="plano position-relative m-0 plano-principal d-md-none"
                        style="width: 100%; transition: none;margin-bottom: 0px;">
                        <p style="font-size: 1.5rem;color: #212529;text-align: center;">Você acaba de ganhar 50% para
                            assinar o nosso plano!</p>
                        <div class="container plano-header align-items-center"
                            style="height:auto;padding-bottom:15px;width:100%;margin-bottom: unset; z-index: 5; width: 100%; margin-left: unset;">
                            <p class="pt-3 mx-auto">curso completo</p>
                            <p class="mb-auto mx-auto">+ bônus</p>
                        </div>
                        <div class="plano-body m-md-auto w-100 border bg-light border-2 bloco" style="padding-top:4%">
                            <div class="row">
                                <div class="col-md-7">
                                    <div id="plano-scroll" class="vantagens mb-3">
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>12 meses de acesso;
                                        </p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>aulas sobre todos os
                                            assuntos de biologia;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>vídeos resoluções de
                                            questões;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>simulados comentados e
                                            gabaritados;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>exercícios resolvidos
                                            e comentados;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>músicas para
                                            memorização;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>material de apoio;</p>
                                        <p style="font-size: 1rem;"><i class="fas fa-check"></i>e muito mais!</p>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <p style="font-size: 1.2rem;">de 12x <s style="font-size: 1.7rem;">R$21,90</s> por
                                    </p>
                                    <span>
                                        <h2 class="position-relative" style="margin-left: 24%">
                                            <span class="position-absolute end-100"
                                                style="top: 29%;font-size: 16px;color: gray;">12x</span>
                                            <span class="position-absolute end-100 plano-preco-b"
                                                style="top: 42%;">R$</span>
                                            10
                                            <span class="position-absolute start-100 plano-preco-b"
                                                style="top: 42%;">,95</span>
                                        </h2>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex" style="margin-top: -70px;width: inherit;">
                            <button class="green-btn btn rounded-pill mx-auto" data-plano="5" type="button"
                                onclick="goToLogin(this);" style="font-size: xx-large; width:228px">eu quero!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- /conteúdo -->



<div class="mt-5"div>

    <div class="simulado-contagem">
        <i class="fa fa-clock-o"></i>
        <p>00:00:00</p>
    </div>

    <script src="{{ url::to('assets/lib/aos/dist/aos.js') }}"></script>
    <script src="{{ url::to('assets/lib/slick/slick.min.js') }}"></script>
    <script src="{{ url::to('assets/js/moment.js') }}"></script>
    <script src="{{ url::to('assets/js/sweetalert.min.js') }}"></script>
    <script src="{{ url::to('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ url::to('assets/js/login.js') }}"></script>
    <script src="{{ url::to('assets/js/main.js') }}"></script>
    <script src="{{ url::to('assets/js/custom.js') }}"></script>

    <script src="{{ URL::to('/js/jquery.iframetracker.min.js') }}"></script>
    <script src="{{ URL::to('/js/jquery.rateyo.min.js') }}"></script>
    <script src="{{ URL::to('/js/easytimer.min.js') }}"></script>
    {{-- <script src="{{URL::to('/js/plataforma.js')}}"></script> --}}
    {{-- <script src="{{URL::to('/js/progressbar.js')}}"></script> --}}
    <script src="{{ URL::to('/js/simulado.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <script type="text/javascript">
        var is_home = false;
        var link = "";
        var timer = new easytimer.Timer();
        timer.start();
        timer.addEventListener('secondsUpdated', function(e) {
            $('.simulado-contagem p').html(timer.getTimeValues().toString());
            $('#simulado_duracao').val(timer.getTimeValues().toString());
        });

        $(document).ready(function() {
            var imagens = document.querySelectorAll(".plataforma-simulado-questao img");
            for (i = 0; i < imagens.length; i++) {
                let src = imagens[i].getAttribute('src');
                let substring = 'http';
                let hasSubstring = src.search(substring);
                if (hasSubstring < 0)
                    imagens[i].setAttribute('src', "{{ url::to('/') }}/" + imagens[i].getAttribute('src').replace(
                        "../", ""));

                let substring_alt = "https://biologiaaprova.com.br/./uploads/simulado/imgs/";
                let correct_string = "https://admin.biologiaaprova.com.br/production/uploads/simulado/imgs/";
                let hasSubstring_alt = src.search(substring_alt);
                if (hasSubstring_alt < 0)
                    imagens[i].setAttribute('src', imagens[i].getAttribute('src').replace(substring_alt,
                        correct_string));
            }
        });

        function goToLogin(elem) {
            var plano = $(elem).data("plano");
            window.location.href = "{{ url::to('planos/choose') }}" + "/" + plano + "/50";
        }

        // centrarlizar imagens de questões não centralizadas.
        var span_parents = $(".plataforma-simulado-questao").find("img");
        for (i = 0; i < span_parents.length; i++) {
            $(span_parents[i]).parent().html("<center>" + $(span_parents[i]).parent().html() + "</center>");
        }

        $("#form_simulado").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var gabarito = "{{ $simulado->gabarito }}";
            var correctAnswers = gabarito.split(',');
            var userAnswer = '';
            var numCorrect = 0;
            var userAnswersString = '';

            var questoes = $("#form_simulado").children('.plataforma-simulado-questao');
            $(questoes).find('.form-check-label').removeClass('correta');
            $(questoes).find('.form-check-label').removeClass('errada');
            for (var i = 0; i < questoes.length; i++) {
                console.log("QUESTÃO", i);
                label_checked = questoes[i].querySelector('.checked');
                checkedInput = questoes[i].querySelector("input[name='questoes[" + $(questoes[i]).data('id') +
                    "]']:checked");
                userAnswer = (checkedInput || {}).value;
                if (i) userAnswersString = userAnswersString + ',' + userAnswer;
                else userAnswersString = userAnswersString + userAnswer;

                // verificar se usuário acertou.
                if (userAnswer == correctAnswers[i]) {
                    numCorrect++;
                    label_checked.style.color = 'green';
                } else {
                    $(label_checked).addClass('errada');
                }

                // marcar opção correta.
                labels = $(questoes[i]).find('.form-check-label');
                $(labels[correctAnswers[i] - 1]).addClass('correta');

                $(".questao-comentario").fadeIn();
            }

            var idSimulado = $("#form_simulado").find("input[name='id']").val();
            var tempo = $("#form_simulado").find("input[name='duracao']").val();

            var status = "error";
            var title = "Que pena";
            var porcentagem_acertos = numCorrect / questoes.length;
            var passou = (porcentagem_acertos > 0.5);
            if (passou) {
                status = "success";
                title = "Parabéns!";
            }

            let data = {
                id_simulado: idSimulado,
                tempo: tempo,
                acertos: numCorrect,
                resposta: userAnswersString,
                total: questoes.length,
                id_usuario: "{{ $user_aberto->id }}"
            };

            // Salvar resposta.
            $.post("{{ url('/responder-simulado-aberto') }}", data, function(data) {
                    if (data.success) {
                        link =
                            `/resultado_aberto/${data.resposta.idUsuario}/${data.resposta.total}/${data.resposta.acertos}`;
                        $("#btn_finalizar_wrapper").hide();
                        // $("#btn_continuar_wrapper").show();
                        // let texto = `Você acertou ${data.resposta.acertos} ${data.resposta.acertos > 1 ? "questões" : "questão"} de ${data.resposta.total}.\n\nTemos uma novidade para você! Quer saber mais? Clique em 'Continuar'.`;
                        // if (!passou)
                        //     texto = `Você acertou ${data.resposta.acertos} ${data.resposta.acertos > 1 ? "questões" : "questão"} de ${data.resposta.total}.\n\nMas fique tranquilo(a), temos uma novidade para você! Quer saber mais? Clique no botão 'Continuar' no final do simulado.`;
                        // swal(title, texto, status);
                        $("#info-desconto").show();
                        var divElement = document.getElementById("info-desconto");
                        divElement.scroll({
                            top: divElement.scrollHeight, //scroll to the bottom of the element.
                            behavior: 'smooth' //auto, smooth, initial, inherit.
                        });
                    } else {
                        swal("Erro", `Não foi possível continuar, tente mais tarde.`, "error");
                        console.log("respoonder simulado falhou!!");
                    }
                })
                .fail(function() {
                    swal("Erro", `Não foi possível continuar, tente mais tarde.`, "error");
                    console.log("responder simulado falhou!!")
                });
        });

        function strTimeToSec(time) {
            timeArray = time.split(':');
            hour = timeArray[0] * 60 * 60;
            min = timeArray[1] * 60;
            sec = timeArray[2];
            return hour + min + sec;
        }

        function continuar() {
            window.location.href = link;
        }
    </script>
